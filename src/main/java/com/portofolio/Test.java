package com.portofolio;

import java.io.IOException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class Test {

	public static void main(String[] args) throws IOException {
		List<Map<String,Object>> tmp=new ArrayList<Map<String,Object>>();
		
		for(EnumAlamat a:EnumSet.allOf(EnumAlamat.class)) {
			Map<String,Object> dataEnumAlamat=new HashMap<String, Object>();
			dataEnumAlamat.put("id", a.getId());
			dataEnumAlamat.put("name", a.getName());
			tmp.add(dataEnumAlamat);
		}
		System.out.println("done");
	}

	
}
