
package com.portofolio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;
/**
*
* @author 
*/
@SpringBootApplication
@EnableAsync
@EnableTransactionManagement(proxyTargetClass=false)
@EnableScheduling

public class PortofolioMasterMain {
	public static void main(String[] args) {
		SpringApplication.run(PortofolioMasterMain.class, args);
	}
	
}