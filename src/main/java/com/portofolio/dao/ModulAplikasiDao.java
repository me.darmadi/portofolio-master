package com.portofolio.dao;

import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.portofolio.entity.ModulAplikasi;

@Lazy
@Repository
public interface ModulAplikasiDao extends CrudRepository<ModulAplikasi, String> {

	@Query(QListAll)
	Page<Map<String, Object>> findAllList(@Param("statusEnabled") boolean statusEnabled,Pageable page);

	@Query(QListAll + " and model.namaModulAplikasi like %:namaModulAplikasi% ")
	Page<Map<String, Object>> findAllList(@Param("namaModulAplikasi") String namaModulAplikasi,@Param("statusEnabled") boolean statusEnabled, Pageable page);

	@Query(QListAll + " and model.kdModulAplikasi =:kdModulAplikasi")
	Map<String, Object> findByKdModulAplikasi(@Param("kdModulAplikasi") String kdModulAplikasi,@Param("statusEnabled") boolean statusEnabled);

	@Query("select p from ModulAplikasi p where p.statusEnabled=true")
	List<ModulAplikasi> findAll();
	
	String QListAll = "select new map(model.kdModulAplikasi as kdModulAplikasi " + ", model.namaModulAplikasi as namaModulAplikasi "
			+ ", model.reportDisplay as reportDisplay " + ", modulHead.namaModulAplikasi as namaModulAplikasHead "
			+ ", model.kdModulAplikasiHead as kdModulAplikasiHead " + ", model.modulIconImage as modulIconImage "
			+ ", model.modulNoUrut as modulNoUrut " + ", model.statusEnabled as statusEnabled "
			+ ", model.noRec as noRec " + ", model.version as version )"
			+ " from ModulAplikasi model left join model.modulAplikasiHead modulHead "
			+ " where model.statusEnabled =:statusEnabled ";



}
