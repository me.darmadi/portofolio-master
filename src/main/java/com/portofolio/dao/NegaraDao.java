package com.portofolio.dao;

import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.portofolio.entity.Negara;

@Lazy
@Repository
public interface NegaraDao extends CrudRepository<Negara, Integer> {

    
    @Query(QListAll)
    Page<Map<String, Object>> findAllList(@Param("statusEnabled") boolean statusEnabled,Pageable page);
    
    @Query(QListAll + "and model.namaNegara like %:namaNegara% ")
    Page<Map<String, Object>> findAllList(@Param("namaNegara") String namaNegara,@Param("statusEnabled") boolean statusEnabled, Pageable page);
       
    @Query(QListAll + "and model.namaNegara like %:namaNegara%  ")
    List<Map<String, Object>> findAllList(@Param("namaNegara") String namaNegara,@Param("statusEnabled") boolean statusEnabled);
    
    @Query(QListAll + "and model.kdNegara = :kdNegara")
    Map<String, Object> findByKdNegara(@Param("kdNegara") Integer kdNegara,@Param("statusEnabled") boolean statusEnabled);    
    
    String QListAll ="select new map(model.kdNegara as kdNegara "
            + ", model.namaNegara as namaNegara " 
            + ", model.reportDisplay as reportDisplay " 
            + ", model.kodeExternal as kodeExternal " 
            + ", model.namaExternal as namaExternal "  
            + ", model.statusEnabled as statusEnabled " 
            + ", model.noRec as noRec " 
            + ",  model.version as version )" 
            + " from Negara model where model.statusEnabled=:statusEnabled " ;

}
