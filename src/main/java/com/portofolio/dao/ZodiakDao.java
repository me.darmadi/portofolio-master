package com.portofolio.dao;

import java.util.List;
import java.util.Map;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.portofolio.entity.Zodiak;
import com.portofolio.entity.vo.ZodiakId;

import org.springframework.context.annotation.Lazy;

@Lazy
@Repository
public interface ZodiakDao extends CrudRepository<Zodiak, ZodiakId> {

	@Cacheable("ZodiakDaofindOneBy")
	Zodiak findOneByIdKodeAndIdKdNegara(Integer kode, Integer kdNegara);

	@Query(QListAll + "where  model.namaZodiak like %:namaZodiak% ")
	@Cacheable("ZodiakDaoFindAllList")
	Page<Map<String, Object>> findAllList(@Param("namaZodiak") String namaZodiak, Pageable page);

	@Query(QListAll + "where model.id.kdNegara =:kdNegara  and model.namaZodiak like %:namaZodiak% ")
	@Cacheable("ZodiakDaoFindAllListPage")
	Page<Map<String, Object>> findAllList(@Param("kdNegara") Integer kdNegara, @Param("namaZodiak") String namaZodiak,
			Pageable page);

	@Query(QListAll + "where model.id.kdNegara =:kdNegara and model.id.kode like :kode")
	@Cacheable("ZodiakDaoFindByKode")
	Map<String, Object> findByKode(@Param("kdNegara") Integer kdNegara, @Param("kode") Integer kode);

	String QListAll = "select new map(model.id.kdZodiak as kdZodiak,model.id.kdProfile as kdProfile " + ", model.namaZodiak as namaZodiak "
			+ ", model.reportDisplay as reportDisplay " + ", model.noUrut as noUrut "
			+ ", model.jamLahirAwal as jamLahirAwal " + ", model.jamLahirAkhir as jamLahirAkhir "
			+ ", negara.namaNegara as namaNegara " + ", model.kodeExternal as kodeExternal "
			+ ", model.namaExternal as namaExternal " + ", model.statusEnabled as statusEnabled "
			+ ", model.noRec as noRec " + ", model.version as version )" + " from Zodiak model "
			+ " left join model.negara negara ";

	@Query(QListAll + "where model.id.kdNegara =:kdNegara  and model.statusEnabled = true ")
	List<Map<String, Object>> findAllZodiak(@Param("kdNegara") Integer kdNegara);

}
