package com.portofolio.dao;

import java.util.List;
import java.util.Map;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.portofolio.dtosql.RuanganDtoSql;
import com.portofolio.entity.Ruangan;
import com.portofolio.entity.vo.RuanganId;

@Lazy
@Repository("ruanganDao")
public interface RuanganDao extends CrudRepository<Ruangan, RuanganId> {


	@Query(QRuanganDtoSql)
	@Cacheable("RuanganDaoFindAllList")
	List<RuanganDtoSql> findAllList(@Param("kdProfile") Integer kdProfile,@Param("statusEnabled") boolean statusEnabled, Pageable page);
	
	@Query(QRuanganDtoSql + " and model.namaRuangan like %:namaRuangan% "
			+ "or (model.id.kdRuangan in (select model2.kdRuanganHead from Ruangan model2 where model2.namaRuangan like %:namaRuangan% and model2.id.kdProfile=:kdProfile and model2.statusEnabled=:statusEnabled )) "
			+ "or (model.kdRuanganHead in (select model2.id.kdRuangan from Ruangan model2 where model2.namaRuangan like %:namaRuangan% and model2.id.kdProfile=:kdProfile and model2.statusEnabled=:statusEnabled ))")
	@Cacheable("RuanganDaoFindAllListPage")
	List<RuanganDtoSql> findAllList(@Param("kdProfile") Integer kdProfile,@Param("namaRuangan") String namaRuangan,@Param("statusEnabled") boolean statusEnabled, Pageable page);


	@Query(QRuanganDtoSql+" and model.id.kdRuangan=:kdRuangan "
			+ "or (model.id.kdRuangan in (select model2.kdRuanganHead from Ruangan model2 where model2.id.kdRuangan=:kdRuangan and model2.statusEnabled=:statusEnabled )) "
			+ "or (model.kdRuanganHead in (select model2.id.kdRuangan from Ruangan model2 where model2.id.kdRuangan=:kdRuangan and model2.statusEnabled=:statusEnabled ))")
	List<RuanganDtoSql> findByKdRuanganWithIsHierarkiTrue(@Param("kdProfile") Integer kdProfile,@Param("kdRuangan") String kdRuangan,@Param("statusEnabled") boolean statusEnabled);
	
	@Query(QRuanganDtoSql+" and model.id.kdRuangan=:kdRuangan ")
	List<RuanganDtoSql> findByKdRuanganWithIsHierarkiFalse(@Param("kdProfile") Integer kdProfile,@Param("kdRuangan") String kdRuangan,@Param("statusEnabled") boolean statusEnabled);

	
	String QRuanganDtoSql= "select new com.portofolio.dtosql.RuanganDtoSql (model.id.kdRuangan, model.namaRuangan, model.reportDisplay, model.noRuangan"
			+ ", model.kodeExternal, model.namaExternal, model.statusEnabled, model.noRec "
			+ ", model.version, ruanganHead.id.kdRuangan,ruanganHead.namaRuangan,departemen.id.kdDepartemen,departemen.namaDepartemen "
			+ ", alamat.id.kdAlamat "
			+ ", CONCAT(alamat.alamatLengkap,' RT ',alamat.rTRW,',Desa/Kel ',desaKelurahan.namaDesaKelurahan,',Kecamatan ',kecamatan.namaKecamatan,',',kotaKabupaten.namaKotaKabupaten,',',propinsi.namaPropinsi,',',alamat.kodePos )"
			+ ", pegawai.id.kdPegawai, pegawai.namaLengkap )"
			+ " from Ruangan model  " + " left join model.pegawai pegawai "
			+ " left join model.id id left join model.departemen departemen " + " left join model.alamat alamat "
			+ " left join alamat.propinsi propinsi " + " left join alamat.negara negara "
			+ " left join alamat.desaKelurahan desaKelurahan " + " left join alamat.kecamatan kecamatan "
			+ " left join alamat.kotaKabupaten kotaKabupaten " + " left join alamat.jenisAlamat jenisAlamat "
			+ " left join model.lokasiKerja ruanganHead "
			+ " left join model.pegawai pegawai "
			+ " where id.kdProfile=:kdProfile and model.statusEnabled=:statusEnabled ";
	
	String QRuanganAll= "select new map (model.id.kdRuangan as kdRuangan, model.namaRuangan as namaRuangan, model.reportDisplay as reportDisplay, model.noRuangan as noRuangan"
			+ ", model.kodeExternal as kodeExternal, model.namaExternal as namaExternal, model.statusEnabled as statusEnabled, model.noRec  as noRec"
			+ ", model.version as version, ruanganHead.id.kdRuangan as kdRuanganHead,ruanganHead.namaRuangan as namaRuanganHead,departemen.id.kdDepartemen as kdDepartemen,departemen.namaDepartemen as namaDepartemen"
			+ ", alamat.id.kdAlamat as kdAlamat "
			+ ", CONCAT(alamat.alamatLengkap,' RT ',alamat.rTRW,',Desa/Kel ',desaKelurahan.namaDesaKelurahan,',Kecamatan ',kecamatan.namaKecamatan,',',kotaKabupaten.namaKotaKabupaten,',',propinsi.namaPropinsi,',',alamat.kodePos ) as alamatLengkap"
			+ ", pegawai.id.kdPegawai as kdPegawai, pegawai.namaLengkap as namaLengkap )"
			+ " from Ruangan model  " + " left join model.pegawai pegawai "
			+ " left join model.id id left join model.departemen departemen " + " left join model.alamat alamat "
			+ " left join alamat.propinsi propinsi " + " left join alamat.negara negara "
			+ " left join alamat.desaKelurahan desaKelurahan " + " left join alamat.kecamatan kecamatan "
			+ " left join alamat.kotaKabupaten kotaKabupaten " + " left join alamat.jenisAlamat jenisAlamat "
			+ " left join model.lokasiKerja ruanganHead "
			+ " left join model.pegawai pegawai "
			+ " where id.kdProfile=:kdProfile and model.statusEnabled=:statusEnabled ";

	@Query(QRuanganAll + " and model.kdRuanganHead=:kdHead")
	List<Map<String, Object>> findAllMAListWithHead(@Param("kdProfile") Integer kdProfile,@Param("kdHead") String kdHead,@Param("statusEnabled") boolean statusEnabled);
	

	@Query(QRuanganAll + " and model.kdRuanganHead is null ")
	List<Map<String, Object>> getHead(@Param("kdProfile") Integer kdProfile,@Param("statusEnabled") boolean statusEnabled);

	@Query("select r from Ruangan r where r.statusEnabled=true and r.id.kdProfile=:kdProfile and r.kdRuanganHead is not null ")
	List<Ruangan> findAllRuanganHeadIsNotNull(@Param("kdProfile") Integer kdProfile);



	
}
