package com.portofolio.dao;

import java.util.List;
import java.util.Map;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.portofolio.entity.MapKelompokUserToJabatan;
import com.portofolio.entity.vo.MapKelompokUserToJabatanId;

import org.springframework.context.annotation.Lazy;

@Lazy
@Repository
public interface MapKelompokUserToJabatanDao extends CrudRepository<MapKelompokUserToJabatan, MapKelompokUserToJabatanId>{
	@Cacheable("mapKelompokUserToJabatanDaofindOneBy")
	MapKelompokUserToJabatan findOneByIdKdKelompokUserAndIdKdKategoryPegawaiAndIdKdJabatanAndIdKdProfile(Integer kdKelompokUser,
			String kdKategoryPegawai, String kdJabatan, Integer kdProfile);

	@Query(QListAll)
	@Cacheable("mapKelompokUserToJabatanDaoFindAllList")
	Page<Map<String, Object>> findAllList(@Param("kdProfile") Integer kdProfile, Pageable page);

	@Query(QListAll + " and kelompokUser.namaKelompokUser like %:namaKelompokUser% ")
	@Cacheable("mapKelompokUserToJabatanDaoFindAllListPage")
	Page<Map<String, Object>> findAllList(@Param("kdProfile") Integer kdProfile, @Param("namaKelompokUser") String namaKelompokUser, Pageable page);

	@Query(QListAll + " and model.id.kdKelompokUser =:kdKelompokUser and model.id.kdKategoryPegawai = :kdKategoryPegawai ")
	@Cacheable("mapKelompokUserToJabatanDaoFindByKode")
	List<Map<String, Object>> findByKode(@Param("kdProfile") Integer kdProfile, @Param("kdKelompokUser") Integer kdKelompokUser, @Param("kdKategoryPegawai") String kdKategoryPegawai);

	@Modifying
	@Cacheable("setStatusEnabledFalseDao")
	// @Transactional
	@Query("update MapKelompokUserToJabatan m set m.statusEnabled = false where m.id.kdProfile =:kdProfile and m.id.kdKelompokUser =:kdKelompokUser and m.id.kdKategoryPegawai =:kdKategoryPegawai")
	public void setStatusEnabledFalse(@Param("kdProfile") Integer kdProfile,
			@Param("kdKelompokUser") Integer kdKelompokUser, @Param("kdKategoryPegawai") String kdKategoryPegawai);

	@Cacheable("countmapKelompokUserToJabatan")
	Long countIdKdKelompokUserByIdKdKelompokUserAndIdKdProfile(Integer kdKelompokUser, Integer kdProfile);

	String QListAll = " select new map(model.id.kdProfile as kdProfile, "
			+ " model.id.kdJabatan as kdJabatan, jabatan.namaJabatan as namaJabatan, "
			+ " model.id.kdKelompokUser as kdKelompokUser, kelompokUser.namaKelompokUser as namaKelompokUser, "
			+ " model.id.kdKategoryPegawai as kdKategoryPegawai, kategoryPegawai.namaKategoryPegawai as namaKategoryPegawai, "
			+ " model.noRec as noRec , model.statusEnabled as statusEnabled, model.version as version)  from "
			+ " MapKelompokUserToJabatan model "
			+ " left join model.jabatan jabatan "
			+ " left join model.kelompokUser kelompokUser "
			+ " left join model.kategoryPegawai as kategoryPegawai "
			+ " where model.id.kdProfile =:kdProfile and model.statusEnabled = true ";
}
