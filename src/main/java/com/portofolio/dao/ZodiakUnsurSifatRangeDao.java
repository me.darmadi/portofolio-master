package com.portofolio.dao;

import java.util.List;
import java.util.Map;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.portofolio.entity.ZodiakUnsurSifatRange;
import com.portofolio.entity.vo.ZodiakUnsurSifatRangeId;

import org.springframework.context.annotation.Lazy;

@Lazy
@Repository
public interface ZodiakUnsurSifatRangeDao extends CrudRepository<ZodiakUnsurSifatRange, ZodiakUnsurSifatRangeId> {

	@Cacheable("ZodiakUnsurSifatRangeDaofindOneBy")
	ZodiakUnsurSifatRange findOneByIdKdZodiakAndIdKdZodiakUnsurAndIdKdNegaraAndIdTglAwalAndIdTglAkhirAndIdSifat(
			Integer kdZodiak, Integer kdZodiakUnsur, Integer kdNegara, Long tglAwal, Long tglAkhir, String sifat);

	ZodiakUnsurSifatRange findOneByNoRecAndIdKdNegara(String noRec, Integer kdNegara);

	String QListAll = "select new map(model.id.kdZodiak as kdZodiak " + ", model.id.kdZodiakUnsur as kdZodiakUnsur "
			+ ", model.id.kdNegara as kdNegara " + ", model.id.tglAwal as tglAwal " + ", model.id.tglAkhir as tglAkhir "
			+ ", model.id.sifat as sifat " + ", model.statusEnabled as statusEnabled " + ", model.noRec as noRec "
			+ ", zodiak.namaZodiak as namaZodiak " + ", zodiakUnsur.namaZodiakUnsur as namaZodiakUnsur "
			+ ", negara.namaNegara as namaNegara, model.version as version )" + " from ZodiakUnsurSifatRange model "
			+ " left join model.id id " + " left join model.negara negara " + " left join model.zodiak zodiak "
			+ " left join model.zodiakUnsur zodiakUnsur ";

	@Query(QListAll + " where zodiak.namaZodiak like %:namaZodiak% ")
	@Cacheable("ZodiakUnsurSifatRangeDaoFindAllList")
	Page<Map<String, Object>> findAllList(@Param("namaZodiak") String namaZodiak, Pageable page);

	@Query(QListAll + " where model.id.kdNegara =:kdNegara and zodiak.namaZodiak like %:namaZodiak% ")
	@Cacheable("ZodiakUnsurSifatRangeDaoFindAllListPage")
	Page<Map<String, Object>> findAllList(@Param("kdNegara") Integer kdNegara, @Param("namaZodiak") String namaZodiak,
			Pageable page);

	@Query(QListAll + " where model.id.kdNegara =:kdNegara and model.id.kdZodiak =:kdZodiak")
	@Cacheable("ZodiakUnsurSifatRangeDaoFindByKode")
	List<Map<String, Object>> findByKode(@Param("kdNegara") Integer kdNegara,
			@Param("kdZodiak") Integer kdZodiak/* , @Param("kdZodiak") Integer kdZodiakUnsur */);

	@Query("select new map(" + "model.id.kdZodiak as kdZodiak" + ", model.id.kdZodiakUnsur as kdZodiakUnsur"
			+ ", zdk.namaZodiak as namaZodiak" + ", zdkUnsur.namaZodiakUnsur as namaZodiakUnsur" + ") "
			+ "	from ZodiakUnsurSifatRange model " + " left join model.zodiak zdk "
			+ " left join model.zodiakUnsur zdkUnsur" + " where model.statusEnabled=true"
			+ " and model.id.kdNegara=:kdNegara" + " and model.id.tglAwal<=:tglLahir "
			+ " and model.id.tglAkhir>=:tglLahir")
	List<Map<String, Object>> findZodiak(@Param("tglLahir") Long tglLahir, @Param("kdNegara") Integer kdNegara);

	@Query(QListAll + " where model.id.kdNegara =:kdNegara and model.statusEnabled = true ")
	List<Map<String, Object>> findAllUnsurSifat(@Param("kdNegara") Integer kdNegara);

}
