package com.portofolio.dao;

import java.util.List;
import java.util.Map;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.portofolio.entity.Bahasa;
import com.portofolio.entity.vo.BahasaId;


@Lazy
@Repository
public interface BahasaDao extends CrudRepository<Bahasa, BahasaId> {


	@Query(QListAll + " and model.namaBahasa like %:namaBahasa% ")
	Page<Map<String, Object>> findAllList(@Param("namaBahasa") String namaBahasa,@Param("statusEnabled") boolean statusEnabled, Pageable page);

	@Query(QListAll)
	Page<Map<String, Object>> findAllList(@Param("statusEnabled") boolean statusEnabled,Pageable page);

	@Query(QListAll + "and model.id.kdNegara=:kdNegara and model.id.kdBahasa =:kdBahasa")
	Map<String, Object> findById(@Param("kdNegara") Integer kdNegara, @Param("kdBahasa") Integer kdBahasa, @Param("statusEnabled") boolean statusEnabled);

	String QListAll = "select new map(model.id.kdBahasa as kdBahasa" + ", model.namaBahasa as namaBahasa "
			+ ", model.reportDisplay as reportDisplay " + ", negara.namaNegara as namaNegara "
			+ ", model.kodeExternal as kodeExternal " + ", model.namaExternal as namaExternal "
			+ ", model.statusEnabled as statusEnabled " + ", model.noRec as noRec "
			+ ",  negara.namaNegara as namaNegara " + ", model.version as version )"
			+ " from Bahasa model left join model.id id " + "left join model.negara negara "
			+ "left join model.negara negara where model.statusEnabled=:statusEnabled ";

	

}
