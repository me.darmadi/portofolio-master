package com.portofolio.dao;

import java.util.List;
import java.util.Map;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.portofolio.entity.Kecamatan;
import com.portofolio.entity.vo.KecamatanId;

@Lazy
@Repository
public interface KecamatanDao extends CrudRepository<Kecamatan, KecamatanId> {

    @Query(QListAll + " and  model.id.kdNegara=:kdNegara ")
    Page<Map<String, Object>> findAllList( @Param("kdNegara") Integer kdNegara,@Param("statusEnabled") boolean statusEnabled, Pageable page );
     
    @Query(QListAll + " and model.id.kdNegara=:kdNegara  and model.namaKecamatan like %:namaKecamatan% ")
    Page<Map<String, Object>> findAllList(@Param("kdNegara") Integer kdNegara, @Param("namaKecamatan") String namaKecamatan,@Param("statusEnabled") boolean statusEnabled, Pageable page );
    
    @Query(QListAll + " and model.id.kdNegara=:kdNegara  and model.id.kdKecamatan =:kdKecamatan")
    Map<String, Object> findByKdKecamatan(@Param("kdNegara") Integer kdNegara, @Param("kdKecamatan") Integer kdKecamatan, @Param("statusEnabled") boolean statusEnabled);
        
    @Query(QListAll + " and model.id.kdNegara=:kdNegara  and model.kdKotaKabupaten = :kdKotaKabupaten ")    
	List<Map<String, Object>>  findByKdKotaKabupaten(@Param("kdNegara")Integer kdNegara,@Param("kdKotaKabupaten") Integer kdKotaKabupaten,@Param("statusEnabled") boolean statusEnabled); 

    String QListAll ="select new map(model.id.kdKecamatan as kdKecamatan "
            + ", model.namaKecamatan as namaKecamatan " 
            + ", model.reportDisplay as reportDisplay " 
            + ", model.kdPropinsi as kdPropinsi "
            + ", propinsi.namaPropinsi as namaPropinsi " 
            + ", model.kdKotaKabupaten as kdKotaKabupaten "
            + ", kotaKabupaten.namaKotaKabupaten as namaKotaKabupaten " 
            + ", model.kodeExternal as kodeExternal " 
            + ", model.namaExternal as namaExternal "  
            + ", model.statusEnabled as statusEnabled " 
            + ", model.noRec as noRec " 
            + ", negara.namaNegara as namaNegara"
            + ", model.version as version )" 
            + " from Kecamatan model "
            + "left join model.id id "
            + "left join model.negara negara "
            + "left join model.kotaKabupaten kotaKabupaten "
            + "left join model.propinsi propinsi where model.statusEnabled=:statusEnabled " ;
}
