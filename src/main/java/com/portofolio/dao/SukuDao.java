package com.portofolio.dao;

import java.util.Map;

import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.portofolio.entity.Suku;
import com.portofolio.entity.vo.SukuId;

@Lazy
@Repository
public interface SukuDao extends CrudRepository<Suku, SukuId> {


	@Query(QListAll + "and model.id.kdNegara=:kdNegara ")
	Page<Map<String, Object>> findAllList(@Param("kdNegara") Integer kdNegara,@Param("statusEnabled") boolean statusEnabled, Pageable page);

	@Query(QListAll + "and model.id.kdNegara =:kdNegara  and model.namaSuku like %:namaSuku% ")
	Page<Map<String, Object>> findAllList(@Param("kdNegara") Integer kdNegara, @Param("namaSuku") String namaSuku,@Param("statusEnabled") boolean statusEnabled,Pageable page);

	@Query(QListAll + "and model.id.kdNegara =:kdNegara  and model.id.kdSuku=:kdSuku")
	Map<String, Object> findByKdSuku(@Param("kdNegara") Integer kdNegara, @Param("kdSuku") Integer kdSuku,@Param("statusEnabled") boolean statusEnabled);
	

	String QListAll = "select new map(model.id.kdSuku as kdSuku, model.namaSuku as namaSuku "
			+ ", model.reportDisplay as reportDisplay " + ", model.kodeExternal as kodeExternal "
			+ ", model.namaExternal as namaExternal " + ", model.statusEnabled as statusEnabled "
			+ ", model.noRec as noRec " + ",  negara.namaNegara as namaNegara, model.version as version )"
			+ " from Suku model left join model.id id left join model.negara negara where model.statusEnabled=:statusEnabled ";


}
