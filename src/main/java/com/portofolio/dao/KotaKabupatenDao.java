package com.portofolio.dao;

import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.portofolio.entity.KotaKabupaten;
import com.portofolio.entity.vo.KotaKabupatenId;

@Lazy
@Repository
public interface KotaKabupatenDao extends CrudRepository<KotaKabupaten, KotaKabupatenId> {


	@Query(QListAll + " and model.id.kdNegara=:kdNegara ")
	Page<Map<String, Object>> findAllList(@Param("kdNegara") Integer kdNegara,@Param("statusEnabled") boolean statusEnabled,Pageable page);

	@Query(QListAll + "and model.id.kdNegara =:kdNegara and model.namaKotaKabupaten like %:namaKotaKabupaten% ")
	Page<Map<String, Object>> findAllList(@Param("kdNegara") Integer kdNegara,
			@Param("namaKotaKabupaten") String namaKotaKabupaten,@Param("statusEnabled") boolean statusEnabled,Pageable page);

	@Query(QListAll + " and model.id.kdNegara =:kdNegara " + " and model.id.kdKotaKabupaten =:kdKotaKabupaten")
	Map<String, Object> findByKdKotaKabupaten(@Param("kdNegara") Integer kdNegara, @Param("kdKotaKabupaten") Integer kdKotaKabupaten,@Param("statusEnabled") boolean statusEnabled);
	
	@Query(QListAll + " and model.id.kdNegara =:kdNegara " + " and model.kdPropinsi =:kdPropinsi   ")
	List<Map<String, Object>> findByKdPropinsi(@Param("kdPropinsi") Integer kdPropinsi,
			@Param("kdNegara") Integer kdNegara,@Param("statusEnabled") boolean statusEnabled);

	String QListAll = "select new map(model.id.kdKotaKabupaten as kdKotaKabupaten" + ", model.namaKotaKabupaten as namaKotaKabupaten "
			+ ", model.reportDisplay as reportDisplay " + ", model.kdPropinsi as kdPropinsi "
			+ ", model.kodeExternal as kodeExternal " + ", model.namaExternal as namaExternal "
			+ ", model.statusEnabled as statusEnabled " + ", model.noRec as noRec "
			+ ", negara.namaNegara as namaNegara" + ", model.version as version "
			+ ", objPropinsi.namaPropinsi as namaPropinsi)" + " from KotaKabupaten model " + "left join model.id id "
			+ "left join model.negara negara " + "left join model.objPropinsi objPropinsi where model.statusEnabled =:statusEnabled ";

	

}
