package com.portofolio.dao;

import java.util.List;
import java.util.Map;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.portofolio.entity.ZodiakUnsur;
import com.portofolio.entity.vo.ZodiakUnsurId;

@Lazy
@Repository
public interface ZodiakUnsurDao extends CrudRepository<ZodiakUnsur, ZodiakUnsurId> {

	@Cacheable("ZodiakUnsurDaofindOneBy")
	ZodiakUnsur findOneByIdKodeAndIdKdNegara(Integer kode, Integer kdNegara);

	@Query(QListAll + "where model.namaZodiakUnsur like %:namaZodiakUnsur% ")
	@Cacheable("ZodiakUnsurDaoFindAllList")
	Page<Map<String, Object>> findAllList(@Param("namaZodiakUnsur") String namaZodiakUnsur, Pageable page);

	@Query(QListAll + "where model.id.kdNegara =:kdNegara  and model.namaZodiakUnsur like %:namaZodiakUnsur% ")
	@Cacheable("ZodiakUnsurDaoFindAllListPage")
	Page<Map<String, Object>> findAllList(@Param("kdNegara") Integer kdNegara,
			@Param("namaZodiakUnsur") String namaZodiakUnsur, Pageable page);

	@Query(QListAll + "where model.id.kdNegara =:kdNegara  and model.id.kode like :kode")
	@Cacheable("ZodiakUnsurDaoFindByKode")
	Map<String, Object> findByKode(@Param("kdNegara") Integer kdNegara, @Param("kode") Integer kode);

	String QListAll = "select new map(model.id.kdZodiakUnsur as kdZodiakUnsur,model.id.kdProfile as kdProfile " + ", model.namaZodiakUnsur as namaZodiakUnsur "
			+ ", model.reportDisplay as reportDisplay " + ", model.noUrut as noUrut "
			+ ", negara.namaNegara as namaNegara " + ", model.kodeExternal as kodeExternal "
			+ ", model.namaExternal as namaExternal " + ", model.statusEnabled as statusEnabled "
			+ ", model.noRec as noRec " + ", model.version as version )" + " from ZodiakUnsur model "
			+ " left join model.negara negara ";

	@Query(QListAll + "where model.id.kdNegara =:kdNegara and model.statusEnabled = true ")
	List<Map<String, Object>> findAllZodiakUnsur(@Param("kdNegara") Integer kdNegara);

}
