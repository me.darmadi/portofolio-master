package com.portofolio.dao;

import java.util.Map;

import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.portofolio.entity.GolonganDarah;
import com.portofolio.entity.vo.GolonganDarahId;

@Lazy
@Repository
public interface GolonganDarahDao extends CrudRepository<GolonganDarah, GolonganDarahId> {


	@Query(QListAll + "and model.id.kdNegara=:kdNegara ")
	Page<Map<String, Object>> findAllList(@Param("kdNegara") Integer kdNegara,@Param("statusEnabled") boolean statusEnabled, Pageable page);

	@Query(QListAll + "and model.id.kdNegara=:kdNegara  and model.namaGolonganDarah like %:namaGolonganDarah% ")
	Page<Map<String, Object>> findAllList(@Param("kdNegara") Integer kdNegara,
			@Param("namaGolonganDarah") String namaGolonganDarah,@Param("statusEnabled") boolean statusEnabled, Pageable page);

	@Query(QListAll + "and model.id.kdNegara=:kdNegara  and model.id.kdGolonganDarah like :kdGolonganDarah")
	Map<String, Object> findByKdGolonganDarah(@Param("kdNegara") Integer kdNegara, @Param("kdGolonganDarah") Integer kdGolonganDarah,@Param("statusEnabled") boolean statusEnabled);
	

	String QListAll = "select new map(model.id.kdGolonganDarah as kdGolonganDarah" + ", model.namaGolonganDarah as namaGolonganDarah "
			+ ", model.reportDisplay as reportDisplay " + ", model.kodeExternal as kodeExternal "
			+ ", model.namaExternal as namaExternal " + ", model.statusEnabled as statusEnabled "
			+ ", model.noRec as noRec " + ",  negara.namaNegara as namaNegara" + ", model.version as version ) "
			+ " from GolonganDarah model " + "left join model.id id " + "left join model.negara negara where model.statusEnabled=:statusEnabled ";



}
