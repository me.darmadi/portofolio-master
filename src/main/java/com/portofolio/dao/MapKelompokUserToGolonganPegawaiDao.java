package com.portofolio.dao;

import java.util.List;
import java.util.Map;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.portofolio.entity.MapKelompokUserToGolonganPegawai;
import com.portofolio.entity.vo.MapKelompokUserToGolonganPegawaiId;

import org.springframework.context.annotation.Lazy;

@Lazy
@Repository
public interface MapKelompokUserToGolonganPegawaiDao extends CrudRepository<MapKelompokUserToGolonganPegawai, MapKelompokUserToGolonganPegawaiId>{
	@Cacheable("mapKelompokUserToGolonganPegawaiDaofindOneBy")
	MapKelompokUserToGolonganPegawai findOneByIdKdKelompokUserAndIdKdKategoryPegawaiAndIdKdGolonganPegawaiAndIdKdProfile(Integer kdKelompokUser,
			String kdKategoryPegawai, Integer kdGolonganPegawai, Integer kdProfile);

	@Query(QListAll)
	@Cacheable("mapKelompokUserToGolonganPegawaiDaoFindAllList")
	Page<Map<String, Object>> findAllList(@Param("kdProfile") Integer kdProfile, Pageable page);

	@Query(QListAll + " and kelompokUser.namaKelompokUser like %:namaKelompokUser% ")
	@Cacheable("mapKelompokUserToGolonganPegawaiDaoFindAllListPage")
	Page<Map<String, Object>> findAllList(@Param("kdProfile") Integer kdProfile, @Param("namaKelompokUser") String namaKelompokUser, Pageable page);
	
	@Query(QListAll + " and kelompokUser.id.kode!=1 ")
	@Cacheable("mapKelompokUserToGolonganPegawaiDaoFindAllList")
	Page<Map<String, Object>> findAllListNonAdmin(@Param("kdProfile") Integer kdProfile, Pageable page);

	@Query(QListAll + " and kelompokUser.id.kode!=1 and kelompokUser.namaKelompokUser like %:namaKelompokUser% ")
	@Cacheable("mapKelompokUserToGolonganPegawaiDaoFindAllListPage")
	Page<Map<String, Object>> findAllListNonAdmin(@Param("kdProfile") Integer kdProfile, @Param("namaKelompokUser") String namaKelompokUser, Pageable page);
	
	
	@Query(QListGroup+QListGroupBy)
	Page<Map<String, Object>> findGroup(@Param("kdProfile") Integer kdProfile, Pageable page);

	@Query(QListGroup + " and (kelompokUser.namaKelompokUser like %:namaKelompokUser% or kategoryPegawai.namaKategoryPegawai like %:namaKelompokUser% ) "+QListGroupBy)
	Page<Map<String, Object>> findGroup(@Param("kdProfile") Integer kdProfile, @Param("namaKelompokUser") String namaKelompokUser, Pageable page);

	@Query(QListGroup+" and model.id.kdKelompokUser!=1 "+QListGroupBy)
	Page<Map<String, Object>> findGroupNonAdmin(@Param("kdProfile") Integer kdProfile, Pageable page);

	@Query(QListGroup + " and model.id.kdKelompokUser!=1 and (kelompokUser.namaKelompokUser like %:namaKelompokUser% or kategoryPegawai.namaKategoryPegawai like %:namaKelompokUser% ) "+QListGroupBy)
	Page<Map<String, Object>> findGroupNonAdmin(@Param("kdProfile") Integer kdProfile, @Param("namaKelompokUser") String namaKelompokUser, Pageable page);

	
	@Query(QListSubGroup +" order by golonganPegawai.namaGolonganPegawai ")
	List<String> findSubGroup(@Param("kdProfile") Integer kdProfile , @Param("kdKelompokUser") Integer kdKelompokUser
			//, @Param("kdKategoryPegawai") String kdKategoryPegawai
			);
	
	@Query("select model.id.kode from KategoryPegawai model where model.id.kdProfile=:kdProfile")
	List<String> getAllKdKategoryPegawai(@Param("kdProfile") Integer kdProfile);
	
	@Query("select model.id.kode from GolonganPegawai model where model.id.kdProfile=:kdProfile")
	List<Integer> getAllKdGolonganPegawai(@Param("kdProfile") Integer kdProfile);
	
	

	@Query(QListAll + " and model.id.kdKelompokUser =:kdKelompokUser and model.id.kdKategoryPegawai = :kdKategoryPegawai ")
	@Cacheable("mapKelompokUserToGolonganPegawaiDaoFindByKode")
	List<Map<String, Object>> findByKode(@Param("kdProfile") Integer kdProfile, @Param("kdKelompokUser") Integer kdKelompokUser, @Param("kdKategoryPegawai") String kdKategoryPegawai);
	
	@Query(QListAll + " and model.id.kdKelompokUser not in(:kdKelompokUser) and model.id.kdGolonganPegawai =:kdGolonganPegawai and model.id.kdKategoryPegawai = :kdKategoryPegawai  and model.statusEnabled=true ")
	List<Map<String, Object>> findKelompokUserPair(@Param("kdProfile") Integer kdProfile, @Param("kdKelompokUser") Integer kdKelompokUser, @Param("kdGolonganPegawai") Integer kdGolonganPegawai, @Param("kdKategoryPegawai") String kdKategoryPegawai);
	
	@Query(QListAll + " and model.id.kdKelompokUser not in(:kdKelompokUser,1) and model.id.kdGolonganPegawai =:kdGolonganPegawai and model.id.kdKategoryPegawai = :kdKategoryPegawai  and model.statusEnabled=true ")
	List<Map<String, Object>> findKelompokUserPairNonAdmin(@Param("kdProfile") Integer kdProfile, @Param("kdKelompokUser") Integer kdKelompokUser, @Param("kdGolonganPegawai") Integer kdGolonganPegawai, @Param("kdKategoryPegawai") String kdKategoryPegawai);
	
	
	@Query("select new map(model.id.kode as kdGolonganPegawai "
			+ ", model.namaGolonganPegawai as namaGolonganPegawai ) from GolonganPegawai model where model.id.kdProfile =:kdProfile and model.statusEnabled = true")
	List<Map<String, Object>> listGolonganPegawaiActive(@Param("kdProfile") Integer kdProfile);
	
	@Query("select model.statusEnabled from MapKelompokUserToGolonganPegawai model where model.id.kdProfile=:kdProfile and model.id.kdKelompokUser=:kdKelompokUser and model.id.kdGolonganPegawai =:kdGolonganPegawai and model.id.kdKategoryPegawai = :kdKategoryPegawai ")
	Boolean getStatusEnabledByKode(@Param("kdProfile") Integer kdProfile, @Param("kdKelompokUser") Integer kdKelompokUser,
			@Param("kdKategoryPegawai") String kdKategoryPegawai, @Param("kdGolonganPegawai")  Integer kdGolonganPegawai);
	

	@Modifying
	@Cacheable("setStatusEnabledFalseDao")
	// @Transactional
	@Query("update MapKelompokUserToGolonganPegawai m set m.statusEnabled = false where m.id.kdProfile =:kdProfile and m.id.kdKelompokUser =:kdKelompokUser and m.id.kdKategoryPegawai =:kdKategoryPegawai")
	public void setStatusEnabledFalse(@Param("kdProfile") Integer kdProfile,
			@Param("kdKelompokUser") Integer kdKelompokUser, @Param("kdKategoryPegawai") String kdKategoryPegawai);
	
	@Modifying
	@Cacheable("setStatusEnabledFalseDao")
	// @Transactional
	@Query("update MapKelompokUserToGolonganPegawai m set m.statusEnabled = false where m.id.kdProfile =:kdProfile and m.id.kdKelompokUser =:kdKelompokUser")
	public void setStatusEnabledFalseV2(@Param("kdProfile") Integer kdProfile,
			@Param("kdKelompokUser") Integer kdKelompokUser);

	@Cacheable("countmapKelompokUserToGolonganPegawai")
	Long countIdKdKelompokUserByIdKdKelompokUserAndIdKdProfile(Integer kdKelompokUser, Integer kdProfile);

	String QListAll = " select new map(model.id.kdProfile as kdProfile, "
			+ " model.id.kdGolonganPegawai as kdGolonganPegawai, golonganPegawai.namaGolonganPegawai as namaGolonganPegawai, "
			+ " model.id.kdKelompokUser as kdKelompokUser, kelompokUser.namaKelompokUser as namaKelompokUser, "
			+ " model.id.kdKategoryPegawai as kdKategoryPegawai, kategoryPegawai.namaKategoryPegawai as namaKategoryPegawai, "
			+ " model.noRec as noRec , model.statusEnabled as statusEnabled, model.version as version)  from "
			+ " MapKelompokUserToGolonganPegawai model "
			+ " left join model.golonganPegawai golonganPegawai "
			+ " left join model.kelompokUser kelompokUser "
			+ " left join model.kategoryPegawai as kategoryPegawai "
			+ " where model.id.kdProfile =:kdProfile and model.statusEnabled = true ";
	
	String QListGroup = " select new map(model.id.kdProfile as kdProfile, "
			+ " model.id.kdKelompokUser as kdKelompokUser, kelompokUser.namaKelompokUser as namaKelompokUser "
			//+ " ,model.id.kdKategoryPegawai as kdKategoryPegawai, kategoryPegawai.namaKategoryPegawai as namaKategoryPegawai "
			+ " )  from "
			+ " MapKelompokUserToGolonganPegawai model "
			+ " left join model.kelompokUser kelompokUser "
			+ " left join model.kategoryPegawai as kategoryPegawai "
			+ " where model.id.kdProfile =:kdProfile and model.statusEnabled = true ";
	String QListGroupBy = " group by model.id.kdProfile, "
			//+ " model.id.kdKategoryPegawai,kategoryPegawai.namaKategoryPegawai, "
			+ " model.id.kdKelompokUser, kelompokUser.namaKelompokUser ";
	
	String QListSubGroup = "select distinct golonganPegawai.namaGolonganPegawai "
			+ " from MapKelompokUserToGolonganPegawai model left join model.golonganPegawai golonganPegawai where model.id.kdProfile=:kdProfile "
			//+ " and model.id.kdKategoryPegawai=:kdKategoryPegawai "
			+ " and model.id.kdKelompokUser=:kdKelompokUser and model.statusEnabled=true ";
}
