package com.portofolio.dao;

import java.util.List;
import java.util.Map;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.portofolio.entity.MataUang;
import com.portofolio.entity.vo.MataUangId;

@Lazy
@Repository
public interface MataUangDao extends CrudRepository<MataUang, MataUangId> {

	@Cacheable("MataUangDaofindOneBy")
	MataUang findOneByIdKodeAndIdKdNegara(Integer kode, Integer kdNegara);

	@Query(QListAll + "where model.id.kdNegara =:kdNegara and model.id.kode = :kode ")
	@Cacheable("MataUangDaoFindByKode")
	Map<String, Object> findByKode(@Param("kode") Integer kode, @Param("kdNegara") Integer kdNegara);
	 
	@Query(QListAll + "where model.id.kdNegara =:kdNegara " + " and model.namaMataUang like %:namaMataUang% ")
	@Cacheable("MataUangDaoFindAllListPage")
	Page<Map<String, Object>> findAllList(@Param("namaMataUang") String namaMataUang, Pageable page,
			@Param("kdNegara") Integer kdNegara);

	@Query(QListAll + "where model.namaMataUang like %:namaMataUang% ")
	@Cacheable("MataUangDaoFindAllListPage")
	Page<Map<String, Object>> findAllList(@Param("namaMataUang") String namaMataUang, Pageable page );
 
	String QListAll = "select new map(model.id.kdMataUang as kdMataUang,model.id.kdNegara as kdNegara " + ", model.namaMataUang as namaMataUang "
			+ ", model.reportDisplay as reportDisplay " + ", model.currentKursToIDR as currentKursToIDR "
			+ ", model.kodeExternal as kodeExternal " + ", model.namaExternal as namaExternal "
			+ ", model.id.kdNegara as kdNegara " + ", model.statusEnabled as statusEnabled " + ", model.noRec as noRec "
			+ ", negara.namaNegara as namaNegara" + ", model.version as version )" + " from MataUang model "
			+ " left join model.negara negara " ;

	@Query(QListAll + "where model.id.kdNegara =:kdNegara  and model.statusEnabled = true ") 
	List<Map<String, Object>> findAllMataUang(@Param("kdNegara")Integer kdNegara);
	
	@Query("select new map(model.id.kdMataUang as kdMataUang,model.id.kdNegara as kdNegara , model.namaMataUang as namaMataUang) from MataUang model"
			+ " left join model.negara negara where model.statusEnabled = true ") 
	List<Map<String, Object>> findAllMataUang();

}
