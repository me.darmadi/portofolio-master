package com.portofolio.dao;

import java.util.List;
import java.util.Map;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.portofolio.entity.KelompokTransaksi;
import com.portofolio.entity.vo.KelompokTransaksiId;

import org.springframework.context.annotation.Lazy;

@Lazy
@Repository
public interface KelompokTransaksiMasterDao extends JpaRepository<KelompokTransaksi, KelompokTransaksiId> {
	@Cacheable("KelompokTransaksiDaofindOneBy")
	KelompokTransaksi findOneByKdKelompokTransaksi(Integer kdKelompokTransaksi);

	@Query(QListAll)
	@Cacheable("KelompokTransaksiDaoFindAllList")
	Page<Map<String, Object>> findAllList(Pageable page);

	@Query(QListAll + " where model.namaKelompokTransaksi like %:namaKelompokTransaksi% ")
	@Cacheable("KelompokTransaksiDaoFindAllListPage")
	Page<Map<String, Object>> findAllList(@Param("namaKelompokTransaksi") String namaKelompokTransaksi, Pageable page);

	@Query(QListAll + " where model.id.kdKelompokTransaksi = :kdKelompokTransaksi")
	@Cacheable("KelompokTransaksiDaoFindByKode")
	Map<String, Object> findByKode(@Param("kdKelompokTransaksi") Integer kdKelompokTransaksi);

	@Query(QlistWithDetail + " and model.id.kdKelompokTransaksi in :kdKelompokTransaksi  ")
	List<Map<String, Object>> findByKode(@Param("kdKelompokTransaksi") List<Integer> kdKelompokTransaksi,
			@Param("kdProfile") Integer kdProfile);

	@Query(QlistWithDetail)
	List<Map<String, Object>> findAllByKode(@Param("kdProfile") Integer kdProfile);

	String QlistWithDetail = "select new map(model.kdKelompokTransaksi as kdKelompokTransaksi , model.namaKelompokTransaksi as namaKelompokTransaksi,"
			+ " (select  modelDetail.isRealSK from KelompokTransaksiDetail modelDetail "
			+ " where modelDetail.statusEnabled = true and modelDetail.id.kdProfile=:kdProfile and modelDetail.id.kdKelompokTransaksi=model.kdKelompokTransaksi) as isRealSK ) "
			+ " from KelompokTransaksi  model where model.statusEnabled = true ";

	String QListAll = "select new map(model.kdKelompokTransaksi as kdKelompokTransaksi "
			+ ", model.namaKelompokTransaksi as namaKelompokTransaksi ,model.reportDisplay as reportDisplay "
			+ ", model.kodeExternal as kodeExternal , model.namaExternal as namaExternal "
			+ ", model.statusEnabled as statusEnabled, model.noRec as noRec, model.version as version ) from KelompokTransaksi model ";

	@Query("select new map(model.kdKelompokTransaksi as kdKelompokTransaksi "
			+ ", model.namaKelompokTransaksi as namaKelompokTransaksi ) from KelompokTransaksi model where model.statusEnabled  = true ")
	List<Map<String, Object>> findAllKelTrans();

}
