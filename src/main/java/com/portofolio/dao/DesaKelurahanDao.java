package com.portofolio.dao;

import java.util.List;
import java.util.Map;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.portofolio.entity.DesaKelurahan;
import com.portofolio.entity.vo.DesaKelurahanId;

@Lazy
@Repository
public interface DesaKelurahanDao extends CrudRepository<DesaKelurahan, DesaKelurahanId> {

	@Query(QListAll + "and model.id.kdNegara=:kdNegara ")
	Page<Map<String, Object>> findAllList(@Param("kdNegara") Integer kdNegara,@Param("statusEnabled") boolean statusEnabled, Pageable page);

	@Query(QListAll + "and model.id.kdNegara=:kdNegara  and model.namaDesaKelurahan like %:namaDesaKelurahan% ")
	Page<Map<String, Object>> findAllList(@Param("kdNegara") Integer kdNegara,
			@Param("namaDesaKelurahan") String namaDesaKelurahan,@Param("statusEnabled") boolean statusEnabled, Pageable page);

	@Query(QListAll + "and model.id.kdNegara=:kdNegara  and model.id.kdDesaKelurahan =:kdDesaKelurahan")
	Map<String, Object> findByKode(@Param("kdNegara") Integer kdNegara, @Param("kdDesaKelurahan") Integer kdDesaKelurahan,@Param("statusEnabled") boolean statusEnabled);

	String QListAll = "select new map(model.id.kdDesaKelurahan as kdDesaKelurahan, model.namaDesaKelurahan as namaDesaKelurahan "
			+ ", model.reportDisplay as reportDisplay " + ", model.kodePos as kodePos "
			+ ", model.kdPropinsi as kdPropinsi " + ", propinsi.namaPropinsi as namaPropinsi "
			+ ", model.kdKotaKabupaten as kdKotaKabupaten " + ", kotaKabupaten.namaKotaKabupaten as namaKotaKabupaten "
			+ ", model.kdKecamatan as kdKecamatan " + ", kecamatan.namaKecamatan as namaKecamatan "
			+ ", model.kodeExternal as kodeExternal " + ", model.namaExternal as namaExternal "
			+ ", model.statusEnabled as statusEnabled " + ", model.noRec as noRec "
			+ ",  negara.namaNegara as namaNegara" + ", model.version as version )" + " from DesaKelurahan model "
			+ "left join model.id id " + "left join model.negara negara " + "left join model.propinsi propinsi "
			+ "left join model.kotaKabupaten kotaKabupaten " + "left join model.kecamatan kecamatan where model.statusEnabled =:statusEnabled ";
	
	@Query(QListAll + "and model.id.kdNegara=:kdNegara  and model.kdKecamatan =:kdKecamatan  ")
	List<Map<String, Object>> findByKdKecamatan(@Param("kdNegara")Integer kdNegara,@Param("kdKecamatan") Integer kdKecamatan,@Param("statusEnabled") boolean statusEnabled);

}
