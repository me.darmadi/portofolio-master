package com.portofolio.dao;

import java.util.List;
import java.util.Map;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.portofolio.entity.PenghasilanTidakKenaPajak;
import com.portofolio.entity.vo.PenghasilanTidakKenaPajakId;

@Lazy
@Repository
public interface PenghasilanTidakKenaPajakDao
		extends CrudRepository<PenghasilanTidakKenaPajak, PenghasilanTidakKenaPajakId> {
	
    @Query("select new map(model.id.kode as id_kode"
            + ", model.statusPTKP as statusPTKP) "
            + " from PenghasilanTidakKenaPajak model "
            + " where model.id.kdProfile=:kdProfile "
            + " and model.kdStatusPerkawinan=:kdStatusPerkawinanR "
            + " and model.statusEnabled=true ")
    List<Map<String,Object>> findMasterPTKP(@Param("kdProfile") Integer kdProfile, @Param("kdStatusPerkawinanR") Integer kdStatusPerkawinanR);


	@Cacheable("PenghasilanTidakKenaPajakDaofindOneBy")
	PenghasilanTidakKenaPajak findOneByIdKodeAndIdKdProfile(Integer kode, Integer kdProfile);

	@Query(QListAll)
	@Cacheable("PenghasilanTidakKenaPajakDaoFindAllList")
	Page<Map<String, Object>> findAllList(@Param("kdProfile") Integer kdProfile, Pageable page);

	@Query(QListAll + " and model.statusPTKP like %:statusPTKP% ")
	@Cacheable("PenghasilanTidakKenaPajakDaoFindAllListPage")
	Page<Map<String, Object>> findAllList(@Param("kdProfile") Integer kdProfile, @Param("statusPTKP") String statusPTKP,
			Pageable page);

	@Query(QListAll + " and model.id.kode = :kode")
	@Cacheable("PenghasilanTidakKenaPajakDaoFindByKode")
	Map<String, Object> findByKode(@Param("kdProfile") Integer kdProfile, @Param("kode") Integer kode);

	String QListAll = "select new map(model.id.kdPtkp as kdPtkp,model.id.kdProfile as kdProfile " + ", model.statusPTKP as statusPTKP "
	// + ", status1.namaStatus as namaStatusPTKP "
			+ ", model.reportDisplay as reportDisplay " + ", model.deskripsi as deskripsi "
			+ ", model.kdStatusPerkawinan as kdStatusPerkawinan " + ", status.namaStatus as namaStatusPerkawinan "
			+ ", model.qtyAnak as qtyAnak " + ", model.kodeExternal as kodeExternal "
			+ ", model.namaExternal as namaExternal " + ", model.kdDepartemen as kdDepartemen "
			+ ", model.statusEnabled as statusEnabled " + ", model.noRec as noRec "
			+ ", model.totalHargaPTKP as totalHargaPTKP "
			+ ", departemen.namaDepartemen as namaDepartemen, model.version as version )"
			+ " from PenghasilanTidakKenaPajak model " + " left join model.status status "
			// + " left join model.status1 status1 "
			+ " left join model.id id left join model.departemen departemen where id.kdProfile=:kdProfile and departemen.id.kdProfile=id.kdProfile ";// and model.statusEnabled = true  ";

}
