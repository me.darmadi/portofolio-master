package com.portofolio.dao;

import java.util.List;
import java.util.Map;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.portofolio.entity.Propinsi;
import com.portofolio.entity.vo.PropinsiId;


@Lazy
@Repository
public interface PropinsiDao extends CrudRepository<Propinsi, PropinsiId> {
    

    @Query(QListAll + " and id.kdNegara=:kdNegara ")
    Page<Map<String, Object>> findAllList(@Param("kdNegara") Integer kdNegara,@Param("statusEnabled") boolean statusEnabled, Pageable page);
    
    @Query(QListAll + " and id.kdNegara=:kdNegara  and model.namaPropinsi like %:namaPropinsi% ")
    Page<Map<String, Object>> findAllList(@Param("kdNegara") Integer kdNegara, @Param("namaPropinsi") String namaPropinsi,@Param("statusEnabled") boolean statusEnabled, Pageable page );
    
    @Query(QListAll 
            + " and id.kdNegara=:kdNegara and model.id.kdPropinsi =:kdPropinsi")
    Map<String, Object> findByKdPropinsi(@Param("kdNegara") Integer kdProfile, @Param("kdPropinsi") Integer kdPropinsi,@Param("statusEnabled") boolean statusEnabled );
    
    @Query(QListAll + " and id.kdNegara=:kdNegara  ") 
    List<Map<String, Object>> findByKdNegara(@Param("kdNegara") Integer kdNegara,@Param("statusEnabled") boolean statusEnabled); 
    

    String QListAll ="select new map(model.id.kdPropinsi as kdPropinsi,model.id.kdNegara as kdNegara "
            + ", model.namaPropinsi as namaPropinsi " 
            + ", model.reportDisplay as reportDisplay " 
            + ", model.id.kdNegara as kdNegara " 
            + ", model.kodeExternal as kodeExternal " 
            + ", model.namaExternal as namaExternal "  
            + ", model.statusEnabled as statusEnabled " 
            + ", model.noRec as noRec " 
            + ", negara.namaNegara as namaNegara, model.version as version )" 
            + " from Propinsi model left join model.id id left join model.negara negara where model.statusEnabled=:statusEnabled ";
   

}
