
package com.portofolio.dao;

import com.portofolio.entity.Kalender;
import com.portofolio.entity.vo.KalenderId;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Lazy
@Repository("kalenderDao")
public interface KalenderDao extends JpaRepository<Kalender, KalenderId> {

	@Cacheable("KalenderDaofindOneByTanggal")
	Kalender findOneByTanggalAndIdKdProfile(Long tanggal, Integer kdProfile);

	@Cacheable("KalenderDaofindOneBy")
	Kalender findOneByIdKdTanggalAndIdKdProfileAndKdDepartemen(Integer kdTanggal, Integer kdProfile,
			String KdDepartemen);

	@Query(QListAll)
	@Cacheable("KalenderDaoFindAllList")
	Page<Map<String, Object>> findAllList(@Param("kdProfile") Integer kdProfile, Pageable page,
			@Param("kdDepartemen") String kdDepartemen);

	@Query(QListAll + " and model.tanggal like %:tanggal% ")
	@Cacheable("KalenderDaoFindAllListPage")
	Page<Map<String, Object>> findAllList(@Param("kdProfile") Integer kdProfile, @Param("tanggal") String tanggal,
			Pageable page, @Param("kdDepartemen") String kdDepartemen);

	@Query(QListAll + " and model.id.kdTanggal = :kdTanggal")
	@Cacheable("KalenderDaoFindByKode")
	Map<String, Object> findByKode(@Param("kdProfile") Integer kdProfile, @Param("kdTanggal") Integer kdTanggal,
			@Param("kdDepartemen") String kdDepartemen);


	String QListAll = "select new map(model.id.kdTanggal as kdKdTanggal,model.id.kdProfile as kdProfile " + ", model.tanggal as tanggal "
			+ ", model.namaHari as namaHari " + ", model.namaBulan as namaBulan "
			+ ", model.hariKeDlmMinggu as hariKeDlmMinggu " + ", model.hariKeDlmBulan as hariKeDlmBulan "
			+ ", model.hariKeDlmTahun as hariKeDlmTahun " + ", model.mingguKeDlmTahun as mingguKeDlmTahun "
			+ ", model.bulanKeDlmTahun as bulanKeDlmTahun " + ", model.tahunKalender as tahunKalender "
			+ ", model.tahunFiscal as tahunFiscal " + ", model.bulanFiscal as bulanFiscal "
			+ ", model.triwulanKeDlmTahun as triwulanKeDlmTahun " + ", model.semesterKeDlmTahun as semesterKeDlmTahun "
			+ ", model.kdDepartemen as kdDepartemen " + ", model.statusEnabled as statusEnabled "
			+ ", model.noRec as noRec " + ", departemen.namaDepartemen as namaDepartemen, model.version as version )"
			+ " from Kalender model left join model.id id left join model.departemen departemen left join model.profile profile where profile.kode =:kdProfile and model.kdDepartemen = :kdDepartemen and model.statusEnabled = true ";

    @Query("select new map(model.id.kdTanggal as kode) from Kalender model where model.id.kdProfile=:kdProfile and model.tanggal between :tglAwal and :tglAkhir ")
    Map<String,Object> findKodeByTanggal(@Param("kdProfile")Integer kdProfile, @Param("tglAwal")Long tglAwal,@Param("tglAkhir")Long tglAkhir);

}
