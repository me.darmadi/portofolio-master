package com.portofolio.dao;

import java.util.List;
import java.util.Map;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.portofolio.entity.Hari;
import com.portofolio.entity.vo.HariId;

@Lazy
@Repository
public interface HariDao extends CrudRepository<Hari, HariId> {

	@Cacheable("HariDaofindOneBy")
	Hari findOneByIdKodeAndIdKdNegara(Integer kode, Integer kdNegara);

	@Query(QListAll + "where model.namaHari like %:namaHari% ")
	@Cacheable("HariDaoFindAllListPage")
	Page<Map<String, Object>> findAllList(@Param("namaHari") String namaHari, Pageable page);

	@Query(QListAll + "where model.id.kdNegara=:kdNegara  and model.namaHari like %:namaHari% ")
	@Cacheable("HariDaoFindAllListPage")
	Page<Map<String, Object>> findAllList(@Param("kdNegara") Integer kdNegara, @Param("namaHari") String namaHari,
			Pageable page);

	@Query(QListAll + " where model.id.kdNegara = :kdNegara and model.id.kode like :kode")
	@Cacheable("HariDaoFindByKode")
	Map<String, Object> findByKode(@Param("kdNegara") Integer kdNegara, @Param("kode") Integer kode);

	String QListAll = "select new map(model.id.kdHari as kdHari,model.id.kdProfile as kdProfile " 
			+ ", model.namaHari as namaHari "
			+ ", model.reportDisplay as reportDisplay "
			+ ", model.noUrutHariKe as noUrutHariKe "
			+ ", model.kodeExternal as kodeExternal "
			+ ", model.namaExternal as namaExternal "
			+ ", model.statusEnabled as statusEnabled " 
	        + ", model.formatAngka as formatAngka " 
	        + ", model.formatAngkaRomawi as formatAngkaRomawi "  
			+ ", model.noRec as noRec "
			+ ", negara.namaNegara as namaNegara"
			+ ", model.version as version )"
			+ " from Hari model left join model.id id " 
			+ " left join model.negara negara ";
	 

	@Query(QListAll + "where model.id.kdNegara=:kdNegara and model.statusEnabled = true  ")
	List<Map<String, Object>> findAllHari(@Param("kdNegara") Integer kdNegara);

}
