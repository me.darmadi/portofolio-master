package com.portofolio.dao;

import java.util.Map;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.portofolio.entity.Profile;
import com.portofolio.entity.vo.ProfileId;

@Repository("profileDao")
public interface ProfileDao extends JpaRepository<Profile, Integer> {
	
	@Query(QListAll)
	Page<Map<String, Object>> findAllList(@Param("statusEnabled") boolean statusEnabled,Pageable page);

	@Query(QListAll + " and model.namaLengkap like %:namaLengkap% ")
	Page<Map<String, Object>> findAllList(@Param("namaLengkap") String namaLengkap,@Param("statusEnabled") boolean statusEnabled, Pageable page);

	@Query(QListAll + " and model.kode =:kdProfile")
	Map<String, Object> findByKdProfile(@Param("kdProfile") Integer kdProfile,@Param("statusEnabled") boolean statusEnabled);

	

	String QListAll="select new map ("
			+ " model.kode as kdProfile"
			+ ",model.namaLengkap as namaLengkap"
			+ ",model.reportDisplay as reportDisplay"
			+ ",model.tglRegistrasi as tglRegistrasi"
			+ ",model.kdAlamat as kdAlamat"
			+ ",model.gambarLogo as gambarLogo"
			+ ",model.kdJenisProfile as kdJenisProfile"
			+ ",jenisProfile.namaJenisProfile as namaJenisProfile"
			+ ",negara.namaNegara as namaNegara "
			+ ",negara.kdNegara as kdNegara"
			+ ",model.version as version"
			+ ",model.noRec as noRec"
			+ ",model.statusEnabled as statusEnabled"
			+ ",model.namaExternal as namaExternal"
			+ ",model.kodeExternal as kodeExternal"
			+ ",CONCAT(alamat.alamatLengkap,' RT ',alamat.rTRW,',Desa/Kel ',desaKelurahan.namaDesaKelurahan,',Kecamatan ',kecamatan.namaKecamatan,',',kotaKabupaten.namaKotaKabupaten,',',propinsi.namaPropinsi,',',alamat.kodePos ) as alamatLengkap"
			+ ") from Profile model left join model.jenisProfile jenisProfile "
			+ " left join model.negara negara"
			+ " left join model.alamat alamat "
			+ " left join alamat.propinsi propinsi "
			+ " left join alamat.kotaKabupaten kotaKabupaten " 
			+ " left join alamat.kecamatan kecamatan "
			+ " left join alamat.desaKelurahan desaKelurahan " 
			+ " where model.statusEnabled=:statusEnabled ";

}
