package com.portofolio.dao;

import java.util.List;
import java.util.Map;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.portofolio.entity.Agama;
import com.portofolio.entity.vo.AgamaId;



@Lazy
@Repository
public interface AgamaDao extends CrudRepository<Agama, AgamaId> {

	Agama findOneByIdKdAgamaAndIdKdNegara(Integer kdAgama, Integer kdNegara);

	@Query(QListAll)
	Page<Map<String, Object>> findAllList(@Param("kdNegara") Integer kdNegara,@Param("statusEnabled") boolean statusEnabled, Pageable page);

	@Query(QListAll + "and model.namaAgama like %:namaAgama% ")
	Page<Map<String, Object>> findAllList(@Param("kdNegara") Integer kdNegara, @Param("namaAgama") String namaAgama,@Param("statusEnabled") boolean statusEnabledParameter,Pageable page);

	@Query(QListAll + "and model.id.kdAgama =:kdAgama")
	Map<String, Object> findByKdAgama(@Param("kdNegara") Integer kdNegara, @Param("kdAgama") Integer kdAgama,@Param("statusEnabled") boolean statusEnabled);

	String QListAll = "select new map(model.id.kdAgama as kdAgama,model.id.kdNegara as kdNegara " + ", model.namaAgama as namaAgama " 
			+ ", model.reportDisplay as reportDisplay " + ", model.kodeExternal as kodeExternal "
			+ ", model.namaExternal as namaExternal " + ", model.statusEnabled as statusEnabled "
			+ ", model.noRec as noRec " + ", negara.namaNegara as namaNegara, model.version as version )"
			+ " from Agama model left join model.negara negara where  model.statusEnabled =:statusEnabled and  model.id.kdNegara=:kdNegara ";


}
