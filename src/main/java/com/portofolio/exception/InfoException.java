package com.portofolio.exception;

public class InfoException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7884228681064640342L;

	public InfoException() {
		super();
	}
	
	public InfoException(String message) {
		 super(message);
	}
	
	public InfoException(String message, Throwable cause) {
		super(message, cause);
	}
}
