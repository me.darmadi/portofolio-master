package com.portofolio.exception;

public class StorageFileNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -8780475171931857512L;
	
	public StorageFileNotFoundException() {
		super();
	}
	
	public StorageFileNotFoundException(String message) {
		 super(message);
	}
	
	public StorageFileNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

}
