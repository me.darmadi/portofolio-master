package com.portofolio.exception;

public class AvordoException extends RuntimeException  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7884228681064640342L;

	public AvordoException() {
		super();
	}
	
	public AvordoException(String message) {
		 super(message);
	}
	
	public AvordoException(String message, Throwable cause) {
		super(message, cause);
	}

}
