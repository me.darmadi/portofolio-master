package com.portofolio;
public enum EnumPerson {

    PEMILIK(1, "PML-"),
    PENANGGUNGJAWAB(2, "PNJ-");

    private java.lang.String name;

    private java.lang.Integer id;

    EnumPerson(Integer id, java.lang.String name) {
        this.name = name;
        this.id = id;
    }

    public java.lang.String getName() {
        return name;
    }

    public java.lang.Integer getId() {
        return id;
    }
}