package com.portofolio.entity;


import org.hibernate.annotations.ForeignKey;
import static com.portofolio.base.adapter.DatabaseAdapter.Constant.*;

import javax.persistence.*;
import javax.validation.constraints.*;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.portofolio.entity.Profile;
import com.portofolio.entity.vo.MapLoginUserToPegawaiId;

import org.hibernate.annotations.Type;


@Entity
@Table(name = "MapLoginUserToPegawai_S", indexes = {
		@Index(name = "MapLoginUserToPegawai_S_Index", columnList = "statusEnabled", unique = false) })
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class MapLoginUserToPegawai {

	@EmbeddedId
	private MapLoginUserToPegawaiId id;

	private static final String cDefKdKelompokUser = INTEGER;
	@Column(name = "KdKelompokUser", columnDefinition = cDefKdKelompokUser)
	@NotNull(message = "maploginusertopegawai.kdkelompokuser.notnull")
	private Integer kdKelompokUser;

	private static final String cDefStatusEnabled = INTEGER;
	@Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
	@NotNull(message = "maploginusertopegawai.statusenabled.notnull")
	@Type(type = "org.hibernate.type.NumericBooleanType") private Boolean statusEnabled;

	private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG;
	@Column(name = "NoRec", columnDefinition = cDefNoRec)
	@NotNull(message = "maploginusertopegawai.norec.notnull")
	private String noRec;

	private static final String cDefVersion = INTEGER;
	@Version
	@Column(name = "version", nullable = false, columnDefinition = cDefVersion)
	@NotNull(message = "maploginusertopegawai.version.notnull")
	private Integer version;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false), })
	//@ForeignKey(name="none")
	private Profile profile;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdPegawai", referencedColumnName = "KdPegawai", insertable = false, updatable = false), })
	//@ForeignKey(name="none")
	private Pegawai pegawai;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdKelompokUser", referencedColumnName = "KdKelompokUser", insertable = false, updatable = false), })
	//@ForeignKey(name="none")
	private KelompokUser kelompokUser;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdUser", referencedColumnName = "KdUser", insertable = false, updatable = false), })
	//@ForeignKey(name="none")
	private LoginUser loginUser;
}
