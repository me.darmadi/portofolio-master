package com.portofolio.entity.vo;

import javax.validation.constraints.*;
import  javax.validation.constraints.Max;
import  javax.validation.constraints.Min;
import  javax.validation.constraints.NotNull;

import static com.portofolio.base.adapter.DatabaseAdapter.Constant.*;

import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.EqualsAndHashCode;


@Embeddable
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class LoginUserId implements java.io.Serializable {
     /**
     * 
     */
    private static final String cDefKdUser =INTEGER;
    @Column(name = "KdUser",   columnDefinition = cDefKdUser)
    @NotNull(message = "loginuserid.kduser.notnull")
    @GeneratedValue
    private Integer kdUser;
    
    

}
