package com.portofolio.entity.vo;

import static com.portofolio.base.adapter.DatabaseAdapter.Constant.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Embeddable
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class KotaKabupatenId implements java.io.Serializable {
	/**
	* 
	*/
	private static final String cDefKdNegara = INTEGER;
	@Column(name = "KdNegara", columnDefinition = cDefKdNegara)
	@NotNull(message = "KotaKabupaten.kdnegara.notnull")
	private Integer kdNegara;

	private static final String cDefKdKotaKabupaten = INTEGER;
	@Column(name = "KdKotaKabupaten", columnDefinition = cDefKdKotaKabupaten)
	@NotNull(message = "kotakabupatenid.kdkotakabupaten.notnull")
	private Integer kdKotaKabupaten;

}
