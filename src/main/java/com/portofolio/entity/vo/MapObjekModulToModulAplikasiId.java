/* --------------------------------------------------------
 Generated by Lukman Hakim using sql2java (http://sql2java.sourceforge.net/ )
 jdbc driver used at code generation time: net.sourceforge.jtds.jdbc.Driver
 
 Author : Lukman Hakim (lukman.uki@gmail.com) 
 Date Created : 03/11/2017
 --------------------------------------------------------
*/
package com.portofolio.entity.vo;

import javax.validation.constraints.*;
import  javax.validation.constraints.Max;
import  javax.validation.constraints.Min;
import  javax.validation.constraints.NotNull;

import static com.portofolio.base.adapter.DatabaseAdapter.Constant.*;

import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.EqualsAndHashCode;


@Embeddable
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class MapObjekModulToModulAplikasiId implements java.io.Serializable {
     /**
     * 
     */
    private static final String cDefKdProfile =INTEGER;
    @Column(name = "KdProfile",   columnDefinition = cDefKdProfile)
    @NotNull(message = "mapobjekmodultomodulaplikasiid.kdprofile.notnull")
    private Integer kdProfile;
    
    private static final String cDefKdObjekModulAplikasi = VARCHAR + AWAL_KURUNG + 6 + AKHIR_KURUNG ;
    @Column(name = "KdObjekModulAplikasi",   columnDefinition = cDefKdObjekModulAplikasi)
    @NotNull(message = "mapobjekmodultomodulaplikasiid.kdobjekmodulaplikasi.notnull")
    private String kdObjekModulAplikasi;
    
    private static final String cDefKdModulAplikasi = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG ;
    @Column(name = "KdModulAplikasi",   columnDefinition = cDefKdModulAplikasi)
    @NotNull(message = "mapobjekmodultomodulaplikasiid.kdmodulaplikasi.notnull")
    private String kdModulAplikasi;
    
    

}
