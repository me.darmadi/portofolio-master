package com.portofolio.entity.vo;

import lombok.*;

import static com.portofolio.base.adapter.DatabaseAdapter.Constant.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

@Embeddable
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class RuanganId implements java.io.Serializable {
	
	private static final String cDefKdProfile = INTEGER;
	@Column(name = "KdProfile", columnDefinition = cDefKdProfile)
	@NotNull(message = "ruanganid.kdprofile.notnull")
	private Integer kdProfile;

	private static final String cDefKdRuangan = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "KdRuangan", columnDefinition = cDefKdRuangan)
	@NotNull(message = "ruanganid.kdruangan.notnull")
	private String kdRuangan;

}
