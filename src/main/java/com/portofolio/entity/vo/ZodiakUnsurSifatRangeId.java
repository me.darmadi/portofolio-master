package com.portofolio.entity.vo;

import static com.portofolio.base.adapter.DatabaseAdapter.Constant.AKHIR_KURUNG;
import static com.portofolio.base.adapter.DatabaseAdapter.Constant.AWAL_KURUNG;
import static com.portofolio.base.adapter.DatabaseAdapter.Constant.INTEGER;
import static com.portofolio.base.adapter.DatabaseAdapter.Constant.INTEGER;
import static com.portofolio.base.adapter.DatabaseAdapter.Constant.VARCHAR;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import  javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Embeddable
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class ZodiakUnsurSifatRangeId implements java.io.Serializable {
     /**
     * 
     */
	private static final String cDefKdNegara = INTEGER;
	@Column(name = "KdNegara", columnDefinition = cDefKdNegara)
	@NotNull(message = "zodiakunsursifatrangeid.kdnegara.notnull")
	private Integer kdNegara;
	
    
    private static final String cDefTglAwal = INTEGER ;
    @Column(name = "TglAwal",   columnDefinition = cDefTglAwal)
    @NotNull(message = "zodiakunsursifatrangeid.tglawal.notnull")
    private Long tglAwal;
    
    private static final String cDefTglAkhir = INTEGER ;
    @Column(name = "TglAkhir",   columnDefinition = cDefTglAkhir)
    @NotNull(message = "zodiakunsursifatrangeid.tglakhir.notnull")
    private Long tglAkhir;
    
    private static final String cDefKdZodiak =INTEGER;
    @Column(name = "KdZodiak",   columnDefinition = cDefKdZodiak)
    @NotNull(message = "zodiakunsursifatrangeid.kdzodiak.notnull")
    private Integer kdZodiak;
    
    private static final String cDefKdZodiakUnsur =INTEGER;
    @Column(name = "KdZodiakUnsur",   columnDefinition = cDefKdZodiakUnsur)
    @NotNull(message = "zodiakunsursifatrangeid.kdzodiakunsur.notnull")
    private Integer kdZodiakUnsur;
    
    private static final String cDefSifat = VARCHAR + AWAL_KURUNG + 4 + AKHIR_KURUNG ;
    @Column(name = "Sifat",   columnDefinition = cDefSifat)
    @NotNull(message = "zodiakunsursifatrangeid.sifat.notnull")
    private String sifat;
    
    

}
