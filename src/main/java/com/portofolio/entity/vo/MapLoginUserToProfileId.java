package com.portofolio.entity.vo;

import static com.portofolio.base.adapter.DatabaseAdapter.Constant.AKHIR_KURUNG;
import static com.portofolio.base.adapter.DatabaseAdapter.Constant.AWAL_KURUNG;
import static com.portofolio.base.adapter.DatabaseAdapter.Constant.INTEGER;
import static com.portofolio.base.adapter.DatabaseAdapter.Constant.INTEGER;
import static com.portofolio.base.adapter.DatabaseAdapter.Constant.VARCHAR;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import  javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Embeddable
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class MapLoginUserToProfileId implements java.io.Serializable {
     /**
     * 
     */
    private static final String cDefKdUser =INTEGER;
    @Column(name = "KdUser",   columnDefinition = cDefKdUser)
    @NotNull(message = "maploginusertoprofileid.kduser.notnull")
    private Long kdUser;
    
    private static final String cDefKdProfile =INTEGER;
    @Column(name = "KdProfile",   columnDefinition = cDefKdProfile)
    @NotNull(message = "maploginusertoprofileid.kdprofile.notnull")
    private Integer kdProfile;
    
    private static final String cDefKdModulAplikasi = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG ;
    @Column(name = "KdModulAplikasi",   columnDefinition = cDefKdModulAplikasi)
    @NotNull(message = "maploginusertoprofileid.kdmodulaplikasi.notnull")
    private String kdModulAplikasi;
    
    private static final String cDefKdRuangan = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG ;
    @Column(name = "KdRuangan",   columnDefinition = cDefKdRuangan)
    @NotNull(message = "maploginusertoprofileid.kdruangan.notnull")
    private String kdRuangan;
    
    

}
