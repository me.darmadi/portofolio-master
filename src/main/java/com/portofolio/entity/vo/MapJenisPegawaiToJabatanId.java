package com.portofolio.entity.vo;

import static com.portofolio.base.adapter.DatabaseAdapter.Constant.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Embeddable
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class MapJenisPegawaiToJabatanId implements java.io.Serializable {
	/**
	* 
	*/
	private static final String cDefKdProfile = INTEGER;
	@Column(name = "KdProfile", columnDefinition = cDefKdProfile)
	@NotNull(message = "mapjenispegawaitojabatanid.kdprofile.notnull")
	private Integer kdProfile;

	private static final String cDefKdJenisPegawai = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "KdJenisPegawai", columnDefinition = cDefKdJenisPegawai)
	@NotNull(message = "mapjenispegawaitojabatanid.kdjenispegawai.notnull")
	private String kdJenisPegawai;

	private static final String cDefKdJabatan = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "KdJabatan", columnDefinition = cDefKdJabatan)
	@NotNull(message = "mapjenispegawaitojabatanid.kdjabatan.notnull")
	private String kdJabatan;

}
