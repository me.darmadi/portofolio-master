package com.portofolio.entity.vo;

import static com.portofolio.base.adapter.DatabaseAdapter.Constant.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Embeddable
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class HariId implements java.io.Serializable {
	private static final String cDefKdNegara = INTEGER;
	@Column(name = "KdNegara", columnDefinition = cDefKdNegara)
	@NotNull(message = "hari.kdnegara.notnull")
	private Integer kdNegara;

	private static final String cDefKdHari = INTEGER;
	@Column(name = "KdHari", columnDefinition = cDefKdHari)
	@NotNull(message = "hariid.kdhari.notnull")
	private Integer kdHari;

}
