package com.portofolio.entity.vo;

import javax.validation.constraints.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import static com.portofolio.base.adapter.DatabaseAdapter.Constant.*;

import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.EqualsAndHashCode;

@Embeddable
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class LevelTingkatId implements java.io.Serializable {
	/**
	* 
	*/
	private static final String cDefKdProfile = INTEGER;
	@Column(name = "KdProfile", columnDefinition = cDefKdProfile)
	@NotNull(message = "leveltingkatid.kdprofile.notnull")
	private Integer kdProfile;

	private static final String cDefKdLevelTingkat = INTEGER;
	@Column(name = "KdLevelTingkat", columnDefinition = cDefKdLevelTingkat)
	@NotNull(message = "leveltingkatid.kdleveltingkat.notnull")
	private Integer kdLevelTingkat;

}
