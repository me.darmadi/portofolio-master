package com.portofolio.entity.vo;

import static com.portofolio.base.adapter.DatabaseAdapter.Constant.INTEGER;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Embeddable
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class JenisKelaminId implements java.io.Serializable {
	private static final String cDefKdNegara = INTEGER;
	@Column(name = "KdNegara", columnDefinition = cDefKdNegara)
	@NotNull(message = "jeniskelaminid.kdnegara.notnull")
	private Integer kdNegara;

	private static final String cDefKdJenisKelamin = INTEGER;
	@Column(name = "KdJenisKelamin", columnDefinition = cDefKdJenisKelamin)
	@NotNull(message = "jeniskelaminid.kdjeniskelamin.notnull")
	private Integer kdJenisKelamin;

}
