package com.portofolio.entity.vo;

import static com.portofolio.base.adapter.DatabaseAdapter.Constant.INTEGER;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import  javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Embeddable
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class ZodiakId implements java.io.Serializable {
     /**
     * 
     */
	private static final String cDefKdNegara = INTEGER;
	@Column(name = "KdNegara", columnDefinition = cDefKdNegara)
	@NotNull(message = "zodiakid.kdnegara.notnull")
	private Integer kdNegara;
    
    private static final String cDefKdZodiak =INTEGER;
    @Column(name = "KdZodiak",   columnDefinition = cDefKdZodiak)
    @NotNull(message = "zodiakid.kdzodiak.notnull")
    private Integer kdZodiak;
    
    

}
