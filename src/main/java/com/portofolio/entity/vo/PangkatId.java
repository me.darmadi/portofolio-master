package com.portofolio.entity.vo;

import static com.portofolio.base.adapter.DatabaseAdapter.Constant.INTEGER;
import static com.portofolio.base.adapter.DatabaseAdapter.Constant.INTEGER;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import  javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Embeddable
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class PangkatId implements java.io.Serializable {
     /**
     * 
     */
    private static final String cDefKdProfile =INTEGER;
    @Column(name = "KdProfile",   columnDefinition = cDefKdProfile)
    @NotNull(message = "pangkatid.kdprofile.notnull")
    private Integer kdProfile;
    
    private static final String cDefKdPangkat =INTEGER;
    @Column(name = "KdPangkat",   columnDefinition = cDefKdPangkat)
    @NotNull(message = "pangkatid.kdpangkat.notnull")
    private Integer kdPangkat;
    
    

}
