/* --------------------------------------------------------
 Generated by Lukman Hakim using sql2java (http://sql2java.sourceforge.net/ )
 jdbc driver used at code generation time: net.sourceforge.jtds.jdbc.Driver
 
 Author : Lukman Hakim (lukman.uki@gmail.com) 
 Date Created : 25/08/2017
 --------------------------------------------------------
*/
package com.portofolio.entity.vo;

import lombok.*;

import static com.portofolio.base.adapter.DatabaseAdapter.Constant.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

@Embeddable
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class ModulAplikasiId implements java.io.Serializable {
	/*private static final String cDefKdProfile = INTEGER;
	@Column(name = "KdProfile", columnDefinition = cDefKdProfile)
	@NotNull(message = "modulaplikasiid.kdprofile.notnull")
	private Integer kdProfile;*/

	private static final String cDefKdModulAplikasi = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "KdModulAplikasi", columnDefinition = cDefKdModulAplikasi)
	@NotNull(message = "modulaplikasiid.kdmodulaplikasi.notnull")
	private String kdModulAplikasi;

}
