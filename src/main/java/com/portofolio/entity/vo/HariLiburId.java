/* --------------------------------------------------------
 Generated by Lukman Hakim using sql2java (http://sql2java.sourceforge.net/ )
 jdbc driver used at code generation time: net.sourceforge.jtds.jdbc.Driver
 
 Author : Lukman Hakim (lukman.uki@gmail.com) 
 Date Created : 25/08/2017
 --------------------------------------------------------
*/
package com.portofolio.entity.vo;

import lombok.*;

import static com.portofolio.base.adapter.DatabaseAdapter.Constant.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
@Embeddable
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class HariLiburId implements java.io.Serializable {
	private static final String cDefKdProfile = INTEGER;
	@Column(name = "KdProfile", columnDefinition = cDefKdProfile)
	@NotNull(message = "hariliburid.kdprofile.notnull")
	private Integer kdProfile;

	private static final String cDefKdHariLibur = INTEGER;
	@Column(name = "KdHariLibur", columnDefinition = cDefKdHariLibur)
	@NotNull(message = "hariliburid.kdharilibur.notnull")
	private Integer kdHariLibur;

}
