package com.portofolio.entity.vo;

import static com.portofolio.base.adapter.DatabaseAdapter.Constant.AKHIR_KURUNG;
import static com.portofolio.base.adapter.DatabaseAdapter.Constant.AWAL_KURUNG;
import static com.portofolio.base.adapter.DatabaseAdapter.Constant.INTEGER;
import static com.portofolio.base.adapter.DatabaseAdapter.Constant.VARCHAR;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Embeddable
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class LanguageSettingId implements java.io.Serializable {

	private static final String cDefKdModulAplikasi = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG ;
    @Column(name = "KdModulAplikasi",   columnDefinition = cDefKdModulAplikasi)
    @NotNull(message = "mapmodulaplikasitoobjekmodulid.kdmodulaplikasi.notnull")
    private String kdModulAplikasi;
    
    private static final String cDefKdVersion =INTEGER;
    @Column(name = "KdVersion",   columnDefinition = cDefKdVersion)
    @NotNull(message = "mapmodulaplikasitoobjekmodulid.kdversion.notnull")
    private Integer kdVersion;
    
    private static final String cDefKdBahasa = INTEGER;
	@Column(name = "KdBahasa", columnDefinition = cDefKdBahasa)
	@NotNull(message = "bahasaid.kdbahasa.notnull")
	private Integer kdBahasa;
	
	private static final String cDefKdProfile =INTEGER;
    @Column(name = "KdProfile",   columnDefinition = cDefKdProfile)
    @NotNull(message = "jabatanid.kdprofile.notnull")
    private Integer kdProfile;
    
    

}
