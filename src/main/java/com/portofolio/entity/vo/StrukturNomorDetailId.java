/* --------------------------------------------------------
 Generated by Lukman Hakim using sql2java (http://sql2java.sourceforge.net/ )
 jdbc driver used at code generation time: net.sourceforge.jtds.jdbc.Driver
 
 Author : Lukman Hakim (lukman.uki@gmail.com) 
 Date Created : 31/10/2017
 --------------------------------------------------------
*/
package com.portofolio.entity.vo;

import javax.validation.constraints.*;
import  javax.validation.constraints.Max;
import  javax.validation.constraints.Min;
import  javax.validation.constraints.NotNull;

import static com.portofolio.base.adapter.DatabaseAdapter.Constant.*;

import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.EqualsAndHashCode;


@Embeddable
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class StrukturNomorDetailId implements java.io.Serializable {
     /**
     * 
     */
    private static final String cDefKdProfile =INTEGER;
    @Column(name = "KdProfile",   columnDefinition = cDefKdProfile)
    @NotNull(message = "strukturnomordetailid.kdprofile.notnull")
    private Integer kdProfile;
    
    private static final String cDefKdStrukturNomor =INTEGER;
    @Column(name = "KdStrukturNomor",   columnDefinition = cDefKdStrukturNomor)
    @NotNull(message = "strukturnomordetailid.kdstrukturnomor.notnull")
    private Integer kdStrukturNomor;
    
    private static final String cDefKdLevelTingkat =INTEGER;
    @Column(name = "KdLevelTingkat",   columnDefinition = cDefKdLevelTingkat)
    @NotNull(message = "strukturnomordetailid.kdleveltingkat.notnull")
    private Integer kdLevelTingkat;
    
    

}
