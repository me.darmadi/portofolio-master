package com.portofolio.entity.vo;

import static com.portofolio.base.adapter.DatabaseAdapter.Constant.INTEGER;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import  javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Embeddable
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class ZodiakUnsurId implements java.io.Serializable {
     /**
     * 
     */
	private static final String cDefKdNegara = INTEGER;
	@Column(name = "KdNegara", columnDefinition = cDefKdNegara)
	@NotNull(message = "zodiakunsurid.kdnegara.notnull")
	private Integer kdNegara;
	
    private static final String cDefKdZodiakUnsur =INTEGER;
    @Column(name = "KdZodiakUnsur",   columnDefinition = cDefKdZodiakUnsur)
    @NotNull(message = "zodiakunsurid.kdzodiakunsur.notnull")
    private Integer kdZodiakUnsur;
    
    

}
