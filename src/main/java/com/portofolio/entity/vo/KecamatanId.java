package com.portofolio.entity.vo;

import lombok.*;

import static com.portofolio.base.adapter.DatabaseAdapter.Constant.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

@Embeddable
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class KecamatanId implements java.io.Serializable {

	private static final String cDefKdNegara = INTEGER;
	@Column(name = "KdNegara", columnDefinition = cDefKdNegara)
	@NotNull(message = "Kecamatan.kdnegara.notnull")
	private Integer kdNegara;
	private static final String cDefKdKecamatan = INTEGER;
	@Column(name = "KdKecamatan", columnDefinition = cDefKdKecamatan)
	@NotNull(message = "kecamatanid.kdkecamatan.notnull")
	private Integer kdKecamatan;

}
