/* --------------------------------------------------------
 Generated by Lukman Hakim using sql2java (http://sql2java.sourceforge.net/ )
 jdbc driver used at code generation time: net.sourceforge.jtds.jdbc.Driver
 
 Author : Lukman Hakim (lukman.uki@gmail.com) 
 Date Created : 15/12/2017
 --------------------------------------------------------
*/
package com.portofolio.entity;


import org.hibernate.annotations.ForeignKey;
import static com.portofolio.base.adapter.DatabaseAdapter.Constant.*;

import javax.persistence.*;
import javax.validation.constraints.*;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.portofolio.entity.Departemen;
import com.portofolio.entity.vo.RekananId;

import org.hibernate.annotations.Type;

@Entity
@Table(name = "Rekanan_M", indexes = { @Index(name = "Rekanan_M_Index", columnList = "statusEnabled", unique = false) })
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Rekanan {

	@EmbeddedId
	private RekananId id;

	private static final String cDefNamaRekanan = VARCHAR + AWAL_KURUNG + 80 + AKHIR_KURUNG;
	@Column(name = "NamaRekanan", columnDefinition = cDefNamaRekanan)
	@NotNull(message = "rekanan.namarekanan.notnull")
	private String namaRekanan;

	private static final String cDefReportDisplay = VARCHAR + AWAL_KURUNG + 80 + AKHIR_KURUNG;
	@Column(name = "ReportDisplay", columnDefinition = cDefReportDisplay)
	@NotNull(message = "rekanan.reportdisplay.notnull")
	private String reportDisplay;

	private static final String cDefContactPerson = VARCHAR + AWAL_KURUNG + 80 + AKHIR_KURUNG;
	@Column(name = "ContactPerson", columnDefinition = cDefContactPerson)
	private String contactPerson;

	private static final String cDefNPWP = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "NPWP", columnDefinition = cDefNPWP)
	private String nPWP;

	private static final String cDefNoPKP = VARCHAR + AWAL_KURUNG + 40 + AKHIR_KURUNG;
	@Column(name = "NoPKP", columnDefinition = cDefNoPKP)
	private String noPKP;

	private static final String cDefKdAccount = INTEGER;
	@Column(name = "KdAccount", columnDefinition = cDefKdAccount)
	private Integer kdAccount;

	private static final String cDefKdJenisRekanan = INTEGER;
	@Column(name = "KdJenisRekanan", columnDefinition = cDefKdJenisRekanan)
	@NotNull(message = "rekanan.kdjenisrekanan.notnull")
	private Integer kdJenisRekanan;

	private static final String cDefKdPegawai = VARCHAR + AWAL_KURUNG + 5 + AKHIR_KURUNG;
	@Column(name = "KdPegawai", columnDefinition = cDefKdPegawai)
	private String kdPegawai;

	private static final String cDefKdRekananHead = INTEGER;
	@Column(name = "KdRekananHead", columnDefinition = cDefKdRekananHead)
	private Integer kdRekananHead;

	private static final String cDefKdDepartemen = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "KdDepartemen", columnDefinition = cDefKdDepartemen)
	@NotNull(message = "rekanan.kddepartemen.notnull")
	private String kdDepartemen;

	private static final String cDefTglDaftar = INTEGER;
	@Column(name = "TglDaftar", columnDefinition = cDefTglDaftar)
	@NotNull(message = "rekanan.tgldaftar.notnull")
	private Long tglDaftar;

	private static final String cDefKodeExternal = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "KodeExternal", columnDefinition = cDefKodeExternal)
	private String kodeExternal;

	private static final String cDefNamaExternal = VARCHAR + AWAL_KURUNG + 80 + AKHIR_KURUNG;
	@Column(name = "NamaExternal", columnDefinition = cDefNamaExternal)
	private String namaExternal;

	private static final String cDefTglAwal = INTEGER;
	@Column(name = "TglAwal", columnDefinition = cDefTglAwal)
	private Long tglAwal;

	private static final String cDefTglAkhir = INTEGER;
	@Column(name = "TglAkhir", columnDefinition = cDefTglAkhir)
	private Long tglAkhir;

	private static final String cDefGambarLogo = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG; 
	@Column(name = "GambarLogo", columnDefinition = cDefGambarLogo)
	private String gambarLogo;

	private static final String cDefStatusEnabled = INTEGER;
	@Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
	@NotNull(message = "rekanan.statusenabled.notnull")
	@Type(type = "org.hibernate.type.NumericBooleanType") private Boolean statusEnabled;

	private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG;
	@Column(name = "NoRec", columnDefinition = cDefNoRec)
	@NotNull(message = "rekanan.norec.notnull")
	private String noRec;

	private static final String cDefVersion = INTEGER;
	@Version
	@Column(name = "version", nullable = false, columnDefinition = cDefVersion)
	@NotNull(message = "rekanan.version.notnull")
	private Integer version;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdDepartemen", referencedColumnName = "KdDepartemen", insertable = false, updatable = false), })
	//@ForeignKey(name="none")
	private Departemen departemen;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdRekananHead", referencedColumnName = "KdRekanan", insertable = false, updatable = false), })
	//@ForeignKey(name="none")
	private Rekanan rekananHead;
}
