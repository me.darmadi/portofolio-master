package com.portofolio.entity;


import org.hibernate.annotations.ForeignKey;
import static com.portofolio.base.adapter.DatabaseAdapter.Constant.*;

import javax.persistence.*;
import javax.validation.constraints.*;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.portofolio.entity.vo.ZodiakId;

import org.hibernate.annotations.Type;



@Entity
@Table(name = "Zodiak_M",indexes = {@Index(name = "Zodiak_M_Index",  columnList="statusEnabled", unique = false) })
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Zodiak {
    
    @EmbeddedId
    private ZodiakId id ;
    
    private static final String cDefNamaZodiak = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG ;
    @Column(name = "NamaZodiak", columnDefinition = cDefNamaZodiak)
    @NotNull(message = "zodiak.namazodiak.notnull")
    private String namaZodiak;
    
    private static final String cDefReportDisplay = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG ;
    @Column(name = "ReportDisplay", columnDefinition = cDefReportDisplay)
    @NotNull(message = "zodiak.reportdisplay.notnull")
    private String reportDisplay;
    
    private static final String cDefNoUrut =INTEGER;
    @Column(name = "NoUrut", columnDefinition = cDefNoUrut)
    @NotNull(message = "zodiak.nourut.notnull")
    private Integer noUrut;
    
    private static final String cDefJamLahirAwal = VARCHAR + AWAL_KURUNG + 5 + AKHIR_KURUNG ;
    @Column(name = "JamLahirAwal", columnDefinition = cDefJamLahirAwal)
    private String jamLahirAwal;
    
    private static final String cDefJamLahirAkhir = VARCHAR + AWAL_KURUNG + 5 + AKHIR_KURUNG ;
    @Column(name = "JamLahirAkhir", columnDefinition = cDefJamLahirAkhir)
    private String jamLahirAkhir;
         
    private static final String cDefKodeExternal = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG ;
    @Column(name = "KodeExternal", columnDefinition = cDefKodeExternal)
    private String kodeExternal;
    
    private static final String cDefNamaExternal = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG ;
    @Column(name = "NamaExternal", columnDefinition = cDefNamaExternal)
    private String namaExternal;
         
    private static final String cDefStatusEnabled =INTEGER;
    @Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
    @NotNull(message = "zodiak.statusenabled.notnull")
    @Type(type = "org.hibernate.type.NumericBooleanType") private Boolean statusEnabled;
    
    private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG ;
    @Column(name = "NoRec", columnDefinition = cDefNoRec)
    @NotNull(message = "zodiak.norec.notnull")
    private String noRec;
    
    private static final String cDefVersion =INTEGER;
    @Version
    @Column(name = "version", nullable = false,  columnDefinition = cDefVersion)
    @NotNull(message = "zodiak.version.notnull")
    private Integer version;
    
 
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
          @JoinColumn(name="KdNegara", referencedColumnName="KdNegara",insertable=false, updatable=false),
    })
    //@ForeignKey(name="none")
     private Negara negara;
     
}
