package com.portofolio.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.portofolio.entity.vo.MataUangId;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.Type;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonIgnore;

import static com.portofolio.base.adapter.DatabaseAdapter.Constant.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
@Entity
@Table(name = "MataUang_M")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor  @JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class MataUang{
	@EmbeddedId
	private MataUangId id;

	private static final String cDefCurrentKursToIDR = DECIMAL;
	@Column(name = "CurrentKursToIDR", columnDefinition = cDefCurrentKursToIDR)
	@NotNull(message = "matauang.currentkurstoidr.notnull")
	private java.math.BigDecimal currentKursToIDR;
 
	private static final String cDefMataUang = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "MataUang", columnDefinition = cDefMataUang)
	@NotNull(message = "matauang.matauang.notnull")
	private String namaMataUang;
	
	private static final String cDefReportDisplay = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "ReportDisplay", columnDefinition = cDefReportDisplay)
	@NotNull(message = "matauang.reportdisplay.notnull")
	private String reportDisplay;

	private static final String cDefKodeExternal = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "KodeExternal", columnDefinition = cDefKodeExternal)
	private String kodeExternal;

	private static final String cDefNamaExternal = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "NamaExternal", columnDefinition = cDefNamaExternal)
	private String namaExternal;

	private static final String cDefStatusEnabled = INTEGER;
	@Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
	@NotNull(message = "matauang.statusenabled.notnull")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean statusEnabled;

	private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG;
	@Column(name = "NoRec", columnDefinition = cDefNoRec)
	@NotNull(message = "matauang.norec.notnull")
	private String noRec;

	private static final String cDefVersion = INTEGER;
	@Version
	@Column(name = "version", nullable = false, columnDefinition = cDefVersion)
	@NotNull(message = "matauang.version.notnull")
	private Integer version;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	//@ForeignKey(name="none")
	@JoinColumns({
			@JoinColumn(name = "kdNegara", referencedColumnName = "kdNegara", insertable = false, updatable = false), }) 
	private Negara negara;
}


