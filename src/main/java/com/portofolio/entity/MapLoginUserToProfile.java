package com.portofolio.entity;


import org.hibernate.annotations.ForeignKey;
import static com.portofolio.base.adapter.DatabaseAdapter.Constant.*;

import javax.persistence.*;
import javax.validation.constraints.*;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.portofolio.entity.Profile;
import com.portofolio.entity.vo.MapLoginUserToProfileId;

import org.hibernate.annotations.Type;



@Entity
@Table(name = "MapLoginUserToProfile_S",indexes = {@Index(name = "MapLoginUserToProfile_S_Index",  columnList="statusEnabled", unique = false) })
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class MapLoginUserToProfile {
    
    @EmbeddedId
    private MapLoginUserToProfileId id ;
    
    private static final String cDefStatusEnabled =INTEGER;
    @Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
    @NotNull(message = "maploginusertoprofile.statusenabled.notnull")
    @Type(type = "org.hibernate.type.NumericBooleanType") private Boolean statusEnabled;
    
    private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG ;
    @Column(name = "NoRec", columnDefinition = cDefNoRec)
    @NotNull(message = "maploginusertoprofile.norec.notnull")
    private String noRec;
    
    private static final String cDefVersion =INTEGER;
    @Version
    @Column(name = "version", nullable = false,  columnDefinition = cDefVersion)
    @NotNull(message = "maploginusertoprofile.version.notnull")
    private Integer version;
    

    private static final String cDefKdBahasa =INTEGER;
    @Column(name = "KdBahasa", columnDefinition = cDefKdBahasa)
    private Integer kdBahasa;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
    })
    //@ForeignKey(name="none")
     private Profile profile;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdModulAplikasi", referencedColumnName="KdModulAplikasi",insertable=false, updatable=false),
    })
    //@ForeignKey(name="none")
     private ModulAplikasi modulAplikasi;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdRuangan", referencedColumnName="KdRuangan",insertable=false, updatable=false),
    })
    //@ForeignKey(name="none")
     private Ruangan ruangan;
    
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdUser", referencedColumnName = "KdUser", insertable = false, updatable = false), })
	//@ForeignKey(name="none")
	private LoginUser loginUser;

}
