package com.portofolio.entity;

import org.hibernate.annotations.ForeignKey;
import static com.portofolio.base.adapter.DatabaseAdapter.Constant.*;

import javax.persistence.*;
import javax.validation.constraints.*;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.portofolio.entity.vo.DesaKelurahanId;

import org.hibernate.annotations.Type;



@Entity
@Table(name = "DesaKelurahan_M",indexes = {@Index(name = "DesaKelurahan_M_Index",  columnList="statusEnabled", unique = false) })
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class DesaKelurahan {
    
    @EmbeddedId
    private DesaKelurahanId id ;
    
    private static final String cDefNamaDesaKelurahan = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG ;
    @Column(name = "NamaDesaKelurahan", columnDefinition = cDefNamaDesaKelurahan)
    @NotNull(message = "desakelurahan.namadesakelurahan.notnull")
    private String namaDesaKelurahan;
    
    private static final String cDefReportDisplay = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG ;
    @Column(name = "ReportDisplay", columnDefinition = cDefReportDisplay)
    @NotNull(message = "desakelurahan.reportdisplay.notnull")
    private String reportDisplay;
    
    private static final String cDefKodePos = VARCHAR + AWAL_KURUNG + 10 + AKHIR_KURUNG ;
    @Column(name = "KodePos", columnDefinition = cDefKodePos)
    private String kodePos;
    
    private static final String cDefKdPropinsi =INTEGER;
    @Column(name = "KdPropinsi", columnDefinition = cDefKdPropinsi)
    @NotNull(message = "desakelurahan.kdpropinsi.notnull")
    private Integer kdPropinsi;
    
    private static final String cDefKdKotaKabupaten =INTEGER;
    @Column(name = "KdKotaKabupaten", columnDefinition = cDefKdKotaKabupaten)
    @NotNull(message = "desakelurahan.kdkotakabupaten.notnull")
    private Integer kdKotaKabupaten;
    
    private static final String cDefKdKecamatan =INTEGER;
    @Column(name = "KdKecamatan", columnDefinition = cDefKdKecamatan)
    @NotNull(message = "desakelurahan.kdkecamatan.notnull")
    private Integer kdKecamatan;
    
    private static final String cDefKodeExternal = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG ;
    @Column(name = "KodeExternal", columnDefinition = cDefKodeExternal)
    private String kodeExternal;
    
    private static final String cDefNamaExternal = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG ;
    @Column(name = "NamaExternal", columnDefinition = cDefNamaExternal)
    private String namaExternal; 
    
    private static final String cDefStatusEnabled =INTEGER;
    @Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
    @NotNull(message = "desakelurahan.statusenabled.notnull")
    @Type(type = "org.hibernate.type.NumericBooleanType") private Boolean statusEnabled;
    
    private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG ;
    @Column(name = "NoRec", columnDefinition = cDefNoRec)
    @NotNull(message = "desakelurahan.norec.notnull")
    private String noRec;
    
    private static final String cDefVersion =INTEGER;
    @Version
    @Column(name = "version", nullable = false,  columnDefinition = cDefVersion)
    @NotNull(message = "desakelurahan.version.notnull")
    private Integer version;
    
 
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdNegara", referencedColumnName="KdNegara",insertable=false, updatable=false),
        @JoinColumn(name="KdPropinsi", referencedColumnName="KdPropinsi",insertable=false, updatable=false),
    })
    //@ForeignKey(name="none")
     private Propinsi propinsi;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdNegara", referencedColumnName="KdNegara",insertable=false, updatable=false),
        @JoinColumn(name="KdKotaKabupaten", referencedColumnName="KdKotaKabupaten",insertable=false, updatable=false),
    })
    //@ForeignKey(name="none")
     private KotaKabupaten kotaKabupaten;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdNegara", referencedColumnName="KdNegara",insertable=false, updatable=false),
        @JoinColumn(name="KdKecamatan", referencedColumnName="KdKecamatan",insertable=false, updatable=false),
    })
    //@ForeignKey(name="none")
     private Kecamatan kecamatan;
     

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	//@ForeignKey(name="none")
	@JoinColumns({
			@JoinColumn(name = "kdNegara", referencedColumnName = "kdNegara", insertable = false, updatable = false), }) 
	private Negara negara;
	
}
