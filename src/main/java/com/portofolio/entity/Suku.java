package com.portofolio.entity;

import static com.portofolio.base.adapter.DatabaseAdapter.Constant.*;

import javax.persistence.*;
import javax.validation.constraints.*;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.portofolio.entity.vo.SukuId;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.Type;



@Entity
@Table(name = "Suku_M",indexes = {@Index(name = "Suku_M_Index",  columnList="statusEnabled", unique = false) })
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Suku {
    
    @EmbeddedId
    private SukuId id ;
    
    private static final String cDefSuku = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG ;
    @Column(name = "Suku", columnDefinition = cDefSuku)
    @NotNull(message = "suku.suku.notnull")
    private String namaSuku;
    
    private static final String cDefReportDisplay = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG ;
    @Column(name = "ReportDisplay", columnDefinition = cDefReportDisplay)
    @NotNull(message = "suku.reportdisplay.notnull")
    private String reportDisplay;
    
    private static final String cDefKodeExternal = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG ;
    @Column(name = "KodeExternal", columnDefinition = cDefKodeExternal)
    private String kodeExternal;
    
    private static final String cDefNamaExternal = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG ;
    @Column(name = "NamaExternal", columnDefinition = cDefNamaExternal)
    private String namaExternal;
     
    private static final String cDefStatusEnabled =INTEGER;
    @Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
    @NotNull(message = "suku.statusenabled.notnull")
    @Type(type = "org.hibernate.type.NumericBooleanType") private Boolean statusEnabled;
    
    private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG ;
    @Column(name = "NoRec", columnDefinition = cDefNoRec)
    @NotNull(message = "suku.norec.notnull")
    private String noRec;
    
    private static final String cDefVersion =INTEGER;
    @Version
    @Column(name = "version", nullable = false,  columnDefinition = cDefVersion)
    @NotNull(message = "suku.version.notnull")
    private Integer version;
    

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	//@ForeignKey(name="none")
	@JoinColumns({
			@JoinColumn(name = "kdNegara", referencedColumnName = "kdNegara", insertable = false, updatable = false), }) 
	private Negara negara;
	
}
