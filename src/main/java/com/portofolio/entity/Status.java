/* --------------------------------------------------------
 Generated by Lukman Hakim using sql2java (http://sql2java.sourceforge.net/ )
 jdbc driver used at code generation time: net.sourceforge.jtds.jdbc.Driver
 
 Author : Lukman Hakim (lukman.uki@gmail.com) 
 Date Created : 25/08/2017
 --------------------------------------------------------
 */
package com.portofolio.entity;

import static com.portofolio.base.adapter.DatabaseAdapter.Constant.*;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.portofolio.entity.Departemen;
import com.portofolio.entity.vo.StatusId;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Entity
@Table(name = "Status_M")
@Getter
@Setter
@AllArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Status {
	@EmbeddedId
	private StatusId id;

	private static final String cDefKdDepartemen = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "KdDepartemen", columnDefinition = cDefKdDepartemen)
	@NotNull(message = "status.kddepartemen.notnull")
	private String kdDepartemen;

	private static final String cDefKdStatusHead = INTEGER;
	@Column(name = "KdStatusHead", columnDefinition = cDefKdStatusHead)
	private Integer kdStatusHead;

	private static final String cDefNamaStatus = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "NamaStatus", columnDefinition = cDefNamaStatus)
	@NotNull(message = "status.namastatus.notnull")
	private String namaStatus;
	
	private static final String cDefReportDisplay = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "ReportDisplay", columnDefinition = cDefReportDisplay)
	@NotNull(message = "status.reportdisplay.notnull")
	private String reportDisplay;

	private static final String cDefKodeExternal = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "KodeExternal", columnDefinition = cDefKodeExternal)
	private String kodeExternal;

	private static final String cDefNamaExternal = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "NamaExternal", columnDefinition = cDefNamaExternal)
	private String namaExternal;

	private static final String cDefStatusEnabled = INTEGER;
	@Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
	@NotNull(message = "pegawai.statusenabled.notnull")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean statusEnabled;

	private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG;
	@Column(name = "NoRec", columnDefinition = cDefNoRec)
	@NotNull(message = "status.norec.notnull")
	private String noRec;

	private static final String cDefVersion = INTEGER;
	@Version
	@Column(name = "version", nullable = false, columnDefinition = cDefVersion)
	@NotNull(message = "status.version.notnull")
	private Integer version;
	

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdDepartemen", referencedColumnName = "KdDepartemen", insertable = false, updatable = false), })
	//@ForeignKey(name="none")
	private Departemen departemen;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdStatusHead", referencedColumnName = "KdStatus", insertable = false, updatable = false), })
	//@ForeignKey(name="none")
	private Status statusHead;
	
	 // tambahan untuk recursive
    public Status() {
		this.children = new ArrayList<Status>();
	}
    
    @Transient
    List<Status> children;
    
	public void addChild(Status child) {
		if (!this.children.contains(child) && child != null)
			this.children.add(child);
	}

}


