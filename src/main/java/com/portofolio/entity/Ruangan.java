package com.portofolio.entity;


import org.hibernate.annotations.ForeignKey;
import static com.portofolio.base.adapter.DatabaseAdapter.Constant.*;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.*;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.portofolio.entity.Departemen;
import com.portofolio.entity.Profile;
import com.portofolio.entity.vo.RuanganId;

import org.hibernate.annotations.Type;


@Entity
@Table(name = "Ruangan_M", indexes = { @Index(name = "Ruangan_M_Index", columnList = "statusEnabled", unique = false) })
@Getter
@Setter
@AllArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Ruangan {

	@EmbeddedId
	private RuanganId id;
	
	private static final String cDefKdRuanganHead = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "KdRuanganHead", columnDefinition = cDefKdRuanganHead)
	private String kdRuanganHead;

	private static final String cDefNamaRuangan = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG;
	@Column(name = "NamaRuangan", columnDefinition = cDefNamaRuangan)
	@NotNull(message = "ruangan.namaruangan.notnull")
	private String namaRuangan;

	private static final String cDefReportDisplay = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG;
	@Column(name = "ReportDisplay", columnDefinition = cDefReportDisplay)
	@NotNull(message = "ruangan.reportdisplay.notnull")
	private String reportDisplay;

	private static final String cDefNoRuangan = VARCHAR + AWAL_KURUNG + 4 + AKHIR_KURUNG;
	@Column(name = "NoRuangan", columnDefinition = cDefNoRuangan)
	private String noRuangan;

	private static final String cDefKdDepartemen = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "KdDepartemen", columnDefinition = cDefKdDepartemen)
	@NotNull(message = "ruangan.kddepartemen.notnull")
	private String kdDepartemen;

	private static final String cDefKdAlamat = INTEGER;
	@Column(name = "KdAlamat", columnDefinition = cDefKdAlamat)
	private Integer kdAlamat;

	private static final String cDefKdPegawai = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG ;
	@Column(name = "KdPegawaiKepala", columnDefinition = cDefKdPegawai)
	private String kdPegawaiKepala;

	private static final String cDefKodeExternal = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "KodeExternal", columnDefinition = cDefKodeExternal)
	private String kodeExternal;

	private static final String cDefNamaExternal = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG;
	@Column(name = "NamaExternal", columnDefinition = cDefNamaExternal)
	private String namaExternal;

	private static final String cDefStatusEnabled = INTEGER;
	@Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
	@NotNull(message = "ruangan.statusenabled.notnull")
	@Type(type = "org.hibernate.type.NumericBooleanType") private Boolean statusEnabled;

	private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG;
	@Column(name = "NoRec", columnDefinition = cDefNoRec)
	@NotNull(message = "ruangan.norec.notnull")
	private String noRec;

	private static final String cDefVersion = INTEGER;
	@Version
	@Column(name = "version", nullable = false, columnDefinition = cDefVersion)
	@NotNull(message = "ruangan.version.notnull")
	private Integer version;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false), })
	//@ForeignKey(name="none")
	private Profile profile;


	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdDepartemen", referencedColumnName = "KdDepartemen", insertable = false, updatable = false), })
	//@ForeignKey(name="none")
	private Departemen departemen;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdRuanganHead", referencedColumnName = "KdRuangan", insertable = false, updatable = false), })
	//@ForeignKey(name="none")
	private Ruangan lokasiKerja;


	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdPegawaiKepala", referencedColumnName = "KdPegawai", insertable = false, updatable = false), })
	//@ForeignKey(name="none")
	private Pegawai pegawai;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdAlamat", referencedColumnName = "KdAlamat", insertable = false, updatable = false), })
	//@ForeignKey(name="none")
	private Alamat alamat;
	
	 // tambahan untuk recursive
    public Ruangan() {
		this.children = new ArrayList<Ruangan>();
	}
    
    @Transient
    List<Ruangan> children;
    
	public void addChild(Ruangan child) {
		if (!this.children.contains(child) && child != null)
			this.children.add(child);
	}

}
