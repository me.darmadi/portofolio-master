package com.portofolio.entity;


import org.hibernate.annotations.ForeignKey;
import static com.portofolio.base.adapter.DatabaseAdapter.Constant.*;

import javax.persistence.*;
import javax.validation.constraints.*;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.portofolio.entity.Departemen;
import com.portofolio.entity.Profile;
import com.portofolio.entity.vo.PangkatId;

import org.hibernate.annotations.Type;



@Entity
@Table(name = "Pangkat_M",indexes = {@Index(name = "Pangkat_M_Index",  columnList="statusEnabled", unique = false) })
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Pangkat {
    
    @EmbeddedId
    private PangkatId id ;
    
    private static final String cDefNamaPangkat = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG ;
    @Column(name = "NamaPangkat", columnDefinition = cDefNamaPangkat)
    @NotNull(message = "pangkat.namapangkat.notnull")
    private String namaPangkat;
    
    private static final String cDefReportDisplay = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG ;
    @Column(name = "ReportDisplay", columnDefinition = cDefReportDisplay)
    @NotNull(message = "pangkat.reportdisplay.notnull")
    private String reportDisplay;
    
    private static final String cDefNoUrut =INTEGER;
    @Column(name = "NoUrut", columnDefinition = cDefNoUrut)
    @NotNull(message = "pangkat.nourut.notnull")
    private Integer noUrut;


    private static final String cDefKdPangkatHead =INTEGER;
    @Column(name = "KdPangkatHead", columnDefinition = cDefKdPangkatHead)
    private Integer kdPangkatHead;
    
    private static final String cDefKodeExternal = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG ;
    @Column(name = "KodeExternal", columnDefinition = cDefKodeExternal)
    private String kodeExternal;
    
    private static final String cDefNamaExternal = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG ;
    @Column(name = "NamaExternal", columnDefinition = cDefNamaExternal)
    private String namaExternal;
    
    
    private static final String cDefStatusEnabled =INTEGER;
    @Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
    @NotNull(message = "pangkat.statusenabled.notnull")
    @Type(type = "org.hibernate.type.NumericBooleanType") private Boolean statusEnabled;
    
    private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG ;
    @Column(name = "NoRec", columnDefinition = cDefNoRec)
    @NotNull(message = "pangkat.norec.notnull")
    private String noRec;
    
    private static final String cDefVersion =INTEGER;
    @Version
    @Column(name = "version", nullable = false,  columnDefinition = cDefVersion)
    @NotNull(message = "pangkat.version.notnull")
    private Integer version;

    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
    })
    //@ForeignKey(name="none")
     private Profile profile;

    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdPangkatHead", referencedColumnName="KdPangkat",insertable=false, updatable=false),
    })
    //@ForeignKey(name="none")
     private Pangkat pangkatHead;
    
}
