package com.portofolio.entity;

import static com.portofolio.base.adapter.DatabaseAdapter.Constant.AKHIR_KURUNG;
import static com.portofolio.base.adapter.DatabaseAdapter.Constant.AWAL_KURUNG;
import static com.portofolio.base.adapter.DatabaseAdapter.Constant.FLOAT;
import static com.portofolio.base.adapter.DatabaseAdapter.Constant.INTEGER;
import static com.portofolio.base.adapter.DatabaseAdapter.Constant.INTEGER;
import static com.portofolio.base.adapter.DatabaseAdapter.Constant.VARCHAR;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Type;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "KelompokTransaksi_M", indexes = {
		@Index(name = "KelompokTransaksi_M_Index", columnList = "statusEnabled", unique = false) })
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class KelompokTransaksi {

	private static final String cDefKdKelompokTransaksi = INTEGER;
	@Column(name = "KdKelompokTransaksi", columnDefinition = cDefKdKelompokTransaksi)
	@NotNull(message = "kelompoktransaksiid.kdkelompoktransaksi.notnull")
	@Id
	private Integer kdKelompokTransaksi;

	private static final String cDefKelompokTransaksi = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG;
	@Column(name = "KelompokTransaksi", columnDefinition = cDefKelompokTransaksi)
	@NotNull(message = "kelompoktransaksi.kelompoktransaksi.notnull")
	private String namaKelompokTransaksi;

	private static final String cDefReportDisplay = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG;
	@Column(name = "ReportDisplay", columnDefinition = cDefReportDisplay)
	@NotNull(message = "kelompoktransaksi.reportdisplay.notnull")
	private String reportDisplay;

	private static final String cDefKodeExternal = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "KodeExternal", columnDefinition = cDefKodeExternal)
	private String kodeExternal;

	private static final String cDefNamaExternal = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG;
	@Column(name = "NamaExternal", columnDefinition = cDefNamaExternal)
	private String namaExternal;

	private static final String cDefStatusEnabled = INTEGER;
	@Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
	@NotNull(message = "kelompoktransaksi.statusenabled.notnull")
	@Type(type = "org.hibernate.type.NumericBooleanType") private Boolean statusEnabled;

	private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG;
	@Column(name = "NoRec", columnDefinition = cDefNoRec)
	@NotNull(message = "kelompoktransaksi.norec.notnull")
	private String noRec;
	
	private static final String cDefisCostInOut =INTEGER;
    @Column(name = "isCostInOut", columnDefinition = cDefisCostInOut)
    private Boolean isCostInOut;

	private static final String cDefVersion = INTEGER;
	@Version
	@Column(name = "version", nullable = false, columnDefinition = cDefVersion)
	@NotNull(message = "kelompoktransaksi.version.notnull")
	private Integer version;

}
