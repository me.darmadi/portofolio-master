package com.portofolio.entity;


import org.hibernate.annotations.ForeignKey;
import static com.portofolio.base.adapter.DatabaseAdapter.Constant.*;

import javax.persistence.*;
import javax.validation.constraints.*;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.portofolio.entity.vo.KotaKabupatenId;

import org.hibernate.annotations.Type;



@Entity
@Table(name = "KotaKabupaten_M",indexes = {@Index(name = "KotaKabupaten_M_Index",  columnList="statusEnabled", unique = false) })
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class KotaKabupaten {
    
    @EmbeddedId
    private KotaKabupatenId id ;
    
    private static final String cDefNamaKotaKabupaten = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG ;
    @Column(name = "NamaKotaKabupaten", columnDefinition = cDefNamaKotaKabupaten)
    @NotNull(message = "kotakabupaten.namakotakabupaten.notnull")
    private String namaKotaKabupaten;
    
    private static final String cDefReportDisplay = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG ;
    @Column(name = "ReportDisplay", columnDefinition = cDefReportDisplay)
    @NotNull(message = "kotakabupaten.reportdisplay.notnull")
    private String reportDisplay;
    
    private static final String cDefKdPropinsi =INTEGER;
    @Column(name = "KdPropinsi", columnDefinition = cDefKdPropinsi)
    @NotNull(message = "kotakabupaten.kdpropinsi.notnull")
    private Integer kdPropinsi;
    
    private static final String cDefKodeExternal = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG ;
    @Column(name = "KodeExternal", columnDefinition = cDefKodeExternal)
    private String kodeExternal;
    
    private static final String cDefNamaExternal = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG ;
    @Column(name = "NamaExternal", columnDefinition = cDefNamaExternal)
    private String namaExternal;
     
    private static final String cDefStatusEnabled =INTEGER;
    @Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
    @NotNull(message = "kotakabupaten.statusenabled.notnull")
    @Type(type = "org.hibernate.type.NumericBooleanType") private Boolean statusEnabled;
    
    private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG ;
    @Column(name = "NoRec", columnDefinition = cDefNoRec)
    @NotNull(message = "kotakabupaten.norec.notnull")
    private String noRec;
    
    private static final String cDefVersion =INTEGER;
    @Version
    @Column(name = "version", nullable = false,  columnDefinition = cDefVersion)
    @NotNull(message = "kotakabupaten.version.notnull")
    private Integer version;
        
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	//@ForeignKey(name="none")
	@JoinColumns({
			@JoinColumn(name = "kdNegara", referencedColumnName = "kdNegara", insertable = false, updatable = false), }) 
	private Negara negara;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdNegara", referencedColumnName="KdNegara",insertable=false, updatable=false),
        @JoinColumn(name="KdPropinsi", referencedColumnName="KdPropinsi",insertable=false, updatable=false),
    })
    //@ForeignKey(name="none")
     private Propinsi objPropinsi;
    
    
}
