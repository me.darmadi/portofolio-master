package com.portofolio.entity;


import org.hibernate.annotations.ForeignKey;
import static com.portofolio.base.adapter.DatabaseAdapter.Constant.*;

import javax.persistence.*;
import javax.validation.constraints.*;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.portofolio.entity.vo.ZodiakUnsurSifatRangeId;

import org.hibernate.annotations.Type;




@Entity
@Table(name = "ZodiakUnsurSifatRange_M",indexes = {@Index(name = "ZodiakUnsurSifatRange_M_Index",  columnList="statusEnabled", unique = false) })
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class ZodiakUnsurSifatRange {
    
    @EmbeddedId
    private ZodiakUnsurSifatRangeId id ;
    
    private static final String cDefStatusEnabled =INTEGER;
    @Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
    @NotNull(message = "zodiakunsursifatrange.statusenabled.notnull")
    @Type(type = "org.hibernate.type.NumericBooleanType") private Boolean statusEnabled;
    
    private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG ;
    @Column(name = "NoRec", columnDefinition = cDefNoRec)
    @NotNull(message = "zodiakunsursifatrange.norec.notnull")
    private String noRec;
    
    private static final String cDefVersion =INTEGER;
    @Version
    @Column(name = "version", nullable = false,  columnDefinition = cDefVersion)
    @NotNull(message = "zodiakunsursifatrange.version.notnull")
    private Integer version;
    
    @ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	//@ForeignKey(name="none")
	@JoinColumns({
			@JoinColumn(name = "kdNegara", referencedColumnName = "kdNegara", insertable = false, updatable = false), }) 
	private Negara negara;
	
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="kdNegara", referencedColumnName="kdNegara",insertable=false, updatable=false),
        @JoinColumn(name="KdZodiak", referencedColumnName="KdZodiak",insertable=false, updatable=false),
    })
    //@ForeignKey(name="none")
     private Zodiak zodiak;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="kdNegara", referencedColumnName="kdNegara",insertable=false, updatable=false),
        @JoinColumn(name="KdZodiakUnsur", referencedColumnName="KdZodiakUnsur",insertable=false, updatable=false),
    })
    //@ForeignKey(name="none")
     private ZodiakUnsur zodiakUnsur;
}
