package com.portofolio.entity;

import static com.portofolio.base.adapter.DatabaseAdapter.Constant.*;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.portofolio.entity.Departemen;
import com.portofolio.entity.LevelTingkat;
import com.portofolio.entity.vo.PegawaiId;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "Pegawai_M", 
	indexes = { 
				@Index(columnList = "StatusEnabled", name = "statusEnabledPegawai_M"), 
				@Index(columnList = "namaLengkap", name = "namaLengkapPegawai_M"),  /// GANTI INDEX KE create Index lowercase function 
				@Index(columnList = "namaKeluarga", name = "namaKeluargaPegawai_M"), /// GANTI INDEX KE create Index lowercase function 
				@Index(columnList = "nPWP", name = "nPWPPegawai_M"), 
				
			}
)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Pegawai  {
	@EmbeddedId
	private PegawaiId id;

	private static final String cDefFingerPrintID = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "FingerPrintID", columnDefinition = cDefFingerPrintID)
	private String fingerPrintID;

	private static final String cDefKdAgama = INTEGER;
	@Column(name = "KdAgama", columnDefinition = cDefKdAgama)
	private Integer kdAgama;

	private static final String cDefKdDepartemen = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "KdDepartemen", columnDefinition = cDefKdDepartemen)
	@NotNull(message = "pegawai.kddepartemen.notnull")
	private String kdDepartemen;

	private static final String cDefKdGolonganDarah = INTEGER;
	@Column(name = "KdGolonganDarah", columnDefinition = cDefKdGolonganDarah)
	private Integer kdGolonganDarah;

	private static final String cDefKdHubunganKeluarga = INTEGER;
	@Column(name = "KdHubunganKeluarga", columnDefinition = cDefKdHubunganKeluarga)
	private Integer kdHubunganKeluarga;

	private static final String cDefKdJabatan = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "KdJabatan", columnDefinition = cDefKdJabatan)
	private String kdJabatan;

	private static final String cDefKdJenisKelamin = INTEGER;
	@Column(name = "KdJenisKelamin", columnDefinition = cDefKdJenisKelamin)
	@NotNull(message = "pegawai.kdjeniskelamin.notnull")
	private Integer kdJenisKelamin;

	private static final String cDefKdJenisPegawai = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "KdJenisPegawai", columnDefinition = cDefKdJenisPegawai)
	private String kdJenisPegawai;

	private static final String cDefKdJenisPegawaiLamar = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "KdJenisPegawaiLamar", columnDefinition = cDefKdJenisPegawaiLamar)
	private String kdJenisPegawaiLamar;

	private static final String cDefKdKategoryPegawai = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "KdKategoryPegawai", columnDefinition = cDefKdKategoryPegawai)
	private String kdKategoryPegawai;

	private static final String cDefKdKualifikasiJurusan = VARCHAR + AWAL_KURUNG + 4 + AKHIR_KURUNG;
	@Column(name = "KdKualifikasiJurusan", columnDefinition = cDefKdKualifikasiJurusan)
	private String kdKualifikasiJurusan;

	private static final String cDefKdNegara = INTEGER;
	@Column(name = "KdNegara", columnDefinition = cDefKdNegara)
	private Integer kdNegara;

	private static final String cDefKdGolonganPegawai = INTEGER;
	@Column(name = "KdGolonganPegawai", columnDefinition = cDefKdGolonganPegawai)
	private Integer kdGolonganPegawai;

	private static final String cDefKdPangkat = INTEGER;
	@Column(name = "KdPangkat", columnDefinition = cDefKdPangkat)
	private Integer kdPangkat;

	private static final String cDefKdPegawaiHead = VARCHAR + AWAL_KURUNG + 5 + AKHIR_KURUNG;
	@Column(name = "KdPegawaiHead", columnDefinition = cDefKdPegawaiHead)
	private String kdPegawaiHead;
	
	private static final String cDefKdPegawaiKK = VARCHAR + AWAL_KURUNG + 5 + AKHIR_KURUNG;
	@Column(name = "KdPegawaiKK", columnDefinition = cDefKdPegawaiKK)
	private String kdPegawaiKK;

	private static final String cDefKdPendidikanTerakhir = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "KdPendidikanTerakhir", columnDefinition = cDefKdPendidikanTerakhir)
	private String kdPendidikanTerakhir;

	private static final String cDefKdRuanganKerja = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "KdRuanganKerja", columnDefinition = cDefKdRuanganKerja)
	private String kdRuanganKerja;

	private static final String cDefKdStatusPegawai = INTEGER;
	@Column(name = "KdStatusPegawai", columnDefinition = cDefKdStatusPegawai)
	private Integer kdStatusPegawai;

	private static final String cDefKdStatusPerkawinan = INTEGER;
	@Column(name = "KdStatusPerkawinan", columnDefinition = cDefKdStatusPerkawinan)
	private Integer kdStatusPerkawinan;

	private static final String cDefKdSuku = INTEGER;
	@Column(name = "KdSuku", columnDefinition = cDefKdSuku)
	private Integer kdSuku;

	private static final String cDefKdTitle = INTEGER;
	@Column(name = "KdTitle", columnDefinition = cDefKdTitle)
	private Integer kdTitle;

	private static final String cDefKdTypePegawai = INTEGER;
	@Column(name = "KdTypePegawai", columnDefinition = cDefKdTypePegawai)
	private Integer kdTypePegawai;

	private static final String cDefNIKIntern = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "NIKIntern", columnDefinition = cDefNIKIntern)
	private String nIKIntern;

	private static final String cDefNPWP = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "NPWP", columnDefinition = cDefNPWP)
	private String nPWP;

	private static final String cDefNamaKeluarga = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "NamaKeluarga", columnDefinition = cDefNamaKeluarga)
	private String namaKeluarga;

	private static final String cDefNamaLengkap = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "NamaLengkap", columnDefinition = cDefNamaLengkap)
	@NotNull(message = "pegawai.namalengkap.notnull")
	private String namaLengkap;
    
    private static final String cDefNamaAwal = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "NamaAwal", columnDefinition = cDefNamaAwal)
	private String namaAwal;

	private static final String cDefNamaTengah = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "NamaTengah", columnDefinition = cDefNamaTengah)
	private String namaTengah;

	private static final String cDefNamaAkhir = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "NamaAkhir", columnDefinition = cDefNamaAkhir)
	private String namaAkhir;
    
	private static final String cDefNamaPanggilan = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "NamaPanggilan", columnDefinition = cDefNamaPanggilan)
	private String namaPanggilan;


	private static final String cDefPhotoDiri = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "PhotoDiri", columnDefinition = cDefPhotoDiri)
	private String photoDiri;

	private static final String cDefQtyAnak = INTEGER;
	@Column(name = "QtyAnak", columnDefinition = cDefQtyAnak)
	private Integer qtyAnak;

	private static final String cDefQtyTanggungan = INTEGER;
	@Column(name = "QtyTanggungan", columnDefinition = cDefQtyTanggungan)
	private Integer qtyTanggungan;

	private static final String cDefStatusRhesus = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "StatusRhesus", columnDefinition = cDefStatusRhesus)
	private String statusRhesus;

	private static final String cDefTempatLahir = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "TempatLahir", columnDefinition = cDefTempatLahir)
	private String tempatLahir;

	private static final String cDefTglDaftar = INTEGER;
	@Column(name = "TglDaftar", columnDefinition = cDefTglDaftar)
	@NotNull(message = "pegawai.tgldaftar.notnull")
	private Long tglDaftar;

	private static final String cDefTglKeluar = INTEGER;
	@Column(name = "TglKeluar", columnDefinition = cDefTglKeluar)
	private Long tglKeluar;

	private static final String cDefTglLahir = INTEGER;
	@Column(name = "TglLahir", columnDefinition = cDefTglLahir)
	@NotNull(message = "pegawai.tgllahir.notnull")
	private Long tglLahir;

	private static final String cDefTglMasuk = INTEGER;
	@Column(name = "TglMasuk", columnDefinition = cDefTglMasuk)
	private Long tglMasuk;
	
	private static final String cDefKdZodiak = INTEGER;
	@Column(name = "KdZodiak", columnDefinition = cDefKdZodiak)
	private Integer kdZodiak;

	private static final String cDefKdZodiakUnsur = INTEGER;
	@Column(name = "KdZodiakUnsur", columnDefinition = cDefKdZodiakUnsur)
	private Integer kdZodiakUnsur;

	private static final String cDefZodiakSifat = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "ZodiakSifat", columnDefinition = cDefZodiakSifat)
	private String zodiakSifat;

	private static final String cDefKdNegaraAsal = INTEGER;
	@Column(name = "KdNegaraAsal", columnDefinition = cDefKdNegaraAsal)
	private Integer kdNegaraAsal;
			
	
	private static final String cDefReportDisplay = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "ReportDisplay", columnDefinition = cDefReportDisplay)
	private String reportDisplay;

	private static final String cDefKodeExternal = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "KodeExternal", columnDefinition = cDefKodeExternal)
	private String kodeExternal;

	private static final String cDefNamaExternal = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "NamaExternal", columnDefinition = cDefNamaExternal)
	private String namaExternal;

	private static final String cDefStatusEnabled = INTEGER;
	@Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
	@NotNull(message = "pegawai.statusenabled.notnull")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean statusEnabled;

	private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG;
	@Column(name = "NoRec", columnDefinition = cDefNoRec)
	@NotNull(message = "negara.norec.notnull")
	private String noRec;

	private static final String cDefVersion = INTEGER;
	@Version
	@Column(name = "version", nullable = false, columnDefinition = cDefVersion)
	@NotNull(message = "negara.version.notnull")
	private Integer version;
	

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	//@ForeignKey(name="none")
	@JoinColumns({
			@JoinColumn(name = "KdNegara", referencedColumnName = "KdNegara", insertable = false, updatable = false),
			@JoinColumn(name = "KdAgama", referencedColumnName = "KdAgama", insertable = false, updatable = false), })
	private Agama agama;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	//@ForeignKey(name="none")
	@JoinColumns({
			@JoinColumn(name = "KdNegara", referencedColumnName = "KdNegara", insertable = false, updatable = false),
			@JoinColumn(name = "kdZodiak", referencedColumnName = "kdZodiak", insertable = false, updatable = false), })
	private Zodiak zodiak;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	//@ForeignKey(name="none")
	@JoinColumns({
			@JoinColumn(name = "KdNegara", referencedColumnName = "KdNegara", insertable = false, updatable = false),
			@JoinColumn(name = "KdZodiakUnsur", referencedColumnName = "KdZodiakUnsur", insertable = false, updatable = false), })
	private ZodiakUnsur zodiakUnsur;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	//@ForeignKey(name="none")
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdDepartemen", referencedColumnName = "KdDepartemen", insertable = false, updatable = false), })
	private Departemen departemen;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	//@ForeignKey(name="none")
	@JoinColumns({
			@JoinColumn(name = "KdNegara", referencedColumnName = "KdNegara", insertable = false, updatable = false),
			@JoinColumn(name = "KdGolonganDarah", referencedColumnName = "KdGolonganDarah", insertable = false, updatable = false), })
	private GolonganDarah golonganDarah;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	//@ForeignKey(name="none")
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdHubunganKeluarga", referencedColumnName = "KdHubunganKeluarga", insertable = false, updatable = false), })
	private HubunganKeluarga hubunganKeluarga;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	//@ForeignKey(name="none")
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdJabatan", referencedColumnName = "KdJabatan", insertable = false, updatable = false), })
	private Jabatan jabatan;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	//@ForeignKey(name="none")
	@JoinColumns({
			@JoinColumn(name = "KdNegara", referencedColumnName = "KdNegara", insertable = false, updatable = false),
			@JoinColumn(name = "KdJenisKelamin", referencedColumnName = "KdJenisKelamin", insertable = false, updatable = false), })
	private JenisKelamin jenisKelamin;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	//@ForeignKey(name="none")
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdJenisPegawai", referencedColumnName = "KdJenisPegawai", insertable = false, updatable = false), })
	private JenisPegawai jenisPegawai;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	//@ForeignKey(name="none")
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdKategoryPegawai", referencedColumnName = "KdKategoryPegawai", insertable = false, updatable = false), })
	private KategoryPegawai kategoryPegawai;

//	@ManyToOne(fetch = FetchType.LAZY)
//	@JsonIgnore
//	@JoinColumns({
//			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
//			@JoinColumn(name = "KdKelompokUser", referencedColumnName = "KdKelompokUser", insertable = false, updatable = false), })
//	
//	private KelompokUser kelompokUser;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	//@ForeignKey(name="none")
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdKualifikasiJurusan", referencedColumnName = "KdKualifikasiJurusan", insertable = false, updatable = false), })
	private KualifikasiJurusan kualifikasiJurusan;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	//@ForeignKey(name="none")
	@JoinColumns({ 
			@JoinColumn(name = "KdNegara", referencedColumnName = "KdNegara", insertable = false, updatable = false), })
	private Negara negara;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	//@ForeignKey(name="none")
	@JoinColumns({ 
			@JoinColumn(name = "kdNegaraAsal", referencedColumnName = "KdNegara", insertable = false, updatable = false), })
	private Negara negaraAsal;


	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	//@ForeignKey(name="none")
	@JoinColumns({
			@JoinColumn(name = "KdNegara", referencedColumnName = "KdNegara", insertable = false, updatable = false),
			@JoinColumn(name = "KdSuku", referencedColumnName = "KdSuku", insertable = false, updatable = false), })
	private Suku suku;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	//@ForeignKey(name="none")
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdTypePegawai", referencedColumnName = "KdTypePegawai", insertable = false, updatable = false), })
	private TypePegawai typePegawai;



	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	//@ForeignKey(name="none")
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdPendidikanTerakhir", referencedColumnName = "KdPendidikan", insertable = false, updatable = false), })
	private Pendidikan pendidikanTerakhir;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	//@ForeignKey(name="none")
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdJenisPegawaiLamar", referencedColumnName = "KdJenisPegawai", insertable = false, updatable = false), })
	private JenisPegawai jenisPegawaiLamar;


	@OneToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	//@ForeignKey(name="none")
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdPegawaiHead", referencedColumnName = "KdPegawai", insertable = false, updatable = false), })
	private Pegawai pegawaiHead;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	//@ForeignKey(name="none")
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdPegawaiKK", referencedColumnName = "KdPegawai", insertable = false, updatable = false), })
	private Pegawai pegawaiKK;
	
//	@OneToOne(fetch = FetchType.LAZY)
//	@JsonIgnore
//	@JoinColumns({
//			@JoinColumn(name = "kdProfileBefore", referencedColumnName = "KdProfile", insertable = false, updatable = false),
//			@JoinColumn(name = "KdPegawaiBefore", referencedColumnName = "KdPegawai", insertable = false, updatable = false), })
//	private Pegawai pegawaiBefore;


	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	//@ForeignKey(name="none")
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdRuanganKerja", referencedColumnName = "kdRuangan", insertable = false, updatable = false), })
	private Ruangan ruanganKerja;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	//@ForeignKey(name="none")
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdTitle", referencedColumnName = "KdTitle", insertable = false, updatable = false), })
	private Title title;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	//@ForeignKey(name="none")
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false, nullable = true),
			@JoinColumn(name = "KdStatusPerkawinan", referencedColumnName = "KdStatus", insertable = false, updatable = false, nullable = true) })
	private Status statusPerkawinan;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	//@ForeignKey(name="none")
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false, nullable = true),
			@JoinColumn(name = "KdStatusPegawai", referencedColumnName = "KdStatus", insertable = false, updatable = false, nullable = true), })
	private Status statusPegawai;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	//@ForeignKey(name="none")
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdPangkat", referencedColumnName = "KdPangkat", insertable = false, updatable = false), })
	private Pangkat pangkat;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	//@ForeignKey(name="none")
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdGolonganPegawai", referencedColumnName = "KdGolonganPegawai", insertable = false, updatable = false, nullable = true), })	
	private GolonganPegawai golonganPegawai;
	


}
