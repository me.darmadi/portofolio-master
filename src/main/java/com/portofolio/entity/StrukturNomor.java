package com.portofolio.entity;


import org.hibernate.annotations.ForeignKey;
import static com.portofolio.base.adapter.DatabaseAdapter.Constant.*;

import javax.persistence.*;
import javax.validation.constraints.*;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.portofolio.entity.Departemen;
import com.portofolio.entity.Profile;
import com.portofolio.entity.StrukturNomor;
import com.portofolio.entity.TypeDataObjek;
import com.portofolio.entity.vo.StrukturNomorId;

import org.hibernate.annotations.Type;



@Entity
@Table(name = "StrukturNomor_M", 
indexes = {
  		@Index(name = "StrukturNomor_M_Index1",  
  			columnList="KdProfile, isDefault, StrukturNomor", unique = false),
		@Index(name = "StrukturNomor_M_Index2",  
			columnList="StatusEnabled", unique = false) 
  		}
)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class StrukturNomor {
    
    @EmbeddedId
    private StrukturNomorId id ;
    
    private static final String cDefStrukturNomor = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG ;
    @Column(name = "StrukturNomor", columnDefinition = cDefStrukturNomor)
    @NotNull(message = "strukturnomor.strukturnomor.notnull")
    private String namaStrukturNomor;
    
    private static final String cDefReportDisplay = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG ;
    @Column(name = "ReportDisplay", columnDefinition = cDefReportDisplay)
    @NotNull(message = "strukturnomor.reportdisplay.notnull")
    private String reportDisplay;
    
    private static final String cDefDeskripsiDetail = VARCHAR + AWAL_KURUNG + 150 + AKHIR_KURUNG ;
    @Column(name = "DeskripsiDetail", columnDefinition = cDefDeskripsiDetail)
    private String deskripsiDetail;
    
    private static final String cDefKdDepartemen = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG ;
    @Column(name = "KdDepartemen", columnDefinition = cDefKdDepartemen)
    private String kdDepartemen;
    
    private static final String cDefisDefault =INTEGER;
    @Column(name = "isDefault", columnDefinition = cDefisDefault)
    @NotNull(message = "strukturnomor.isdefault.notnull")
    private Integer isDefault;
    
    private static final String cDefQtyDigitNomor =INTEGER;
    @Column(name = "QtyDigitNomor", columnDefinition = cDefQtyDigitNomor)
    @NotNull(message = "strukturnomor.qtydigitnomor.notnull")
    private Integer qtyDigitNomor;
    
    private static final String cDefFormatNomor = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG ;
    @Column(name = "FormatNomor", columnDefinition = cDefFormatNomor)
    @NotNull(message = "strukturnomor.formatnomor.notnull")
    private String formatNomor;
    
    private static final String cDefisAutoIncrement =INTEGER;
    @Column(name = "isAutoIncrement", columnDefinition = cDefisAutoIncrement)
    @NotNull(message = "strukturnomor.isautoincrement.notnull")
    private Integer isAutoIncrement;
    
    private static final String cDefKdStrukturNomorHead =INTEGER;
    @Column(name = "KdStrukturNomorHead", columnDefinition = cDefKdStrukturNomorHead)
    private Integer kdStrukturNomorHead;
    
    private static final String cDefKeteranganLainnya = VARCHAR + AWAL_KURUNG + 150 + AKHIR_KURUNG ;
    @Column(name = "KeteranganLainnya", columnDefinition = cDefKeteranganLainnya)
    private String keteranganLainnya;
    
    private static final String cDefStatusEnabled =INTEGER;
    @Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
    @NotNull(message = "strukturnomor.statusenabled.notnull")
    @Type(type = "org.hibernate.type.NumericBooleanType") private Boolean statusEnabled;
    
    private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG ;
    @Column(name = "NoRec", columnDefinition = cDefNoRec)
    @NotNull(message = "strukturnomor.norec.notnull")
    private String noRec;
    
    private static final String cDefVersion =INTEGER;
    @Version
    @Column(name = "version", nullable = false,  columnDefinition = cDefVersion)
    @NotNull(message = "strukturnomor.version.notnull")
    private Integer version;
    
    private static final String cDefKdTypeDataObjek =INTEGER;
    @Column(name = "KdTypeDataObjek", columnDefinition = cDefKdTypeDataObjek)
    private Integer kdTypeDataObjek;
    
    private static final String cDefuseQtyDigitCode =INTEGER;
    @Column(name = "IsUseQtyDigitCode", columnDefinition = cDefuseQtyDigitCode)
    private Boolean isUseQtyDigitCode;
    
    @ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	//@ForeignKey(name="none")
	@JoinColumns({
			@JoinColumn(name = "KdTypeDataObjek", referencedColumnName = "KdTypeDataObjek", insertable = false, updatable = false), })
	private TypeDataObjek typeDataObjek;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
    })
    //@ForeignKey(name="none")
     private Profile profile;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdDepartemen", referencedColumnName="KdDepartemen",insertable=false, updatable=false),
    })
    //@ForeignKey(name="none")
     private Departemen departemen;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdStrukturNomorHead", referencedColumnName="KdStrukturNomor",insertable=false, updatable=false),
    })
    //@ForeignKey(name="none")
     private StrukturNomor strukturNomorHead;
}
