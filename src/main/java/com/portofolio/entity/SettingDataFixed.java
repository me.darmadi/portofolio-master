package com.portofolio.entity;


import org.hibernate.annotations.ForeignKey;
import static com.portofolio.base.adapter.DatabaseAdapter.Constant.*;

import javax.persistence.*;
import javax.validation.constraints.*;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.portofolio.entity.Departemen;
import com.portofolio.entity.Profile;
import com.portofolio.entity.vo.SettingDataFixedId;

import org.hibernate.annotations.Type;



@Entity
@Table(name = "SettingDataFixed_M",indexes = {@Index(name = "SettingDataFixed_M_Index",  columnList="statusEnabled", unique = false) })
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class SettingDataFixed {
    
    @EmbeddedId
    private SettingDataFixedId id ;
    
    private static final String cDefTypeField = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG ;
    @Column(name = "TypeField", columnDefinition = cDefTypeField)
    @NotNull(message = "settingdatafixed.typefield.notnull")
    private String typeField;
    
    private static final String cDefNilaiField = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG ;
    @Column(name = "NilaiField", columnDefinition = cDefNilaiField)
    @NotNull(message = "settingdatafixed.nilaifield.notnull")
    private String nilaiField;
    
    private static final String cDefTabelRelasi = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG ;
    @Column(name = "TabelRelasi", columnDefinition = cDefTabelRelasi)
    private String tabelRelasi;
    
    private static final String cDefFieldKeyTabelRelasi = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG ;
    @Column(name = "FieldKeyTabelRelasi", columnDefinition = cDefFieldKeyTabelRelasi)
    private String fieldKeyTabelRelasi;
    
    private static final String cDefFieldReportDisplayTabelRelasi = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG ;
    @Column(name = "FieldReportDisplayTabelRelasi", columnDefinition = cDefFieldReportDisplayTabelRelasi)
    private String fieldReportDisplayTabelRelasi;
    
    private static final String cDefKeteranganFungsi = VARCHAR + AWAL_KURUNG + 150 + AKHIR_KURUNG ;
    @Column(name = "KeteranganFungsi", columnDefinition = cDefKeteranganFungsi)
    private String keteranganFungsi;
    
    
    private static final String cDefStatusEnabled =INTEGER;
    @Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
    @Type(type = "org.hibernate.type.NumericBooleanType") private Boolean statusEnabled;
    
    private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG ;
    @Column(name = "NoRec", columnDefinition = cDefNoRec)
    @NotNull(message = "settingdatafixed.norec.notnull")
    private String noRec;
    
    private static final String cDefVersion =INTEGER;
    @Version
    @Column(name = "version", nullable = false,  columnDefinition = cDefVersion)
    @NotNull(message = "settingdatafixed.version.notnull")
    private Integer version;
    

    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
    })
    //@ForeignKey(name="none")
     private Profile profile;
    
   
}
