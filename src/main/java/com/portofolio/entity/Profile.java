package com.portofolio.entity;


import static com.portofolio.base.adapter.DatabaseAdapter.Constant.AKHIR_KURUNG;
import static com.portofolio.base.adapter.DatabaseAdapter.Constant.AWAL_KURUNG;
import static com.portofolio.base.adapter.DatabaseAdapter.Constant.INTEGER;
import static com.portofolio.base.adapter.DatabaseAdapter.Constant.VARCHAR;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.Type;
import org.springframework.data.annotation.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "Profile_M", indexes = { @Index(name = "Profile_M_Index", columnList = "statusEnabled", unique = false) })
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Profile {

	private static final String cDefKdProfile = INTEGER;
	@Column(name = "KdProfile", columnDefinition = cDefKdProfile)
	@Id
	@GeneratedValue
	private Integer kode;

	private static final String cDefNamaLengkap = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "NamaLengkap", columnDefinition = cDefNamaLengkap)
	@NotNull(message = "profile.namalengkap.notnull")
	private String namaLengkap;

	private static final String cDefReportDisplay = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "ReportDisplay", columnDefinition = cDefReportDisplay)
	@NotNull(message = "profile.reportDisplay.notnull")
	private String reportDisplay;

	private static final String cDefTglRegistrasi = INTEGER;
	@Column(name = "TglRegistrasi", columnDefinition = cDefTglRegistrasi)
	private Long tglRegistrasi;



	private static final String cDefKdAlamat = INTEGER;
	@Column(name = "KdAlamat", columnDefinition = cDefKdAlamat)
	private Integer kdAlamat;


	private static final String cDefGambarLogo = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "GambarLogo", columnDefinition = cDefGambarLogo)
	private String gambarLogo;

	private static final String cDefKdJenisProfile = INTEGER;
	@Column(name = "KdJenisProfile", columnDefinition = cDefKdJenisProfile)
	@NotNull(message = "profile.kdjenisprofile.notnull")
	private Integer kdJenisProfile;


	private static final String cDefKdNegara = INTEGER;
	@Column(name = "KdNegara", columnDefinition = cDefKdNegara)
	private Integer kdNegara;

	private static final String cDefKodeExternal = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "KodeExternal", columnDefinition = cDefKodeExternal)
	private String kodeExternal;

	private static final String cDefNamaExternal = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "NamaExternal", columnDefinition = cDefNamaExternal)
	private String namaExternal;


	private static final String cDefTglDaftar = INTEGER;
	@Column(name = "TglDaftar", columnDefinition = cDefTglDaftar)
	private Long tglDaftar;



	private static final String cDefStatusEnabled = INTEGER;
	@Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
	@Type(type = "org.hibernate.type.NumericBooleanType") private Boolean statusEnabled;

	private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG;
	@Column(name = "NoRec", columnDefinition = cDefNoRec)
	@NotNull(message = "NoRec Harus Diisi")
	private String noRec;

	

	private static final String cDefVersion = INTEGER;
	@Version
	@Column(name = "version", nullable = false, columnDefinition = cDefVersion)
	@NotNull(message = "Version Harus Di Isi")
	private Integer version;






	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdJenisProfile", referencedColumnName = "KdJenisProfile", insertable = false, updatable = false), })
	//@ForeignKey(name="none")
	private JenisProfile jenisProfile;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			// @JoinColumn(name="KdProfile",
			// referencedColumnName="KdProfile",insertable=false, updatable=false),
			@JoinColumn(name = "KdProfileHead", referencedColumnName = "KdProfile", insertable = false, updatable = false), })
	//@ForeignKey(name="none")
	private Profile profile;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			 @JoinColumn(name = "KdNegara", referencedColumnName = "KdNegara", insertable = false, updatable = false), })
	//@ForeignKey(name="none")
	private Negara negara;

	

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
			@JoinColumn(name = "KdProfile", referencedColumnName = "KdProfile", insertable = false, updatable = false),
			@JoinColumn(name = "KdAlamat", referencedColumnName = "KdAlamat", insertable = false, updatable = false), })
	//@ForeignKey(name="none")
	private Alamat alamat;


	

}
