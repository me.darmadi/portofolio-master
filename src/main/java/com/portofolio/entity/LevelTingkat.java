package com.portofolio.entity;


import org.hibernate.annotations.ForeignKey;
import static com.portofolio.base.adapter.DatabaseAdapter.Constant.*;

import javax.persistence.*;
import javax.validation.constraints.*;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.portofolio.entity.Departemen;
import com.portofolio.entity.LevelTingkat;
import com.portofolio.entity.Profile;
import com.portofolio.entity.vo.LevelTingkatId;

import org.hibernate.annotations.Type;



@Entity
@Table(name = "LevelTingkat_M",indexes = {@Index(name = "LevelTingkat_M_Index",  columnList="statusEnabled", unique = false) })
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class LevelTingkat {
    
    @EmbeddedId
    private LevelTingkatId id ;
    
    private static final String cDefLevelTingkat = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG ;
    @Column(name = "LevelTingkat", columnDefinition = cDefLevelTingkat)
    @NotNull(message = "leveltingkat.leveltingkat.notnull")
    private String namaLevelTingkat;
    
    private static final String cDefReportDisplay = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG ;
    @Column(name = "ReportDisplay", columnDefinition = cDefReportDisplay)
    @NotNull(message = "leveltingkat.reportdisplay.notnull")
    private String reportDisplay;
    
    private static final String cDefKdLevelTingkatHead =INTEGER;
    @Column(name = "KdLevelTingkatHead", columnDefinition = cDefKdLevelTingkatHead)
    private Integer kdLevelTingkatHead;
    
    private static final String cDefNoUrut =INTEGER;
    @Column(name = "NoUrut", columnDefinition = cDefNoUrut)
    @NotNull(message = "leveltingkat.nourut.notnull")
    private Integer noUrut;
    
    private static final String cDefKdDepartemen = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG ;
    @Column(name = "KdDepartemen", columnDefinition = cDefKdDepartemen)
    @NotNull(message = "leveltingkat.kddepartemen.notnull")
    private String kdDepartemen;
    
    private static final String cDefKodeExternal = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG ;
    @Column(name = "KodeExternal", columnDefinition = cDefKodeExternal)
    private String kodeExternal;
    
    private static final String cDefNamaExternal = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG ;
    @Column(name = "NamaExternal", columnDefinition = cDefNamaExternal)
    private String namaExternal;
    
    private static final String cDefStatusEnabled =INTEGER;
    @Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
    @NotNull(message = "leveltingkat.statusenabled.notnull")
    @Type(type = "org.hibernate.type.NumericBooleanType") private Boolean statusEnabled;
    
    private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG ;
    @Column(name = "NoRec", columnDefinition = cDefNoRec)
    @NotNull(message = "leveltingkat.norec.notnull")
    private String noRec;
    
    private static final String cDefVersion =INTEGER;
    @Version
    @Column(name = "version", nullable = false,  columnDefinition = cDefVersion)
    @NotNull(message = "leveltingkat.version.notnull")
    private Integer version;
    

    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
    })
    //@ForeignKey(name="none")
     private Profile profile;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdLevelTingkatHead", referencedColumnName="KdLevelTingkat",insertable=false, updatable=false),
    })
    //@ForeignKey(name="none")
     private LevelTingkat levelTingkatHead;
    
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdDepartemen", referencedColumnName="KdDepartemen",insertable=false, updatable=false),
    })
    //@ForeignKey(name="none")
     private Departemen departemen;
}
