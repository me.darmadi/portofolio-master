package com.portofolio.entity;

import static com.portofolio.base.adapter.DatabaseAdapter.Constant.AKHIR_KURUNG;
import static com.portofolio.base.adapter.DatabaseAdapter.Constant.AWAL_KURUNG;
import static com.portofolio.base.adapter.DatabaseAdapter.Constant.INTEGER;
import static com.portofolio.base.adapter.DatabaseAdapter.Constant.INTEGER;
import static com.portofolio.base.adapter.DatabaseAdapter.Constant.TEXT;
import static com.portofolio.base.adapter.DatabaseAdapter.Constant.INTEGER;
import static com.portofolio.base.adapter.DatabaseAdapter.Constant.VARCHAR;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Type;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "LoginUser_S", indexes = {
		@Index(name = "LoginUser_S_Index", columnList = "statusEnabled", unique = false) })
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class LoginUser {

	private static final String cDefKdUser = INTEGER;
	@Column(name = "KdUser", columnDefinition = cDefKdUser)
	//@NotNull(message = "loginuser.kduser.notnull")
	@Id
	@GeneratedValue
	private Long kdUser;

	private static final String cDefNamaUserMobile = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "NamaUserMobile", columnDefinition = cDefNamaUserMobile)
	private String namaUserMobile;

	private static final String cDefNamaUserEmail = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG;
	@Column(name = "NamaUserEmail", columnDefinition = cDefNamaUserEmail)
	private String namaUserEmail;

	private static final String cDefKataSandi = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG;
	@Column(name = "KataSandi", columnDefinition = cDefKataSandi)
	@NotNull(message = "loginuser.katasandi.notnull")
	private String kataSandi;

	private static final String cDefHintKataSandi = VARCHAR + AWAL_KURUNG + 150 + AKHIR_KURUNG;
	@Column(name = "HintKataSandi", columnDefinition = cDefHintKataSandi)
	private String hintKataSandi;

	private static final String cDefTglDaftar = INTEGER;
	@Column(name = "TglDaftar", columnDefinition = cDefTglDaftar)
	@NotNull(message = "loginuser.tgldaftar.notnull")
	private Long tglDaftar;

	private static final String cDefNamaLengkap = VARCHAR + AWAL_KURUNG + 80 + AKHIR_KURUNG;
	@Column(name = "NamaLengkap", columnDefinition = cDefNamaLengkap)
	private String namaLengkap;

	private static final String cDefTglLahir = INTEGER;
	@Column(name = "TglLahir", columnDefinition = cDefTglLahir)
	private Long tglLahir;

	private static final String cDefJenisKelamin = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "JenisKelamin", columnDefinition = cDefJenisKelamin)
	private String jenisKelamin;

	private static final String cDefNoIdentitas = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "NoIdentitas", columnDefinition = cDefNoIdentitas)
	private String noIdentitas;

	private static final String cDefAlamatLengkap = VARCHAR + AWAL_KURUNG + 200 + AKHIR_KURUNG;
	@Column(name = "AlamatLengkap", columnDefinition = cDefAlamatLengkap)
	private String alamatLengkap;

	private static final String cDefImageUser = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	//@Max(value = 100, message = "loginuser.imageuser.max")
	@Column(name = "ImageUser", columnDefinition = cDefImageUser)
	private String imageUser;

	private static final String cDefPathFileImageUser = VARCHAR + AWAL_KURUNG + 300 + AKHIR_KURUNG;
	@Column(name = "PathFileImageUser", columnDefinition = cDefPathFileImageUser)
	private String pathFileImageUser;
	
	private static final String cDefInfoDashboard = TEXT;
	@Column(name = "InfoDashboard", columnDefinition = cDefInfoDashboard)
	private String infoDashboard;

	private static final String cDefStatusEnabled = INTEGER;
	@Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
	@NotNull(message = "loginuser.statusenabled.notnull")
	@Type(type = "org.hibernate.type.NumericBooleanType") private Boolean statusEnabled;

	private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG;
	@Column(name = "NoRec", columnDefinition = cDefNoRec)
	@NotNull(message = "loginuser.norec.notnull")
	private String noRec;

	private static final String cDefVersion = INTEGER;
	@Version
	@Column(name = "version", nullable = false, columnDefinition = cDefVersion)
	@NotNull(message = "loginuser.version.notnull")
	private Integer version;
}
