package com.portofolio.entity;


import org.hibernate.annotations.ForeignKey;
import static com.portofolio.base.adapter.DatabaseAdapter.Constant.*;

import java.math.BigDecimal;

import javax.persistence.*;
import javax.validation.constraints.*;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.portofolio.entity.Departemen;
import com.portofolio.entity.Profile;
import com.portofolio.entity.vo.ProdukId;

import org.hibernate.annotations.Type;



@Entity
@Table(name = "Produk_M",indexes = {@Index(name = "Produk_M_Index",  columnList="statusEnabled", unique = false) })
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Produk {
    
    @EmbeddedId
    private ProdukId id ;
    
    private static final String cDefNamaProduk = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG ;
    @Column(name = "NamaProduk", columnDefinition = cDefNamaProduk)
    @NotNull(message = "produk.namaproduk.notnull")
    private String namaProduk;
    
    private static final String cDefReportDisplay = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG ;
    @Column(name = "ReportDisplay", columnDefinition = cDefReportDisplay)
    @NotNull(message = "produk.reportdisplay.notnull")
    private String reportDisplay;
    
    private static final String cDefKdJenisProduk =INTEGER;
    @Column(name = "KdJenisProduk", columnDefinition = cDefKdJenisProduk)
    private Integer kdJenisProduk;
    
    private static final String cDefKodeExternal = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG ;
    @Column(name = "KodeExternal", columnDefinition = cDefKodeExternal)
    private String kodeExternal;
    
    private static final String cDefNamaExternal = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG ;
    @Column(name = "NamaExternal", columnDefinition = cDefNamaExternal)
    private String namaExternal;
    
    
    private static final String cDefStatusEnabled =INTEGER;
    @Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
    @NotNull(message = "produk.statusenabled.notnull")
    @Type(type = "org.hibernate.type.NumericBooleanType") private Boolean statusEnabled;
    
    private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG ;
    @Column(name = "NoRec", columnDefinition = cDefNoRec)
    @NotNull(message = "produk.norec.notnull")
    private String noRec;
    
    private static final String cDefVersion =INTEGER;
    @Version
    @Column(name = "version", nullable = false,  columnDefinition = cDefVersion)
    @NotNull(message = "produk.version.notnull")
    private Integer version;
    
    private static final String cDefHarga =BIGDECIMAL;
    @Column(name = "harga", nullable = false,  columnDefinition = cDefHarga)
    @NotNull(message = "produk.harga.notnull")
    private BigDecimal harga;

    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
    })
    //@ForeignKey(name="none")
     private Profile profile;

    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdProfile", referencedColumnName="KdProfile",insertable=false, updatable=false),
        @JoinColumn(name="KdJenisProduk", referencedColumnName="KdJenisProduk",insertable=false, updatable=false),
    })
    //@ForeignKey(name="none")
     private JenisProduk jenisProduk;
    
}
