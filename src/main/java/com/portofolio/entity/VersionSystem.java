/* --------------------------------------------------------
 Generated by Gunandi using sql2java (http://sql2java.sourceforge.net/ )
 jdbc driver used at code generation time: net.sourceforge.jtds.jdbc.Driver
 
 Author : Gunandi (andri.gunandi1@gmail.com) 
 Date Created : 07/01/2019
 --------------------------------------------------------
*/
package com.portofolio.entity;

import static com.portofolio.base.adapter.DatabaseAdapter.Constant.*;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.hibernate.annotations.ForeignKey;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import  lombok.AllArgsConstructor;
import  lombok.Getter;
import  lombok.NoArgsConstructor;
import  lombok.Setter;


@Entity
@Table(name = "VersionSystem_S",indexes = {@Index(name = "VersionSystem_S_Index",  columnList="statusEnabled", unique = false) })
@Getter
@Setter
@AllArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class VersionSystem {

	private static final String cDefkdVersion = INTEGER;
	@Column(name = "KdVersion", columnDefinition = cDefkdVersion)
	@Id
	@GeneratedValue
	private Integer kdVersion;
    
    private static final String cDefNamaVersion = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG ;
    @Column(name = "NamaVersion", columnDefinition = cDefNamaVersion)
    @NotNull(message = "version.namaversion.notnull")
    private String namaVersion;
    
    private static final String cDefReportDisplay = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG ;
    @Column(name = "ReportDisplay", columnDefinition = cDefReportDisplay)
    @NotNull(message = "version.reportdisplay.notnull")
    private String reportDisplay;
    
    private static final String cDefKdVersionHead =INTEGER;
    @Column(name = "KdVersionHead", columnDefinition = cDefKdVersionHead)
    private Integer kdVersionHead;
    
    private static final String cDefNoUrut =INTEGER;
    @Column(name = "NoUrut", columnDefinition = cDefNoUrut)
    private Integer noUrut;
    
    private static final String cDefStatusEnabled =INTEGER;
    @Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
    @NotNull(message = "version.statusenabled.notnull")
    private Boolean statusEnabled;
    
    private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG ;
    @Column(name = "NoRec", columnDefinition = cDefNoRec)
    @NotNull(message = "version.norec.notnull")
    private String noRec;
    
    private static final String cDefVersion =INTEGER;
    @Version
    @Column(name = "version", nullable = false,  columnDefinition = cDefVersion)
    @NotNull(message = "version.version.notnull")
    private Integer version;
     
    @ManyToOne(fetch= FetchType.LAZY)
    @JsonIgnore
    //@ForeignKey(name="none")
    @JoinColumns({
        @JoinColumn(name="KdVersionHead", referencedColumnName="KdVersion",insertable=false, updatable=false),
    })
    // //@ForeignKey(name="none")
     private VersionSystem versionHead;
    
    
    
    // tambahan untuk recursive
    
    @Transient
    List<VersionSystem> children;
    
    public VersionSystem() {
		this.children = new ArrayList<VersionSystem>();
	}
    
	public void addChild(VersionSystem child) {
		if (!this.children.contains(child) && child != null)
			this.children.add(child);
	}

}
