package com.portofolio.entity;

import static com.portofolio.base.adapter.DatabaseAdapter.Constant.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Type;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Entity
@Table(name = "Negara_M")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor  @JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Negara  {
	
	private static final String cDefKdNegara = INTEGER;
	@Column(name = "KdNegara", columnDefinition = cDefKdNegara)
	@Id	 
	private Integer kdNegara;
  
	private static final String cDefNamaNegara = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "NamaNegara", columnDefinition = cDefNamaNegara)
	@NotNull(message = "negara.namanegara.notnull")
	private String namaNegara;

	private static final String cDefReportDisplay = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "ReportDisplay", columnDefinition = cDefReportDisplay)
	@NotNull(message = "negara.reportdisplay.notnull")
	private String reportDisplay;

	private static final String cDefKodeExternal = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "KodeExternal", columnDefinition = cDefKodeExternal)
	private String kodeExternal;

	private static final String cDefNamaExternal = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG;
	@Column(name = "NamaExternal", columnDefinition = cDefNamaExternal)
	private String namaExternal;

	private static final String cDefStatusEnabled = INTEGER;
	@Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
	@NotNull(message = "negara.statusenabled.notnull")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean statusEnabled;

	private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG;
	@Column(name = "NoRec", columnDefinition = cDefNoRec)
	@NotNull(message = "negara.norec.notnull")
	private String noRec;

	private static final String cDefVersion = INTEGER;
	@Version
	@Column(name = "version", nullable = false, columnDefinition = cDefVersion)
	@NotNull(message = "negara.version.notnull")
	private Integer version;
	
 
}


