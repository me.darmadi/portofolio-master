package com.portofolio.dtosql;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class RuanganDtoSql {

	private String kdRuangan;
	
	private String namaRuangan;
	
	private String reportDisplay;

	private String noRuangan;

	private String kodeExternal;

	private String namaExternal;

	private Boolean statusEnabled;

	private String noRec;

	private Integer version;
	
	private String kdRuanganHead;
	
	private String namaRuanganHead;
	
	private String kdDepartemen;
	
	private String namaDepartemen;

	private Integer kdAlamat;
	
	private String alamatLengkap;

	private String kdPegawaiKepala;
	
	private String namaPegawaiKepala;
	
	@Transient
    List<RuanganDtoSql> children=new ArrayList<RuanganDtoSql>();

    
	public void addChild(RuanganDtoSql child) {
		if (!this.children.contains(child) && child != null)
			this.children.add(child);
	}

	public RuanganDtoSql(String kdRuangan, String namaRuangan, String reportDisplay, String noRuangan,
			 String kodeExternal, String namaExternal, Boolean statusEnabled, String noRec,
			Integer version, String kdRuanganHead, String namaRuanganHead, String kdDepartemen, String namaDepartemen,
			Integer kdAlamat, String alamatLengkap, String kdPegawaiKepala, String namaPegawaiKepala) {
		super();
		this.kdRuangan = kdRuangan;
		this.namaRuangan = namaRuangan;
		this.reportDisplay = reportDisplay;
		this.noRuangan = noRuangan;
		this.kodeExternal = kodeExternal;
		this.namaExternal = namaExternal;
		this.statusEnabled = statusEnabled;
		this.noRec = noRec;
		this.version = version;
		this.kdRuanganHead = kdRuanganHead;
		this.namaRuanganHead = namaRuanganHead;
		this.kdDepartemen = kdDepartemen;
		this.namaDepartemen = namaDepartemen;
		this.kdAlamat = kdAlamat;
		this.alamatLengkap = alamatLengkap;
		this.kdPegawaiKepala = kdPegawaiKepala;
		this.namaPegawaiKepala = namaPegawaiKepala;
	}


	
	
	

}
