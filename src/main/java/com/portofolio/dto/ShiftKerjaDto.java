/* --------------------------------------------------------
 Generated by Lukman Hakim using sql2java (http://sql2java.sourceforge.net/ )
 jdbc driver used at code generation time: net.sourceforge.jtds.jdbc.Driver
 
 Author : Lukman Hakim (lukman.uki@gmail.com) 
 Date Created : 16/11/2017
 --------------------------------------------------------
*/
package com.portofolio.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Max;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class ShiftKerjaDto {
	
	private Integer kdShiftKerja;
	
    @NotNull(message = "shiftkerja.namashift.notnull")
    private String namaShift;
    
    @NotNull(message = "shiftkerja.reportdisplay.notnull")
    private String reportDisplay;
    
    @NotNull(message = "shiftkerja.jammasuk.notnull")
    private String jamMasuk;
    
    @NotNull(message = "shiftkerja.jampulang.notnull")
    private String jamPulang;
    
    private Float qtyJamKerjaEfektif;
    
    private String jamBreakAwal;
    
    private String jamBreakAkhir;
    
    private Float qtyJamBreakEfektif;
    
    @NotNull(message = "shiftkerja.factorrate.notnull")
    private Float factorRate;
    
    @NotNull(message = "shiftkerja.operatorfactorrate.notnull")
    private String operatorFactorRate;
    
    private Integer kdKomponenIndex;
    
    private String kodeExternal;
    
    private String namaExternal;
    
    private String kdDepartemen;
    
    @NotNull(message = "shiftkerja.statusenabled.notnull")
    private Boolean statusEnabled;
    
    private String noRec;
    
    private Integer version;
    

}
