package com.portofolio.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Lukman Hakim
 * @Created 1/2/2018
 */
@Setter
@Getter
public class KodeStringDto {
    private  String kode;
}
