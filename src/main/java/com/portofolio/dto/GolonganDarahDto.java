package com.portofolio.dto;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class GolonganDarahDto {
    
    private String namaGolonganDarah;
    
    @NotNull(message = "golongandarah.reportdisplay.notnull")
    private String reportDisplay;
    
    private String kodeExternal;
    
    private String namaExternal;
    
    @NotNull(message = "golongandarah.statusenabled.notnull")
    private Boolean statusEnabled;
    

}
