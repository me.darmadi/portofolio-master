package com.portofolio.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PegawaiDto  {


	@NotNull(message = "pegawai.kddepartemen.notnull")
	private String kdDepartemen;
	
	@NotNull(message = "pegawai.kdjeniskelamin.notnull")
	private Integer kdJenisKelamin;
	
	@NotNull(message = "pegawai.namalengkap.notnull")
	private String namaLengkap;

	private Integer kdGolonganDarah;

	private String kdJabatan;

	private String kdJenisPegawai;

	private String kdKategoryPegawai;

	private Integer kdGolonganPegawai;

	private Integer kdPangkat;

	private String kdPegawaiHead;
	
	private String kdPendidikanTerakhir;

	private String kdRuanganKerja;

	private Integer kdStatusPerkawinan;

	private Integer kdSuku;

	private Integer kdTitle;

	private Integer kdTypePegawai;

	private String nIKIntern;

	private String nPWP;

	private String namaAwal;

	private String namaTengah;

	private String namaAkhir;
    
	private String photoDiri;

	private String tempatLahir;
	
	private String fingerPrintID;

	private Integer kdAgama;

	@NotNull(message = "pegawai.tgllahir.notnull")
	private Long tglLahir;

	private Long tglMasuk;

	private Integer kdNegaraAsal;
			
	@NotNull(message = "pegawai.statusenabled.notnull")
	private Boolean statusEnabled;
	
	@Valid
	private AlamatDto alamat;
	

}
