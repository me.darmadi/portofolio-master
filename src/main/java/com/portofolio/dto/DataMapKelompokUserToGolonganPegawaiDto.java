package com.portofolio.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DataMapKelompokUserToGolonganPegawaiDto {
	List<MapKelompokUserToGolonganPegawaiDto> mapKelompokUserToGolonganPegawai;
}
