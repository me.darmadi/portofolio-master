package com.portofolio.dto;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class KecamatanDto {
    
    
    @NotNull(message = "kecamatan.namakecamatan.notnull")
    private String namaKecamatan;
    
    @NotNull(message = "kecamatan.reportdisplay.notnull")
    private String reportDisplay;
    
    @NotNull(message = "kecamatan.kdpropinsi.notnull")
    private Integer kdPropinsi;
    
    @NotNull(message = "kecamatan.kdkotakabupaten.notnull")
    private Integer kdKotaKabupaten;
    
    private String kodeExternal;
    
    private String namaExternal;
    
    @NotNull(message = "kecamatan.statusenabled.notnull")
    private Boolean statusEnabled;
    
    
    

}
