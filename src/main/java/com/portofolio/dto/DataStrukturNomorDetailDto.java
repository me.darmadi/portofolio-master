package com.portofolio.dto;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DataStrukturNomorDetailDto {
	@NotNull(message = "strukturnomordetailid.kdstrukturnomor.notnull")
    private Integer kdStrukturNomor;
	
    @NotNull(message = "strukturnomordetail.kdleveltingkat.notnull")
    private Integer kdLevelTingkat;
	
    @NotNull(message = "strukturnomordetail.qtydigitkode.notnull")
    private Integer qtyDigitKode;
    
    @NotNull(message = "strukturnomordetail.kodeurutawal.notnull")
    private String kodeUrutAwal;
    
    @NotNull(message = "strukturnomordetail.kodeurutakhir.notnull")
    private String kodeUrutAkhir;
    
    @NotNull(message = "strukturnomordetail.isautoincrement.notnull")
    private Integer isAutoIncrement;
    
    @NotNull(message = "strukturnomordetail.formatkode.notnull")
    private String formatKode;
    
    private String statusResetNomor;
    
    private String keteranganLainnya;
    
    private String kdDepartemen;
    
    @NotNull(message = "strukturnomordetail.statusenabled.notnull")
    private Boolean statusEnabled;
    
    private String noRec;
    
    private Integer version;
}
