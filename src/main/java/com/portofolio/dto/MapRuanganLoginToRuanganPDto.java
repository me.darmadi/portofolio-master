/* --------------------------------------------------------
 Generated by Lukman Hakim using sql2java (http://sql2java.sourceforge.net/ )
 jdbc driver used at code generation time: net.sourceforge.jtds.jdbc.Driver
 
 Author : Lukman Hakim (lukman.uki@gmail.com) 
 Date Created : 17/01/2018
 --------------------------------------------------------
*/
package com.portofolio.dto;

import java.util.Set;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MapRuanganLoginToRuanganPDto {

	@NotNull(message = "maRuanganlogintoruanganpdto.kdRuanganLogin.notnull")
	private String kdRuanganLogin;
	
	@NotNull(message = "maRuanganlogintoruanganpdto.enumKelompokTransaksi.notnull")
	private Integer enumKelompokTransaksi;
	
	@NotNull(message = "maRuanganlogintoruanganpdto.enumJenisMaping.notnull")
	private Integer enumJenisMaping;

	@Valid
	@NotNull(message = "maRuanganlogintoruanganpdto.kdRuanganPelayanan.notnull")
	private Set<KodeStringDto> kdRuanganPelayanan;

}
