package com.portofolio.dto;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class SukuDto {
    
    
    @NotNull(message = "suku.suku.notnull")
    private String namaSuku;
    
    @NotNull(message = "suku.reportdisplay.notnull")
    private String reportDisplay;
    
    private String kodeExternal;
    
    private String namaExternal;
    
    @NotNull(message = "suku.statusenabled.notnull")
    private Boolean statusEnabled;
    

}
