package com.portofolio.dto;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class MapKelompokUserToGolonganPegawaiDto {
	
	@NotNull(message = "mapkelompokusertogolonganPegawaiid.kdgolonganPegawai.notnull")
	private Integer kdGolonganPegawai;

	@NotNull(message = "mapkelompokusertogolonganPegawaiid.kdkategorypegawai.notnull")
	private String kdKategoryPegawai;

	@NotNull(message = "mapkelompokusertogolonganPegawaiid.kdkelompokuser.notnull")
	private Integer kdKelompokUser;
	
	@NotNull(message = "mapkelompokusertogolonganPegawai.statusenabled.notnull")
	private Boolean statusEnabled;

	private String noRec;

	private Integer version;
}
