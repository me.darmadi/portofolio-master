package com.portofolio.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DataMapObjekModulToKelompokUserDto {
	List<MapObjekModulToKelompokUserDto> mapObjekModulToKelompokUser;
}
