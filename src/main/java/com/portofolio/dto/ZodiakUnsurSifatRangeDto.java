package com.portofolio.dto;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class ZodiakUnsurSifatRangeDto {
		    
	@NotNull(message = "zodiakunsursifatrangeid.tglawal.notnull")
	private Long tglAwal;
	    
	@NotNull(message = "zodiakunsursifatrangeid.tglakhir.notnull")
	private Long tglAkhir;

	@NotNull(message = "zodiakunsursifatrangeid.kdNegara.notnull")
	private Integer kdNegara;
	
	@NotNull(message = "zodiakunsursifatrangeid.kdzodiak.notnull")
	private Integer kdZodiak;
	    
	@NotNull(message = "zodiakunsursifatrangeid.kdzodiakunsur.notnull")
	private Integer kdZodiakUnsur;

	//@Max(value = 4, message = "zodiakunsursifatrangeid.sifat.max")
	@NotNull(message = "zodiakunsursifatrangeid.sifat.notnull")
	private String sifat;
	
	@NotNull(message = "zodiakunsursifatrange.statusenabled.notnull")
    private Boolean statusEnabled;

    private String kodeExternal;
    
    private String namaExternal;
    
    private String noRec;
    
    private Integer version;
    

}
