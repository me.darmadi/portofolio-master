package com.portofolio.dto;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LanguageDetailDto {
	
	@NotNull(message = "kdAntrianTidakBolehKosong")
    @ApiModelProperty(value="kdAntrianRegistrasi" ,required=true)
    private String kdModulAplikasi;
    
	@NotNull(message = "kdVerionTidakBolehKosong")
    @ApiModelProperty(value="kdVersion" ,required=true)
    private Integer kdVersion;
    
	@NotNull(message = "kdBahasaTidakBolehKosong")
    @ApiModelProperty(value="kdBahasa" ,required=true)
	private Integer kdBahasa;
	
	Set<Map<String,String>> data;
	
}
