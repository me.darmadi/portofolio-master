package com.portofolio.dto;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class MapKelompokUserToJabatanDto {
	
	@NotNull(message = "mapkelompokusertojabatanid.kdjabatan.notnull")
	private String kdJabatan;

	@NotNull(message = "mapkelompokusertojabatanid.kdkategorypegawai.notnull")
	private String kdKategoryPegawai;

	@NotNull(message = "mapkelompokusertojabatanid.kdkelompokuser.notnull")
	private Integer kdKelompokUser;
	
	@NotNull(message = "mapjenispegawaitojabatan.statusenabled.notnull")
	private Boolean statusEnabled;

	private String noRec;

	private Integer version;
}
