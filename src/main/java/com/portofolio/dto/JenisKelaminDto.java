package com.portofolio.dto;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class JenisKelaminDto {
    
    
    @NotNull(message = "jeniskelamin.jeniskelamin.notnull")
    private String namaJenisKelamin;
    
    @NotNull(message = "jeniskelamin.reportdisplay.notnull")
    private String reportDisplay;
    
    private String kodeExternal;
    
    private String namaExternal;
    
    @NotNull(message = "jeniskelamin.statusenabled.notnull")
    private Boolean statusEnabled;
    
    

}
