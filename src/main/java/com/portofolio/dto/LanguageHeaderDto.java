package com.portofolio.dto;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LanguageHeaderDto {
	
	
	List<LanguageDetailDto> detail;
	
}
