
package com.portofolio.dto;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MapLoginUserToPegawaiDto {
 
	@NotNull(message = "MapLoginUserToPegawaiDto.kduser.notnull")
	private Long kdUser;

	@NotNull(message = "MapLoginUserToPegawaiDto.kdpegawai.notnull")
	private String kdPegawai;

	@NotNull(message = "MapLoginUserToPegawaiDto.kdkelompokuser.notnull")
	private Integer kdKelompokUser;

	@NotNull(message = "MapLoginUserToPegawaiDto.statusenabled.notnull")
	private Boolean statusEnabled;
 

}
