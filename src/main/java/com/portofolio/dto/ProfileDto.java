package com.portofolio.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProfileDto {
	
	
	@NotNull(message = "profiledto.namalengkap.notnull")
	private String namaLengkap;
	
	@NotNull(message = "profiledto.reportDisplay.notnull")
	private String reportDisplay;
	
	private String gambarLogo;

	@NotNull(message = "profiledto.kdjenisprofile.notnull")
	private Integer kdJenisProfile;

	private String kodeExternal;

	private String namaExternal;

	@NotNull(message = "profiledto.statusenabled.notnull")
	private Boolean statusEnabled;
	
	@Valid
	private AlamatDto alamat;
	
	
}
