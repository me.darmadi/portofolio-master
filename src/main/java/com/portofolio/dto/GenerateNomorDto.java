package com.portofolio.dto;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GenerateNomorDto
{
    private Long tanggal;
    
    @NotNull(message="harus diisi")
    private Integer kdKelompokTransaksi;

    private Integer quantity;
    
    
}