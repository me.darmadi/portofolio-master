package com.portofolio.dto;

import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LanguageSubDetailDto {
	
	@NotNull(message = "contentTidakBolehKosong")
    @ApiModelProperty(value="content" ,required=true)
	private String content;
	
	List<Map<String,String>> data;
	
}
