package com.portofolio.dto;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class AgamaDto {
    
    @NotNull(message = "agama.agama.notnull")
    private String namaAgama;
    
    @NotNull(message = "agama.reportdisplay.notnull")
    private String reportDisplay;
    
    private String kodeExternal;
    
    private String namaExternal;
    
    @NotNull(message = "agama.statusenabled.notnull")
    private Boolean statusEnabled;
    
}
