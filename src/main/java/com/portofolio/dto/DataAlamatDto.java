package com.portofolio.dto;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DataAlamatDto {
	
	private Integer kdProfile;
	
	private Integer kdAlamat;
	
    @NotNull(message = "alamat.kdjenisalamat.notnull")
    private Integer kdJenisAlamat;
    
    private String namaTempatGedung;
    
    @NotNull(message = "alamat.alamatlengkap.notnull")
    private String alamatLengkap;
    
    private Integer kdDesaKelurahan;
    
    private String desaKelurahan;
    
    private String rTRW;
    
    private Integer kdKecamatan;
    
    private String kecamatan;
    
    private Integer kdKotaKabupaten;
    
    private String kotaKabupaten;
    
    @NotNull(message = "alamat.kdpropinsi.notnull")
    private Integer kdPropinsi;
    
    private String kodePos;
    
    @NotNull(message = "alamat.kdnegara.notnull")
    private Integer kdNegara;
    
    private String fixedPhone1;
    
    private String fixedPhone2;
    
    private String mobilePhone1;
    
    private String mobilePhone2;
    
    private String faksimile1;
    
    private String faksimile2;
    
    private String alamatEmail;
    
    private String website;
    
    private String blackBerry;
    
    private String whatsApp;
    
    private String yahooMessenger;
    
    private String twitter;
    
    private String facebook;
    
    private String line;    
 
	private Float garisLintangLatitude;
 
	private Float garisBujurLongitude;
    
    @NotNull(message = "alamat.isprimaryaddress.notnull")
    private Integer isPrimaryAddress;
    
    @NotNull(message = "alamat.isshippingaddress.notnull")
    private Integer isShippingAddress;
    
    @NotNull(message = "alamat.isbillingaddress.notnull")
    private Integer isBillingAddress;
    
    private String kdDepartemen;
    
    private Boolean statusEnabled; 
}
