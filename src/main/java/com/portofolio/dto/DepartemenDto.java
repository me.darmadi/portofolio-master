/* --------------------------------------------------------
 Generated by Lukman Hakim using sql2java (http://sql2java.sourceforge.net/ )
 jdbc driver used at code generation time: net.sourceforge.jtds.jdbc.Driver
 
 Author : Lukman Hakim (lukman.uki@gmail.com) 
 Date Created : 25/10/2017
 --------------------------------------------------------
*/
package com.portofolio.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;


@Getter
@Setter
public class DepartemenDto {
    
    
	@NotNull(message = "departemen.namadepartemen.notnull")
	private String namaDepartemen;

	@NotNull(message = "departemen.reportdisplay.notnull")
	private String reportDisplay;

	private String kdPegawaiKepala;

	private String kdDepartemenHead;

	private String kodeExternal;

	private String namaExternal;

	@NotNull(message = "departemen.statusenabled.notnull")
	private Boolean statusEnabled;

}
