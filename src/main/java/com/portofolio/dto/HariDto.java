package com.portofolio.dto;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class HariDto {
    
    private Integer kdHari;
    
    @NotNull(message = "hari.namahari.notnull")
    private String namaHari;
    
    @NotNull(message = "hari.reportdisplay.notnull")
    private String reportDisplay;
    
    @NotNull(message = "hari.nourutharike.notnull")
    private Integer noUrutHariKe;
    
    private String kodeExternal;
    
    private String namaExternal;
    
    @NotNull(message = "hari.kdNegara.notnull")
    private Integer kdNegara;
    
    @NotNull(message = "hari.statusenabled.notnull")
    private Boolean statusEnabled;
    
    private String noRec;
    
    private Integer version; 
    
    private Integer formatAngka;
    
    private String formatAngkaRomawi;
     
    

}
