/* --------------------------------------------------------
 Generated by Lukman Hakim using sql2java (http://sql2java.sourceforge.net/ )
 jdbc driver used at code generation time: net.sourceforge.jtds.jdbc.Driver
 
 Author : Lukman Hakim (lukman.uki@gmail.com) 
 Date Created : 25/10/2017
 --------------------------------------------------------
*/
package com.portofolio.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Max;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class StatusDto {
    
    @NotNull(message = "status.namastatus.notnull")
    private String namaStatus;
    
    @NotNull(message = "status.reportdisplay.notnull")
    private String reportDisplay;
    
    //@NotNull(message = "status.kdstatushead.notnull")
    private Integer kdStatusHead;
    
    private String kodeExternal;
    
    private String namaExternal;
    
    @NotNull(message = "status.statusenabled.notnull")
    private Boolean statusEnabled;
    
    

}
