package com.portofolio.dto;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LevelTingkatDto {

	private Integer kdLevelTingkat;

	@NotNull(message = "leveltingkat.leveltingkat.notnull")
	private String namaLevelTingkat;

	@NotNull(message = "leveltingkat.reportdisplay.notnull")
	private String reportDisplay;

	private Integer kdLevelTingkatHead;

	@NotNull(message = "leveltingkat.nourut.notnull")
	private Integer noUrut;

	// @NotNull(message = "leveltingkat.kddepartemen.notnull")
	private String kdDepartemen;

	private String kodeExternal;

	private String namaExternal;

	@NotNull(message = "leveltingkat.statusenabled.notnull")
	private Boolean statusEnabled;

	private String noRec;

	private Integer version;

}
