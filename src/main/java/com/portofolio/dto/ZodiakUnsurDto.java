package com.portofolio.dto;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class ZodiakUnsurDto {
    
    private Integer kdZodiakUnsur;
    
    @NotNull(message = "zodiakunsur.namazodiakunsur.notnull")
    private String namaZodiakUnsur;
    
    private String reportDisplay;
    
    @NotNull(message = "zodiakunsur.nourut.notnull")
    private Integer noUrut;
    
    private Integer kdNegara;
    
    private String kodeExternal;
    
    private String namaExternal;
    
   // @NotNull(message = "zodiakunsur.kddepartemen.notnull")
 
    
    @NotNull(message = "zodiakunsur.statusenabled.notnull")
    private Boolean statusEnabled;
    
    private String noRec;
    
    private Integer version;
    

}
