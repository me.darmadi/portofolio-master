package com.portofolio.dto;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class BahasaDto {
    
    @NotNull(message = "bahasa.namabahasa.notnull")
    private String namaBahasa;
    
    @NotNull(message = "bahasa.reportdisplay.notnull")
    private String reportDisplay;
    
    private String kodeExternal;
    
    private String namaExternal; 
    
    @NotNull(message = "bahasa.statusenabled.notnull")
    private Boolean statusEnabled;
    
    
}
