/* --------------------------------------------------------
 Generated by Lukman Hakim using sql2java (http://sql2java.sourceforge.net/ )
 jdbc driver used at code generation time: net.sourceforge.jtds.jdbc.Driver
 
 Author : Lukman Hakim (lukman.uki@gmail.com) 
 Date Created : 02/11/2017
 --------------------------------------------------------
*/
package com.portofolio.dto;

import java.util.List;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class RegistrasiLoginUserDto {
	
	private String kdPegawai;	
	
    private Integer kdKelompokUser;

    @NotNull(message = "loginuser.namaUserMobile.notnull")
    private String namaUserMobile;
    
    @NotNull(message = "loginuser.namaUserEmail.notnull")
    private String namaUserEmail;
    
    @NotNull(message = "loginuser.katasandi.notnull")
    private String kataSandi;
    
    private String hintKataSandi;
    
    @NotNull(message = "loginuser.statusenabled.notnull")
    private Boolean statusEnabled;
         
    private List<MapLoginUserToProfileDto> MapLoginUserToProfileDto;
    
}
