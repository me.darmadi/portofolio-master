package com.portofolio.dto;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class DesaKelurahanDto {
    
    @NotNull(message = "desakelurahan.namadesakelurahan.notnull")
    private String namaDesaKelurahan;
    
    @NotNull(message = "desakelurahan.reportdisplay.notnull")
    private String reportDisplay;
    
    private String kodePos;
    
    @NotNull(message = "desakelurahan.kdpropinsi.notnull")
    private Integer kdPropinsi;
    
    @NotNull(message = "desakelurahan.kdkotakabupaten.notnull")
    private Integer kdKotaKabupaten;
    
    @NotNull(message = "desakelurahan.kdkecamatan.notnull")
    private Integer kdKecamatan;
    
    private String kodeExternal;
    
    private String namaExternal;
    
    @NotNull(message = "desakelurahan.statusenabled.notnull")
    private Boolean statusEnabled;
    

}
