package com.portofolio;
public enum EnumKelompokTransaksi {

    FORM_ANTRIAN_REGISTRASI_PELAYANAN(1, "Form Antrian Registrasi Pelayanan"),
    MONITORING_ANTRIAN_REGISTRASI_PELAYANAN(2, "Monitoring Antrian Registrasi Pelayanan");

    private java.lang.String name;

    private java.lang.Integer id;

    EnumKelompokTransaksi(Integer id, java.lang.String name) {
        this.name = name;
        this.id = id;
    }

    public java.lang.String getName() {
        return name;
    }

    public java.lang.Integer getId() {
        return id;
    }
}