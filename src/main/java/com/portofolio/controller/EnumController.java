package com.portofolio.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.portofolio.base.LocaleController;
import com.portofolio.dto.AgamaDto;
import com.portofolio.service.AgamaService;
import com.portofolio.service.EnumService;
import com.portofolio.util.RestUtil;

import io.swagger.annotations.ApiOperation;

@SuppressWarnings("rawtypes")
@Lazy
@RestController
@RequestMapping("/emum")
public class EnumController extends LocaleController {
	@Lazy
	@Autowired	
	private EnumService enumService;


	@ApiOperation("Api Untuk Menampilkan EnumKelompokTransaksi")
	@GetMapping("/findAllEnumKelompokTransaksi")
	public ResponseEntity<Map<String, Object>> findAllEnumKelompokTransaksi() {
		Map<String, Object> result = enumService.findAllEnumKelompokTransaksi();
		return RestUtil.getJsonResponse(result);
	}
	
	@ApiOperation("Api Untuk Menampilkan EnumKelompokTransaksi")
	@GetMapping("/findAllEnumJenisMaping")
	public ResponseEntity<Map<String, Object>> findAllEnumJenisMaping() {
		Map<String, Object> result = enumService.findAllEnumJenisMaping();
		return RestUtil.getJsonResponse(result);
	}


	
}
