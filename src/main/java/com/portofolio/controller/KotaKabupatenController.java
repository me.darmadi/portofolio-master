package com.portofolio.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.portofolio.base.LocaleController;
import com.portofolio.dto.KotaKabupatenDto;
import com.portofolio.service.KotaKabupatenService;
import com.portofolio.util.RestUtil;

import io.swagger.annotations.ApiOperation;

@SuppressWarnings("rawtypes")
@Lazy
@RestController
@RequestMapping("/kotaKabupaten")
public class KotaKabupatenController extends LocaleController {
	@Lazy
	@Autowired
	private KotaKabupatenService kotakabupatenService;

	@ApiOperation("Api Untuk Menyimpan KotaKabupaten")
	@PostMapping("/save")
	public ResponseEntity<Map<String, Object>> save(@Valid @RequestBody KotaKabupatenDto entity, HttpServletRequest request) {
		Map<String, Object> result = kotakabupatenService.save(entity);
		return RestUtil.getJsonResponse(result);
	}

	@ApiOperation("Api Untuk Update KotaKabupaten")
	@PutMapping("/update")
	public ResponseEntity<Map<String, Object>> update(
			@RequestParam(value = "kdKotaKabupaten", required = true) Integer kdKotaKabupaten,
			@RequestParam(value = "version", required = true) Integer version,
			@Valid @RequestBody KotaKabupatenDto entity, HttpServletRequest request) {
		Map<String, Object> result = kotakabupatenService.update(entity, kdKotaKabupaten,version);
		return RestUtil.getJsonResponse(result);
	}

	@ApiOperation("Api Untuk Menghapus KotaKabupaten BerdasarkanKdKotaKabupaten")
	@DeleteMapping("/delete")
	public ResponseEntity<Map<String, Object>> deleteById(@RequestParam(
			value = "kdKotaKabupaten", required = true) Integer kdKotaKabupaten,
			@RequestParam(value = "version", required = true) Integer version) {
		Map<String, Object> result = kotakabupatenService.deleteByKode(kdKotaKabupaten, version);
		return RestUtil.getJsonResponse(result);
	}

	@ApiOperation("Api Untuk Menampilkan Semua KotaKabupaten")
	@GetMapping("/grid")
	public ResponseEntity<Map<String, Object>> grid(@RequestParam(value = "page", defaultValue = "1") Integer page,
			@RequestParam(value = "rows", defaultValue = stringAsIntegerMaxValue) Integer rows,
			@RequestParam(value = "dir", defaultValue = "namaKotaKabupaten") String dir,
			@RequestParam(value = "sort", defaultValue = "desc") String sort,
			@RequestParam(value = "namaKotaKabupaten", required = false, defaultValue="") String namaKotaKabupaten,
			@RequestParam(value = "statusEnabled", required = true) boolean statusEnabled) {
		Map<String, Object> result = kotakabupatenService.grid(page, rows, sort, dir, namaKotaKabupaten, statusEnabled);
		return RestUtil.getJsonResponse(result);
	}

	@ApiOperation("Api Untuk Menampilkan KotaKabupaten Berdasarkan kdKotaKabupaten")
	@GetMapping("/findByKdKotaKabupaten/")
	public ResponseEntity<Map<String, Object>> findByKdKotaKabupaten(
			@RequestParam(value = "kdKotaKabupaten", required = true) Integer kdKotaKabupaten,
			HttpServletRequest request) {
		Map<String, Object> result = kotakabupatenService.findByKdKotaKabupaten(kdKotaKabupaten);
		return RestUtil.getJsonResponse(result);
	}

	@ApiOperation("Api Untuk Menampilkan Semua KotaKabupaten")
	@GetMapping("/findByKdPropinsi")
	public ResponseEntity<Map<String, Object>> findByKdPropinsi(
			@RequestParam(value = "kdPropinsi") Integer kdPropinsi) {
		Map<String, Object> result = kotakabupatenService.findByKdPropinsi(kdPropinsi);
		return RestUtil.getJsonResponse(result);
	}

}
