package com.portofolio.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.portofolio.base.LocaleController;
import com.portofolio.dto.DesaKelurahanDto;
import com.portofolio.service.DesaKelurahanService;
import com.portofolio.util.RestUtil;

import io.swagger.annotations.ApiOperation;

@SuppressWarnings("rawtypes")
@Lazy
@RestController
@RequestMapping("/desaKelurahan")
public class DesaKelurahanController extends LocaleController {
	@Lazy
	@Autowired
	private DesaKelurahanService desakelurahanService;

	@ApiOperation("Api Untuk Menyimpan DesaKelurahan")
	@PostMapping("/save")
	public ResponseEntity<Map<String, Object>> save(@Valid @RequestBody DesaKelurahanDto entity, HttpServletRequest request) {
		Map<String, Object> result = desakelurahanService.save(entity);
		return RestUtil.getJsonResponse(result);
	}

	@ApiOperation("Api Untuk Update DesaKelurahan")
	@PutMapping("/update")
	public ResponseEntity<Map<String, Object>> update(
			@RequestParam(value = "kdDesaKelurahan", required = true) Integer kdDesaKelurahan,
			@RequestParam(value = "version", required = true) Integer version,
			@Valid @RequestBody DesaKelurahanDto entity, HttpServletRequest request) {
		Map<String, Object> result = desakelurahanService.update(entity, kdDesaKelurahan, version);
		return RestUtil.getJsonResponse(result);
	}

	@ApiOperation("Api Untuk Menghapus DesaKelurahan BerdasarkanKdDesaKelurahan")
	@DeleteMapping("/delete")
	public ResponseEntity<Map<String, Object>> deleteById(
			@RequestParam(value = "kdDesaKelurahan", required = true) Integer kdDesaKelurahan,
			@RequestParam(value = "version", required = true) Integer version) {
		Map<String, Object> result = desakelurahanService.deleteById(kdDesaKelurahan,version);
		return RestUtil.getJsonResponse(result);
	}

	@ApiOperation("Api Untuk Menampilkan Semua DesaKelurahan")
	@GetMapping("/grid")
	public ResponseEntity<Map<String, Object>> grid(@RequestParam(value = "page", defaultValue = "1") Integer page,
			@RequestParam(value = "rows", defaultValue = stringAsIntegerMaxValue) Integer rows,
			@RequestParam(value = "dir", defaultValue = "namaDesaKelurahan") String dir,
			@RequestParam(value = "sort", defaultValue = "desc") String sort,
			@RequestParam(value = "namaDesaKelurahan", required = false, defaultValue = "") String namaDesaKelurahan,
			@RequestParam(value = "statusEnabled", required = true) boolean statusEnabled) {
		Map<String, Object> result = desakelurahanService.grid(page, rows, sort, dir, namaDesaKelurahan,statusEnabled);
		return RestUtil.getJsonResponse(result);
	}

	@ApiOperation("Api Untuk Menampilkan DesaKelurahan Berdasarkan kdDesaKelurahan")
	@GetMapping("/findByKdDesaKelurahan")
	public ResponseEntity<Map<String, Object>> findByKdDesaKelurahan(
			@RequestParam(value = "kdDesaKelurahan",required = true) Integer kdDesaKelurahan) {
		Map<String, Object> result = desakelurahanService.findByKdDesaKelurahan(kdDesaKelurahan);
		return RestUtil.getJsonResponse(result);
	}

	@ApiOperation("Api Untuk Menampilkan Semua DesaKelurahan berdasarkan kdKecamatan")
	@GetMapping("/findByKdKecamatan")
	public ResponseEntity<Map<String, Object>> findByKdKecamatan(
			@RequestParam(value = "kdKecamatan", required = false) Integer kdKecamatan) {
		Map<String, Object> result = desakelurahanService.findByKdKecamatan(kdKecamatan);
		return RestUtil.getJsonResponse(result);
	}
}
