package com.portofolio.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.portofolio.service.MenuDinamisService;
import com.portofolio.util.RestUtil;

import io.swagger.annotations.ApiOperation;


/**
*
* @author Syamsu Rizal Ali <syamsu.rizal@jasamedika.co.id>
*/
@RestController
@RequestMapping("/menuDinamis")
public class MenuDinamisController {

	@Autowired 
	MenuDinamisService menuDinamisService;
	
	@ApiOperation("Api Untuk Menampilkan grid Semua menu Dinamis")
	@GetMapping("/findAll")
	public ResponseEntity<Map<String, Object>> findAll() {
		Map<String, Object> result = menuDinamisService.selectAllObjekAplikasi();
		return RestUtil.getJsonResponse(result);
	}
	
}
