/* --------------------------------------------------------
 Generated by Lukman Hakim using sql2java (http://sql2java.sourceforge.net/ )
 jdbc driver used at code generation time: net.sourceforge.jtds.jdbc.Driver
 
 Author : Lukman Hakim (lukman.uki@gmail.com) 
 Date Created : 25/10/2017
 --------------------------------------------------------
*/
package com.portofolio.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.portofolio.base.LocaleController;
import com.portofolio.dto.JenisAlamatDto;
import com.portofolio.service.JenisAlamatService;
import com.portofolio.util.RestUtil;

import io.swagger.annotations.ApiOperation;


@SuppressWarnings("rawtypes")
@Lazy
@RestController
@RequestMapping("/jenisAlamat")
public class JenisAlamatController extends LocaleController {
    @Lazy
	@Autowired
    private JenisAlamatService jenisAlamatService;
    
    
    @ApiOperation("Api Untuk Menyimpan JenisAlamat")
    @PostMapping("/save")
    public ResponseEntity<Map<String, Object>> save(@Valid @RequestBody JenisAlamatDto entity,HttpServletRequest request) {
        Map<String, Object> result = jenisAlamatService.save(entity);
        return RestUtil.getJsonResponse(result);
    }
    
    @ApiOperation("Api Untuk Update JenisAlamat")
    @PutMapping("/update")
    public ResponseEntity<Map<String, Object>> update(
    		@RequestParam(value = "kdJenisAlamat", required = true) Integer kdJenisAlamat,	
    		@RequestParam(value = "version", required = true) Integer version,	
    		@Valid @RequestBody JenisAlamatDto entity, HttpServletRequest request) {
        Map<String, Object> result = jenisAlamatService.update(entity,kdJenisAlamat,version);
        return RestUtil.getJsonResponse(result);
    }
    
    
    @ApiOperation("Api Untuk Menghapus JenisAlamat BerdasarkanKdJenisAlamat")
    @DeleteMapping("/delete")
    public ResponseEntity<Map<String, Object>> deleteById(
    		@RequestParam(value = "kdJenisAlamat", required = true) Integer kdJenisAlamat,	
    		@RequestParam(value = "version", required = true) Integer version) {
        Map<String, Object> result = jenisAlamatService.deleteByKdJenisAlamat(kdJenisAlamat, version);
        return RestUtil.getJsonResponse(result);
    }
    
    
    @ApiOperation("Api Untuk Menampilkan Semua JenisAlamat")
    @GetMapping("/grid")
    public ResponseEntity<Map<String, Object>> grid(			
    		@RequestParam(value = "page", defaultValue = "1") Integer page,			
    		@RequestParam(value = "rows", defaultValue = "10") Integer rows,			
    		@RequestParam(value = "dir", defaultValue = "namaJenisAlamat") String dir,			
    		@RequestParam(value = "sort", defaultValue = "desc") String sort,			
    		@RequestParam(value = "namaJenisAlamat", required = false) String namaJenisAlamat,
    		@RequestParam(value = "statusEnabled", required = true) boolean statusEnabled) {
        Map<String, Object> result = jenisAlamatService.grid(page, rows, sort, dir, namaJenisAlamat,statusEnabled);
        return RestUtil.getJsonResponse(result);
    }
    
    
    @ApiOperation("Api Untuk Menampilkan JenisAlamat Berdasarkan kode")
    @GetMapping("/findByKdJenisAlamat")
    public ResponseEntity<Map<String, Object>> findByKode(@RequestParam(value = "kdJenisAlamat", required = true) Integer kdJenisAlamat, HttpServletRequest request) {
        Map<String, Object> result = jenisAlamatService.findByKdJenisAlamat(kdJenisAlamat);
        return RestUtil.getJsonResponse(result);
    }
    

}
