package com.portofolio.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.portofolio.base.LocaleController;
import com.portofolio.dto.SukuDto;
import com.portofolio.service.SukuService;
import com.portofolio.util.RestUtil;

import io.swagger.annotations.ApiOperation;

@SuppressWarnings("rawtypes")
@Lazy
@RestController
@RequestMapping("/suku")
public class SukuController extends LocaleController {
	@Lazy
	@Autowired
	private SukuService sukuService;

	@ApiOperation("Api Untuk Menyimpan Suku")
	@PostMapping("/save")
	public ResponseEntity<Map<String, Object>> save(@Valid @RequestBody SukuDto entity, HttpServletRequest request) {
		Map<String, Object> result = sukuService.save(entity);
		return RestUtil.getJsonResponse(result);
	}

	@ApiOperation("Api Untuk Update Suku")
	@PutMapping("/update")
	public ResponseEntity<Map<String, Object>> update(
			@RequestParam(value = "kdSuku", required = true) Integer kdSuku,
			@RequestParam(value = "version", required = true) Integer version,
			@Valid @RequestBody SukuDto sukuDto,
			HttpServletRequest request) {
		Map<String, Object> result = sukuService.update(sukuDto, kdSuku, version);
		return RestUtil.getJsonResponse(result);
	}

	@ApiOperation("Api Untuk Menghapus Suku BerdasarkanKdSuku")
	@DeleteMapping("/delete")
	public ResponseEntity<Map<String, Object>> deleteById(
			@RequestParam(value = "kdSuku", required = true) Integer kdSuku,
			@RequestParam(value = "version", required = true) Integer version
			) {
		Map<String, Object> result = sukuService.deleteByKdSuku(kdSuku,version);
		return RestUtil.getJsonResponse(result);
	}

	@ApiOperation("Api Untuk Menampilkan Semua Suku")
	@GetMapping("/grid")
	public ResponseEntity<Map<String, Object>> grid(@RequestParam(value = "page", defaultValue = "1") Integer page,
			@RequestParam(value = "rows", defaultValue = stringAsIntegerMaxValue) Integer rows,
			@RequestParam(value = "dir", defaultValue = "namaSuku") String dir,
			@RequestParam(value = "sort", defaultValue = "desc") String sort,
			@RequestParam(value = "namaSuku", required = false, defaultValue = "") String namaSuku,
			@RequestParam(value = "statusEnabled", required = true) boolean statusEnabled) {
		Map<String, Object> result = sukuService.grid(page, rows, sort, dir, namaSuku, statusEnabled);
		return RestUtil.getJsonResponse(result);
	}

	@ApiOperation("Api Untuk Menampilkan Suku Berdasarkan kode")
	@GetMapping("/findByKdSuku")
	public ResponseEntity<Map<String, Object>> findByKode(
			@RequestParam(value = "kdSuku", required = true) Integer kdSuku,
			HttpServletRequest request) {
		Map<String, Object> result = sukuService.findByKdSuku(kdSuku);
		return RestUtil.getJsonResponse(result);
	}


}
