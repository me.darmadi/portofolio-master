/* --------------------------------------------------------
 Generated by Lukman Hakim using sql2java (http://sql2java.sourceforge.net/ )
 jdbc driver used at code generation time: net.sourceforge.jtds.jdbc.Driver
 
 Author : Lukman Hakim (lukman.uki@gmail.com) 
 Date Created : 25/10/2017
 --------------------------------------------------------
*/
package com.portofolio.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.portofolio.base.LocaleController;
import com.portofolio.dto.KategoryPegawaiDto;
import com.portofolio.service.KategoryPegawaiService;
import com.portofolio.util.RestUtil;

import io.swagger.annotations.ApiOperation;


@SuppressWarnings("rawtypes")
@Lazy
@RestController
@RequestMapping("/kategoryPegawai")
public class KategoryPegawaiController extends LocaleController {
    @Lazy
	@Autowired
    private KategoryPegawaiService kategoryPegawaiService;
    
    
    @ApiOperation("Api Untuk Menyimpan KategoryPegawai")
    @PostMapping("/save")
    public ResponseEntity<Map<String, Object>> save(@Valid @RequestBody KategoryPegawaiDto entity,HttpServletRequest request) {
        Map<String, Object> result = kategoryPegawaiService.save(entity);
        return RestUtil.getJsonResponse(result);
    }
    
    @ApiOperation("Api Untuk Update KategoryPegawai")
    @PutMapping("/update")
    public ResponseEntity<Map<String, Object>> update(
    		@RequestParam(value = "kdKategoryPegawai", required = true) String kdKategoryPegawai,
    		@RequestParam(value = "version", required = true) Integer version,
    		@Valid @RequestBody KategoryPegawaiDto entity, HttpServletRequest request) {
        Map<String, Object> result = kategoryPegawaiService.update(entity,kdKategoryPegawai,version);
        return RestUtil.getJsonResponse(result);
    }
    
    
    @ApiOperation("Api Untuk Menghapus KategoryPegawai Berdasarkan KdKategoryPegawai")
    @DeleteMapping("/delete")
    public ResponseEntity<Map<String, Object>> deleteById(
    		@RequestParam(value = "kdKategoryPegawai", required = true) String kdKategoryPegawai,
    		@RequestParam(value = "version", required = true) Integer version
    		) {
        Map<String, Object> result = kategoryPegawaiService.deleteById(kdKategoryPegawai, version);
        return RestUtil.getJsonResponse(result);
    }
    
    
    @ApiOperation("Api Untuk Menampilkan Semua KategoryPegawai")
    @GetMapping("/grid")
    public ResponseEntity<Map<String, Object>> grid(
    		@RequestParam(value = "page", defaultValue = "1") Integer page, 
    		@RequestParam(value = "rows", defaultValue = "10") Integer rows,			
    		@RequestParam(value = "dir", defaultValue = "namaKategoryPegawai") String dir,			
    		@RequestParam(value = "sort", defaultValue = "desc") String sort,			
    		@RequestParam(value = "namaKategoryPegawai", required = false) String namaKategoryPegawai,
    		@RequestParam(value = "statusEnabled", required = true) boolean statusEnabled) {
        Map<String, Object> result = kategoryPegawaiService.grid(page, rows, sort, dir, namaKategoryPegawai,statusEnabled);
        return RestUtil.getJsonResponse(result);
    }
    
    
    @ApiOperation("Api Untuk Menampilkan KategoryPegawai Berdasarkan kdKategoryPegawai")
    @GetMapping("/findByKdKategoryPegawai")
    public ResponseEntity<Map<String, Object>> findByKdKategoryPegawai(
    		@RequestParam(value = "kdKategoryPegawai", required = true) String kdKategoryPegawai, 
    		HttpServletRequest request) {
        Map<String, Object> result = kategoryPegawaiService.findByKdKategoryPegawai(kdKategoryPegawai);
        return RestUtil.getJsonResponse(result);
    }


}
