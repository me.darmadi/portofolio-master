package com.portofolio.controller;

import java.io.IOException;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.portofolio.dto.LanguageDetailDto;
import com.portofolio.dto.LanguageHeaderDto;
import com.portofolio.service.LanguageService;
import com.portofolio.util.RestUtil;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/languageSetting")
public class LanguageController {

	@Autowired
	LanguageService languageService;

	@ApiOperation(value = "Api Untuk Simpan Language Setting", consumes = "application/json")
	@PostMapping(path = "/save")
	@ResponseBody
	public ResponseEntity<Map<String, Object>>  save(@RequestBody LanguageDetailDto dto) throws Exception {
		Map<String, Object> result = languageService.save(dto);
		return RestUtil.getJsonResponse(result);
	}
	
	@ApiOperation("Api Untuk Menampilkan Translasi Berdasarkan kdModulAplikasi,kdVersion,dan kdBahasa")
	@GetMapping("/findByKdModulAplikasiAndKdVersionAndKdBahasaTree")
	public ResponseEntity<Map<String, Object>> findByKdModulAplikasiAndKdVersionAndKdBahasaTree(
			@RequestParam(value = "kdModulAplikasi", required=true) String kdModulAplikasi,
			@RequestParam(value = "kdVersion", required=true) Integer kdVersion,
			@RequestParam(value = "kdBahasa", required=true) Integer kdBahasa) throws IOException {
		Map<String, Object> result = languageService.findByKdModulAplikasiAndKdVersionAndKdBahasaTree(kdModulAplikasi,kdVersion,kdBahasa);
		
		return RestUtil.getJsonResponse(result);
	}
	
	@ApiOperation("Api Untuk Menampilkan Translasi Berdasarkan kdModulAplikasi,kdVersion,dan kdBahasa")
	@GetMapping("/findByKdModulAplikasiAndKdVersionAndKdBahasaFlat")
	public ResponseEntity<Map<String, Object>> findByKdModulAplikasiAndKdVersionAndKdBahasaFlat(
			@RequestParam(value = "kdModulAplikasi", required=true) String kdModulAplikasi,
			@RequestParam(value = "kdVersion", required=true) Integer kdVersion,
			@RequestParam(value = "kdBahasa", required=true) Integer kdBahasa) throws IOException {
		Map<String, Object> result = languageService.findByKdModulAplikasiAndKdVersionAndKdBahasaFlat(kdModulAplikasi,kdVersion,kdBahasa);
		
		return RestUtil.getJsonResponse(result);
	}
	
	@ApiOperation("Api Untuk Menampilkan Translasi aktif Berdasarkan login")
	@GetMapping("/getLang")
	public ResponseEntity<Map<String, Object>> getLang() throws IOException {
		Map<String, Object> result = languageService.getLang();
		
		return RestUtil.getJsonResponse(result);
	}

}
