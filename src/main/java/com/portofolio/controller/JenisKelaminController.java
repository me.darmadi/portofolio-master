package com.portofolio.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.portofolio.base.LocaleController;
import com.portofolio.dto.JenisKelaminDto;
import com.portofolio.service.JenisKelaminService;
import com.portofolio.util.RestUtil;

import io.swagger.annotations.ApiOperation;

@SuppressWarnings("rawtypes")
@Lazy
@RestController
@RequestMapping("/jenisKelamin")
public class JenisKelaminController extends LocaleController {
	@Lazy
	@Autowired
	private JenisKelaminService jenisKelaminService;

	@ApiOperation("Api Untuk Menyimpan JenisKelamin")
	@PostMapping("/save")
	public ResponseEntity<Map<String, Object>> save(@Valid @RequestBody JenisKelaminDto entity, HttpServletRequest request) {
		Map<String, Object> result = jenisKelaminService.save(entity);
		return RestUtil.getJsonResponse(result);
	}

	@ApiOperation("Api Untuk Update JenisKelamin")
	@PutMapping("/update")
	public ResponseEntity<Map<String, Object>> update(
			@RequestParam(value = "kdJenisKelamin", required = true) Integer kdJenisKelamin,
			@RequestParam(value = "version", required = true) Integer version,
			@Valid @RequestBody JenisKelaminDto entity, 
			HttpServletRequest request) {
		Map<String, Object> result = jenisKelaminService.update(entity, kdJenisKelamin,version);
		return RestUtil.getJsonResponse(result);
	}

	@ApiOperation("Api Untuk Menghapus JenisKelamin BerdasarkanKdJenisKelamin")
	@DeleteMapping("/delete")
	public ResponseEntity<Map<String, Object>> deleteById(
			@RequestParam(value = "kdJenisKelamin", required = true) Integer kdJenisKelamin,
			@RequestParam(value = "version", required = true) Integer version) {
		Map<String, Object> result = jenisKelaminService.deleteByKode(kdJenisKelamin,version);
		return RestUtil.getJsonResponse(result);
	}

	@ApiOperation("Api Untuk Menampilkan Semua JenisKelamin")
	@GetMapping("/grid")
	public ResponseEntity<Map<String, Object>> findAll(@RequestParam(value = "page", defaultValue = "1") Integer page,
			@RequestParam(value = "rows", defaultValue = stringAsIntegerMaxValue) Integer rows,
			@RequestParam(value = "dir", defaultValue = "namaJenisKelamin") String dir,
			@RequestParam(value = "sort", defaultValue = "desc") String sort,
			@RequestParam(value = "namaJenisKelamin", required = false, defaultValue = "") String namaJenisKelamin,
			@RequestParam(value = "statusEnabled", required = true) boolean statusEnabled) {
		Map<String, Object> result = jenisKelaminService.grid(page, rows, sort, dir, namaJenisKelamin, statusEnabled);
		return RestUtil.getJsonResponse(result);
	}

	@ApiOperation("Api Untuk Menampilkan JenisKelamin Berdasarkan kode")
	@GetMapping("/findByKdJenisKelamin")
	public ResponseEntity<Map<String, Object>> findByKode(@RequestParam(value = "kdJenisKelamin", required = true) Integer kdJenisKelamin) {
		Map<String, Object> result = jenisKelaminService.findByKdJenisKelamin(kdJenisKelamin);
		return RestUtil.getJsonResponse(result);
	}

	
	
}
