package com.portofolio.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.portofolio.base.LocaleController;
import com.portofolio.dto.AgamaDto;
import com.portofolio.service.AgamaService;
import com.portofolio.util.RestUtil;

import io.swagger.annotations.ApiOperation;

@SuppressWarnings("rawtypes")
@Lazy
@RestController
@RequestMapping("/agama")
public class AgamaController extends LocaleController {
	@Lazy
	@Autowired	
	private AgamaService agamaService;
	
	@ApiOperation("Api Untuk Menyimpan Agama")
	@PostMapping("/save")
	public ResponseEntity<Map<String, Object>> save(
			@Valid @RequestBody AgamaDto entity, 
			HttpServletRequest request) {
		Map<String, Object> result = agamaService.save(entity);
		return RestUtil.getJsonResponse(result);
	}

	@ApiOperation("Api Untuk Update Agama")
	@PutMapping("/update")
	public ResponseEntity<Map<String, Object>> update(
			@RequestParam(value = "kdAgama", required = true) Integer kdAgama,
			@RequestParam(value = "version", required = true) Integer version, 
			@Valid @RequestBody AgamaDto entity,
			HttpServletRequest request) {
		Map<String, Object> result = agamaService.update(kdAgama,version,entity);
		
		return RestUtil.getJsonResponse(result);
	}

	@ApiOperation("Api Untuk Menghapus Agama Berdasarkan KdAgama")
	@DeleteMapping("/delete")
	public ResponseEntity<Map<String, Object>> deleteById(
			@RequestParam(value = "kdAgama", required = true) Integer kdAgama,
			@RequestParam(value = "version", required = true) Integer version) {
		Map<String, Object> result = agamaService.deleteById(kdAgama, version);
		
		return RestUtil.getJsonResponse(result);
	}

	@ApiOperation("Api Untuk Menampilkan grid Semua Agama")
	@GetMapping("/grid")
	public ResponseEntity<Map<String, Object>> grid(@RequestParam(value = "page", defaultValue = "1") Integer page,
			@RequestParam(value = "rows", defaultValue = stringAsIntegerMaxValue) Integer rows,
			@RequestParam(value = "dir", defaultValue = "namaAgama") String dir,
			@RequestParam(value = "sort", defaultValue = "desc") String sort,
			@RequestParam(value = "namaAgama", required = false, defaultValue = "") String namaAgama,
			@RequestParam(value = "statusEnabled", required = true) boolean statusEnabled) {
		Map<String, Object> result = agamaService.grid(page, rows, sort, dir, namaAgama,statusEnabled);
		
		return RestUtil.getJsonResponse(result);
	}

	@ApiOperation("Api Untuk Menampilkan Agama Berdasarkan KdAgama")
	@GetMapping("/findByKdAgama")
	public ResponseEntity<Map<String, Object>> findByKdAgama(
			@RequestParam(value = "kdAgama", required = true) Integer kdAgama) {
		Map<String, Object> result = agamaService.findByKdAgama(kdAgama);
		return RestUtil.getJsonResponse(result);
	}


	
}
