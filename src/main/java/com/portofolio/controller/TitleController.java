/* --------------------------------------------------------
 Generated by Lukman Hakim using sql2java (http://sql2java.sourceforge.net/ )
 jdbc driver used at code generation time: net.sourceforge.jtds.jdbc.Driver
 
 Author : Lukman Hakim (lukman.uki@gmail.com) 
 Date Created : 25/10/2017
 --------------------------------------------------------
*/
package com.portofolio.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.portofolio.base.LocaleController;
import com.portofolio.dto.TitleDto;
import com.portofolio.service.TitleService;
import com.portofolio.util.RestUtil;

import io.swagger.annotations.ApiOperation;


@SuppressWarnings("rawtypes")
@Lazy
@RestController
@RequestMapping("/title")
public class TitleController extends LocaleController {
    @Lazy
	@Autowired
    private TitleService titleService;
    
    
    @ApiOperation("Api Untuk Menyimpan Title")
    @PostMapping("/save")
    public ResponseEntity<Map<String, Object>> save(@Valid @RequestBody TitleDto entity,HttpServletRequest request) {
        Map<String, Object> result = titleService.save(entity);
        return RestUtil.getJsonResponse(result);
    }
    
    @ApiOperation("Api Untuk Update Title")
    @PutMapping("/update")
    public ResponseEntity<Map<String, Object>> update(
    		@RequestParam(value = "kdTitle", required = true) Integer kdTitle,
    		@RequestParam(value = "version", required = true) Integer version,
    		@Valid @RequestBody TitleDto entity, HttpServletRequest request) {
        Map<String, Object> result = titleService.update(entity,kdTitle,version);
        return RestUtil.getJsonResponse(result);
    }
    
    
    @ApiOperation("Api Untuk Menghapus Title Berdasarkan KdTitle")
    @DeleteMapping("/delete")
    public ResponseEntity<Map<String, Object>> deleteById(
    		@RequestParam(value = "kdTitle", required = true) Integer kdTitle,
    		@RequestParam(value = "version", required = true) Integer version
    		) {
        Map<String, Object> result = titleService.deleteById(kdTitle, version);
        return RestUtil.getJsonResponse(result);
    }
    
    
    @ApiOperation("Api Untuk Menampilkan Semua Title")
    @GetMapping("/grid")
    public ResponseEntity<Map<String, Object>> grid(
    		@RequestParam(value = "page", defaultValue = "1") Integer page, 
    		@RequestParam(value = "rows", defaultValue = "10") Integer rows,			
    		@RequestParam(value = "dir", defaultValue = "namaTitle") String dir,			
    		@RequestParam(value = "sort", defaultValue = "desc") String sort,			
    		@RequestParam(value = "namaTitle", required = false) String namaTitle,
    		@RequestParam(value = "statusEnabled", required = true) boolean statusEnabled) {
        Map<String, Object> result = titleService.grid(page, rows, sort, dir, namaTitle,statusEnabled);
        return RestUtil.getJsonResponse(result);
    }
    
    
    @ApiOperation("Api Untuk Menampilkan Title Berdasarkan kdTitle")
    @GetMapping("/findByKdTitle")
    public ResponseEntity<Map<String, Object>> findByKdTitle(
    		@RequestParam(value = "kdTitle", required = true) Integer kdTitle, 
    		HttpServletRequest request) {
        Map<String, Object> result = titleService.findByKdTitle(kdTitle);
        return RestUtil.getJsonResponse(result);
    }


}
