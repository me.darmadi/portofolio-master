// Decompiled by Jad v1.5.8e2. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://kpdus.tripod.com/jad.html
// Decompiler options: packimports(3)
// Source File Name:   Table.java

package com.portofolio.sql2java;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

// Referenced classes of package com.netkernel.sql2java:
//            Column, Database

public class Table {

	public Table() {
		colHash = new Hashtable();
		cols = new Vector();
		priKey = new Vector();
		impKey = new Vector();
		manyToManyHash = new Hashtable();
	}

	public boolean isRelationTable() {
		return impKey.size() > 1;
	}

	public boolean relationConnectsTo(Table otherTable) {
		if (equals(otherTable)) {
			return false;
		}
		for (int i = 0; i < impKey.size(); i++) {
			Column c = (Column) impKey.get(i);
			if (c.getTableName().equals(otherTable.getName())) {
				return true;
			}
		}

		return false;
	}

	public Table[] linkedTables(Database pDatabase, Table pTable) {
		Vector pVector = new Vector();
		for (int iIndex = 0; iIndex < impKey.size(); iIndex++) {
			Column pColumn = (Column) impKey.get(iIndex);
			if (pColumn.getTableName().equals(pTable.getName())) {
				continue;
			}
			Table pTableToAdd = pDatabase.getTable(pColumn.getTableName());
			if (!pVector.contains(pTableToAdd)) {
				pVector.add(pTableToAdd);
			}
		}

		Table pReturn[] = new Table[pVector.size()];
		pVector.copyInto(pReturn);
		return pReturn;
	}

	public Column getForeignKeyFor(Table pTable) {
		Vector pVector = new Vector();
		for (int iIndex = 0; iIndex < impKey.size(); iIndex++) {
			Column pColumn = (Column) impKey.get(iIndex);
			if (pColumn.getTableName().equals(pTable.getName())) {
				return pColumn;
			}
		}

		return null;
	}

	public Column getPrimaryKey() {
		Vector pVector = new Vector();
		for (int iIndex = 0; iIndex < cols.size(); iIndex++) {
			Column pColumn = (Column) cols.get(iIndex);
			if (pColumn.isPrimaryKey()) {
				return pColumn;
			}
		}

		return null;
	}

	public void setDatabase(Database db) {
		this.db = db;
	}

	public void setCatalog(String catalog) {
		this.catalog = catalog;
	}

	public void setSchema(String schema) {
		this.schema = schema;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getCatalog() {
		return catalog;
	}

	public String getSchema() {
		return schema;
	}

	public String getName() {
		return name;
	}

	public String getType() {
		return type;
	}

	public String getRemarks() {
		return remarks;
	}

	public Column[] getColumns() {
		Column list[] = new Column[cols.size()];
		cols.copyInto(list);
		return list;
	}

	public Column getColumn(String name) {
		return (Column) colHash.get(name.toLowerCase());
	}

	public void addColumn(Column column) {
		colHash.put(column.getName().toLowerCase(), column);
		cols.addElement(column);
	}

	public void removeColumn(Column column) {
		cols.removeElement(column);
		colHash.remove(column.getName().toLowerCase());
	}

	public Column[] getPrimaryKeys() {
		Column list[] = new Column[priKey.size()];
		priKey.copyInto(list);
		return list;
	}

	public void addPrimaryKey(Column column) {
		priKey.addElement(column);
		column.isPrimaryKey(true);
	}

	public Column[] getImportedKeys() {
		Column list[] = new Column[impKey.size()];
		impKey.copyInto(list);
		return list;
	}

	public void addImportedKey(Column column) {
		impKey.addElement(column);
		Column myColumn = getColumn(column.getForeignKeyColName());
		myColumn.setPointsTo(column);
	}

	public Column[][] getManyToManyKeys() {
		Column list[][] = new Column[manyToManyHash.size()][2];
		int i = 0;
		for (Enumeration e = manyToManyHash.keys(); e.hasMoreElements();) {
			Column fk = (Column) e.nextElement();
			Column pk = (Column) manyToManyHash.get(fk);
			list[i][0] = fk;
			list[i][1] = pk;
			i++;
		}

		return list;
	}

	public void addManyToManyKey(Column fk, Column pk) {
		manyToManyHash.put(fk, pk);
	}

	private Hashtable colHash;
	private Vector cols;
	private Vector priKey;
	private Vector impKey;
	private Hashtable manyToManyHash;
	private String catalog;
	private String schema;
	private String name;
	private String type;
	private String remarks;
	private Database db;
}
