// Decompiled by Jad v1.5.8e2. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://kpdus.tripod.com/jad.html
// Decompiler options: packimports(3)
// Source File Name:   PreparedManagerWriter.java

package com.portofolio.sql2java;

import java.util.ArrayList;

// Referenced classes of package com.netkernel.sql2java:
//            CodeWriter, Table, UserCodeParser, Column,
//            Database, Main

public class PreparedManagerWriter extends CodeWriter {

	public PreparedManagerWriter() {
	}

	public boolean isStringInArrayList(ArrayList pArrayList, String strValue) {
		for (int iIndex = 0; iIndex < pArrayList.size(); iIndex++) {
			String strP = (String) pArrayList.get(iIndex);
			if (strP.equalsIgnoreCase(strValue)) {
				return true;
			}
		}

		return false;
	}

	  
	private String getHint(Table table) {
		String hint = Main.getProperty("generatedkey.statement", "");
		int index = hint.indexOf("<TABLE>");
		if (index > 0) {
			String tmp = hint.substring(0, index) + table.getName();
			if (hint.length() > index + "<TABLE>".length()) {
				tmp = tmp
						+ hint.substring(index + "<TABLE>".length(), hint
								.length());
			}
			hint = tmp;
		}
		return hint;
	}

	protected static final String POST_INSERT_BLOCK = "postinsert";
	protected static final String PRE_INSERT_BLOCK = "preinsert";
	protected static final String POST_UPDATE_BLOCK = "postupdate";
	protected static final String PRE_UPDATE_BLOCK = "preupdate";
}
