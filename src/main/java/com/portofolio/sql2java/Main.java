// Decompiled by Jad v1.5.8e2. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://kpdus.tripod.com/jad.html
// Decompiler options: packimports(3)
// Source File Name:   Main.java

package com.portofolio.sql2java;

import java.util.ArrayList;
import java.util.Properties;
import java.util.StringTokenizer;

// Referenced classes of package com.netkernel.sql2java:
//            Database, PreparedManagerWriter

public class Main {

	public Main() {
	}

	public static void main(String argv[]) {
		prop = new Properties();
		try {
			prop.load(new Main().getClass().getResourceAsStream(
					"sql2java.properties"));
			Database db = new Database();
			db.setDriver(getProperty("jdbc.driver"));
			db.setURL(getProperty("jdbc.url"));
			db.setUsername(getProperty("jdbc.username"));
			db.setPassword(getProperty("jdbc.password"));
			db.setCatalog(getProperty("jdbc.catalog"));
			db.setSchema(getProperty("jdbc.schema"));
			db.setTableNamePattern(getProperty("jdbc.tablenamepattern"));
			String tt = getProperty("jdbc.tabletypes", "TABLE");
			StringTokenizer st = new StringTokenizer(tt, ",");
			ArrayList al = new ArrayList();
			for (; st.hasMoreTokens(); al.add(st.nextToken().trim())) {
				;
			}
			db.setTableTypes((String[]) al.toArray(new String[al.size()]));
			db.load();
			PreparedManagerWriter writer = new PreparedManagerWriter();
			writer.setDatabase(db);
			writer.setProperties(prop);
			writer.process();
			 
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	public static String getProperty(String key) {
		String s = prop.getProperty(key);
		return s == null ? s : s.trim();
	}

	public static String getProperty(String key, String default_val) {
		return prop.getProperty(key, default_val);
	}

	private static Properties prop;
}
