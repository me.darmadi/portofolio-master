// Decompiled by Jad v1.5.8e2. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://kpdus.tripod.com/jad.html
// Decompiler options: packimports(3)
// Source File Name:   CodeWriter.java

package com.portofolio.sql2java;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.annotation.Lazy;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiOperation;

// Referenced classes of package com.netkernel.sql2java:
//            UserCodeParser, Database, Table, Column

public abstract class CodeWriter {
	protected static final String MGR_SUFFIX = "Manager";
	protected static final String MGR_CLASS = "Manager";
	protected static final String ITERATOR_SUFFIX = "Iterator";
	protected static final String LISTENER_SUFFIX = "Listener";
	protected static final String BEAN_SUFFIX = "Bean";
	protected static final String COMPARATOR_SUFFIX = "Comparator";
	protected static final String SPACE = "    ";
	protected static final String IMPORT_BLOCK = "imports";
	protected static final String EXTENDS_BLOCK = "extends";
	protected static final String CLASS_BLOCK = "class";
	public static final String LINE_SEP = System.getProperty("line.separator");
	protected String mgrImports[];
	protected String basePackage;
	protected String destDir;
	protected Table table;
	protected UserCodeParser userCode;
	protected PrintWriter writer;
	protected String className;
	protected String classPrefix;
	protected String pkg;
	protected static String dateClassName;
	protected static String timeClassName;
	protected static String timestampClassName;
	protected Database db;
	protected Properties prop;
	protected Hashtable includeHash;
	protected Hashtable excludeHash;
	protected String tableName;
	protected String typePK;
	protected Map<String, String> messageList = new HashMap<String, String>();
	StringBuffer messageBuffer = new StringBuffer();

	public CodeWriter() {
	}

	public void setClassPrefix(String prefix) {
		classPrefix = prefix;
	}

	public String getClassPrefix() {
		return classPrefix;
	}

	public void setDatabase(Database db) {
		this.db = db;
	}

	public void setProperties(Properties prop) throws Exception {
		this.prop = prop;
		dateClassName = prop.getProperty("jdbc2java.date", "java.sql.Date");
		timeClassName = prop.getProperty("jdbc2java.time", "java.sql.Time");
		timestampClassName = prop.getProperty("jdbc2java.timestamp", "java.sql.Timestamp");
		basePackage = prop.getProperty("mgrwriter.package");
		destDir = prop.getProperty("mgrwriter.destdir");
		setClassPrefix(prop.getProperty("mgrwriter.classprefix"));
		excludeHash = setHash(prop.getProperty("mgrwriter.exclude"));
		includeHash = setHash(prop.getProperty("mgrwriter.include"));
		if (basePackage == null) {
			throw new Exception("Missing property: mgrwriter.package");
		}
		if (destDir == null) {
			throw new Exception("Missing property: mgrwriter.destdir");
		}
		File dir = new File(destDir);
		try {
			dir.mkdirs();
		} catch (Exception e) {
		}
		if (!dir.isDirectory() || !dir.canWrite()) {
			throw new Exception("Cannot write to: " + destDir);
		} else {
			return;
		}
	}

	private Hashtable setHash(String str) {
		if (str == null || str.trim().equals("")) {
			return null;
		}
		Hashtable hash = new Hashtable();
		String val;
		for (StringTokenizer st = new StringTokenizer(str); st.hasMoreTokens(); hash.put(val, val)) {
			val = st.nextToken().toLowerCase();
		}

		return hash;
	}

	public synchronized void process() throws Exception {
		writeClasses();
	}

	private void writeClasses() throws Exception {
		mgrImports = new String[3];
		mgrImports[0] = "java.util.ArrayList";
		mgrImports[1] = "java.sql.*";
		// writeManagerBase();
		Table tables[] = db.getTables();
		System.out.println("Jumlah Table :" + db.getTables().length);
		for (int i = 0; i < tables.length; i++) {
			if (includeHash != null) {
				if (includeHash.get(tables[i].getName().toLowerCase()) != null) {
					writeTable(tables[i]);
				}
				continue;
			}
			if (excludeHash != null) {
				if (excludeHash.get(tables[i].getName().toLowerCase()) == null) {
					writeTable(tables[i]);
				}
			} else {
				writeTable(tables[i]);
			}
		}
		// Make routing and menu
		// writeAngularRoute(tables);
	}

	private void writeTable(Table table) throws Exception {
		if (table.getColumns().length == 0) {
			System.out.println("WARNING: No column found in table " + table.getName()
					+ ". Skipping code generation for this table.");
			return;
		} else {
			this.table = table;
			/* Java */

			// if (!this.table.getName().contains("_T")) {
			getTipePk();
			writeCore();

			writeDao();
			writeDTO();
			// writeDaoCustomImpl();
			// writeDaoCustom();
			// writeService();
			writeServiceImpl();
			writeController();

			// }
			// writeFileMessage(messageList);
			// writeMessage(messageBuffer, "message.properties");

			/* Front End */

			// Versi adminLte (China)
			// writeAngularModul();
			// writeAngularComponentTs();
			// writeAngularComponentHtml();
			// writeAngularService();

			// Versi Angular 4 PrimeNg (Premium)

			// writeAngularUnitTest2();
			writeAngularComponentSCSS2();
			writeAngularInterface2();
			writeAngularComponentHtml2();
			// writeAngularService2();
			writeAngularComponentTs2();
			writeAngularComponentTsProperties();

			return;
		}

	}

	private static String toCamelCase(String s) {
		String[] parts = s.split("_");
		String camelCaseString = "";
		for (String part : parts) {
			camelCaseString = camelCaseString + "_" + toProperCase(part);
		}
		return camelCaseString.substring(1);
	}

	private static String toProperCase(String s) {
		return s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
	}

	private static String toLowerCase(String s) {
		return s.substring(0, 1).toLowerCase() + s.substring(1);
	}

	private static String toUpperCase(String s) {
		return s.substring(0, 1).toUpperCase() + s.substring(1);
	}
	
	private static String deleteKd(String s) {
		return s.substring(2);
	}
	private void writeFileMessage(Map<String, String> ldapContent) throws FileNotFoundException, IOException {
		// Map<String, String> ldapContent = new HashMap<String, String>();
		Properties properties = new Properties();

		for (Map.Entry<String, String> entry : ldapContent.entrySet()) {
			if (entry.getKey().equalsIgnoreCase("#")) {

			}
			properties.put(entry.getKey(), entry.getValue());
		}

		properties.store(new FileOutputStream("data.properties"), null);
	}

	private void writeCore() throws Exception {
		Column columns[] = table.getColumns();
		className = generateBeanClassName();
		String compositeClassName = className + "Id";
		String[] tableNameSplit = table.getName().split("_");
		String importData = "";
		if (tableNameSplit[1].equalsIgnoreCase("M") ||  tableNameSplit[1].equalsIgnoreCase("S")) {// Master
			pkg = basePackage + ".entity";
			importData = basePackage + ".entity.vo." + compositeClassName;
		//tableNameSplit[1].equalsIgnoreCase("S")) {// S
			pkg = basePackage + ".entity";
			importData = basePackage + ".entity.vo." + compositeClassName;
		} else { // T
			pkg = basePackage + ".entity";
			importData = basePackage + ".entity.vo." + compositeClassName;
		}

		initWriter();
		String[] importList = { " static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.*",
				" javax.persistence.*", " javax.validation.constraints.*", importData,

				" lombok.AllArgsConstructor", " lombok.Getter", " lombok.NoArgsConstructor", " lombok.Setter",
				"com.fasterxml.jackson.annotation.JsonIgnore",
				"com.fasterxml.jackson.annotation.JsonIgnoreProperties" };
		// " lombok.NonNull",
		// ini di lepas
		// " com.jasamedika.medifirst2000.base.entity.BaseMaster",

		// String tableName= toCamelCase(table.getName());
		String indexAnotation = "Table(name = \"" + table.getName() + "\")";

		for (Column column : columns) {
			if (toLowerCase(convertName(column.getName())).equalsIgnoreCase("statusEnabled")) {
				indexAnotation = "Table(name = \"" + table.getName() + "\",indexes = {@Index(name = \""
						+ table.getName() + "_Index\",  columnList=\"statusEnabled\", unique = false) })";
			}
		}
		String[] annotationLists = { "Entity", indexAnotation, "Getter", "Setter", "AllArgsConstructor",
				"NoArgsConstructor", "JsonIgnoreProperties({ \"hibernateLazyInitializer\", \"handler\" })" };

		String extendsClass = null;// "BaseMaster";

		writePreamble(importList, extendsClass, null, annotationLists);
		ArrayList<Column> columnComposites = new ArrayList<Column>();
		boolean isWasGenerated = false;
		ArrayList<String> Fk = new ArrayList<String>();
		for (Column column : columns) {
			if (convertName(column.getName()).startsWith("Kd") || convertName(column.getName()).startsWith("No")) {
				String name = "kd" + generateBeanClassName();
				// String nameVariablePrimaryKey= ?"kode":
				// toLowerCase(convertName(column.getName()));
				if (!(name).equalsIgnoreCase(convertName(column.getName()))) {
					Fk.add(convertName(column.getName()));
					// System.out.println("ColumnName :"+convertName(column.getName()));
				} /*
					 * if (column.getName().equalsIgnoreCase("kdGProduk")) {
					 * Fk.add(convertName(column.getName())); }
					 */

			}
			// System.out.println(column.toString());
			// Primary Key
			if (column.isPrimaryKey()) {
				columnComposites.add(column);
				//
				if (!isWasGenerated) {
					indent(1, "");
					indent(1, "@EmbeddedId");
					indent(1, "private " + compositeClassName + " id ;");
					indent(1, "");
					isWasGenerated = true;
				}
			} else {
				String nameVariable = (convertName(column.getName()).toLowerCase()
						.equalsIgnoreCase(className.toLowerCase())) ? "nama" + className
								: toLowerCase(convertName(column.getName()));
				// Tidak Sama dengan BaseMaster
				boolean isBaseMaster = false;
				/*
				 * toLowerCase(convertName(column.getName())).equalsIgnoreCase("statusEnabled")
				 * ||
				 * toLowerCase(convertName(column.getName())).equalsIgnoreCase("namaExternal")
				 * ||
				 * toLowerCase(convertName(column.getName())).equalsIgnoreCase("kodeExternal")
				 * ||
				 * toLowerCase(convertName(column.getName())).equalsIgnoreCase("reportDisplay")
				 * || toLowerCase(convertName(column.getName())).equalsIgnoreCase("noRec") ?
				 * true : false;
				 */
				if (!isBaseMaster) {
					// Char atau Varchar
					if (column.getDatabaseType().equalsIgnoreCase("CLOB")
							|| column.getDatabaseType().equalsIgnoreCase("CHAR")
							|| column.getDatabaseType().equalsIgnoreCase("VARCHAR")) {

						// cek apabila ada kata tgl ganti ke date
						int longData = column.getSize();
						/*
						 * if
						 * (toLowerCase(convertName(column.getName())).equalsIgnoreCase("namaExternal")
						 * ||
						 * toLowerCase(convertName(column.getName())).equalsIgnoreCase("reportDisplay"))
						 * { longData = 150; } if
						 * (toLowerCase(convertName(column.getName())).equalsIgnoreCase("kodeExternal"))
						 * { longData = 30; }
						 */
						if (convertName(column.getName()).indexOf("Tgl") >= 0) {
							indent(1, "private static final String cDef" + (convertName(column.getName()))
									+ " = INTEGER ;");
						} else {
							if (column.getSize() > 8000) {

								indent(1, "private static final String cDef" + (convertName(column.getName()))
										+ " = TEXT;");
							} else if (column.getDatabaseType().equalsIgnoreCase("CHAR")) {

								indent(1, "private static final String cDef" + (convertName(column.getName()))
										+ " = CHAR + AWAL_KURUNG + " + longData + " + AKHIR_KURUNG ;");
							} else {

								indent(1, "private static final String cDef" + (convertName(column.getName()))
										+ " = VARCHAR + AWAL_KURUNG + " + longData + " + AKHIR_KURUNG ;");
							}
						}

					} // Apabila Timestamp
					else if (column.getDatabaseType().equalsIgnoreCase("TIMESTAMP")) {

						indent(1,
								"private static final String cDef" + (convertName(column.getName())) + " = INTEGER ;");
						// indent(1, "@Temporal(TemporalType.TIMESTAMP)");
					} // Apabila ketemu sama blob dibuat nama file saja
					else if (column.getDatabaseType().equalsIgnoreCase("BLOB")) {
						indent(1, "private static final String cDef" + (convertName(column.getName()))
								+ " = VARCHAR + AWAL_KURUNG + " + 100 + " + AKHIR_KURUNG ;");
					/*	String nameMessage = className.toLowerCase() + "."
								+ (convertName(column.getName())).toLowerCase() + ".max";
						indent(1, "@Max(value = 100, message = \"" + nameMessage + "\")");*/
					} // Apabila ketemu sama double diganti sama BigDecimal biar lebih presisi
					else if (column.getDatabaseType().equalsIgnoreCase("DOUBLE")) {
						indent(1, "private static final String cDef" + (convertName(column.getName()))
								+ " = BIGDECIMAL + AWAL_KURUNG + " + "\"15,2\"" + " + AKHIR_KURUNG ;");
						// @Column(name = "max",precision=10, scale=2)
					} else if (column.getDatabaseType().equalsIgnoreCase("REAL")) {
						indent(1, "private static final String cDef" + (convertName(column.getName())) + " = FLOAT ;");
						// @Column(name = "max",precision=10, scale=2)
					} else {
						indent(1, "private static final String cDef" + (convertName(column.getName())) + " ="
								+ column.getDatabaseType() + ";");
						if (column.getDatabaseType().equalsIgnoreCase("VARCHAR")) {
							String nameMessage = className.toLowerCase() + "."
									+ (convertName(column.getName())).toLowerCase() + ".max";
							// indent(1, "@Max(value = 100, message = \"" + nameMessage + "\")");
							indent(1, "@Max(value = " + column.getSize() + ", message = \"" + nameMessage + "\")");

							// indent(1, "@Max(value = "+column.getSize()+", message = \""+
							// (convertName(column.getName())) +" max. "+column.getSize()+"\")");
						}

					}

					indent(1, "@Column(name = \"" + (convertName(column.getName())) + "\", columnDefinition = cDef"
							+ (convertName(column.getName())) + ")");

					if (column.getNullable() == 0) {
						String nameMessage = className.toLowerCase() + "."
								+ (convertName(column.getName()).toLowerCase()) + ".notnull";
						if (!column.getName().contains("Head")) {
							indent(1, "@NotNull(message = \"" + nameMessage + "\")");
						}
					}

					if (column.getJavaType() == null || column.getJavaType().equalsIgnoreCase("Clob")) {
						indent(1, "private String " + toLowerCase(convertName(column.getName())) + ";");
					} /*
						 * else if (column.getJavaType().equalsIgnoreCase("DOUBLE")) { indent(1,
						 * "private java.math.BigDecimal  " + toLowerCase(convertName(column.getName()))
						 * + ";"); } else if (column.getJavaType().equalsIgnoreCase("Float")) {
						 * indent(1, "private Double  " + toLowerCase(convertName(column.getName())) +
						 * ";"); }
						 */ else {
						// String nameVariable=
						// (convertName(column.getName()).toLowerCase().indexOf(className.toLowerCase()))>0
						// ?"nama"+className : toLowerCase(convertName(column.getName()));
						if (convertName(column.getName()).indexOf("Tgl") >= 0) {
							indent(1, "private Long  " + nameVariable + ";");
						} else {
							String tipedata = column.getJavaType().toString();
							if (toLowerCase(convertName(column.getName())).equalsIgnoreCase("statusEnabled")) {
								tipedata = "Boolean";
							}
							indent(1, "private " + tipedata + " " + nameVariable + ";");
						}

					}
					indent(1, "");
				}
			}
		}
		indent(1, "private static final String cDefVersion =SMALLINT;");
		indent(1, "@Version");
		indent(1, "@Column(name = \"version\", nullable = false,  columnDefinition = cDefVersion)");
		// + \", COMMENT 'Hibernate Version Required'\")");
		indent(1, "@NotNull(message = \"" + className.toLowerCase() + ".version.notnull\")");
		indent(1, "private Integer version;");

		// indent(1, "private boolean _isNew = true;");
		indent(1, "");
		
		///comment if database has relation
		
		indent(1, "@ManyToOne(fetch = FetchType.LAZY)");
		indent(1, "@JsonIgnore");
		indent(1, "@JoinColumns({");
		indent(1, "    @JoinColumn(name=\"KdProfile\", referencedColumnName=\"KdProfile\",insertable=false, updatable=false),");
		indent(1, "})");
		indent(1, " private Profile profile;");
				indent(1, "");
		indent(1, "@ManyToOne(fetch = FetchType.LAZY)");
		indent(1, "@JsonIgnore");
		indent(1, "@JoinColumns({");
		indent(1, "		@JoinColumn(name = \"KdProfile\", referencedColumnName = \"KdProfile\", insertable = false, updatable = false),");
		indent(1, "		@JoinColumn(name = \"KdDepartemen\", referencedColumnName = \"KdDepartemen\", insertable = false, updatable = false), })");
		indent(1, "private Departemen departemen;");

		// indent(1, "}");
		indent(0, "");

		writeFK(Fk);
		writeEnd();
		writeCompositeId(columnComposites);
	}

	private void writeFK(ArrayList<String> datas) {
		for (String data : datas) {

			Table tables[] = db.getTables();
			for (Table table : tables) {
				String entityName = generateBeanClassName(table.getName());
				Boolean isHead = false;
				String tempNama = data.replaceAll("Kd", "").replaceAll("Askep", "");
				if (tempNama.toLowerCase().contains("head")) {
					tempNama = tempNama.replaceAll("Head", "");
					isHead = true;
				}
				if (tempNama.equalsIgnoreCase("GProduk")) {
					tempNama = "GeneralProduk";
				}
				if (tempNama.equalsIgnoreCase("NoSK")) {
					tempNama = "SuratKeputusan";
				} else if (tempNama.equalsIgnoreCase("NoRegisterAset")) {
					tempNama = "RegistrasiAset";
				} else if (tempNama.equalsIgnoreCase("NoHistori") || tempNama.equalsIgnoreCase("NoOrder")
						|| tempNama.equalsIgnoreCase("NoVerifikasi") || tempNama.equalsIgnoreCase("NoRetur")
						|| tempNama.equalsIgnoreCase("NoResep") || tempNama.equalsIgnoreCase("NoPosting")
						|| tempNama.equalsIgnoreCase("NoPlanning") || tempNama.equalsIgnoreCase("NoPelayanan")
						|| tempNama.equalsIgnoreCase("NoKonfirmasi") || tempNama.equalsIgnoreCase("NoKirim")
						|| tempNama.equalsIgnoreCase("NoClosing")) {
					tempNama = "Struk" + tempNama.replaceAll("No", "");
				} else if (tempNama.toLowerCase().contains("kdstatus")) {
					tempNama = "Status";
				}
				if (entityName.equalsIgnoreCase(tempNama)) {

					indent(1, "");
					indent(1, "@ManyToOne(fetch= FetchType.LAZY)");
					indent(1, "@JsonIgnore");
					indent(1, "@JoinColumns({");
					Column[] columns = table.getColumns();
					for (Column column : columns) {
						if (column.isPrimaryKey()) {
							String fieldrelate = column.getName();
							String columnName = column.getName();
							if (column.getName().equalsIgnoreCase("NoRegisterAset")) {
								fieldrelate = "NoRegistrasiAset";
							}
							if (column.getName().toLowerCase().contains("kdstatus")) {
								fieldrelate = "KdStatus";
							}
							if (isHead && !column.getName().toLowerCase().contains("kdprofile")) {
								columnName = columnName + "Head";
							}
							indent(2, "@JoinColumn(name=\"" + columnName + "\", referencedColumnName=\"" + fieldrelate
									+ "\",insertable=false, updatable=false),");
						}
					}

					indent(1, "})");
					indent(1, "// @ForeignKey(name=\"none\")");
					if (entityName.equalsIgnoreCase("kotakabupaten") || entityName.equalsIgnoreCase("negara")
							|| entityName.equalsIgnoreCase("propinsi") || entityName.equalsIgnoreCase("desakelurahan")
							|| entityName.equalsIgnoreCase("kecamatan")) {
						indent(1, " private " + entityName + " obj" + entityName + ";");
					} else if (isHead) {
						indent(1, " private " + entityName + " " + toLowerCase(entityName) + "Head" + ";");
					} else {
						indent(1, " private " + entityName + " " + toLowerCase(entityName) + ";");
					}

					break;
				}
			}
		}

	}

	private void writeDTO() throws Exception {
		Column columns[] = table.getColumns();
		String entityClassName = generateBeanClassName();
		className = entityClassName + "Dto";

		pkg = basePackage + ".dto";
		initWriter();
		/*
		 * String[] importList = { " javax.validation.constraints.NotNull",
		 * " org.hibernate.validator.constraints.NotBlank",
		 * " com.jasamedika.medifirst2000.base.*", " javax.validation.constraints.Max",
		 * " lombok.Getter", " lombok.Setter" };
		 */
		String[] importList = { " javax.validation.constraints.NotNull", " lombok.Getter", " lombok.Setter" };

		String tableName = toCamelCase(table.getName());
		String[] annotationLists = { "Getter", "Setter" };
		String extendsClass = null;// "BaseMasterDto";
		String[] tableNameSplit = table.getName().split("_");
		String importData = "";
		String jenisTable = tableNameSplit[1].toString();
		/*
		 * if (tableNameSplit[1].equalsIgnoreCase("M") ||  tableNameSplit[1].equalsIgnoreCase("S")) {// Master pkg = basePackage +
		 * ".dto"; extendsClass = "BaseMasterIntegerDto"; } else if
		 * (tableNameSplit[1].equalsIgnoreCase("S")) {// S pkg = basePackage + ".dto";
		 * extendsClass = "BaseMasterIntegerDto"; } else { // T pkg = basePackage +
		 * ".dto"; extendsClass = "BaseMasterStringDto"; }
		 */
		writePreamble(importList, extendsClass, null, annotationLists);
		ArrayList<Column> columnComposites = new ArrayList<Column>();
		boolean isWasGenerated = false;
		messageList.put("#", entityClassName);

		messageBuffer.append("\n");
		messageBuffer.append("#" + entityClassName);
		messageBuffer.append("\n");
		for (Column column : columns) {
			// System.out.println(column.toString());
			// Primary Key
			if (column.isPrimaryKey()) {
				/*
				 * columnComposites.add(column); // if(!isWasGenerated){ indent(1,
				 * "@EmbeddedId"); indent(1, "private " + compositeClassName + " id ;");
				 * indent(1, ""); isWasGenerated = true; }
				 */
				if (!column.getName().equalsIgnoreCase("KdProfile")) {
					if ((column.getName().equalsIgnoreCase("Kd" + entityClassName)
							|| column.getName().equalsIgnoreCase("kdgproduk")) && jenisTable.equalsIgnoreCase("M")) {
						indent(1, "");
						indent(1, "private " + typePK + " kode;");
						indent(1, "");
					}
				}
			} else {
				String nameVariable = (convertName(column.getName()).toLowerCase()
						.equalsIgnoreCase(entityClassName.toLowerCase())) ? "nama" + entityClassName
								: toLowerCase(convertName(column.getName()));
				// Tidak Sama dengan BaseMaster
				boolean isBaseMaster = false;
				/*
				 * toLowerCase(convertName(column.getName())).equalsIgnoreCase("statusEnabled")
				 * ||
				 * toLowerCase(convertName(column.getName())).equalsIgnoreCase("namaExternal")
				 * ||
				 * toLowerCase(convertName(column.getName())).equalsIgnoreCase("kodeExternal")
				 * ||
				 * toLowerCase(convertName(column.getName())).equalsIgnoreCase("reportDisplay")
				 * || toLowerCase(convertName(column.getName())).equalsIgnoreCase("noRec") ?
				 * true : false;
				 */
				if (!isBaseMaster) {
					// Char atau Varchar
					int longData = column.getSize();

					if (!toLowerCase(convertName(column.getName())).equalsIgnoreCase("norec")
							&& !toLowerCase(convertName(column.getName())).equalsIgnoreCase("version")
							&& !toLowerCase(convertName(column.getName())).equalsIgnoreCase("kdDepartemen")) {

						if (toLowerCase(convertName(column.getName())).equalsIgnoreCase("namaExternal")
								|| toLowerCase(convertName(column.getName())).equalsIgnoreCase("reportDisplay")) {
							longData = 150;
						}
						if (toLowerCase(convertName(column.getName())).equalsIgnoreCase("kodeExternal")) {
							longData = 30;
						}
						if (column.getDatabaseType().equalsIgnoreCase("CLOB")
								|| column.getDatabaseType().equalsIgnoreCase("CHAR")
								|| column.getDatabaseType().equalsIgnoreCase("VARCHAR")) {
							// indent(1, "private static final String cDef"+ (convertName(column.getName()))
							// +" = VARCHAR + AWAL_KURUNG + "+ 100 +" + AKHIR_KURUNG ;");
							/*
							 * String nameMessage = entityClassName.toLowerCase() + "." +
							 * (convertName(column.getName())).toLowerCase() + ".max";
							 * 
							 * indent(1, "@Max(value = " + longData + ", message = \"" + nameMessage +
							 * "\")"); messageBuffer .append(nameMessage + "=" +
							 * convertName(column.getName()) + " max " + column.getSize());
							 * messageBuffer.append("\n");
							 */
						} // Apabila Timestamp
						else if (column.getDatabaseType().equalsIgnoreCase("TIMESTAMP")) {

							// indent(1, "private static final String cDef"+ (convertName(column.getName()))
							// +" = DATE ;");
							// indent(1, "@Temporal(TemporalType.TIMESTAMP)");
						} // Apabila ketemu sama blob dibuat nama file saja
						else if (column.getDatabaseType().equalsIgnoreCase("BLOB")) {
							// indent(1, "private static final String cDef"+ (convertName(column.getName()))
							// +" = VARCHAR + AWAL_KURUNG + "+ 100 +" + AKHIR_KURUNG ;");
						} // Apabila ketemu sama double diganti sama BigDecimal biar lebih presisi
						else if (column.getDatabaseType().equalsIgnoreCase("DOUBLE")) {
							// indent(1, "private static final String cDef"+ (convertName(column.getName()))
							// +" = BIGDECIMAL + AWAL_KURUNG + "+ "\"15,2\"" +" + AKHIR_KURUNG ;");
							// @Column(name = "max",precision=10, scale=2)
						} else {
							// indent(1, "private static final String cDef"+ (convertName(column.getName()))
							// +" ="+ column.getDatabaseType() +";");
						}

						// indent(1, "@Column(name = \""+ (convertName(column.getName())) +"\",
						// columnDefinition = cDef"+ (convertName(column.getName())) +")");

						if (column.getNullable() == 0) {
							String nameMessageBlank = entityClassName.toLowerCase() + "."
									+ (convertName(column.getName())).toLowerCase() + ".notblank";
							String nameMessage = entityClassName.toLowerCase() + "."
									+ (convertName(column.getName())).toLowerCase() + ".notnull";
							messageBuffer.append(nameMessage + "=" + convertName(column.getName()) + " Harus Di isi");
							messageBuffer.append("\n");
							// messageBuffer.append(nameMessageBlank+"="+convertName(column.getName())+"
							// Harus Di isi");
							// messageBuffer.append("\n");
							messageList.put(nameMessage, convertName(column.getName()) + " Harus Di isi");
							// messageList.put(nameMessageBlank, convertName(column.getName())+" Harus Di
							// isi");
							// indent(1, "@NotBlank(message = \""+ nameMessageBlank +"\")");
							if (!toLowerCase(convertName(column.getName())).equalsIgnoreCase("noRec")) {
								indent(1, "@NotNull(message = \"" + nameMessage + "\")");
							}
						}

						if (column.getJavaType() == null || column.getJavaType().equalsIgnoreCase("Clob")) {
							indent(1, "private String " + toLowerCase(convertName(column.getName())) + ";");
						} /*
							 * else if (column.getJavaType().equalsIgnoreCase("DOUBLE")) { indent(1,
							 * "private java.math.BigDecimal  " + toLowerCase(convertName(column.getName()))
							 * + ";"); } else if (column.getJavaType().equalsIgnoreCase("Float")) {
							 * indent(1, "private Double  " + toLowerCase(convertName(column.getName())) +
							 * ";"); }
							 */else {
							// String nameVariable=
							// (convertName(column.getName()).toLowerCase().indexOf(className.toLowerCase()))>0
							// ?"nama"+className : toLowerCase(convertName(column.getName()));
							if (convertName(column.getName()).indexOf("Tgl") >= 0) {
								indent(1, "private Long  " + nameVariable + ";");
							} else {
								String tipedata = column.getJavaType().toString();
								if (toLowerCase(convertName(column.getName())).equalsIgnoreCase("statusEnabled")) {
									tipedata = "Boolean";
								}
								indent(1, "private " + tipedata + " " + nameVariable + ";");
							}

						}
					}
					indent(1, "");
				}
			}
		}
		// indent(1, "private Integer version;");
		// indent(1, "private boolean _isNew = true;");
		indent(1, "");

		// indent(1, "}");
		indent(0, "");
		writeEnd();
		// writeCompositeId(columnComposites);
	}

	private void writeCompositeId(ArrayList<Column> columns) throws Exception {
		className = generateBeanClassName() + "Id";
		String tableNameSplit[] = table.getName().split("_");
		if (tableNameSplit[1].equalsIgnoreCase("M") ||  tableNameSplit[1].equalsIgnoreCase("S")) {// Master
			pkg = basePackage + ".entity.vo";
		//tableNameSplit[1].equalsIgnoreCase("S")) {// S
			pkg = basePackage + ".entity.vo";
		} else { // T
			pkg = basePackage + ".entity.vo";
		}
		// pkg = basePackage+".entity.vo";
		initWriter();
		String[] importList = { " static com.jasamedika.medifirst2000.base.adapter.DatabaseAdapter.Constant.*",
				" javax.validation.constraints.*", "  javax.validation.constraints.Max",
				"  javax.validation.constraints.Min", "  javax.validation.constraints.NotNull", " javax.persistence.*",
				" lombok.AllArgsConstructor", " lombok.Getter", " lombok.NoArgsConstructor", " lombok.Setter",
				" lombok.EqualsAndHashCode" };/*
												 * " lombok.NonNull",
												 * " com.jasamedika.medifirst2000.base.entity.BaseMaster",
												 * 
												 */
		String[] annotationLists = { "Embeddable", "Getter", "Setter", "AllArgsConstructor", "NoArgsConstructor",
				"EqualsAndHashCode" };
		String implementsdsClass[] = { "java.io.Serializable" };
		writePreamble(importList, null, implementsdsClass, annotationLists);
		indent(1, " /**");
		indent(1, " * ");
		indent(1, " */");
		for (Column column : columns) {
			// Char atau Varchar
			if (column.getDatabaseType().equalsIgnoreCase("CHAR")) {
				indent(1, "private static final String cDef" + (convertName(column.getName()))
						+ " = CHAR + AWAL_KURUNG + " + column.getSize() + " + AKHIR_KURUNG ;");
			} else if (column.getDatabaseType().equalsIgnoreCase("VARCHAR")) {
				indent(1, "private static final String cDef" + (convertName(column.getName()))
						+ " = VARCHAR + AWAL_KURUNG + " + column.getSize() + " + AKHIR_KURUNG ;");
			} // Apabila Timestamp
			else if (column.getDatabaseType().equalsIgnoreCase("TIMESTAMP")) {

				indent(1, "private static final String cDef" + (convertName(column.getName())) + " = DATE ;");

			} else {
				indent(1, "private static final String cDef" + (convertName(column.getName())) + " ="
						+ column.getDatabaseType() + ";");
			}
			indent(1, "@Column(name = \"" + (convertName(column.getName())) + "\",   columnDefinition = cDef"
					+ (convertName(column.getName())) + ")");
			String nameMessage = className.toLowerCase() + "." + (convertName(column.getName()).toLowerCase())
					+ ".notnull";

			indent(1, "@NotNull(message = \"" + nameMessage + "\")");
			String name = "kd" + generateBeanClassName();
			String nameVariablePrimaryKey = (name).equalsIgnoreCase(convertName(column.getName())) ? "kode"
					: toLowerCase(convertName(column.getName()));
			if (column.getName().equalsIgnoreCase("kdGProduk")) {
				nameVariablePrimaryKey = "kode";
			}
			indent(1, "private " + column.getJavaType() + " " + nameVariablePrimaryKey + ";");

			indent(1, "");
		}

		indent(1, "");

		indent(0, "");
		writeEnd();
	}

	//
	private void writeDao() throws Exception {
		Column columns[] = table.getColumns();

		String beanClass = generateBeanClassName();
		String beanCompositeClass = beanClass + "Id";
		className = beanClass + "Dao";
		pkg = basePackage + ".dao";
		initWriter();

		String[] importList = { " java.util.List", " java.util.Map", " org.springframework.cache.annotation.Cacheable",
				" org.springframework.data.domain.Page", " org.springframework.data.domain.Pageable",
				" org.springframework.data.jpa.repository.JpaRepository",
				" org.springframework.data.jpa.repository.Query", " org.springframework.data.repository.query.Param",
				" org.springframework.stereotype.Repository", " " + basePackage + ".entity.vo." + beanCompositeClass,
				" " + basePackage + ".entity." + beanClass };

		String tableName = toCamelCase(table.getName());
		String[] annotationLists = { "Repository(\"" + beanClass.toLowerCase() + "Dao\")", };
		String extendsClass = "JpaRepository<" + beanClass + ", " + beanCompositeClass + ">";

		writePreambleDao(importList, extendsClass, null, annotationLists);
		ArrayList<Column> columnComposites = new ArrayList<Column>();
		indent(0, "");
		indent(1, "@Cacheable(\"" + beanClass + "DaofindOneBy\")");
		indent(1, "" + beanClass + " findOneByIdKodeAndIdKdProfile(" + typePK + " kode, Integer kdProfile);");
		indent(1, "");
		indent(0, "");
		indent(1, "@Query(QListAll)");
		indent(1, "@Cacheable(\"" + beanClass + "DaoFindAllList\")");
		indent(1, "Page<Map<String, Object>> findAllList(@Param(\"kdProfile\") Integer kdProfile, Pageable page);");
		indent(1, "");
		indent(1, "@Query(QListAll + \" and model.nama" + beanClass + " like %:nama" + beanClass + "% \")");
		indent(1, "@Cacheable(\"" + beanClass + "DaoFindAllListPage\")");
		indent(1, "Page<Map<String, Object>> findAllList(@Param(\"kdProfile\") Integer kdProfile, @Param(\"nama"
				+ beanClass + "\") String nama" + beanClass + ", Pageable page);");
		indent(1, "");
		indent(1, "@Query(QListAll + \" and model.id.kode =:kode\")");
		indent(1, "@Cacheable(\"" + beanClass + "DaoFindByKode\")");
		indent(1, "Map<String, Object> findByKode(@Param(\"kdProfile\") Integer kdProfile, @Param(\"kode\") " + typePK
				+ " kode);");
		indent(1, "");
		indent(1, "@Query(\"select new map(model.id as kode, )  from "+ beanClass +" model  \"");
	    indent(1, "		+ \" where profile.kode =:kdProfile and model.statusEnabled=true\") ");
	    indent(1, " List<Map<String, Object>> findAllData(@Param(\"kdProfile\") Integer kdProfile);");
		indent(1, "");
		indent(1, "");
		indent(1, "String QListAll =\"select new map(model.id as kode \"");
		for (Column column : columns) {
			if (!column.isPrimaryKey()) {
				String fl = column.getName();
				if (column.getName().equalsIgnoreCase(beanClass.toLowerCase())) {
					fl = "Nama" + fl;
				}
				fl = fl.substring(0, 1).toLowerCase() + fl.substring(1).replace("_", "");
				indent(3, "+ \", model." + fl + " as " + fl + " \" ");
			}
		}
		indent(3, "+ \", model.version as version )\" ");
		indent(3, "+ \" from " + beanClass
				+ " model left join model.id id left join model.profile profile where profile.kode =:kdProfile \"; ");
		// dao with rest api
		/*
		 * indent(0, ""); indent(1, "@RestResource(path=\"PageById\")"); indent(1,
		 * "@ApiOperation(\"Cari Page berdasarkan id\")"); indent(1,
		 * " @ApiImplicitParams({ "); indent(1,
		 * "@ApiImplicitParam(name = \"id\", paramType = \"query\")"); indent(1, "})");
		 * indent(1, "Page<" + beanClass +
		 * "> findById(@Param(\"id\") @RequestParam(\"id\") " + beanCompositeClass +
		 * " id, Pageable page);");
		 * 
		 * for (Column column : columns) { indent(0, ""); //
		 * System.out.println(column.toString()); // Primary Key if
		 * (column.isPrimaryKey()) { String name = "kd" + generateBeanClassName();
		 * String nameVariablePrimaryKey =
		 * (name).equalsIgnoreCase(convertName(column.getName())) ? "Kode" :
		 * toLowerCase(convertName(column.getName()));
		 * 
		 * indent(1, "@RestResource(path=\"PageBy" + convertName(column.getName()) +
		 * "\")"); indent(1, "@ApiOperation(\"Cari Page berdasarkan " +
		 * convertName(column.getName()) + "\")"); indent(1, " @ApiImplicitParams({");
		 * indent(1, "     @ApiImplicitParam(name = \"" +
		 * toLowerCase(convertName(column.getName())) + "\", paramType = \"query\")");
		 * indent(1, " })"); indent(1, "Page<" + beanClass + "> findById" +
		 * toUpperCase(nameVariablePrimaryKey) + "(@Param(\"" +
		 * toLowerCase(convertName(nameVariablePrimaryKey)) + "\")  @RequestParam(\"" +
		 * toLowerCase(nameVariablePrimaryKey) + "\") Integer " +
		 * toLowerCase(nameVariablePrimaryKey) + ", Pageable page);");
		 * columnComposites.add(column); } else { //
		 * System.out.println((convertName(column.getName()).toLowerCase())) ; //
		 * System.out.println((className.toLowerCase())); String nameVariable =
		 * (convertName(column.getName()).toLowerCase()
		 * .equalsIgnoreCase(beanClass.toLowerCase())) ? "nama" + beanClass :
		 * toLowerCase(convertName(column.getName()));
		 * 
		 * // Tidak Sama dengan BaseMaster boolean isBaseMaster =
		 * toLowerCase(convertName(column.getName())).equalsIgnoreCase("statusEnabled")
		 * ||
		 * toLowerCase(convertName(column.getName())).equalsIgnoreCase("namaExternal")
		 * ||
		 * toLowerCase(convertName(column.getName())).equalsIgnoreCase("kodeExternal")
		 * ||
		 * toLowerCase(convertName(column.getName())).equalsIgnoreCase("reportDisplay")
		 * || toLowerCase(convertName(column.getName())).equalsIgnoreCase("noRec") ?
		 * true : false; if (!isBaseMaster) {
		 * 
		 * 
		 * if (column.getJavaType() == null ||
		 * column.getJavaType().equalsIgnoreCase("Clob")){ indent(1,
		 * "@RestResource(path=\"PageBy"+convertName(column.getName())+"\") ");
		 * indent(1,
		 * "@ApiOperation(\"Cari Page berdasarkan "+convertName(column.getName())+"\")")
		 * ; indent(1, "@ApiImplicitParams({"); indent(1, "@ApiImplicitParam(name = \""+
		 * toLowerCase(convertName(column.getName())) + "\", paramType = \"query\")");
		 * indent(1, "})"); indent(1,
		 * "Page<"+beanClass+"> findBy"+convertName(column.getName())+"(@Param(\""+
		 * toLowerCase(convertName(column.getName())) + "\")  @RequestParam(\""+
		 * toLowerCase(convertName(column.getName())) + "\") String "+
		 * toLowerCase(convertName(column.getName())) + ", Pageable page);");
		 * 
		 * }else if (column.getJavaType().equalsIgnoreCase("DOUBLE")){ indent(1,
		 * "@RestResource(path=\"PageBy"+convertName(column.getName())+"\") ");
		 * indent(1,
		 * "@ApiOperation(\"Cari Page berdasarkan "+convertName(column.getName())+"\")")
		 * ; indent(1, "@ApiImplicitParams({"); indent(1, "@ApiImplicitParam(name = \""+
		 * toLowerCase(convertName(column.getName())) + "\", paramType = \"query\")");
		 * indent(1, "})"); indent(1,
		 * "Page<"+beanClass+"> findBy"+convertName(column.getName())+"(@Param(\""+
		 * toLowerCase(convertName(column.getName())) + "\")  @RequestParam(\""+
		 * toLowerCase(convertName(column.getName())) + "\") java.math.BigDecimal "+
		 * toLowerCase(convertName(column.getName())) + ", Pageable page);");
		 * 
		 * }else{ indent(1,
		 * "@RestResource(path=\"PageBy"+toUpperCase(nameVariable)+"\") "); indent(1,
		 * "@ApiOperation(\"Cari Page berdasarkan "+toUpperCase(nameVariable)+"\")");
		 * indent(1, "@ApiImplicitParams({"); indent(1, "@ApiImplicitParam(name = \""+
		 * toLowerCase(nameVariable) + "\", paramType = \"query\")"); indent(1, "})");
		 * indent(1,
		 * "Page<"+beanClass+"> findBy"+toUpperCase(nameVariable)+"(@Param(\""+
		 * toLowerCase(nameVariable) + "\")  @RequestParam(\""+
		 * toLowerCase(nameVariable) + "\") "+column.getJavaType()+" "+
		 * toLowerCase(nameVariable) + ", Pageable page);");
		 * 
		 * }
		 * 
		 * } } // End Else
		 * 
		 * } if (!columnComposites.isEmpty()) { StringBuffer tempStringBuffer1 = new
		 * StringBuffer(); StringBuffer tempStringBuffer2 = new StringBuffer();
		 * StringBuffer tempStringBuffer3 = new StringBuffer(); StringBuffer
		 * tempStringBuffer4 = new StringBuffer(); for (Column column :
		 * columnComposites) { String name = "kd" + generateBeanClassName(); String
		 * nameVariablePrimaryKey =
		 * (name).equalsIgnoreCase(convertName(column.getName())) ? "Kode" :
		 * (convertName(column.getName()));
		 * 
		 * if (tempStringBuffer1.length() == 0 || tempStringBuffer2.length() == 0) {
		 * tempStringBuffer1.append("Id" + convertName(nameVariablePrimaryKey));
		 * tempStringBuffer2.append("Cari Page berdasarkan " +
		 * convertName(nameVariablePrimaryKey) + " ");
		 * 
		 * } else { tempStringBuffer1.append("AndId" +
		 * convertName(nameVariablePrimaryKey)); tempStringBuffer2.append(" And " +
		 * convertName(nameVariablePrimaryKey) + " "); }
		 * tempStringBuffer4.append("@Param(\"" +
		 * toLowerCase(convertName(nameVariablePrimaryKey)) + "\") @RequestParam(\"" +
		 * toLowerCase(convertName(nameVariablePrimaryKey)) + "\") " +
		 * column.getJavaType() + " " + toLowerCase(convertName(nameVariablePrimaryKey))
		 * + ","); tempStringBuffer3.append("@ApiImplicitParam(name = \"" +
		 * toLowerCase(convertName(nameVariablePrimaryKey)) +
		 * "\", paramType = \"query\"),"); } indent(0, ""); indent(1,
		 * "@RestResource(path=\"PageBy" + tempStringBuffer1.toString() + "\") ");
		 * indent(1, "@ApiOperation(\"Cari Page berdasarkan " +
		 * tempStringBuffer2.toString() + "\")"); indent(1, "@ApiImplicitParams({");
		 * indent(1, tempStringBuffer3.substring(0, tempStringBuffer3.length() - 1));
		 * indent(1, " })"); indent(1, "Page<" + beanClass + "> findBy" +
		 * tempStringBuffer1.toString() + "("); indent(1, tempStringBuffer4.toString());
		 * indent(1, "Pageable page);"); indent(0, ""); } // End if
		 */
		indent(0, "");
		writeEnd();

	}

	private void writeDaoCustom() throws Exception {
		Column columns[] = table.getColumns();

		String beanClass = generateBeanClassName();
		String beanCompositeClass = beanClass + "Id";
		className = beanClass + "DaoCustom";
		pkg = basePackage + ".dao.custom";
		initWriter();

		String[] importList = { "  java.util.List", "  java.util.Map", " com.jasamedika.medifirst2000.base.BaseDao",
				" com.jasamedika.medifirst2000.entity." + beanClass };

		String extendsClass = "BaseDao<" + beanClass + ">";

		writePreambleDao(importList, extendsClass, null, null);
		indent(0, "");
		indent(1, "int findByKdProfileCount(Integer kdProfile, String nama" + beanClass + ",Boolean status);");
		indent(0, "");
		indent(1,
				"List<Map<String,Object>> findByKdProfileList(Integer kdProfile, Integer page, Integer limit, String sort, String dir, int rowStart, int rowEnd,String namaAgama,Boolean status);");
		indent(0, "");
		writeEnd();

	}

	private void writeDaoCustomImpl() throws Exception {
		Column columns[] = table.getColumns();

		String beanClass = generateBeanClassName();
		String beanCompositeClass = beanClass + "Id";
		className = beanClass + "DaoCustomImpl";
		pkg = basePackage + ".dao.custom.impl";
		initWriter();

		String[] importList = { "  java.util.List", "  java.util.Map", "  org.springframework.stereotype.Repository",
				"  com.jasamedika.medifirst2000.base.v2.BaseDaoImpl",
				"  com.jasamedika.medifirst2000.dao.custom." + beanClass + "DaoCustom",
				"  com.jasamedika.medifirst2000.entity." + beanClass + "",
				"  com.jasamedika.medifirst2000.entity.vo." + beanClass + "Id",
				" com.jasamedika.medifirst2000.util.CommonUtil", "javax.persistence.Query" };

		String extendsClass = "BaseDaoImpl<" + beanClass + ",  " + beanClass + "Id>";
		String[] implementList = { "" + beanClass + "DaoCustom" };
		String[] annotationList = { "Repository(\"" + beanClass.toLowerCase() + "DaoCustom\")" };
		writePreamble(importList, extendsClass, implementList, annotationList);
		indent(0, "");

		indent(1, "@Override");
		indent(1, "protected Class<" + beanClass + "Id> getIdClass() {");
		indent(2, "	return " + beanClass + "Id.class;");
		indent(1, "}");
		indent(1, "");
		indent(1, "@Override");
		indent(1, "protected Class< " + beanClass + "> getDomainClass() {");
		indent(2, "	return " + beanClass + ".class;");
		indent(1, "}");

		indent(1, "@Override");
		indent(1, "public int findByKdProfileCount(Integer kdProfile,String nama" + beanClass + ", Boolean status) {");
		indent(2, "StringBuffer buffer = new StringBuffer();");
		indent(2, "buffer.append(\"select count(model) \");");
		indent(2, "buffer.append(\"from " + beanClass
				+ " model left join model.id id left join model.departemen departemen where id.kdProfile=:kdProfile and departemen.id.kdProfile=id.kdProfile \");");
		indent(2, "if(CommonUtil.isNotNullOrEmpty(status) && !status){");
		indent(3, "buffer.append(\" and model.statusEnabled=true and departemen.statusEnabled=true \");");
		indent(3, "buffer.append(\" and model.kdDepartemen=:kdDepartemen \");");
		indent(2, "}");
		indent(1, "if(CommonUtil.isNotNullOrEmpty(nama" + beanClass + ")){");
		indent(1, "buffer.append(\" and model.nama" + beanClass + " like '%\"" + "+ nama" + beanClass + "+\"%' \");");
		indent(1, "}");
		indent(2, "Query query = em.createQuery(buffer.toString());");
		indent(2, "query.setParameter(\"kdProfile\", kdProfile);");
		indent(2, "query.setParameter(\"kdDepartemen\", kdDepartemen);");
		// indent(1,"if(CommonUtil.isNotNullOrEmpty(namaAgama)){");
		// indent(1,"query.setParameter("namaAgama", "%"+ namaAgama.toLowerCase()
		// +"%");");
		// indent(1,"}");
		indent(2, "return ((Long) query.getSingleResult()).intValue();");
		indent(1, "}");
		indent(0, "");
		indent(1, "@Override");
		indent(1,
				"public List<Map<String, Object>> findByKdProfileList(Integer kdProfile, Integer page, Integer limit, String sort,");
		indent(1, "String dir, int rowStart, int rowEnd, String nama" + beanClass + ", Boolean status) {");
		indent(2, "StringBuffer buffer = new StringBuffer();");
		indent(2, "buffer.append(\"select new map(id.kode as kode \");");
		for (Column column : columns) {
			if (!column.isPrimaryKey()) {
				String fl = column.getName();
				if (column.getName().equalsIgnoreCase(beanClass.toLowerCase())) {
					fl = "Nama" + fl;
				}
				fl = fl.substring(0, 1).toLowerCase() + fl.substring(1).replace("_", "");
				indent(2, "buffer.append(\", model." + fl + " as " + fl + " \"); ");
			}
		}
		indent(2, "buffer.append(\", departemen.namaDepartemen as namaDepartemen \");");
		indent(2, "buffer.append(\") \");");
		indent(2, "buffer.append(\"from " + beanClass
				+ " model left join model.id id left join model.departemen departemen where id.kdProfile=:kdProfile and departemen.id.kdProfile=id.kdProfile \");");
		indent(2, "if(CommonUtil.isNotNullOrEmpty(status) && !status){");
		indent(3, "buffer.append(\" and model.statusEnabled=true and departemen.statusEnabled=true \");");
		indent(3, "buffer.append(\" and model.kdDepartemen=:kdDepartemen \");");
		indent(2, "}");
		indent(1, "if(CommonUtil.isNotNullOrEmpty(nama" + beanClass + ")){");
		indent(1, "buffer.append(\" and model.nama" + beanClass + " like '%\"" + "+ nama" + beanClass + "+\"%' \");");
		indent(1, "}");

		indent(2, "buffer.append(\"order by model.\" + dir + \" \" + sort);");
		indent(2, "Query query = em.createQuery(buffer.toString());");
		indent(2, "query.setParameter(\"kdProfile\", kdProfile);");
		indent(2, "query.setParameter(\"kdDepartemen\", kdDepartemen);");
		indent(2, "query.setFirstResult(rowStart);");
		indent(2, "query.setMaxResults(rowEnd);");

		indent(2, "List<Map<String,Object>> list = query.getResultList();");

		indent(2, "return list;");
		indent(1, "}");

		indent(0, "");
		writeEnd();

	}

	private void writeService() throws Exception {
		String beanClass = generateBeanClassName();
		className = beanClass + "Service";
		pkg = basePackage + ".service";
		String classDto = beanClass + "Dto";
		initWriter();

		String[] importList = { "  java.util.List", "  java.util.Map", " com.jasamedika.medifirst2000.base.BaseService",
				" com.jasamedika.medifirst2000.dto." + classDto,

				" org.springframework.data.domain.Page", " org.springframework.data.domain.PageRequest",
				" org.springframework.data.domain.Sort.Direction" };

		String extendsClass = "BaseService";

		writePreambleDao(importList, extendsClass, null, null);
		indent(1, "");
		indent(1, "Map<String, Object> save(" + classDto + " " + classDto.toLowerCase() + ");");
		indent(1, "");
		indent(1, "Map<String, Object> update(" + classDto + " " + classDto.toLowerCase() + ", Integer version);");
		indent(1, "");
		indent(1, "Map<String, Object> deleteByKode(" + typePK + " kode);");
		indent(1, "");
		indent(1, "Map<String, Object> findByKode(" + typePK + " kode);");
		indent(1, "");
		indent(1, "Map<String, Object> findAll(Integer page, Integer limit, String sort, String dir, String nama"
				+ beanClass + ");");

		indent(0, "");

		writeEnd();
	}

	private void writeServiceImpl() throws Exception {
		Column columns[] = table.getColumns();

		String beanClass = generateBeanClassName();
		String beanCompositeClass = beanClass + "Id";
		className = beanClass + "Service";
		pkg = basePackage + ".service";
		initWriter();

		String[] importList = { " java.util.HashMap", " java.util.Map",

				" org.springframework.beans.factory.annotation.Autowired", " org.springframework.stereotype.Service",
				" org.springframework.transaction.annotation.Transactional",

				" com.jasamedika.medifirst2000.base.BaseServiceImpl",
				" com.jasamedika.medifirst2000.dao." + beanClass + "Dao",
				" com.jasamedika.medifirst2000.dto." + beanClass + "Dto",
				" com.jasamedika.medifirst2000.entity." + beanClass + "",
				" com.jasamedika.medifirst2000.entity.vo." + beanClass + "Id",
				" com.jasamedika.medifirst2000.service." + beanClass + "Service", "org.springframework.beans.BeanUtils",
				"com.jasamedika.medifirst2000.util.CommonUtil", "org.springframework.data.domain.Page",
				"org.springframework.data.domain.PageRequest", "org.springframework.data.domain.Sort.Direction",
				"org.springframework.cache.annotation.CacheEvict","org.springframework.context.annotation.Lazy",
				"com.jasamedika.medifirst2000.exception.InfoException" };
		// " org.modelmapper.ModelMapper",
		String[] implementList = { "" + beanClass + "Service" };
		String[] annotationList = { "Service(\"" + toLowerCase(beanClass) + "Service\")" };
		String extendsClass = "BaseServiceImpl";

		writePreambleServiceImpl(importList, extendsClass, implementList, annotationList);
		indent(1, "");
		indent(1, "@Lazy");
		indent(1, "@Autowired");
		indent(1, "private " + beanClass + "Dao " + beanClass.toLowerCase() + "Dao;");
		indent(1, "");
		// indent(1, "private ModelMapper modelMapper = new ModelMapper();");

		// indent(1, "@Override");
		indent(1, "@Transactional");
		indent(1, "public Map<String, Object> save(" + beanClass + "Dto " + beanClass.toLowerCase() + "Dto) {");
		indent(2, "Map<String,Object> data=new HashMap<String,Object>();");
		indent(2, "" + beanClass + " " + beanClass.toLowerCase() + "=new " + beanClass + "();");
		indent(2, "BeanUtils.copyProperties(" + beanClass.toLowerCase() + "Dto," + beanClass.toLowerCase() + ");");
		indent(2, beanClass.toLowerCase() + ".setStatusEnabled(true);");
		indent(2, beanClass.toLowerCase() + ".setNoRec(generateUuid());");
		indent(2, beanClass.toLowerCase() + ".setVersion(1);");
		indent(2, beanClass.toLowerCase() + ".setKdDepartemen(getKdDepartemenFromSession());");
		indent(2, beanClass + "Id id=new " + beanClass + "Id();");
		String[] tableNameSplit = table.getName().split("_");
		String tmp = "";
		if (tableNameSplit[1].equalsIgnoreCase("M") ||  tableNameSplit[1].equalsIgnoreCase("S")) {// Master
			for (Column column : columns) {
				if (column.isPrimaryKey()) {
					if (!column.getName().equalsIgnoreCase("KdProfile")) {
						if (column.getName().equalsIgnoreCase("Kd" + beanClass)
								|| column.getName().equalsIgnoreCase("kdGProduk")) {
							if (column.getDatabaseType().equalsIgnoreCase("CLOB")
									|| column.getDatabaseType().equalsIgnoreCase("CHAR")
									|| column.getDatabaseType().equalsIgnoreCase("VARCHAR")) {
								tmp = ".toString()";
							}
						}
					}
				}
			}
			indent(2, "id.setKode(getIdTerahir(" + beanClass + ".class)" + tmp + ");");
		//tableNameSplit[1].equalsIgnoreCase("S")) {// S

		} else { // T

		}
		indent(2, "id.setKdProfile(getKdProfile());");
		indent(2, "" + beanClass.toLowerCase() + ".setId(id);");
		indent(2, "" + beanClass.toLowerCase() + "=" + beanClass.toLowerCase() + "Dao.save(" + beanClass.toLowerCase()
				+ ");");
		if (tableNameSplit[1].equalsIgnoreCase("M") ||  tableNameSplit[1].equalsIgnoreCase("S")) {// Master
			indent(2, "data.put(\"kd" + beanClass + "\", " + beanClass.toLowerCase() + ".getId().getKode());");
		//tableNameSplit[1].equalsIgnoreCase("S")) {// S

		} else { // T

		}
		indent(2, "data.put(\"message\", \"berhasil.ditambah\");");
		indent(2, "return data;");

		indent(1, "}");
		indent(1, "");
		// indent(1, "@Override");
		indent(1, "@Transactional");
		indent(1, "@CacheEvict(value = {\"" + beanClass + "DaofindOneBy\"}, allEntries = true) ");
		indent(1, "public Map<String, Object> update(" + beanClass + "Dto " + beanClass.toLowerCase() + "Dto) {");
		indent(2, "Map<String,Object> data=new HashMap<String,Object>();");
		indent(2, "" + beanClass + " " + beanClass.toLowerCase() + "= " + beanClass.toLowerCase()
				+ "Dao.findOneByIdKodeAndIdKdProfile(" + beanClass.toLowerCase() + "Dto.getKode(),getKdProfile());");
		indent(2, "if(CommonUtil.isNotNullOrEmpty(" + beanClass.toLowerCase() + ")) {");
		/*
		 * for (Column column : columns) { if (!column.isPrimaryKey()) { String fl =
		 * column.getName(); if
		 * (column.getName().equalsIgnoreCase(beanClass.toLowerCase())) { fl = "Nama" +
		 * fl; } indent(3, beanClass.toLowerCase() + ".set" +
		 * toUpperCase(fl).replace("_", "") + "(" + beanClass.toLowerCase() + "Dto.get"
		 * + toUpperCase(fl).replace("_", "") + "());");
		 * 
		 * } }
		 */
		indent(3, "BeanUtils.copyProperties(" + beanClass.toLowerCase() + "Dto, " + beanClass.toLowerCase() + ");");
		indent(3,
				beanClass.toLowerCase() + "=" + beanClass.toLowerCase() + "Dao.save(" + beanClass.toLowerCase() + ");");
		if (tableNameSplit[1].equalsIgnoreCase("M") ||  tableNameSplit[1].equalsIgnoreCase("S")) {// Master
			indent(2, "data.put(\"kd" + beanClass + "\", " + beanClass.toLowerCase() + ".getId().getKode());");
		//tableNameSplit[1].equalsIgnoreCase("S")) {// S

		} else { // T

		}
		indent(3, "data.put(\"message\", \"berhasil.diubah\");");
		indent(2, "}");
		indent(2, "else {");

		indent(3, " throw new InfoException(\"data tidak tersedia\"); ");
		indent(2, "}");

		indent(1, "");
		indent(2, "return data;");
		indent(1, "}");
		indent(1, "");
		// indent(1, "@Override");
		indent(1, "");
		indent(1, "@CacheEvict(value = {\"" + beanClass + "DaoFindAllList\",\"" + beanClass
				+ "DaoFindAllListPage\"}, allEntries = true) ");
		indent(1, "public Map<String, Object> findAll(Integer page, Integer limit, String sort, String dir, String nama"
				+ beanClass.toLowerCase() + ") {");

		indent(2, "Map<String, Object> result = CommonUtil.createMap();");
		indent(2, "Integer kdProfile = getKdProfile();");
		indent(2, "page --;");
		indent(2,
				"PageRequest pageReq = PageRequest.of(page, limit, \"desc\".equals(sort) ? Direction.DESC : Direction.ASC, dir);");
		indent(2, "Page<Map<String, Object>> pageRes;");

		indent(2, "if (CommonUtil.isNullOrEmpty(nama" + beanClass.toLowerCase() + ")) {");
		indent(3, "pageRes = " + beanClass.toLowerCase() + "Dao.findAllList(kdProfile, pageReq);");
		indent(2, "} else {");
		indent(3, "pageRes = " + beanClass.toLowerCase() + "Dao.findAllList(kdProfile, nama" + beanClass.toLowerCase()
				+ ", pageReq);");
		indent(2, "}");

		indent(2, "result.put(\"" + beanClass + "\", pageRes.getContent());");
		indent(2, "result.put(\"totalPages\", pageRes.getTotalPages());");
		indent(2, "result.put(\"totalRow\", pageRes.getTotalElements());");

		indent(1, "");
		indent(2, "return result;");
		indent(1, "}");
		indent(1, "");

		// indent(1, "@Override");
		indent(1, "");
		indent(1, "@CacheEvict(value = {\"" + beanClass + "DaoFindByKode\"}, allEntries = true) ");
		indent(1, "public Map<String, Object> findByKode(" + typePK + " kode) {");
		indent(2, "Map<String,Object> data=new HashMap<String,Object>();");
		indent(2, "Map<String,Object>" + beanClass.toLowerCase() + " = " + beanClass.toLowerCase()
				+ "Dao.findByKode(getKdProfile(), kode);");
		indent(2, "data.put(\"" + beanClass + "\", " + beanClass.toLowerCase() + ");");
		indent(2, "return data;");
		indent(1, "}");
		indent(1, "");
		// indent(1, "@Override");
		indent(1, "@Transactional");
		indent(1, "public Map<String, Object> deleteByKode(" + typePK + " kode) {");
		indent(2, "Map<String,Object> data=new HashMap<String,Object>();");
		indent(2, "" + beanClass + " " + beanClass.toLowerCase() + "= " + beanClass.toLowerCase()
				+ "Dao.findOneByIdKodeAndIdKdProfile(kode,getKdProfile());");
		indent(1, "");
		indent(2, "if(CommonUtil.isNotNullOrEmpty(" + beanClass.toLowerCase() + ")) {");
		indent(3, beanClass.toLowerCase() + ".setStatusEnabled(false);");
		indent(3, beanClass.toLowerCase() + "Dao.save(" + beanClass.toLowerCase() + ");");
		indent(3, "data.put(\"message\", \"berhasil.dihapus\");");
		indent(3, "data.put(\"kode\", " + beanClass.toLowerCase() + ".getId().getKode());");
		indent(2, "}else{");
		indent(3, " throw new InfoException(\"data tidak tersedia\"); ");
		indent(1, "}");
		indent(1, "");
		indent(2, "return data;");
		indent(1, "}");
	    
		indent(1, ""); 
		indent(1, "public Map<String, Object> findAllData() {");
		indent(1, "    Map<String,Object> data=new HashMap<String,Object>(); ");
		indent(1, "    data.put(\"data\", "+ beanClass.toLowerCase() +"Dao.findAllData(getKdProfile()));");
		indent(1, "    return data;");
		indent(1, "}");
		indent(0, "");
		writeEnd();

	}

	private void writeController() throws Exception {
		Column columns[] = table.getColumns();

		String beanClass = generateBeanClassName();
		String beanCompositeClass = beanClass + "Id";
		className = beanClass + "Controller";
		pkg = basePackage + ".controller";
		initWriter();

		String[] importList = { " java.util.Map",

				" javax.servlet.http.HttpServletRequest", " javax.validation.Valid",

				" org.springframework.beans.factory.annotation.Autowired",
				" org.springframework.web.bind.annotation.DeleteMapping",
				" org.springframework.web.bind.annotation.GetMapping",
				" org.springframework.web.bind.annotation.PathVariable",
				" org.springframework.web.bind.annotation.PostMapping",
				" org.springframework.web.bind.annotation.PutMapping",
				" org.springframework.web.bind.annotation.RequestBody",
				" org.springframework.web.bind.annotation.RequestMapping",
				" org.springframework.web.bind.annotation.RequestParam",
				" org.springframework.web.bind.annotation.RestController",
				" com.jasamedika.medifirst2000.base.LocaleController",
				" com.jasamedika.medifirst2000.dto." + beanClass + "Dto",
				" com.jasamedika.medifirst2000.service." + beanClass + "Service",
				" io.swagger.annotations.ApiOperation", "com.jasamedika.medifirst2000.security.AppPermission"

		};
		String[] implementList = null;
		String[] annotationList = { "SuppressWarnings(\"rawtypes\")", "RestController",
				"RequestMapping(\"/" + beanClass.toLowerCase() + "\")" };
		String extendsClass = "LocaleController";

		writePreambleServiceImpl(importList, extendsClass, implementList, annotationList);

		indent(1, "@Autowired");
		indent(1, "private " + beanClass + "Service " + beanClass.toLowerCase() + "Service;");
		indent(1, "");
		indent(1, "@SuppressWarnings(\"unchecked\")");
		indent(1, "@ApiOperation(\"Api Untuk Menyimpan " + beanClass + "\")");
		indent(1, "@AppPermission(AppPermission.SAVE)");
		indent(1, "@PostMapping(\"/save\")");
		indent(1, "public Map<String,Object> save(@Valid @RequestBody " + beanClass
				+ "Dto entity,HttpServletRequest request) {");
		indent(2, "Map<String, Object> result = " + beanClass.toLowerCase() + "Service.save(entity);");
		indent(2, "return result;");
		indent(1, "}");

		indent(1, "@SuppressWarnings(\"unchecked\")");
		indent(1, "@ApiOperation(\"Api Untuk Update " + beanClass + "\")");
		indent(1, "@AppPermission(AppPermission.UPDATE)");
		indent(1, "@PutMapping(\"/update\")");
		indent(1, "public Map<String, Object> update(@Valid @RequestBody " + beanClass
				+ "Dto entity, HttpServletRequest request) {");
		indent(2, "Map<String, Object> result = " + beanClass.toLowerCase() + "Service.update(entity);");
		indent(2, "return result;");
		indent(1, "}");
		indent(1, "");

		indent(1, "@SuppressWarnings(\"unchecked\")");
		indent(1, "@ApiOperation(\"Api Untuk Menghapus " + beanClass + " BerdasarkanKd" + beanClass + "\")");
		indent(1, "@AppPermission(AppPermission.DELETE)");
		indent(1, "@DeleteMapping(\"/del/{kode}\")");
		indent(1, "public Map<String, Object> deleteById(@PathVariable(\"kode\") " + typePK + " kode) {");
		indent(2, "Map<String, Object> result = " + beanClass.toLowerCase() + "Service.deleteByKode(kode);");
		indent(2, "return result;");
		indent(1, "}");

		indent(1, "");
		indent(1, "@SuppressWarnings(\"unchecked\")");
		indent(1, "@ApiOperation(\"Api Untuk Menampilkan Semua " + beanClass + "\")");
		indent(1, "@AppPermission(AppPermission.SPECIALS)");
		indent(1, "@GetMapping(\"/findAll\")");
		indent(1, "public Map<String,Object> findAll("
				+ "			@RequestParam(value = \"page\", defaultValue = \"1\") Integer page,"
				+ "			@RequestParam(value = \"rows\", defaultValue = \"10\") Integer rows,"
				+ "			@RequestParam(value = \"dir\", defaultValue = \"nama" + beanClass + "\") String dir,"
				+ "			@RequestParam(value = \"sort\", defaultValue = \"desc\") String sort,"
				+ "			@RequestParam(value = \"nama" + beanClass
				+ "\", defaultValue = \"\", required = false) String nama" + beanClass + ") {");
		indent(2, "Map<String, Object> result = " + beanClass.toLowerCase()
				+ "Service.findAll(page, rows, sort, dir, nama" + beanClass + ");");
		indent(2, "return result;");
		indent(1, "}");
		indent(1, "");

		indent(1, "@SuppressWarnings(\"unchecked\")");
		indent(1, "@ApiOperation(\"Api Untuk Menampilkan " + beanClass + " Berdasarkan kode\")");
		indent(1, "@AppPermission(AppPermission.SPECIALS)");
		indent(1, "@GetMapping(\"/findByKode/{kode}\")");
		indent(1, "public Map<String, Object> findByKode(@PathVariable(\"kode\") " + typePK
				+ " kode, HttpServletRequest request) {");
		indent(2, "Map<String, Object> result = " + beanClass.toLowerCase() + "Service.findByKode(kode);");
		indent(2, "return result;");
		indent(1, "}");
		indent(1, "");
		
		indent(1, "@SuppressWarnings(\"unchecked\")");
		indent(1, "@ApiOperation(\"Api Untuk Menampilkan " + beanClass.toLowerCase() + " Berdasarkan kode\")");
		indent(1, "@AppPermission(AppPermission.SPECIALS)");
		indent(1, "@GetMapping(\"/findAllData\")");
		indent(1, "public Map<String, Object> findAllData( HttpServletRequest request) {");
		indent(1, "    Map<String, Object> result = " + beanClass.toLowerCase() + "Service.findAllData();");
		indent(1, "    return result;");
		indent(1, "}");
		indent(0, "");
		writeEnd();

	}

	private void writeAngularModul2() throws Exception {
		Column columns[] = table.getColumns();

		String beanClass = generateBeanClassName();

		className = beanClass;

		String[] tableNameSplit = table.getName().split("_");
		if (tableNameSplit[1].equalsIgnoreCase("M") ||  tableNameSplit[1].equalsIgnoreCase("S")) {// Master
			pkg = basePackage + ".module.master";
		//tableNameSplit[1].equalsIgnoreCase("S")) {// S
			//pkg = basePackage + ".module.support";
		} else { // T
			pkg = basePackage + ".module.transaksi";
		}

		// pkg = basePackage +".module";
		initWriterAngular("module", "ts");

		indent(1, "import { NgModule }       from '@angular/core';");
		indent(1, "import { CommonModule } from '@angular/common';");
		indent(1, "import { RouterModule} from '@angular/router';");
		indent(1, "import {");
		indent(1, "PanelMenuModule,");
		indent(1, "TabMenuModule,   ");
		indent(1, "AutoCompleteModule,");
		indent(1, "ButtonModule,  ");
		indent(1, "PanelModule,    ");
		indent(1, "InputTextModule,");
		indent(1, "DataTableModule,  ");
		indent(1, "DialogModule,   ");
		indent(1, "SplitButtonModule,");
		indent(1, "TabViewModule,  ");
		indent(1, "AccordionModule,");
		indent(1, "SharedModule,");
		indent(1, "CalendarModule,");
		indent(1, "GrowlModule,");
		indent(1, "MultiSelectModule,");
		indent(1, "ListboxModule,");
		indent(1, "DropdownModule,");
		indent(1, "CheckboxModule,");
		indent(1, "MessagesModule,");
		indent(1, "PaginatorModule,");
		indent(1, "ConfirmDialogModule,");
		indent(1, "InputTextareaModule,");
		indent(1, "OverlayPanelModule,");
		indent(1, " TooltipModule,");
		indent(1, "} from 'primeng/primeng';");
		indent(1, "import {FormsModule, ReactiveFormsModule} from \"@angular/forms\";");
		indent(1, "import {HttpModule, JsonpModule} from \"@angular/http\";");
		indent(1, "import {MyBreadcrumbModule} from \"../../../components/my-breadcrumb/my-breadcrumb\";");
		indent(1, "import {" + beanClass + "Component} from \"./" + beanClass.toLowerCase() + ".component\";");

		indent(1, "@NgModule({");
		indent(1, "imports: [");
		indent(1, "FormsModule,");
		indent(1, "ReactiveFormsModule,");
		indent(1, "HttpModule,");
		indent(1, "JsonpModule,");
		indent(1, "CommonModule,");
		indent(1, "ReactiveFormsModule,");
		indent(1, "ButtonModule,");
		indent(1, "PanelModule,");
		indent(1, "InputTextModule,");
		indent(1, "DataTableModule,");
		indent(1, "DialogModule,");
		indent(1, "SharedModule,");
		indent(1, "CalendarModule,");
		indent(1, "GrowlModule,");
		indent(1, "MultiSelectModule,");
		indent(1, "DropdownModule,");
		indent(1, "CheckboxModule,");
		indent(1, "PaginatorModule,");
		indent(1, "TooltipModule,");
		indent(1, "OverlayPanelModule,");
		indent(1, "MyBreadcrumbModule,");
		indent(1, "RouterModule.forChild([");
		indent(1, "  { path:'',component:" + beanClass + "Component}");
		indent(1, " ])");
		indent(1, "],");
		indent(1, "declarations: [" + beanClass + "Component],");
		indent(1, " exports:[RouterModule]");
		indent(1, "})");
		indent(1, "export class " + beanClass + "Module { }");
		indent(1, "");
		indent(0, "");
		// indent(1, content.toString());

		indent(0, "");
		writeEndAngular();

	}

	private void writeAngularUnitTest2() throws Exception {
		Column columns[] = table.getColumns();

		String beanClass = generateBeanClassName();

		className = beanClass;

		String[] tableNameSplit = table.getName().split("_");
		if (tableNameSplit[1].equalsIgnoreCase("M") ||  tableNameSplit[1].equalsIgnoreCase("S")) {// Master
			pkg = basePackage + ".module.master";
		//tableNameSplit[1].equalsIgnoreCase("S")) {// S
			//pkg = basePackage + ".module.support";
		} else { // T
			pkg = basePackage + ".module.transaksi";
		}

		// pkg = basePackage +".module";
		initWriterAngular("spec", "ts");

		indent(1, "import { async, ComponentFixture, TestBed } from '@angular/core/testing';");
		indent(1, "import { " + beanClass + "Component } from './" + beanClass.toLowerCase() + ".component';");
		indent(0, "");
		indent(1, "describe('" + beanClass + "Component', () => {");
		indent(1, " let component: " + beanClass + "Component;");
		indent(1, " let fixture: ComponentFixture<" + beanClass + "Component>;");
		indent(0, "");
		indent(1, " beforeEach(async(() => {");
		indent(1, "  TestBed.configureTestingModule({");
		indent(1, "   declarations: [ " + beanClass + "Component ]");
		indent(1, " })");
		indent(1, "  .compileComponents();");
		indent(1, " }));");
		indent(0, "");
		indent(1, "  beforeEach(() => {");
		indent(1, "  fixture = TestBed.createComponent(" + beanClass + "Component);");
		indent(1, " component = fixture.componentInstance;");
		indent(1, " fixture.detectChanges();");
		indent(1, " });");
		indent(0, "");
		indent(1, " it('should be created', () => {");
		indent(1, "  expect(component).toBeTruthy();");
		indent(1, " });");
		indent(1, "});");

		indent(0, "");
		writeEndAngular();

	}

	private void writeAngularRoute2(Table tables[]) throws Exception {
		String filename = resolveFilenameAngular(pkg, "routing", "", "json");
		System.out.println("Generating " + filename);
		File file = new File(filename);
		(new File(file.getParent())).mkdirs();
		userCode = new UserCodeParser(filename);
		writer = new PrintWriter(new OutputStreamWriter(new FileOutputStream(filename)));
		for (Table table : tables) {
			String fileName = generateCoreClassName(renameTableClassName(table.getName()));
			;
			writer.println("{");
			writer.println(" path: '" + fileName.toLowerCase() + "',");
			writer.println("  loadChildren: '../../../" + fileName.toLowerCase() + "/" + fileName.toLowerCase()
					+ ".module#FormPrimengModule',");
			writer.println("  data: {preload: true}");
			writer.println(" },");
		}
		writer.println("");
		writer.println("");
		for (Table table : tables) {
			String fileName = generateCoreClassName(renameTableClassName(table.getName()));
			;
			writer.println("{");
			writer.println(" \"name\": \"" + fileName.toLowerCase() + "\",");
			writer.println("  \"link\": \"" + fileName.toLowerCase() + "\",");

			writer.println(" },");
		}

		writeEndAngular();

	}

	private void writeAngularService2() throws Exception {
		Column columns[] = table.getColumns();

		String beanClass = generateBeanClassName();

		className = beanClass;
		String[] tableNameSplit = table.getName().split("_");
		if (tableNameSplit[1].equalsIgnoreCase("M") ||  tableNameSplit[1].equalsIgnoreCase("S")) {// Master
			pkg = basePackage + ".module.master";
		//tableNameSplit[1].equalsIgnoreCase("S")) {// S
			//pkg = basePackage + ".module.support";
		} else { // T
			pkg = basePackage + ".module.transaksi";
		}
		initWriterAngular("service", "ts");

		indent(1, "import { Injectable } from '@angular/core';");
		indent(1, "import { Http, Response } from '@angular/http';");
		indent(1, "import { HttpClient } from '../../../global';     ");
		indent(1, "");
		indent(1, "@Injectable()");
		indent(1, "export class " + className + "Service {");
		indent(1, " constructor(private http: HttpClient){}");
		/*
		 * indent(1, " get"+className+"(){"); indent(1,
		 * " return this.http.get('/"+className.toLowerCase()+
		 * "/findAllByKdProfile?page='+page+'&rows='+rows)"); indent(1,
		 * "  .toPromise()"); indent(1, "   .then(res =>  res.json().data)"); indent(1,
		 * "  .then(data => {return data;});"); indent(1, "}");
		 * 
		 * indent(1, "}");
		 */

		indent(0, "");
		writeEndAngular();

	}
	
	private void writeAngularComponentTsProperties() throws Exception {
		Column columns[] = table.getColumns();
		String beanClass = generateBeanClassName();
		className = beanClass;
		String[] tableNameSplit = table.getName().split("_");

		initWriterAngular("properties", "ts");
		indent(0, "{ path: 'master-"+tableNameSplit[0].toLowerCase()+"', loadChildren : './page/master/master-data.module#"+tableNameSplit[0]+"Module'},");  
		indent(1, "");
	    indent(0, "import { "+tableNameSplit[0]+"Component } from './"+tableNameSplit[0].toLowerCase()+"/"+tableNameSplit[0].toLowerCase()+".component';");
	    indent(1, "");
	    indent(0, "@NgModule({");
	    indent(0, "	   declarations : ["+tableNameSplit[0]+"Component],");
	    indent(0, "	   imports: [ RouterModule.forChild([ {canActivate: [AuthGuard], path:'',component: "+tableNameSplit[0]+"Component} ]), SharedModule.forRoot()],");
	    indent(0, "	   exports: [ RouterModule ]");
	    indent(0, "	})");
	    indent(0, "	export class "+tableNameSplit[0]+"Module {}");
	    indent(0, "");
	    indent(1, "//id.json");
	    indent(1, "\"frm" + tableNameSplit[0] + "_title\":\"\",");
	    indent(1, "\"frm_Daftarnama"+className+"Pencarian\":\"\",");
	    indent(1, "\"frm_Daftar" + className+ "Title\":\"\",");
	    for (Column column : columns) {
	    	String nameVariable = (convertName(column.getName()).toLowerCase()
					.equalsIgnoreCase(className.toLowerCase())) ? "nama" + className : (convertName(column.getName()));
	    	if (!toLowerCase(convertName(column.getName())).equalsIgnoreCase("kd" + className)
					&& !toLowerCase(convertName(column.getName())).equalsIgnoreCase("kdProfile")
					&& !toLowerCase(convertName(column.getName())).equalsIgnoreCase("norec")
					&& !toLowerCase(convertName(column.getName())).equalsIgnoreCase("version")
					&& !toLowerCase(convertName(column.getName())).equalsIgnoreCase("kdDepartemen")
					&& !toLowerCase(convertName(column.getName())).equalsIgnoreCase("reportDisplay")
					&& !toLowerCase(convertName(column.getName())).equalsIgnoreCase("kodeExternal")
					&& !toLowerCase(convertName(column.getName())).equalsIgnoreCase("namaExternal")
					&& !toLowerCase(convertName(column.getName())).equalsIgnoreCase("statusEnabled")) {
	    		  indent(1, "\"frm" + toLowerCase(nameVariable) +"\":\"\"");
			}
	      
	    }
			
		writeEndAngular();
	}

	private void writeAngularComponentTs2() throws Exception {
		Column columns[] = table.getColumns();

		String beanClass = generateBeanClassName();

		className = beanClass;
		String[] tableNameSplit = table.getName().split("_");
		if (tableNameSplit[1].equalsIgnoreCase("M") ||  tableNameSplit[1].equalsIgnoreCase("S")) {// Master
			pkg = basePackage + ".module.master";
		//tableNameSplit[1].equalsIgnoreCase("S")) {// S
			//pkg = basePackage + ".module.support";
		} else { // T
			pkg = basePackage + ".module.transaksi";
		}
		initWriterAngular("component", "ts");
		indent(1, "import { Inject, forwardRef, Component, OnInit } from '@angular/core';");
		indent(1, "import { HttpClient } from '../../../global/service/HttpClient';");
		indent(1, "import { Observable } from 'rxjs/Rx';");
		indent(1, "import { " + tableNameSplit[0] + " } from './" + toLowerCase(tableNameSplit[0]) + ".interface';");
		indent(1, "import { Validators, FormControl, FormGroup, FormBuilder } from '@angular/forms';");
		indent(1,
				"import { LazyLoadEvent, ConfirmDialogModule, ConfirmationService, MenuItem } from 'primeng/primeng';");
		indent(1,
				"import { Configuration, AlertService, InfoService, FileService, AuthGuard, ReportService } from '../../../global';");

		indent(1, "@Component({");
		indent(1, "selector: 'app-" + tableNameSplit[0].toLowerCase() + "',");
		indent(1, "templateUrl: '" +  tableNameSplit[0].toLowerCase() + ".component.html',");
		indent(1, "styleUrls: ['" +  tableNameSplit[0].toLowerCase() + ".component.scss'],");
		indent(1, "providers: [ConfirmationService]");

		indent(1, "})");

		indent(1, "export class " + tableNameSplit[0] + "Component implements OnInit {");
		indent(1, "");
		indent(1, "  item: " + tableNameSplit[0] + " = new Inisial" + tableNameSplit[0] + "();");
		indent(1, "  selected: " + tableNameSplit[0] + ";");
		indent(1, "  listData: any[];");
		indent(1, "  dataDummy: {};");
		indent(1, "  versi: any;");
		indent(1, "  form: FormGroup;");
		indent(1, "  formAktif: boolean");
		indent(1, "  items: MenuItem[];");
		indent(1, "  pencarian: string;");
		indent(1, "  report: any;");
		indent(1, "  toReport: any;");
		indent(1, "  totalRecords: number;");
		indent(1, "  page: number;");
		indent(1, "  rows: number;");
		indent(1, "  codes: any[];");
		indent(1, "  kdprof:any;");
		indent(1, "  kddept:any;");
		indent(1, "  laporan: boolean = false;");
		indent(1, "  smbrFile:any; ");
		indent(1, "");
		indent(1, "  constructor(");
		indent(1, "    private alertService: AlertService,");
		indent(1, "    private httpService: HttpClient,");
		indent(1, "    private confirmationService: ConfirmationService,");
		indent(1, "    private fb: FormBuilder,");
		indent(1, "    private fileService: FileService,");
		indent(1, "    private authGuard: AuthGuard,");
		indent(1, "    @Inject(forwardRef(() => ReportService)) private print: ReportService) {");
		indent(1, "  }");

		indent(1, "  ngOnInit() {");
		indent(1, "    this.kdprof = this.authGuard.getUserDto().kdProfile;");
		indent(1, "    this.kddept = this.authGuard.getUserDto().kdDepartemen;");
		indent(1, "    if (this.page == undefined || this.rows == undefined) {");
		indent(1, "      this.page = Configuration.get().page;");
		indent(1, "      this.rows = Configuration.get().rows;");
		indent(1, "    }");
		indent(1, "    this.pencarian = ''; ");
		indent(1, "    this.formAktif = true;");
		indent(1, "    this.get(this.page, this.rows, this.pencarian);");
		indent(1, "    this.form = this.fb.group({");
		indent(1, "   'kode': new FormControl(null),");
		for (Column column : columns) {
			String nameVariable = (convertName(column.getName()).toLowerCase()
					.equalsIgnoreCase(className.toLowerCase())) ? "nama" + className : (convertName(column.getName()));
			if (!toLowerCase(convertName(column.getName())).equalsIgnoreCase("kd" + className)
					&& !toLowerCase(convertName(column.getName())).equalsIgnoreCase("kdProfile")
					&& !toLowerCase(convertName(column.getName())).equalsIgnoreCase("norec")
					&& !toLowerCase(convertName(column.getName())).equalsIgnoreCase("version")
					&& !toLowerCase(convertName(column.getName())).equalsIgnoreCase("kdDepartemen")) {

				if (column.getNullable() == 0) {
					indent(1, "   '" + toLowerCase(nameVariable) + "': new FormControl('',Validators.required),");
				} else {
					indent(1, "   '" + toLowerCase(nameVariable) + "': new FormControl(null),");
				}

			}

		}

		indent(1, "    });");
		indent(1,
				"    this.items = [																																																																																	");
		indent(1,
				"    {                                                                                                                                                                                                                                                                                                                                                ");
		indent(1,
				"      label: 'Pdf', icon: 'fa-file-pdf-o', command: () => {                                                                                                                                                                                                                                                                                          ");
		indent(1,
				"        this.downloadPdf();                                                                                                                                                                                                                                                                                                                          ");
		indent(1,
				"      }                                                                                                                                                                                                                                                                                                                                              ");
		indent(1,
				"    },                                                                                                                                                                                                                                                                                                                                               ");
		indent(1,
				"    {                                                                                                                                                                                                                                                                                                                                                ");
		indent(1,
				"      label: 'Exel', icon: 'fa-file-excel-o', command: () => {                                                                                                                                                                                                                                                                                       ");
		indent(1,
				"        this.downloadExel();                                                                                                                                                                                                                                                                                                                         ");
		indent(1,
				"      }                                                                                                                                                                                                                                                                                                                                              ");
		indent(1,
				"    }];                                                                                                                                                                                                                                                                                                                                              ");
		indent(1,
				"    this.getSmbrFile();                                                                                                                                                                                                                                                                                                                              ");
		indent(1,
				" }                                                                                                                                                                                                                                                                                                                                                   ");
		indent(1,
				"                                                                                                                                                                                                                                                                                                                                                     ");
		indent(1,
				"                                                                                                                                                                                                                                                                                                                                                     ");
		indent(1,
				"                                                                                                                                                                                                                                                                                                                                                     ");
		indent(1,
				"  getSmbrFile(){                                                                                                                                                                                                                                                                                                                                     ");
		indent(1,
				"		this.httpService.get(Configuration.get().dataMasterNew + '/profile/findProfile').subscribe(table => {                                                                                                                                                                                                                                       ");
		indent(1,
				"			this.smbrFile = Configuration.get().resourceFile + '/image/show/' + table.profile.gambarLogo + '?noProfile=true';                                                                                                                                                                                                                       ");
		indent(1,
				"		});                                                                                                                                                                                                                                                                                                                                         ");
		indent(1,
				"	}                                                                                                                                                                                                                                                                                                                                               ");
		indent(1,
				"                                                                                                                                                                                                                                                                                                                                                     ");
		indent(1,
				"                                                                                                                                                                                                                                                                                                                                                     ");
		indent(1,
				"  get(page: number, rows: number, search: any) {                                                                                                                                                                                                                                                                                        ");
		indent(1, "    this.httpService.get(Configuration.get().dataMasterNew + '/" +  tableNameSplit[0].toLowerCase()
				+ "/findAll?page=' + page + '&rows=' + rows + '&dir=nama" + tableNameSplit[0] + "&sort=desc&nama"
				+ tableNameSplit[0]
				+ "='+ search).subscribe(table => {                                                                                                      ");
		indent(1, "      this.listData = table." + tableNameSplit[0]
				+ ";                                                                                                                                                                                                                                                                                                   ");
		indent(1,
				"      this.totalRecords = table.totalRow;                                                                                                                                                                                                                                                                                                            ");
		indent(1,
				"                                                                                                                                                                                                                                                                                                                                                     ");
		indent(1,
				"    });                                                                                                                                                                                                                                                                                                                                              ");
		indent(1,
				"  }                                                                                                                                                                                                                                                                                                                                                  ");
		indent(1,
				"  loadPage(event: LazyLoadEvent) {                                                                                                                                                                                                                                                                                                                   ");
		indent(1,
				"    this.get((event.rows + event.first) / event.rows, event.rows, this.pencarian);                                                                                                                                                                                                                                                ");
		indent(1,
				"    this.page = (event.rows + event.first) / event.rows;                                                                                                                                                                                                                                                                                             ");
		indent(1,
				"    this.rows = event.rows;                                                                                                                                                                                                                                                                                                                          ");
		indent(1,
				"  }                                                                                                                                                                                                                                                                                                                                                  ");
		indent(1,
				"                                                                                                                                                                                                                                                                                                                                                     ");
		indent(1,
				"                                                                                                                                                                                                                                                                                                                                                     ");
		indent(1,
				"  cari() {                                                                                                                                                                                                                                                                                                                                           ");
		indent(1, "    this.httpService.get(Configuration.get().dataMasterNew + '/" +  tableNameSplit[0].toLowerCase()
				+ "/findAll?page=' + Configuration.get().page + '&rows=' + Configuration.get().rows + '&dir=nama"
				+ tableNameSplit[0] + "&sort=desc&nama" + tableNameSplit[0]
				+ "=' + this.pencarian).subscribe(table => {                                                     ");
		indent(1, "     this.listData = table." + tableNameSplit[0]
				+ ";                                                                                                                                                                                                                                                                                                    ");
		indent(1,
				"    });                                                                                                                                                                                                                                                                                                                                              ");
		indent(1,
				"  }                                                                                                                                                                                                                                                                                                                                                  ");
		indent(1,
				"  valuechange(newValue) {                                                                                                                                                                                                                                                                                                                            ");
		indent(1,
				"    this.report = newValue;                                                                                                                                                                                                                                                                                                                          ");
		indent(1,
				"  }                                                                                                                                                                                                                                                                                                                                                  ");
		indent(1,
				"  downloadExel() {                                                                                                                                                                                                                                                                                                                                   ");
		indent(1, "    this.httpService.get(Configuration.get().dataMasterNew + '/" +  tableNameSplit[0].toLowerCase()
				+ "/findAll?page='+this.page+'&rows='+this.rows+'&dir=nama" + tableNameSplit[0]
				+ "&sort=desc').subscribe(table => {                                                                                                                                       ");
		indent(1, "      this.listData = table." + tableNameSplit[0]
				+ ";                                                                                                                                                                                                                                                                                                   ");
		indent(1,
				"      this.codes = [];                                                                                                                                                                                                                                                                                                                               ");
		indent(1,
				"                                                                                                                                                                                                                                                                                                                                                     ");
		indent(1,
				"      for (let i = 0; i < this.listData.length; i++) {                                                                                                                                                                                                                                                                                               ");
		indent(1,
				"          this.codes.push({                                                                                                                                                                                                                                                                                                                          ");
		indent(1,
				"                                                                                                                                                                                                                                                                                                                                                     ");
		indent(1,
				"            kode: this.listData[i].kode.kode,                                                                                                                                                                                                                                                                                                        ");
		indent(1,
				"                                                                                                                                                                                                                                                                                                                                                     ");
		indent(1,
				"          })                                                                                                                                                                                                                                                                                                                                         ");
		indent(1,
				"     }                                                                                                                                                                                                                                                                                                                                               ");
		indent(1,
				"                                                                                                                                                                                                                                                                                                                                                     ");
		indent(1, "      this.fileService.exportAsExcelFile(this.codes, '" + toLowerCase(tableNameSplit[0])
				+ "');                                                                                                                                                                                                                                                          ");
		indent(1,
				"    });                                                                                                                                                                                                                                                                                                                                              ");
		indent(1,
				"                                                                                                                                                                                                                                                                                                                                                     ");
		indent(1,
				"  }                                                                                                                                                                                                                                                                                                                                                  ");
		indent(1,
				"                                                                                                                                                                                                                                                                                                                                                     ");
		indent(1,
				"  downloadPdf() {                                                                                                                                                                                                                                                                                                                                    ");
		indent(1, "    let cetak = Configuration.get().report + '/" + toLowerCase(tableNameSplit[0]) + "/laporan"
				+ tableNameSplit[0]
				+ ".pdf?kdDepartemen='+this.kddept+'&kdProfile='+this.kdprof+'&gambarLogo=' + this.smbrFile +'&download=true';                                                                                                                            ");
		indent(1,
				"    window.open(cetak);                                                                                                                                                                                                                                                                                                                              ");
		indent(1,
				"  }                                                                                                                                                                                                                                                                                                                                                  ");
		indent(1,
				"                                                                                                                                                                                                                                                                                                                                                     ");
		indent(1,
				"  confirmDelete() {                                                                                                                                                                                                                                                                                                                                  ");
		indent(1,
				"    let kode = this.form.get('kode').value;                                                                                                                                                                                                                                                                                                          ");
		indent(1,
				"    if (kode == null || kode == undefined || kode == \"\") {                                                                                                                                                                                                                                                                                           ");
		indent(1, "      this.alertService.warn('Peringatan', 'Pilih Daftar Master " + tableNameSplit[0]
				+ "');                                                                                                                                                                                                                                                             ");
		indent(1,
				"    } else {                                                                                                                                                                                                                                                                                                                                         ");
		indent(1,
				"      this.confirmationService.confirm({                                                                                                                                                                                                                                                                                                             ");
		indent(1,
				"        message: 'Apakah data akan di hapus?',                                                                                                                                                                                                                                                                                                       ");
		indent(1,
				"        header: 'Konfirmasi Hapus',                                                                                                                                                                                                                                                                                                                  ");
		indent(1,
				"        icon: 'fa fa-trash',                                                                                                                                                                                                                                                                                                                         ");
		indent(1,
				"       accept: () => {                                                                                                                                                                                                                                                                                                                               ");
		indent(1,
				"         this.hapus();                                                                                                                                                                                                                                                                                                                               ");
		indent(1,
				"        },                                                                                                                                                                                                                                                                                                                                           ");
		indent(1,
				"        reject: () => {                                                                                                                                                                                                                                                                                                                              ");
		indent(1,
				"          this.alertService.warn('Peringatan', 'Data Tidak Dihapus');                                                                                                                                                                                                                                                                                ");
		indent(1,
				"        }                                                                                                                                                                                                                                                                                                                                            ");
		indent(1,
				"      });                                                                                                                                                                                                                                                                                                                                            ");
		indent(1,
				"    }                                                                                                                                                                                                                                                                                                                                                ");
		indent(1,
				"  }                                                                                                                                                                                                                                                                                                                                                  ");
		indent(1,
				"                                                                                                                                                                                                                                                                                                                                                     ");
		indent(1,
				"  hapus() {                                                                                                                                                                                                                                                                                                                                          ");
		indent(1,
				"   let item = [...this.listData];                                                                                                                                                                                                                                                                                                                    ");
		indent(1,
				"    let deleteItem = item[this.findSelectedIndex()];                                                                                                                                                                                                                                                                                                 ");
		indent(1, "    this.httpService.delete(Configuration.get().dataMasterNew + '/" +  tableNameSplit[0].toLowerCase()
				+ "/del/' + deleteItem.kode.kode).subscribe(response => {                                                                                                                                                                                           ");
		indent(1,
				"      this.alertService.success('Berhasil', 'Data Dihapus');                                                                                                                                                                                                                                                                                         ");
		indent(1,
				"      this.reset();                                                                                                                                                                                                                                                                                                                                  ");
		indent(1,
				"    });                                                                                                                                                                                                                                                                                                                                              ");
		indent(1,
				"                                                                                                                                                                                                                                                                                                                                                     ");
		indent(1,
				" }                                                                                                                                                                                                                                                                                                                                                  ");
		indent(1,
				"                                                                                                                                                                                                                                                                                                                                                     ");
		indent(1,
				"  findSelectedIndex(): number {                                                                                                                                                                                                                                                                                                                      ");
		indent(1,
				"    return this.listData.indexOf(this.selected);                                                                                                                                                                                                                                                                                                     ");
		indent(1,
				"  }                                                                                                                                                                                                                                                                                                                                                  ");
		indent(1,
				"                                                                                                                                                                                                                                                                                                                                                     ");
		indent(1,
				" reset() {                                                                                                                                                                                                                                                                                                                                          ");
		indent(1,
				"   this.ngOnInit();                                                                                                                                                                                                                                                                                                                                 ");
		indent(1,
				"  }                                                                                                                                                                                                                                                                                                                                                  ");
		indent(1,
				"                                                                                                                                                                                                                                                                                                                                                     ");
		indent(1,
				"  confirmUpdate() {                                                                                                                                                                                                                                                                                                                                  ");
		indent(1,
				"   this.confirmationService.confirm({                                                                                                                                                                                                                                                                                                                ");
		indent(1,
				"      message: 'Apakah data akan diperbaharui?',                                                                                                                                                                                                                                                                                                     ");
		indent(1,
				"     header: 'Konfirmasi Pembaharuan',                                                                                                                                                                                                                                                                                                              ");
		indent(1,
				"      accept: () => {                                                                                                                                                                                                                                                                                                                               ");
		indent(1,
				"        this.update();                                                                                                                                                                                                                                                                                                                              ");
		indent(1,
				"      },                                                                                                                                                                                                                                                                                                                                            ");
		indent(1,
				"      reject: () => {                                                                                                                                                                                                                                                                                                                               ");
		indent(1,
				"        this.alertService.warn('Peringatan', 'Data Tidak Diperbaharui');                                                                                                                                                                                                                                                                            ");
		indent(1,
				"      }                                                                                                                                                                                                                                                                                                                                             ");
		indent(1,
				"    });                                                                                                                                                                                                                                                                                                                                             ");
		indent(1,
				"  }                                                                                                                                                                                                                                                                                                                                                 ");
		indent(1,
				"                                                                                                                                                                                                                                                                                                                                                    ");
		indent(1,
				"  update() {                                                                                                                                                                                                                                                                                                                                        ");
		indent(1, "    this.httpService.update(Configuration.get().dataMasterNew + '/" +  tableNameSplit[0].toLowerCase()
				+ "/update', this.form.value).subscribe(response => {                                                                                                                                                                                              ");
		indent(1,
				"      this.alertService.success('Berhasil', 'Data Diperbarui');                                                                                                                                                                                                                                                                                     ");
		indent(1,
				"      // this.get(this.page, this.rows, this.pencarian);                                                                                                                                                                                                                                                                                            ");
		indent(1,
				"      this.reset();                                                                                                                                                                                                                                                                                                                                 ");
		indent(1,
				"    });                                                                                                                                                                                                                                                                                                                                             ");
		indent(1,
				"  }                                                                                                                                                                                                                                                                                                                                                 ");
		indent(1,
				"                                                                                                                                                                                                                                                                                                                                                    ");
		indent(1,
				"  simpan() {                                                                                                                                                                                                                                                                                                                                        ");
		indent(1,
				"    if (this.formAktif == false) {                                                                                                                                                                                                                                                                                                                  ");
		indent(1,
				"      this.confirmUpdate()                                                                                                                                                                                                                                                                                                                          ");
		indent(1,
				"    } else {                                                                                                                                                                                                                                                                                                                                        ");
		indent(1, "      this.httpService.post(Configuration.get().dataMasterNew + '/" +  tableNameSplit[0].toLowerCase()
				+ "/save', this.form.value).subscribe(response => {                                                                                                                                                                                                ");
		indent(1,
				"        this.alertService.success('Berhasil', 'Data Disimpan');                                                                                                                                                                                                                                                                                     ");
		indent(1,
				"        // this.get(this.page, this.rows, this.pencarian);                                                                                                                                                                                                                                                                                          ");
		indent(1,
				"        this.reset();                                                                                                                                                                                                                                                                                                                               ");
		indent(1,
				"      });                                                                                                                                                                                                                                                                                                                                           ");
		indent(1,
				"    }                                                                                                                                                                                                                                                                                                                                               ");
		indent(1,
				"                                                                                                                                                                                                                                                                                                                                                    ");
		indent(1,
				"  }                                                                                                                                                                                                                                                                                                                                                 ");
		indent(1,
				"  validateAllFormFields(formGroup: FormGroup) {                                                                                                                                                                                                                                                                                                     ");
		indent(1,
				"    Object.keys(formGroup.controls).forEach(field => {                                                                                                                                                                                                                                                                                              ");
		indent(1,
				"      const control = formGroup.get(field);                                                                                                                                                                                                                                                                                                         ");
		indent(1,
				"      if (control instanceof FormControl) {                                                                                                                                                                                                                                                                                                         ");
		indent(1,
				"        control.markAsTouched({ onlySelf: true });                                                                                                                                                                                                                                                                                                  ");
		indent(1,
				"      } else if (control instanceof FormGroup) {                                                                                                                                                                                                                                                                                                    ");
		indent(1,
				"        this.validateAllFormFields(control);                                                                                                                                                                                                                                                                                                        ");
		indent(1,
				"      }                                                                                                                                                                                                                                                                                                                                             ");
		indent(1,
				"    });                                                                                                                                                                                                                                                                                                                                             ");
		indent(1,
				"  }                                                                                                                                                                                                                                                                                                                                                 ");
		indent(1,
				"  onSubmit() {                                                                                                                                                                                                                                                                                                                                      ");
		indent(1,
				"    if (this.form.invalid) {                                                                                                                                                                                                                                                                                                                        ");
		indent(1,
				"      this.validateAllFormFields(this.form);                                                                                                                                                                                                                                                                                                        ");
		indent(1,
				"      this.alertService.warn(\"Peringatan\", \"Data Tidak Sesuai\")                                                                                                                                                                                                                                                                                     ");
		indent(1,
				"    } else {                                                                                                                                                                                                                                                                                                                                        ");
		indent(1,
				"      this.simpan();                                                                                                                                                                                                                                                                                                                                ");
		indent(1,
				"    }                                                                                                                                                                                                                                                                                                                                               ");
		indent(1,
				"  }                                                                                                                                                                                                                                                                                                                                                 ");
		indent(1,
				"  onRowSelect(event) {                                                                                                                                                                                                                                                                                                                              ");
		indent(1,
				"    this.formAktif = false;                                                                                                                                                                                                                                                                                                                         ");
		indent(1,
				"    let cloned = this.clone(event.data);                                                                                                                                                                                                                                                                                                            ");
		indent(1,
				"    this.form.setValue(cloned);                                                                                                                                                                                                                                                                                                                     ");
		indent(1,
				"  }                                                                                                                                                                                                                                                                                                                                                 ");
		indent(1, "  clone(cloned: " + tableNameSplit[0] + "): " + tableNameSplit[0]
				+ " {                                                                                                                                                                                                                                                                                                                       ");
		indent(1, "    let hub = new Inisial" + tableNameSplit[0]
				+ "();                                                                                                                                                                                                                                                                                                   ");
		indent(1,
				"    for (let prop in cloned) {                                                                                                                                                                                                                                                                                                                      ");
		indent(1,
				"      hub[prop] = cloned[prop];                                                                                                                                                                                                                                                                                                                     ");
		indent(1,
				"    }                                                                                                                                                                                                                                                                                                                                               ");
		indent(1, "    let fixHub = new Inisial" + tableNameSplit[0]
				+ "();                                                                                                                                                                                                                                                                                                ");
		indent(1,
				"    fixHub = {                                                                                                                                                                                                                                                                                                                                      ");
		indent(1, "     \"kode\": hub.kode.kode,   ");

		for (Column column : columns) {
			String nameVariable = (convertName(column.getName()).toLowerCase()
					.equalsIgnoreCase(className.toLowerCase())) ? "nama" + className : (convertName(column.getName()));
			if (!toLowerCase(convertName(column.getName())).equalsIgnoreCase("kd" + className)
					&& !toLowerCase(convertName(column.getName())).equalsIgnoreCase("kdprofile")
					&& !toLowerCase(convertName(column.getName())).equalsIgnoreCase("norec")
					&& !toLowerCase(convertName(column.getName())).equalsIgnoreCase("version")
					&& !toLowerCase(convertName(column.getName())).equalsIgnoreCase("kdDepartemen")) {
				indent(1, "     \"" + toLowerCase(nameVariable) + "\": hub." + toLowerCase(nameVariable) + ",   ");
			}

		}
		indent(1,
				"    }                                                                                                                                                                                                                                                                                                                                               ");
		indent(1,
				"    return fixHub;                                                                                                                                                                                                                                                                                                                                  ");
		indent(1,
				"  }                                                                                                                                                                                                                                                                                                                                                 ");
		indent(1,
				"                                                                                                                                                                                                                                                                                                                                                    ");
		indent(1,
				"  cetak() {                                                                                                                                                                                                                                                                                                                                         ");
		indent(1,
				"    this.laporan = true;                                                                                                                                                                                                                                                                                                                            ");
		indent(1, "    this.print.showEmbedPDFReport(Configuration.get().report + '/" + toLowerCase(tableNameSplit[0])
				+ "/laporan" + tableNameSplit[0]
				+ ".pdf?kdDepartemen='+this.kddept+'&kdProfile='+this.kdprof+'&gambarLogo=' + this.smbrFile +'&download=false', 'frm"
				+ tableNameSplit[0]
				+ "(_laporanCetak');                                                                               ");
		indent(1,
				"}                                                                                                                                                                                                                                                                                                                                                   ");
		indent(1,
				"tutupLaporan() {                                                                                                                                                                                                                                                                                                                                    ");
		indent(1,
				"  this.laporan = false;                                                                                                                                                                                                                                                                                                                             ");
		indent(1,
				"}                                                                                                                                                                                                                                                                                                                                                   ");
		indent(1,
				"                                                                                                                                                                                                                                                                                                                                                    ");
		indent(1,
				"                                                                                                                                                                                                                                                                                                                                                    ");
		indent(1,
				"}                                                                                                                                                                                                                                                                                                                                                   ");
		indent(1, "class Inisial" + tableNameSplit[0] + " implements " + tableNameSplit[0]
				+ " {                                                                                                                                                                                                                                                                               ");
		indent(1,
				"  constructor(                                                                                                                                                                                                                                                                                                                                      ");
		indent(1,
				"    public kode?,                                                                                                                                                                                                                                                                                                                                   ");

		for (Column column : columns) {
			String nameVariable = (convertName(column.getName()).toLowerCase()
					.equalsIgnoreCase(className.toLowerCase())) ? "nama" + className : (convertName(column.getName()));
			if (!toLowerCase(convertName(column.getName())).equalsIgnoreCase("kd" + className)
					&& !toLowerCase(convertName(column.getName())).equalsIgnoreCase("kdProfile")
					&& !toLowerCase(convertName(column.getName())).equalsIgnoreCase("norec")
					&& !toLowerCase(convertName(column.getName())).equalsIgnoreCase("version")
					&& !toLowerCase(convertName(column.getName())).equalsIgnoreCase("kdDepartemen")) {
				indent(1, "     public " + toLowerCase(nameVariable) + "?,   ");
			}

		}
		indent(1,
				"    )                                                                                                                                                                                                                                                                                                                                               ");
		indent(1,
				"  { }                                                                                                                                                                                                                                                                                                                                               ");
		indent(1,
				"}                                                                                                                                                                                                                                                                                                                                                   ");
		indent(1,
				"                                                                                                                                                                                        ");
		indent(0, "");
		writeEndAngular();
	}

	/*
	 * private void writeAngularComponentTs2() throws Exception { Column columns[] =
	 * table.getColumns();
	 * 
	 * String beanClass = generateBeanClassName();
	 * 
	 * className = beanClass; String[] tableNameSplit = table.getName().split("_");
	 * if (tableNameSplit[1].equalsIgnoreCase("M") ||  tableNameSplit[1].equalsIgnoreCase("S")) {// Master pkg = basePackage +
	 * ".module.master"; //tableNameSplit[1].equalsIgnoreCase("S")) {// S
	 * //pkg = basePackage + ".module.support"; } else { // T pkg = basePackage +
	 * ".module.transaksi"; } initWriterAngular("component", "ts");
	 * 
	 * indent(1, "import { Component, OnInit } from '@angular/core';"); indent(1,
	 * "import { HttpClient} from '../../../global/service/HttpClient';"); indent(1,
	 * "import { Observable } from 'rxjs/Rx';"); indent(1, "import { " + className +
	 * " } from './" + className.toLowerCase() + ".interface';"); indent(1,
	 * "import { Validators,FormControl,FormGroup,FormBuilder} from '@angular/forms';"
	 * ); indent(1,
	 * "import { LazyLoadEvent, Message, ConfirmDialogModule, ConfirmationService } from 'primeng/primeng';"
	 * ); indent(1,
	 * "import { AlertService,  InfoService, Configuration } from '../../../global';"
	 * ); indent(1, "@Component({"); indent(1, "selector: 'app-" +
	 * className.toLowerCase() + "',"); indent(1, "templateUrl: './" +
	 * className.toLowerCase() + ".component.html',"); indent(1, "styleUrls: ['./" +
	 * className.toLowerCase() + ".component.scss'],"); indent(1,
	 * "providers: [ConfirmationService]"); indent(1, "})"); indent(1,
	 * "export class " + className + "Component implements OnInit {"); indent(1,
	 * "item : " + className + " = new Inisial" + className + "();;"); indent(1,
	 * "selected: " + className + ";"); indent(1, "listData: any[];"); indent(1,
	 * "dataDummy: {};"); indent(1, "versi: any;"); indent(1, "form" + className +
	 * ": FormGroup;"); indent(1,
	 * "constructor(private alertService : AlertService,"); indent(1,
	 * "private InfoService: InfoService,"); indent(1,
	 * "private httpService: HttpClient,"); indent(1,
	 * "private confirmationService: ConfirmationService,"); indent(1,
	 * "private fb: FormBuilder) { }"); indent(1, ""); indent(1, ""); indent(1,
	 * "ngOnInit() {"); indent(1,
	 * "this.httpService.get(Configuration.get().dataMaster+'/service/list-generic/?table="
	 * + className + "&select=*').subscribe(table => {"); indent(1,
	 * "this.listData = table.data.data;"); indent(1, "});"); indent(1, "}");
	 * indent(1, ""); indent(1, "confirmDelete() {"); indent(1,
	 * "this.confirmationService.confirm({"); indent(1,
	 * "message: 'Apakah data akan di hapus?',"); indent(1,
	 * "header: 'Konfirmasi Hapus',"); indent(1, "icon: 'fa fa-trash',"); indent(1,
	 * "accept: () => {"); indent(1, "this.hapus();"); indent(1, "},"); indent(1,
	 * "reject: () => {"); indent(1,
	 * "this.alertService.error('Hapus','Data Tidak Dihapus');"); indent(1, "}");
	 * indent(1, "});"); indent(1, "}"); indent(1, "confirmUpdate() {"); indent(1,
	 * "this.confirmationService.confirm({"); indent(1,
	 * "message: 'Apakah data akan diperbaharui?',"); indent(1,
	 * "header: 'Konfirmasi Pembaharuan',"); indent(1, "accept: () => {"); indent(1,
	 * "this.update();"); indent(1, "},"); indent(1, "reject: () => {"); indent(1,
	 * "this.alertService.error('Update','Data Tidak Diperbaharui');"); indent(1,
	 * "}"); indent(1, "});"); indent(1, "}"); indent(1, "tampilPopUp() {");
	 * indent(1, "this.confirmationService.confirm({"); indent(1,
	 * "message: 'Contoh Dialog',"); indent(1, "accept: () => {"); indent(1,
	 * "		                //Actual logic to perform a confirmation");
	 * indent(1, "}"); indent(1, "});"); indent(1, "}"); indent(1, "update() {");
	 * indent(1, "this.httpService.update(Configuration.get().dataMaster+'/" +
	 * className.toLowerCase() +
	 * "/update/'+this.versi, this.item).subscribe(response =>{"); indent(1,
	 * "this.alertService.success('Berhasil','Data Diperbarui');"); indent(1,
	 * "this.httpService.get(Configuration.get().dataMaster+'/service/list-generic/?table="
	 * + className + "&select=*').subscribe(table => {"); indent(1,
	 * "this.listData = table.data.data;"); indent(1, "});"); indent(1, "});");
	 * indent(1, "}"); indent(1, "simpan() {"); indent(1, "this.dataDummy = {");
	 * indent(1, "\"kode\": 0,"); for (Column column : columns) { String
	 * nameVariable = (convertName(column.getName()).toLowerCase()
	 * .equalsIgnoreCase(className.toLowerCase())) ? "nama" + className :
	 * (convertName(column.getName())); indent(1, "\"" + toLowerCase(nameVariable) +
	 * "\": " + (column.isPrimaryKey() ? "this.item.id." + toLowerCase(nameVariable)
	 * : "this.item." + toLowerCase(nameVariable)) + ","); } indent(1, "}");
	 * indent(1, "if (this.item.kode != null || this.item.kode != undefined) {");
	 * indent(1, "this.confirmUpdate()"); indent(1, "} else {"); indent(1,
	 * "this.httpService.post(Configuration.get().dataMaster+'/negara/save?', this.dataDummy).subscribe(response =>{"
	 * ); indent(1, "this.alertService.success('Simpan','Data Disimpan');");
	 * indent(1, "this.reset();"); indent(1,
	 * "this.httpService.get(Configuration.get().dataMaster+'/service/list-generic/?table=Negara&select=*').subscribe(table => {"
	 * ); indent(1, "this.listData = table.data.data;"); indent(1, "});"); indent(1,
	 * "});"); indent(1, "}"); indent(1, ""); indent(1, "}"); indent(1, "");
	 * indent(1, "reset(){"); indent(1, "this.item = {};"); indent(1, "}");
	 * indent(1, "onRowSelect(event) {"); indent(1,
	 * "let cloned = this.clone(event.data);"); indent(1, "this.item = cloned;");
	 * indent(1, "}"); indent(1, "clone(cloned: Negara): Negara {"); indent(1,
	 * "let hub = new InisialNegara();"); indent(1, "for(let prop in cloned) {");
	 * indent(1, "hub[prop] = cloned[prop];"); indent(1, "}"); indent(1,
	 * "let fixHub = new InisialNegara();"); indent(1, "fixHub = {"); indent(1,
	 * "\"kode\": hub.id.kode,"); for (Column column : columns) { String
	 * nameVariable = (convertName(column.getName()).toLowerCase()
	 * .equalsIgnoreCase(className.toLowerCase())) ? "nama" + className :
	 * (convertName(column.getName())); indent(1, "\"" + toLowerCase(nameVariable) +
	 * "\": " + (column.isPrimaryKey() ? "hub.id." + toLowerCase(nameVariable) :
	 * "hub." + toLowerCase(nameVariable)) + ","); }
	 * 
	 * indent(1, "}"); indent(1, "this.versi = hub.version;"); indent(1,
	 * "return fixHub;"); indent(1, "}"); indent(1, "hapus() {"); indent(1,
	 * "let item = [...this.listData]; "); indent(1,
	 * "let deleteItem = item[this.findSelectedIndex()];"); indent(1,
	 * "this.httpService.delete(Configuration.get().dataMaster+'/" +
	 * className.toLowerCase() +
	 * "/del/'+deleteItem.id.kode).subscribe(response => {"); indent(1,
	 * "this.alertService.success('Berhasil','Data Dihapus');"); indent(1,
	 * "this.httpService.get(Configuration.get().dataMaster+'/service/list-generic/?table="
	 * + className + "&select=*').subscribe(table => {"); indent(1,
	 * "this.listData = table.data.data;"); indent(1, "});"); indent(1, "});");
	 * indent(1, ""); indent(1, "}    "); indent(1, ""); indent(1,
	 * "findSelectedIndex(): number {"); indent(1,
	 * "return this.listData.indexOf(this.selected);"); indent(1, "}"); indent(1,
	 * "onDestroy(){");
	 * 
	 * indent(1, "}"); indent(1, "}"); indent(1, ""); indent(1, "class Inisial" +
	 * className + " implements " + className + " {");
	 * 
	 * indent(1, "constructor("); indent(1, "public id?,");
	 * 
	 * for (Column column : columns) { String nameVariable =
	 * (convertName(column.getName()).toLowerCase()
	 * .equalsIgnoreCase(className.toLowerCase())) ? "nama" + className :
	 * (convertName(column.getName())); if
	 * (toLowerCase(nameVariable).equalsIgnoreCase("statusEnabled")) { indent(0,
	 * "statusEnabled?:,"); } else { indent(0, "" + toLowerCase(nameVariable) +
	 * "?: " + (column.getJavaType().equalsIgnoreCase("String") ? "" : "") + ","); }
	 * } indent(1, "public kode?");
	 * 
	 * indent(1, "}"); indent(0, "}");
	 * 
	 * indent(0, ""); writeEndAngular(); }
	 */
	private void writeAngularComponentSCSS2() throws Exception {
		Column columns[] = table.getColumns();

		String beanClass = generateBeanClassName();

		className = beanClass;
		String[] tableNameSplit = table.getName().split("_");
		if (tableNameSplit[1].equalsIgnoreCase("M") ||  tableNameSplit[1].equalsIgnoreCase("S")) {// Master
			pkg = basePackage + ".module.master";
		//tableNameSplit[1].equalsIgnoreCase("S")) {// S
			//pkg = basePackage + ".module.support";
		} else { // T
			pkg = basePackage + ".module.transaksi";
		}
		initWriterAngular("component", "scss");

		indent(0, "");

		indent(0, "");
		writeEndAngular();
	}

	private void writeAngularComponentHtml2() throws Exception {
		Column columns[] = table.getColumns();

		String beanClass = generateBeanClassName();

		className = beanClass;
		String[] tableNameSplit = table.getName().split("_");
		if (tableNameSplit[1].equalsIgnoreCase("M") ||  tableNameSplit[1].equalsIgnoreCase("S")) {// Master
			pkg = basePackage + ".module.master";
		//tableNameSplit[1].equalsIgnoreCase("S")) {// S
			//pkg = basePackage + ".module.support";
		} else { // T
			pkg = basePackage + ".module.transaksi";
		}
		initWriterAngular("component", "html");
		// Init Status Aktif
		indent(1, "<div class=\"ui-fluid\">");
		indent(1, "	<div class=\"ui-g\">");
		indent(1, "		<div class=\"ui-g-12\">");
		indent(1, "			<div class=\"card card-w-title\">");
		indent(1,
				"				<p-confirmDialog header=\"Confirmation\" icon=\"fa fa-question-circle\" width=\"425\"></p-confirmDialog>");

		indent(1, "				<h1>{{ 'frm" + tableNameSplit[0] + "_title' | translate }}</h1>");

		indent(1, "				<div class=\"ui-g form-group\">");
		indent(1, "					<div class=\"ui-g-12\">");
		indent(1, "						<form [formGroup]=\"form\" (ngSubmit)=\"onSubmit()\">");
		indent(1, "							<div class=\"ui-g form-group\">");
		indent(1, "								<div class=\"ui-g-6\">");
		indent(1, "									<div class=\"ui-g-12 ui-md-10\" hidden>");
		indent(1, "										<span class=\"ui-float-label\" hidden>");
		indent(1,
				"											<input id=\"input\" hidden type=\"text\" formControlName=\"kode\" pInputText readonly=\"true\" />");

		indent(1, "										</span>");
		indent(1, "									</div>");
		// loop
		for (Column column : columns) {
			String nameVariable = (convertName(column.getName()).toLowerCase()
					.equalsIgnoreCase(className.toLowerCase())) ? "nama" + className : (convertName(column.getName()));
			if (nameVariable.equalsIgnoreCase("kd" + className)) {

			} else if (!nameVariable.equalsIgnoreCase("kdProfile") && !nameVariable.equalsIgnoreCase("noRec")
					&& !nameVariable.equalsIgnoreCase("version") && !nameVariable.equalsIgnoreCase("statusEnabled")
					&& !nameVariable.equalsIgnoreCase("kdDepartemen") && !nameVariable.equalsIgnoreCase("reportDisplay")
					&& !nameVariable.equalsIgnoreCase("kodeExternal")
					&& !nameVariable.equalsIgnoreCase("namaExternal")) {
				indent(1, "									<div class=\"ui-g-12 ui-md-10\">");
				indent(1, "										<label>{{'frm" + toLowerCase(nameVariable)
						+ "'| translate}}");
				if (column.getNullable() == 0) {
					indent(1,
							"											<span style=\"color: red;font-weight: bolder;\">*</span>");
					indent(1,
							"											<span *ngIf=\"!form.get('"
									+ toLowerCase(nameVariable) + "').valid && form.get('" + toLowerCase(nameVariable)
									+ "').touched\" style=\"color: red\">{{'required'|translate}}</span>");
					
				}	 
				indent(1, "										</label>");
				if (nameVariable.equalsIgnoreCase( "nama" + className)) {
					indent(1,
							"						<input id=\"input\" type=\"text\" formControlName=\""
									+ toLowerCase(nameVariable) + "\" [(ngModel)]=\"" + toLowerCase(nameVariable)
									+ "\" (ngModelChange)=\"valuechange($event)\" pInputText/>");
				}else {
				
					if (nameVariable.substring(0,2).equalsIgnoreCase("kd")) {		 
						indent(1,
								"						<p-dropdown filter=\"filter\" class=\"ui-inputwrapper-filled\" placeholder=\"--Pilih--\" formControlName=\""+toLowerCase(nameVariable)+"\" [options]=\"dropdown"+ toLowerCase(deleteKd(nameVariable))+"\" [autoWidth]=\"false\"></p-dropdown>");
					}else {
						indent(1,
								"						<input id=\"input\" type=\"text\" formControlName=\""
										+ toLowerCase(nameVariable) + "\" [(ngModel)]=\"" + toLowerCase(nameVariable)
										+ "\" pInputText/>");
					}

				}
				
				indent(1, "    									</div>");
			}
		}
		// loop
		indent(1, "									<div class=\"ui-g-12 ui-md-12\">");
		indent(1,
				"										<p-checkbox binary=\"true\" label=\"{{'frm_status'| translate}}\" formControlName=\"statusEnabled\"></p-checkbox>");
		indent(1,
				"										<span *ngIf=\"!form.get('statusEnabled').valid && form.get('statusEnabled').touched\" style=\"color: red\"> *</span>");
		indent(1,
				"										<span *ngIf=\"!form.get('statusEnabled').valid && form.get('statusEnabled').touched\" style=\"color: red\"> {{'required'|translate}}</span>");
		indent(1, "									</div>");
		indent(1, "								</div>");
		indent(1, "								<div class=\"ui-g-6\">");
		indent(1,
				"									<p-fieldset legend=\"Optional\" [toggleable]=\"true\" [collapsed]=\"true\">");
		indent(1, "										<form [formGroup]=\"form\" (ngSubmit)=\"onSubmit()\">");
		indent(1, "											<div class=\"ui-g form-group\">");
		indent(1, "												<div class=\"ui-g-12 ui-md-10\">");
		indent(1, "													<label>{{'frm_reportDisplay'| translate}}");
		indent(1,
				"														<span *ngIf=\"!form.get('reportDisplay').valid && form.get('reportDisplay').touched\" style=\"color: red\">*</span>");
		indent(1,
				"														<span *ngIf=\"!form.get('reportDisplay').valid && form.get('reportDisplay').touched\" style=\"color: red\"> {{'required'|translate}}</span>");
		indent(1, "													</label>");
		indent(1,
				"													<input id=\"input\" formControlName=\"reportDisplay\" [(ngModel)]=\"report\" pInputText />");
		indent(1, "");
		indent(1, "												</div>");
		indent(1, "												<div class=\"ui-g-12 ui-md-10\">");
		indent(1, "													<label>{{'frm_kodeExternal'| translate}}</label>");
		indent(1,
				"													<input id=\"input\" type=\"text\" formControlName=\"kodeExternal\" [(ngModel)]=\"kodeExternal\" pInputText/>");
		indent(1, "												</div>");
		indent(1, "												<div class=\"ui-g-12 ui-md-10\">");
		indent(1, "													<label>{{'frm_namaExternal'| translate}}</label>");
		indent(1,
				"													<input id=\"input\" type=\"text\" formControlName=\"namaExternal\" [(ngModel)]=\"namaExternal\" pInputText/>");
		indent(1, "												</div>");
		indent(1, "											</div>");
		indent(1, "										</form>");
		indent(1, "									</p-fieldset>");
		indent(1, "								</div>");
		indent(1, "");
		indent(1, "");
		indent(1, "								<div class=\"ui-g-12\">");
		indent(1, "									<div class=\"ui-g-12 ui-md-7\"></div>");
		indent(1, "									<div class=\"ui-g-12 ui-md-1\">");
		indent(1,
				"										<button pButton type=\"button\" label=\"{{'frm_btnBatal'|translate}}\" icon=\"fa-refresh\" (click)=\"reset()\"></button>");
		indent(1, "									</div>");
		indent(1, "									<div class=\"ui-g-12 ui-md-1\">");
		indent(1,
				"										<button pButton type=\"submit\" label=\"{{'frm_btnSimpan'|translate}}\" icon=\"fa-save\"></button>");
		indent(1, "									</div>");
		indent(1, "									<div class=\"ui-g-12 ui-md-1\">");
		indent(1,
				"										<button pButton type=\"button\" label=\"{{'frm_btnHapus'|translate}}\" icon=\"fa-trash\" (click)=\"confirmDelete()\"></button>");
		indent(1, "									</div>");
		indent(1, "									<div class=\"ui-g-12 ui-md-1\">");
		indent(1,
				"										<button pButton type=\"button\" label=\"{{'frm_btnCetak'|translate}}\" icon=\"fa-print\" (click)=\"cetak()\"></button>");
		indent(1, "									</div>");
		indent(1, "									<div class=\"ui-g-12 ui-md-1\">");
		indent(1,
				"										<p-splitButton label=\"{{'frm_btnExport'| translate}}\" icon=\"fa-external-link\" [model]=\"items\"></p-splitButton>");
		indent(1, "");
		indent(1, "									</div>");
		indent(1, "								</div>");
		indent(1, "");
		indent(1, "");
		indent(1, "");
		indent(1, "							</div>");
		indent(1, "						</form>");
		indent(1, "					</div>");
		indent(1, "					<div class=\"ui-g-12\">");
		indent(1,
				"						<p-dataTable [value]=\"listData\" [rows]=\"10\" [paginator]=\"true\" [pageLinks]=\"3\"");
		indent(1, " [rowsPerPageOptions]=\"[5,10,20]\" selectionMode=\"single\"");
		indent(1,
				" [(selection)]=\"selected\" (onRowSelect)=\"onRowSelect($event)\" [lazy]=\"true\" [loading]=\"loading\" loadingIcon=\"fa-spinner\"");
		indent(1,
				"						 [totalRecords]=\"totalRecords\" (onLazyLoad)=\"loadPage($event)\" expandableRows=\"true\">");
		indent(1, "");
		indent(1, "							<p-header>");
		indent(1, "								<div class=\"ui-helper-clearfix\">");
		indent(1,
				"									<i class=\"fa fa-search\" style=\"padding:0.5em 0.5em;float:right\"></i>");
		indent(1,
				"									<input type=\"text\" pInputText size=\"50\" placeholder=\"{{'frm_Daftarnama"+className+"Pencarian'|translate}}\"");
		indent(1, "	[(ngModel)]=\"pencarian\" (keyup)=\"cari()\"");
		indent(1, "									 style=\"width: 20%;float:right;\">");
		indent(1, "									<span style=\"float: left;padding:0.3em;\">{{'frm_Daftar" + className
				+ "Title'| translate}}</span>");
		indent(1, "								</div>");
		indent(1, "							</p-header>");
		indent(1,
				"							<p-column expander=\"true\" styleClass=\"col-icon\" [style]=\"{'width':'50px'}\"> </p-column>");

		for (Column column : columns) {
			String nameVariable = (convertName(column.getName()).toLowerCase()
					.equalsIgnoreCase(className.toLowerCase())) ? "nama" + className : (convertName(column.getName()));
			if (!nameVariable.equalsIgnoreCase("kd" + className) && !nameVariable.equalsIgnoreCase("kdProfile")
					&& !nameVariable.equalsIgnoreCase("noRec") && !nameVariable.equalsIgnoreCase("version")
					&& !nameVariable.equalsIgnoreCase("statusEnabled") && !nameVariable.equalsIgnoreCase("kdDepartemen")
					&& !nameVariable.equalsIgnoreCase("reportDisplay") && !nameVariable.equalsIgnoreCase("kodeExternal")
					&& !nameVariable.equalsIgnoreCase("namaExternal")) {
				indent(1, "							<p-column field=\"" + toLowerCase(nameVariable)
						+ "\" header=\"{{'frm" + toLowerCase(nameVariable) + "'|translate}}\"></p-column>");
			}
		}

		indent(1,
				"							<p-column field=\"statusEnabled\" header=\"{{'frm_tableStatus'| translate}}\">");
		indent(1, "								<ng-template let-col=\"rowData\" pTemplate type=\"body\">");
		indent(1, "									{{col.statusEnabled | truefalse | translate}}");
		indent(1, "								</ng-template>");
		indent(1, "							</p-column>");
		indent(1, "							<ng-template let-"+toLowerCase(className) +" pTemplate=\"rowexpansion\">");
		indent(1,
				"								<div class=\"ui-grid ui-grid-responsive ui-fluid\" style=\"padding-left:27px\">");
		indent(1, "									<div class=\"ui-grid ui-grid-responsive ui-grid-pad\">");
		indent(1, "										<div class=\"ui-grid-col-12\">");
		indent(1, "											<div class=\"ui-grid ui-grid-responsive ui-grid-pad\">");

		indent(1, "												<div class=\"ui-grid-row\">");
		indent(1, "													<div class=\"ui-grid-col-2 label\">");
		indent(1,
				"														<span style=\"color:black\">{{ 'frm_optional' | translate}}</span>");
		indent(1, "													</div>");
		indent(1, "												</div>");
		indent(1, "												<hr>");
		indent(1, "												<div class=\"ui-grid-row\">");
		indent(1,
				"													<div class=\"ui-grid label\">{{'frm_tableReportDisplay'| translate}} </div>");
		indent(1,
				"													<div class=\"ui-grid-col-4\"> {{"+toLowerCase(className) +".reportDisplay}}</div>");
		indent(1, "												</div>");
		indent(1, "												<div class=\"ui-grid-row\">");
		indent(1,
				"													<div class=\"ui-grid label\">{{'frm_tableKodeExternal' |translate}} </div>");
		indent(1,
				"													<div class=\"ui-grid-col-4\">{{"+toLowerCase(className) +".kodeExternal}}</div>");
		indent(1, "												</div>");
		indent(1, "												<div class=\"ui-grid-row\">");
		indent(1,
				"													<div class=\"ui-grid label\">{{'frm_tableNamaExternal'|translate}} </div>");
		indent(1,
				"													<div class=\"ui-grid-col-4\">{{"+toLowerCase(className) +".namaExternal}}</div>");
		indent(1, "												</div>");

		indent(1, "											</div>");
		indent(1, "										</div>");
		indent(1, "									</div>");
		indent(1, "								</div>");
		indent(1, "							</ng-template>");
		indent(1, "						</p-dataTable>");
		indent(1, "					</div>");
		indent(1, "				</div>");
		indent(1, "			</div>");
		indent(1, "		</div>");
		indent(1, "	</div>");
		indent(1, "</div>");

		indent(1, " ");

		indent(0, "");
		writeEndAngular();

	}

	/*
	 * private void writeAngularComponentHtml2() throws Exception { Column columns[]
	 * = table.getColumns();
	 * 
	 * String beanClass = generateBeanClassName();
	 * 
	 * className = beanClass; String[] tableNameSplit = table.getName().split("_");
	 * if (tableNameSplit[1].equalsIgnoreCase("M") ||  tableNameSplit[1].equalsIgnoreCase("S")) {// Master pkg = basePackage +
	 * ".module.master"; //tableNameSplit[1].equalsIgnoreCase("S")) {// S
	 * //pkg = basePackage + ".module.support"; } else { // T pkg = basePackage +
	 * ".module.transaksi"; } initWriterAngular("component", "html"); // Init Status
	 * Aktif StringBuffer statusAktifHtml = new StringBuffer();
	 * statusAktifHtml.append("<div class=\"ui-g-12 ui-md-4\">"); statusAktifHtml.
	 * append("<label for=\"input\">{{'Status Aktif'| translate}}</label>");
	 * statusAktifHtml.append("</div>");
	 * statusAktifHtml.append("<div class=\"ui-g-12 ui-md-8\">"); statusAktifHtml
	 * .append("<p-checkbox binary=\"true\" label=\"Aktif\" [(ngModel)]=\"item.statusEnabled\"></p-checkbox>"
	 * ); statusAktifHtml.append("</div>");
	 * 
	 * indent(1, "<div class=\"ui-fluid\">"); indent(1, "<div class=\"ui-g\">");
	 * indent(1, "<div class=\"ui-g-12\">"); indent(1,
	 * "<div class=\"card card-w-title\">"); indent(1,
	 * "<p-confirmDialog header=\"Confirmation\" icon=\"fa fa-question-circle\" width=\"425\"></p-confirmDialog>"
	 * ); indent(1, "<h1>Master {{'Negara'| translate}} </h1>"); indent(1,
	 * "<div class=\"ui-g form-group\">"); int jumlah = (columns.length - 1) / 2;
	 * int count = 1; int temp = jumlah;
	 * 
	 * // indent(1,"<div class=\"ui-g-6\">"); // Header StringBuffer header = new
	 * StringBuffer(); header.append("<div class=\"ui-g-6\">\n"); indent(0,
	 * header.toString()); for (Column column : columns) { String nameVariable =
	 * (convertName(column.getName()).toLowerCase()
	 * .equalsIgnoreCase(className.toLowerCase())) ? "nama" + className :
	 * (convertName(column.getName())); if (count <= jumlah && jumlah == temp) {
	 * indent(1, "<div class=\"ui-g-12 ui-md-4\">"); indent(1,
	 * "<label for=\"input\">{{'" + nameVariable + "'| translate}}</label>");
	 * indent(1, "</div>"); indent(1, "<div class=\"ui-g-12 ui-md-8\">"); indent(1,
	 * "<input id=\"input\" type=\"text\" [(ngModel)]=\"item." +
	 * toLowerCase(nameVariable) + "\" pInputText/>"); indent(1, "</div>");
	 * 
	 * } else { temp = columns.length; jumlah = temp; indent(0, "</div>"); indent(0,
	 * header.toString());
	 * 
	 * indent(1, "<div class=\"ui-g-12 ui-md-4\">"); indent(1,
	 * "<label for=\"input\">{{'" + nameVariable + "'| translate}}</label>");
	 * indent(1, "</div>"); indent(1, "<div class=\"ui-g-12 ui-md-8\">"); indent(1,
	 * "<input id=\"input\" type=\"text\" [(ngModel)]=\"item." +
	 * toLowerCase(nameVariable) + "\" pInputText/>"); indent(1, "</div>");
	 * 
	 * } count++; }
	 * 
	 * indent(1, "<div class=\"ui-g-6\"></div>"); indent(1,
	 * "<div class=\"ui-g-6\">"); indent(1, "<div class=\"ui-g-6 ui-md-3\">");
	 * indent(1,
	 * "<button pButton type=\"submit\" label=\"{{'Simpan'| translate}}\" icon=\"fa-save\" (click)=\"simpan()\"></button>"
	 * ); indent(1, "</div>"); indent(1, "<div class=\"ui-g-6 ui-md-3\">");
	 * indent(1,
	 * "<button pButton type=\"button\" label=\"{{'Hapus'| translate}}\" icon=\"fa-close\" (click)=\"confirmDelete()\"></button>"
	 * ); indent(1, "</div>"); indent(1, "<div class=\"ui-g-6 ui-md-3\">");
	 * indent(1,
	 * "<button pButton type=\"button\" label=\"{{'Batal'| translate}}\" icon=\"fa-close\" (click)=\"reset()\"></button>"
	 * ); indent(1, "</div>"); indent(1, "<div class=\"ui-g-6 ui-md-3\">");
	 * indent(1,
	 * "<button pButton type=\"button\" label=\"{{'Tutup'| translate}}\" icon=\"fa-close\" (click)=\"tampilPopUp()\"></button>"
	 * ); indent(1, "</div>"); indent(1, "</div>"); indent(1,
	 * "<div class=\"ui-g-12\">"); indent(1,
	 * "<p-dataTable [value]=\"listData\" [rows]=\"10\" [paginator]=\"true\" [pageLinks]=\"3\" [rowsPerPageOptions]=\"[5,10,20]\" selectionMode=\"single\" [(selection)]=\"selected\" (onRowSelect)=\"onRowSelect($event)\">"
	 * ); indent(1, "<p-header>Data Event</p-header>");
	 * 
	 * for (Column column : columns) { String nameVariable =
	 * (convertName(column.getName()).toLowerCase()
	 * .equalsIgnoreCase(className.toLowerCase())) ? "nama" + className :
	 * (convertName(column.getName())); if (column.isPrimaryKey()) { indent(0,
	 * "<p-column field=\"id." + toLowerCase(nameVariable) + "\" header=\"{{ '" +
	 * convertName(column.getName()) + "' | translate }}\" ></p-column>"); } else {
	 * indent(0, "<p-column field=\"" + toLowerCase(nameVariable) +
	 * "\" header=\"{{ '" + convertName(column.getName()) +
	 * "' | translate }}\" ></p-column>"); } } indent(1, "</p-dataTable>");
	 * indent(1, "</div>"); indent(1, "</div>"); indent(1, "</div>"); indent(1,
	 * "</div>"); indent(1, "</div>"); indent(1, "</div>");
	 * 
	 * indent(0, ""); writeEndAngular();
	 * 
	 * }
	 */
	private void writeAngularInterface2() throws Exception {
		Column columns[] = table.getColumns();

		String beanClass = generateBeanClassName();

		className = beanClass;
		String[] tableNameSplit = table.getName().split("_");
		if (tableNameSplit[1].equalsIgnoreCase("M") ||  tableNameSplit[1].equalsIgnoreCase("S")) {// Master
			pkg = basePackage + ".module.master";
		//tableNameSplit[1].equalsIgnoreCase("S")) {// S
			//pkg = basePackage + ".module.support";
		} else { // T
			pkg = basePackage + ".module.transaksi";
		}
		initWriterAngular("interface", "ts");

		indent(1, "import {EventEmitter} from '@angular/core';");
		indent(0, "");

		indent(1, "export interface " + beanClass + " {");
		for (Column column : columns) {
			String nameVariable = (convertName(column.getName()).toLowerCase()
					.equalsIgnoreCase(className.toLowerCase())) ? "nama" + className : (convertName(column.getName()));
			if (toLowerCase(nameVariable).equalsIgnoreCase("statusEnabled")) {
				indent(0, "statusEnabled?:boolean;");
			}else if (toLowerCase(nameVariable).equalsIgnoreCase("DescRumusBangun")) {
				indent(0, "" + toLowerCase(nameVariable) + "?:String;");
			} else {
				indent(0, "" + toLowerCase(nameVariable) + "?: "
						+ (column.getJavaType().equalsIgnoreCase("String") ? "string" : "number") + ";");
			}
		}
		indent(0, "kode?:any;");
		indent(1, "}");

		indent(0, "");
		writeEndAngular();

	}

	private void writeAngularModul() throws Exception {
		Column columns[] = table.getColumns();

		String beanClass = generateBeanClassName();

		className = beanClass;

		String[] tableNameSplit = table.getName().split("_");
		if (tableNameSplit[1].equalsIgnoreCase("M") ||  tableNameSplit[1].equalsIgnoreCase("S")) {// Master
			pkg = basePackage + ".module.master";
		//tableNameSplit[1].equalsIgnoreCase("S")) {// S
			//pkg = basePackage + ".module.support";
		} else { // T
			pkg = basePackage + ".module.transaksi";
		}

		// pkg = basePackage +".module";
		initWriterAngular("module", "ts");

		indent(1, "import { NgModule }       from '@angular/core';");
		indent(1, "import { CommonModule } from '@angular/common';");
		indent(1, "import { RouterModule} from '@angular/router';");
		indent(1, "import {");
		indent(1, "PanelMenuModule,");
		indent(1, "TabMenuModule,   ");
		indent(1, "AutoCompleteModule,");
		indent(1, "ButtonModule,  ");
		indent(1, "PanelModule,    ");
		indent(1, "InputTextModule,");
		indent(1, "DataTableModule,  ");
		indent(1, "DialogModule,   ");
		indent(1, "SplitButtonModule,");
		indent(1, "TabViewModule,  ");
		indent(1, "AccordionModule,");
		indent(1, "SharedModule,");
		indent(1, "CalendarModule,");
		indent(1, "GrowlModule,");
		indent(1, "MultiSelectModule,");
		indent(1, "ListboxModule,");
		indent(1, "DropdownModule,");
		indent(1, "CheckboxModule,");
		indent(1, "MessagesModule,");
		indent(1, "PaginatorModule,");
		indent(1, "ConfirmDialogModule,");
		indent(1, "InputTextareaModule,");
		indent(1, "OverlayPanelModule,");
		indent(1, " TooltipModule,");
		indent(1, "} from 'primeng/primeng';");
		indent(1, "import {FormsModule, ReactiveFormsModule} from \"@angular/forms\";");
		indent(1, "import {HttpModule, JsonpModule} from \"@angular/http\";");
		indent(1, "import {MyBreadcrumbModule} from \"../../../components/my-breadcrumb/my-breadcrumb\";");
		indent(1, "import {" + beanClass + "Component} from \"./" + beanClass.toLowerCase() + ".component\";");

		indent(1, "@NgModule({");
		indent(1, "imports: [");
		indent(1, "FormsModule,");
		indent(1, "ReactiveFormsModule,");
		indent(1, "HttpModule,");
		indent(1, "JsonpModule,");
		indent(1, "CommonModule,");
		indent(1, "ReactiveFormsModule,");
		indent(1, "ButtonModule,");
		indent(1, "PanelModule,");
		indent(1, "InputTextModule,");
		indent(1, "DataTableModule,");
		indent(1, "DialogModule,");
		indent(1, "SharedModule,");
		indent(1, "CalendarModule,");
		indent(1, "GrowlModule,");
		indent(1, "MultiSelectModule,");
		indent(1, "DropdownModule,");
		indent(1, "CheckboxModule,");
		indent(1, "PaginatorModule,");
		indent(1, "TooltipModule,");
		indent(1, "OverlayPanelModule,");
		indent(1, "MyBreadcrumbModule,");
		indent(1, "RouterModule.forChild([");
		indent(1, "  { path:'',component:" + beanClass + "Component}");
		indent(1, " ])");
		indent(1, "],");
		indent(1, "declarations: [" + beanClass + "Component],");
		indent(1, " exports:[RouterModule]");
		indent(1, "})");
		indent(1, "export class " + beanClass + "Module { }");
		indent(1, "");
		indent(0, "");
		// indent(1, content.toString());

		indent(0, "");
		writeEndAngular();

	}

	private void writeAngularRoute(Table tables[]) throws Exception {
		String filename = resolveFilenameAngular(pkg, "routing", "", "json");
		System.out.println("Generating " + filename);
		File file = new File(filename);
		(new File(file.getParent())).mkdirs();
		userCode = new UserCodeParser(filename);
		writer = new PrintWriter(new OutputStreamWriter(new FileOutputStream(filename)));
		for (Table table : tables) {
			String fileName = generateCoreClassName(renameTableClassName(table.getName()));
			;
			writer.println("{");
			writer.println(" path: '" + fileName.toLowerCase() + "',");
			writer.println("  loadChildren: '../../../" + fileName.toLowerCase() + "/" + fileName.toLowerCase()
					+ ".module#FormPrimengModule',");
			writer.println("  data: {preload: true}");
			writer.println(" },");
		}
		writer.println("");
		writer.println("");
		for (Table table : tables) {
			String fileName = generateCoreClassName(renameTableClassName(table.getName()));
			;
			writer.println("{");
			writer.println(" \"name\": \"" + fileName.toLowerCase() + "\",");
			writer.println("  \"link\": \"" + fileName.toLowerCase() + "\",");

			writer.println(" },");
		}

		writeEndAngular();

	}

	private void writeAngularService() throws Exception {
		Column columns[] = table.getColumns();

		String beanClass = generateBeanClassName();

		className = beanClass;
		String[] tableNameSplit = table.getName().split("_");
		if (tableNameSplit[1].equalsIgnoreCase("M") ||  tableNameSplit[1].equalsIgnoreCase("S")) {// Master
			pkg = basePackage + ".module.master";
		//tableNameSplit[1].equalsIgnoreCase("S")) {// S
			//pkg = basePackage + ".module.support";
		} else { // T
			pkg = basePackage + ".module.transaksi";
		}
		initWriterAngular("service", "ts");

		indent(1, "import {Injectable} from '@angular/core';");
		indent(1, "//import {Agama} from '../../../common/" + className.toLowerCase() + "';");
		indent(1, "import {Http, Response}  from '@angular/http';");
		indent(1, "import 'rxjs/add/operator/toPromise';");

		indent(1, "import {Ajax} from '../../../common/ajax';");

		indent(1, "@Injectable()");
		indent(1, "export class " + className + "Service {");
		indent(1, " constructor(private http: Http){}");

		indent(1, " /*get" + className + "(){");
		indent(1, "   return this.http.get('assets/data" + className + ".json')");
		indent(1, "  .toPromise()");
		indent(1, "   .then(res => <" + className + "[]> res.json().data)");
		indent(1, "  .then(data => {return data;});");
		indent(1, "}*/");

		indent(1, "}");

		indent(0, "");
		writeEndAngular();

	}

	private void writeAngularComponentTs() throws Exception {
		Column columns[] = table.getColumns();

		String beanClass = generateBeanClassName();

		className = beanClass;
		String[] tableNameSplit = table.getName().split("_");
		if (tableNameSplit[1].equalsIgnoreCase("M") ||  tableNameSplit[1].equalsIgnoreCase("S")) {// Master
			pkg = basePackage + ".module.master";
		//tableNameSplit[1].equalsIgnoreCase("S")) {// S
			//pkg = basePackage + ".module.support";
		} else { // T
			pkg = basePackage + ".module.transaksi";
		}
		initWriterAngular("component", "ts");

		indent(1, "import { Component, OnInit } from '@angular/core';");
		indent(1, "import {Car, Message, SelectItem} from '../../../common/car';");
		indent(1, "import {beforeUrl,pageAnimation} from '../../../common/public-data';");
		indent(1, "import {Validators,FormControl,FormGroup,FormBuilder} from '@angular/forms';");
		indent(1, "@Component({");
		indent(1, " selector: '" + className.toLowerCase() + "',");
		indent(1, "templateUrl: './" + className.toLowerCase() + ".component.html',");
		// indent(1, "styleUrls: ['./"+className.toLowerCase()+".component.css'],");
		indent(1, "animations: [");
		indent(1, " pageAnimation");
		indent(1, "]");
		indent(1, "})");
		indent(1, "export class " + className + "Component implements OnInit {");

		indent(1, " constructor(private fb: FormBuilder) {}");
		indent(1, " ngOnInit() {");
		indent(1, " this.msgs.push({severity:'info', summary:'Info Message', detail:'PrimeNG rocks'});");
		indent(1, "this.userform = this.fb.group({");
		indent(1, "'firstname': new FormControl('', Validators.required),");
		indent(1, "'lastname': new FormControl('', Validators.required),");
		indent(1,
				"'password': new FormControl('', Validators.compose([Validators.required, Validators.minLength(6)])),");
		indent(1, "'description': new FormControl(''),");
		indent(1, " 'gender': new FormControl('', Validators.required)");
		indent(1, " });");

		indent(1, "this.genders = [];");
		indent(1, "this.genders.push({label:'Select Gender', value:''});");
		indent(1, " this.genders.push({label:'Male', value:'Male'});");
		indent(1, " this.genders.push({label:'Female', value:'Female'})");
		;
		indent(1, " }");

		indent(1, " msgs: Message[] = [];");
		indent(1, " userform: FormGroup;");
		indent(1, " submitted: boolean;");
		indent(1, " genders: SelectItem[];");
		indent(1, " description: string;");

		indent(1, "  onSubmit(value: string) {");
		indent(1, "  this.submitted = true;");
		indent(1, "  this.msgs = [];");
		indent(1, "  this.msgs.push({severity:'info', summary:'Success', detail:'Form Submitted'});");
		indent(1, "}");

		indent(1, "get diagnostic() { return JSON.stringify(this.userform.value); }");

		indent(1, "}");

		indent(0, "");
		// indent(1, content.toString());

		indent(0, "");
		writeEndAngular();
	}

	private void writeAngularComponentHtml() throws Exception {
		Column columns[] = table.getColumns();

		String beanClass = generateBeanClassName();

		className = beanClass;
		String[] tableNameSplit = table.getName().split("_");
		if (tableNameSplit[1].equalsIgnoreCase("M") ||  tableNameSplit[1].equalsIgnoreCase("S")) {// Master
			pkg = basePackage + ".module.master";
		////tableNameSplit[1].equalsIgnoreCase("S")) {// S
		//	//pkg = basePackage + ".module.support";
		} else { // T
			pkg = basePackage + ".module.transaksi";
		}
		initWriterAngular("component", "html");

		indent(0, "<div [@pageAnimation] = \"'in'\">");
		indent(0, "<my-breadcrumb name1=\"Master Data\" name2=\"" + className + "\"></my-breadcrumb> ");

		indent(0, " <form [formGroup]=\"userform\" (ngSubmit)=\"onSubmit(userform.value)\">");
		indent(0, "<p-panel header=\"" + className + "\">");

		indent(0, "<div class=\"ui-g ui-fluid\">");
		// Jumlah Column + 4
		int jumlah = (columns.length - 1) / 2;
		// System.out.println(jumlah +" "+columns.length);
		int count = 1;
		int temp = jumlah;
		// Header
		StringBuffer header = new StringBuffer();
		header.append("<div class=\"ui-md-6\">\n");
		indent(0, header.toString());
		for (Column column : columns) {
			String nameVariable = (convertName(column.getName()).toLowerCase()
					.equalsIgnoreCase(className.toLowerCase())) ? "nama" + className : (convertName(column.getName()));
			if (count <= jumlah && jumlah == temp) {
				indent(0, "<div class=\"ui-g-12\">");
				indent(0, "<div class=\"ui-g-4\" style=\"width:114px\">");
				indent(0, nameVariable + " :");
				indent(0, "</div>");
				indent(0, "<div class=\"ui-g-8\">");
				indent(0, "<input pInputText type=\"text\" formControlName=\"" + toLowerCase(nameVariable)
						+ "\" placeholder=\"Required\"/>");
				indent(0, "</div>");
				indent(0, "</div>");
			} else {
				temp = temp + jumlah;
				indent(0, "</div>");
				indent(0, header.toString());

				indent(0, "<div class=\"ui-g-12\">");
				indent(0, "<div class=\"ui-g-4\" style=\"width:114px\">");
				indent(0, nameVariable + " :");
				indent(0, "</div>");
				indent(0, "<div class=\"ui-g-8\">");
				indent(0, "<input pInputText type=\"text\" formControlName=\"" + toLowerCase(nameVariable)
						+ "\" placeholder=\"Required\"/>");
				indent(0, "</div>");
				indent(0, "</div>");
			}
			count++;
		}

		indent(0, "<div class=\"ui-g ui-fluid\">");
		indent(0, "<div class=\"ui-md-12\">");
		indent(0, "<p-dataTable [value]=\"" + className.toLowerCase()
				+ "s\" [paginator]=\"true\" [rows]=\"10\" selectionMode=\"single\" [responsive]=\"true\" [pageLinks]=\"3\">");

		for (Column column : columns) {
			indent(0, "<p-column header=\"" + convertName(column.getName()) + "\" field=\""
					+ convertName(column.getName()) + "\"></p-column>");
		}
		indent(0, "</p-dataTable>	");
		indent(0, "</div>");
		indent(0, "</div>");

		indent(0, "</p-panel>   ");
		indent(0, "<div class=\"ui-g ui-fluid\">");
		indent(0, "<div class=\"ui-md-12\">");
		indent(0, "<div class=\"ui-md-8\">");
		indent(0, "<div class=\"ui-g-2\">");
		indent(0, "<button pButton type=\"button\" label=\"Simpan\"></button>");
		indent(0, "</div>");
		indent(0, "<div class=\"ui-g-2\">");
		indent(0, "<button pButton type=\"button\" label=\"Hapus\"></button>");
		indent(0, "</div>");
		indent(0, "<div class=\"ui-g-2\">");
		indent(0, "<button pButton type=\"button\" label=\"Batal\"></button>");
		indent(0, "</div>");
		indent(0, "</div>");
		indent(0, "</div>");
		indent(0, "</div>");
		indent(0, "</form>");

		// indent(1, content.toString());

		indent(0, "");
		writeEndAngular();

	}

	protected void writeMessage(StringBuffer content, String fileName) throws Exception {
		String filename = resolveFileMessage(pkg, fileName);
		System.out.println("Generating " + filename);
		File file = new File(filename);
		(new File(file.getParent())).mkdirs();
		userCode = new UserCodeParser(filename);
		writer = new PrintWriter(new OutputStreamWriter(new FileOutputStream(filename)));
		writer.println("# Generated using sql2java ");
		writer.println("# Author : Gunandi (andri.gunandi1@gmail.com) ");
		writer.println(" ");
		writer.println(content.toString());
		writer.close();
	}

	protected void initWriter() throws Exception {
		String filename = resolveFilename(pkg, className);
		System.out.println("Generating " + filename);
		File file = new File(filename);
		(new File(file.getParent())).mkdirs();
		userCode = new UserCodeParser(filename);
		writer = new PrintWriter(new OutputStreamWriter(new FileOutputStream(filename)));
		writer.println("/* --------------------------------------------------------");

		writer.println(" Generated by Gunandi using sql2java (http://sql2java.sourceforge.net/ )");
		writer.println(" jdbc driver used at code generation time: " + db.getDriver());
		writer.println(" ");
		writer.println(" Author : Gunandi (andri.gunandi1@gmail.com) ");
		writer.println(" Date Created : " + dateFormatCreatedFile(new Date()));

		writer.println(" --------------------------------------------------------");
		writer.println("*/");
	}

	protected void initWriterAngular(String type, String typeData) throws Exception {
		String filename = resolveFilenameAngular(pkg, className, type, typeData);
		System.out.println("Generating " + filename);
		File file = new File(filename);
		(new File(file.getParent())).mkdirs();
		userCode = new UserCodeParser(filename);
		writer = new PrintWriter(new OutputStreamWriter(new FileOutputStream(filename)));
		// writer.println("/*
		// --------------------------------------------------------");

		// writer.println(" Generated by Gunandi using sql2java ");
		// writer.println(" ");
		// writer.println(" Author : Gunandi (andri.gunandi1@gmail.com) ");
		// writer.println(" Date Created : "+ dateFormatCreatedFile(new Date()));
		//
		// writer.println(" --------------------------------------------------------");
		// writer.println("*/");
	}

	private String dateFormatCreatedFile(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String dateString = sdf.format(date);
		return dateString;
	}

	private void writePreamble() throws Exception {
		writePreamble(new String[0], null, null);
	}

	private void writePreamble(String importList[], String extendsClass, String implementsList[]) throws Exception {
		writer.println("package " + pkg + ";");
		writer.println();
		// writer.println("import java.util.*;");
		for (int i = 0; i < importList.length; i++) {
			writer.print("import ");
			writer.print(importList[i]);
			writer.println(";");
		}

		// writer.println(userCode.getBlock("imports"));
		writer.println();
		writer.print("public class " + className);
		if (extendsClass != null) {
			writer.print(" extends " + extendsClass);
		}
		if (implementsList != null && implementsList.length != 0) {
			writer.print(" implements ");
			for (int i = 0; i < implementsList.length; i++) {
				if (i > 0) {
					writer.print(",");
				}
				writer.print(implementsList[i]);
			}

		}
		// writer.println();
		// writer.println(userCode.getBlock("extends"));
		writer.print(" {");
		writer.println();
	}

	private void writePreamble(String importLists[], String extendsClass, String implementsList[],
			String annotationLists[]) throws Exception {
		writer.println("package " + pkg + ";");
		writer.println();
		// writer.println("import java.util.*;");
		for (String importList : importLists) {
			writer.print("import ");
			writer.print(importList);
			writer.println(";");
		}

		// writer.println(userCode.getBlock("imports"));
		writer.println();
		for (String annotationList : annotationLists) {
			writer.println();
			writer.print("@");
			writer.print(annotationList);

		}
		writer.println();
		writer.print("public class " + className);
		if (extendsClass != null) {
			writer.print(" extends " + extendsClass);
		}
		if (implementsList != null && implementsList.length != 0) {
			writer.print(" implements ");
			for (int i = 0; i < implementsList.length; i++) {
				if (i > 0) {
					writer.print(",");
				}
				writer.print(implementsList[i]);
			}

		}
		// writer.println();
		// writer.println(userCode.getBlock("extends"));
		writer.print(" {");
		writer.println();
	}

	private void writePreambleDao(String importLists[], String extendsClass, String implementsList[],
			String annotationLists[]) throws Exception {
		writer.println("package " + pkg + ";");
		writer.println();
		// writer.println("import java.util.*;");
		for (String importList : importLists) {
			writer.print("import ");
			writer.print(importList);
			writer.println(";");
		}

		// writer.println(userCode.getBlock("imports"));
		writer.println();
		if (annotationLists != null) {
			for (String annotationList : annotationLists) {
				writer.println();
				writer.print("@");
				writer.print(annotationList);

			}
		}
		writer.println();
		writer.print("public interface " + className);
		if (extendsClass != null) {
			writer.print(" extends " + extendsClass);
		}
		if (implementsList != null && implementsList.length != 0) {
			writer.print(" implements ");
			for (int i = 0; i < implementsList.length; i++) {
				if (i > 0) {
					writer.print(",");
				}
				writer.print(implementsList[i]);
			}

		}
		// writer.println();
		// writer.println(userCode.getBlock("extends"));
		writer.print(" {");
		writer.println();
	}

	private void writePreambleServiceImpl(String importLists[], String extendsClass, String implementsList[],
			String annotationLists[]) throws Exception {
		writer.println("package " + pkg + ";");
		writer.println();
		// writer.println("import java.util.*;");
		for (String importList : importLists) {
			writer.print("import ");
			writer.print(importList);
			writer.println(";");
		}

		// writer.println(userCode.getBlock("imports"));
		writer.println();
		if (annotationLists != null) {
			for (String annotationList : annotationLists) {
				writer.println();
				writer.print("@");
				writer.print(annotationList);

			}

		}
		writer.println();
		writer.print("public class " + className);
		if (extendsClass != null) {
			writer.print(" extends " + extendsClass);
		}
		/*
		 * if (implementsList != null && implementsList.length != 0) {
		 * writer.print(" implements "); for (int i = 0; i < implementsList.length; i++)
		 * { if (i > 0) { writer.print(","); } writer.print(implementsList[i]); }
		 * 
		 * }
		 */
		// writer.println();
		// writer.println(userCode.getBlock("extends"));
		writer.print(" {");
		writer.println();
	}

	protected void writeEnd() {
		// writer.println(userCode.getBlock("class"));
		writer.println("}");
		writer.close();
	}

	protected void writeEndAngular() {
		// writer.println(userCode.getBlock("class"));
		// writer.println("}");
		writer.close();
	}

	protected void indent(int tabs, String line) {
		for (int i = 0; i < tabs; i++) {
			writer.print("    ");
		}

		writer.println(line);
	}

	protected String resolveFilename(String pkg, String className) {
		String file = pkg + "." + className;
		return destDir + File.separatorChar + file.replace('.', File.separatorChar) + ".java";
	}

	protected String resolveFilenameAngular(String pkg, String className, String type, String typeData) {
		String file = pkg + "." + className.toLowerCase();
		return destDir + File.separatorChar + file.replace('.', File.separatorChar) + File.separatorChar
				+ className.toLowerCase() + "." + type + "." + typeData;
	}

	protected String resolveFileMessage(String pkg, String className) {
		String file = pkg + "." + className.toLowerCase();
		return destDir + File.separatorChar + file.replace('.', File.separatorChar) + File.separatorChar
				+ className.toLowerCase();
	}

	protected String getVarName(Column c) {
		return c.getName();// convertName(c.getName(), true);
	}

	protected String convertName(String name) {
		return name.replaceAll("_", "");
	}

	protected String generateCoreClassName() {
		return generateCoreClassName(renameTableClassName(table.getName()));
	}

	protected String generateCoreClassName(String strTableName) {
		if (classPrefix != null) {
			return classPrefix + (strTableName);
		} else {
			return (strTableName);
		}
	}

	// Entity Name
	protected String generateBeanClassName() {
		return generateCoreClassName(renameTableClassName(table.getName()));
	}

	protected String generateBeanClassName(String strTableName) {
		return generateCoreClassName(renameTableClassName((strTableName)));
	}

	// CompositeId Name
	protected String generateBeanCompositeClassName() {
		return generateCoreClassName(renameTableClassName(table.getName()) + "Id");
	}

	protected String generateBeanCompositeClassName(String strTableName) {
		return generateCoreClassName(renameTableClassName((strTableName)) + "Id");
	}

	// Dao Name
	protected String generateDaoClassName() {
		return generateCoreClassName(renameTableClassName(table.getName()) + "Dao");
	}

	protected String generateDaoClassName(String strTableName) {
		return generateCoreClassName(renameTableClassName((strTableName)) + "Dao");
	}

	// Remove _T _M and _S
	protected String renameTableClassName(String strTblName) {
		String split[] = strTblName.split("_");
		return split[0];
	}

	private void getTipePk() throws Exception {
		Column columns[] = table.getColumns();
		String beanClass = generateBeanClassName();
		for (Column column : columns) {
			if (column.isPrimaryKey()) {
				if (!column.getName().equalsIgnoreCase("KdProfile")) {
					if (column.getName().equalsIgnoreCase("Kd" + beanClass)
							|| column.getName().equalsIgnoreCase("kdGProduk")) {
						if (column.getDatabaseType().equalsIgnoreCase("CLOB")
								|| column.getDatabaseType().equalsIgnoreCase("CHAR")
								|| column.getDatabaseType().equalsIgnoreCase("VARCHAR")) {
							typePK = "String";
						} else {
							typePK = "Integer";
						}

					}
				}
			}
		}
	}
}
