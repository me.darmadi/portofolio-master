// Decompiled by Jad v1.5.8e2. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://kpdus.tripod.com/jad.html
// Decompiler options: packimports(3)
// Source File Name:   Database.java

package com.portofolio.sql2java;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Hashtable;
import java.util.Vector;

// Referenced classes of package com.netkernel.sql2java:
//            Table, Column

public class Database {

	public Database() {
	}

	public void setDriver(String driver) {
		this.driver = driver;
	}

	public void setURL(String url) {
		this.url = url;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setCatalog(String catalog) {
		this.catalog = catalog;
	}

	public void setSchema(String schema) {
		this.schema = schema;
	}

	public void setTableNamePattern(String tablenamepattern) {
		this.tablenamepattern = tablenamepattern;
	}

	public void setTableTypes(String tt[]) {
		tableTypes = tt;
	}

	public String getDriver() {
		return driver;
	}

	public String getURL() {
		return url;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public String getCatalog() {
		return catalog;
	}

	public String getSchema() {
		return schema;
	}

	public String getTableNamePattern() {
		return tablenamepattern;
	}

	public String[] getTableTypes() {
		return tableTypes;
	}

	public boolean hasLinkedByRelationTable(Table pTable) {
		return getRelationTable(pTable) != null;
	}

	public Table[] getRelationTable(Table pTable) {
		Vector pVector = new Vector();
		for (int iIndex = 0; iIndex < tables.size(); iIndex++) {
			Table pTempTable = (Table) tables.get(iIndex);
			if (!pTable.equals(pTempTable) && pTempTable.isRelationTable()
					&& pTempTable.relationConnectsTo(pTable)
					&& !pVector.contains(pTempTable)) {
				pVector.add(pTempTable);
			}
		}

		Table pReturn[] = new Table[pVector.size()];
		pVector.copyInto(pReturn);
		return pReturn;
	}

	public void load() throws SQLException, ClassNotFoundException {
		Class.forName(driver);
		pConnection = DriverManager.getConnection(url, username, password);
		meta = pConnection.getMetaData();
		System.out.println("DATABASE: " + meta.getDatabaseProductName());
		tables = new Vector();
		tableHash = new Hashtable();
		loadTables();
		loadColumns();
		loadPrimaryKeys();
		loadImportedKeys();
		loadManyToMany();
	}

	public Table[] getTables() {
		Table list[] = new Table[tables.size()];
		tables.copyInto(list);
		return list;
	}

	private void addTable(Table t) {
		tables.addElement(t);
		tableHash.put(t.getName(), t);
	}

	public Table getTable(String name) {
		return (Table) tableHash.get(name);
	}

	private void loadTables() throws SQLException {
		System.out.println("Database::loadTables - " + tablenamepattern);
		ResultSet pResultSet;
		Table table;
		for (pResultSet = meta.getTables(catalog, schema,
				tablenamepattern != null ? tablenamepattern : "SLP%",
				tableTypes); pResultSet.next(); System.out
				.println("Database::loadTables: loaded " + table.getName())) {
			table = new Table();
			table.setDatabase(this);
			table.setCatalog(pResultSet.getString("TABLE_CAT"));
			table.setSchema(pResultSet.getString("TABLE_SCHEM"));
			table.setName(pResultSet.getString("TABLE_NAME"));
			table.setType(pResultSet.getString("TABLE_TYPE"));
			addTable(table);
		}

		pResultSet.close();
	}

	private void loadColumns() throws SQLException {
		System.out.println("Database::loadColumns");
		Table tables[] = getTables();
		for (int i = 0; i < tables.length; i++) {
			Table table = tables[i];
			ResultSet pResultSet = meta.getColumns(catalog, schema, table
					.getName(), "%");
			System.out.println("Database::loadColumns: for table "
					+ table.getName());
			Column c = null;
			for (; pResultSet.next(); table.addColumn(c)) {
				c = new Column();
				c.setDatabase(this);
				c.setCatalog(pResultSet.getString("TABLE_CAT"));
				c.setSchema(pResultSet.getString("TABLE_SCHEM"));
				String strTableName = pResultSet.getString("TABLE_NAME");
				c.setTableName(strTableName);
				String strFieldName = pResultSet.getString("COLUMN_NAME");
				c.setName(strFieldName);
				c.setType(pResultSet.getShort("DATA_TYPE"));
				c.setSize(pResultSet.getInt("COLUMN_SIZE"));
				c.setDecimalDigits(pResultSet.getInt("DECIMAL_DIGITS"));
				c.setRadix(pResultSet.getInt("NUM_PREC_RADIX"));
				c.setNullable(pResultSet.getInt("NULLABLE"));
				c.setRemarks(pResultSet.getString("REMARKS"));
				c.setDefaultValue(pResultSet.getString("COLUMN_DEF"));
				c.setOrdinalPosition(pResultSet.getInt("ORDINAL_POSITION"));
			}

			pResultSet.close();
		}

	}

	private void loadPrimaryKeys() throws SQLException {
		System.out.println("Database::loadPrimaryKeys");
		Table tables[] = getTables();
		for (int i = 0; i < tables.length; i++) {
			Table table = tables[i];
			ResultSet pResultSet = meta.getPrimaryKeys(catalog, schema, table
					.getName());
			do {
				if (!pResultSet.next()) {
					break;
				}
				String colName = pResultSet.getString("COLUMN_NAME");
				System.out.println("Found primary key " + colName
						+ " for table " + table.getName());
				Column col = table.getColumn(colName);
				if (col != null) {
					table.addPrimaryKey(col);
				}
			} while (true);
			pResultSet.close();
		}

	}

	private void loadImportedKeys() throws SQLException {
		System.out.println("Database::loadImportedKeys");
		Table tables[] = getTables();
		for (int i = 0; i < tables.length; i++) {
			Table table = tables[i];
			ResultSet pResultSet = meta.getImportedKeys(catalog, schema, table
					.getName());
			System.out.println("Table: " + table.getName()
					+ ": looking for imported keys...");
			do {
				if (!pResultSet.next()) {
					break;
				}
				String tabName = pResultSet.getString("PKTABLE_NAME");
				String colName = pResultSet.getString("PKCOLUMN_NAME");
				String foreignColName = pResultSet.getString("FKCOLUMN_NAME");
				String foreignTableName = pResultSet.getString("FKTABLE_NAME");
				System.out.println("Table: " + table.getName()
						+ ": Found imported key " + foreignColName + " at "
						+ foreignTableName + " on " + tabName + "." + colName);
				Table otherTable = getTable(tabName);
				if (otherTable != null) {
					Column otherCol = null;
					try {
						otherCol = (Column) otherTable.getColumn(colName)
								.clone();
					} catch (CloneNotSupportedException cnse) {
						throw new SQLException("" + cnse.toString());
					}
					otherCol.setForeignKeyColName(foreignColName);
					otherCol.setForeignKeyTabName(foreignTableName);
					if (otherCol != null) {
						table.addImportedKey(otherCol);
					} else {
						System.out.println("Not foreign field !!!");
					}
				}
			} while (true);
			pResultSet.close();
		}

	}

	private void loadManyToMany() throws SQLException {
		System.out.println("Database::loadManyToMany");
		Table tables[] = getTables();
		for (int i = 0; i < tables.length; i++) {
			Table table = tables[i];
			if (table.getColumns().length != table.getPrimaryKeys().length) {
				continue;
			}
			ResultSet pResultSet = meta.getImportedKeys(catalog, schema, table
					.getName());
			do {
				if (!pResultSet.next()) {
					break;
				}
				String tabName = pResultSet.getString("PKTABLE_NAME");
				String colName = pResultSet.getString("PKCOLUMN_NAME");
				System.out.println(" many to many " + tabName + " " + colName);
				Table pkTable = getTable(tabName);
				Column fkCol = table.getColumn(pResultSet
						.getString("FKCOLUMN_NAME"));
				if (pkTable != null) {
					Column pkCol = pkTable.getColumn(colName);
					if (pkCol != null && fkCol != null) {
						pkTable.addManyToManyKey(fkCol, pkCol);
					}
				}
			} while (true);
			pResultSet.close();
		}

	}

	private String tableTypes[];
	private Connection pConnection;
	private DatabaseMetaData meta;
	private Vector tables;
	private Hashtable tableHash;
	private String driver;
	private String url;
	private String username;
	private String password;
	private String catalog;
	private String schema;
	private String tablenamepattern;
}
