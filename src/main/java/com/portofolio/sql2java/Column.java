// Decompiled by Jad v1.5.8e2. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://kpdus.tripod.com/jad.html
// Decompiler options: packimports(3)
// Source File Name:   Column.java

package com.portofolio.sql2java;

// Referenced classes of package com.netkernel.sql2java:
//            Database, Table, CodeWriter

public class Column implements Cloneable {

	public Column() {
		strCheckingType = "";
		strForeignKeyColName = "";
		strForeignKeyTabName = "";
		pointsTo = null;
	}

	public String toString() {
		return "\n --------- " + tableName + "." + name + " --------- "
				+ "\n schema        = " + schema + "\n tableName     = "
				+ tableName + "\n foreignCol    = " + strForeignKeyColName
				+ "\n foreignTab    = " + strForeignKeyTabName
				+ "\n catalog       = " + catalog + "\n remarks       = "
				+ remarks + "\n defaultValue  = " + defaultValue
				+ "\n decDigits     = " + decDigits + "\n radix         = "
				+ radix + "\n nullable      = " + nullable
				+ "\n ordinal       = " + ordinal + "\n size          = "
				+ size + "\n type          = " + type + " "
				+ "\n isPrimaryKey  = " + (isPrimaryKey ? "true" : "false"
				+ "\nType database ="+ getJavaTypeAsTypeName() 
				+"\n Type ="+getJavaType());
	}

	public String getForeignKeyColName() {
		return strForeignKeyColName;
	}

	public void setForeignKeyColName(String strValue) {
		if (isReserved(strValue)) {
			strForeignKeyColName = "z" + strValue;
		} else {
			strForeignKeyColName = strValue;
		}
	}

	public String getForeignKeyTabName() {
		return strForeignKeyTabName;
	}

	public void setForeignKeyTabName(String strValue) {
		strForeignKeyTabName = strValue;
	}

	public Column getForeignColumn() {
		return db.getTable(getForeignKeyTabName()).getColumn(
				getForeignKeyColName());
	}

	public void setPointsTo(Column c) {
		pointsTo = c;
	}

	public Column getPointsTo() {
		return pointsTo;
	}

	public void setCheckingType(String strValue) {
		strCheckingType = strValue;
	}

	public String getCheckingType() {
		return strCheckingType;
	}

	public void setDatabase(Database db) {
		this.db = db;
	}

	public void setCatalog(String catalog) {
		this.catalog = catalog;
	}

	public void setSchema(String schema) {
		this.schema = schema;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public void setName(String name) {
		if (isReserved(name)) {
			this.name = "z" + name;
		} else {
			this.name = name;
		}
	}

	public void setType(short type) {
		this.type = type;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public void setDecimalDigits(int decDigits) {
		this.decDigits = decDigits;
	}

	public void setRadix(int radix) {
		this.radix = radix;
	}

	public void setNullable(int nullable) {
		this.nullable = nullable;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public void setOrdinalPosition(int ordinal) {
		this.ordinal = ordinal;
	}

	public void isPrimaryKey(boolean isPrimaryKey) {
		this.isPrimaryKey = isPrimaryKey;
	}

	public String getCatalog() {
		return catalog;
	}

	public String getSchema() {
		return schema;
	}

	public String getTableName() {
		return tableName;
	}

	public String getName() {
		return name;
	}

	public short getType() {
		return type;
	}

	public int getSize() {
		return size;
	}

	public int getDecimalDigits() {
		return decDigits;
	}

	public int getRadix() {
		return radix;
	}

	public int getNullable() {
		return nullable;
	}

	public String getRemarks() {
		return remarks;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public int getOrdinalPosition() {
		return ordinal;
	}

	public boolean isPrimaryKey() {
		return isPrimaryKey;
	}

	public String getFullName() {
		return tableName + "." + getName();
	}

	public String getConstName() {
		return getName().toUpperCase();
	}

	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	private void tuoe() {
		throw new UnsupportedOperationException("not supported yet: "
				+ getTableName() + "." + getName() + " "
				+ getJavaTypeAsTypeName());
	}

	private void tiae() {
		throw new IllegalArgumentException("No primary type associated: "
				+ getTableName() + "." + getName());
	}

	public int getMappedType() {
		switch (getType()) {
		case 2003:
			return 0;

		case -5:
			return 11;

		case -2:
			return 3;

		case -7:
			return 2;

		case 2004:
			return 9;

		case 16: // '\020'
			return 2;

		case 1: // '\001'
			return 13;

		case 2005:
			return 4;

		case 70: // 'F'
			return 16;

		case 91: // '['
			if (CodeWriter.dateClassName.equals("java.util.Date")) {
				return 6;
			}
			if (CodeWriter.dateClassName.equals("java.sql.Date")) {
				return 5;
			}
			tuoe();
			// fall through

		case 3: // '\003'
			return getDecimalDigits() <= 0 ? 11 : 1;

		case 2001:
			return 17;

		case 8: // '\b'
			return 7;

		case 6: // '\006'
			return 7;

		case 4: // '\004'
			return 10;

		case 2000:
			return 17;

		case -4:
			return 3;

		case -1:
			return 13;

		case 2: // '\002'
			return getDecimalDigits() <= 0 ? 11 : 1;

		case 1111:
			return 17;

		case 7: // '\007'
			return 8;

		case 2006:
			return 12;

		case 5: // '\005'
			return 10;

		case 2002:
			return 17;

		case 92: // '\\'
			if (CodeWriter.timeClassName.equals("java.util.Date")) {
				return 6;
			}
			if (CodeWriter.timeClassName.equals("java.sql.Time")) {
				return 14;
			}
			tuoe();
			// fall through

		case 93: // ']'
			if (CodeWriter.timestampClassName.equals("java.util.Date")) {
				return 6;
			}
			if (CodeWriter.timestampClassName.equals("java.sql.Timestamp")) {
				return 15;
			}
			tuoe();
			// fall through

		case -6:
			return 10;

		case -3:
			return 3;

		case 12: // '\f'
			return 13;

		default:
			tuoe();
			return -1;
		}
	}

	public String getJavaType() {
		switch (getMappedType()) {
		case 0: // '\0'
			return "Array";

		case 1: // '\001'
			return "java.math.BigDecimal";

		case 2: // '\002'
			return "Boolean";

		case 3: // '\003'
			return "byte[]";

		case 4: // '\004'
			return "Clob";

		case 5: // '\005'
			return "java.sql.Date";

		case 6: // '\006'
			return "java.util.Date";

		case 7: // '\007'
			return "Double";

		case 8: // '\b'
			return "Float";

		case 10: // '\n'
			return "Integer";

		case 11: // '\013'
			return "Long";

		case 12: // '\f'
			return "Ref";

		case 13: // '\r'
			return "String";

		case 14: // '\016'
			return "java.sql.Time";

		case 15: // '\017'
			return "java.sql.Timestamp";

		case 16: // '\020'
			return "java.net.URL";

		case 17: // '\021'
			return "Object";

		case 9: // '\t'
		default:
			return null;
		}
	}

	public String getJavaPrimaryType() throws IllegalArgumentException {
		switch (getMappedType()) {
		case 2: // '\002'
			return "boolean";

		case 5: // '\005'
			return "long";

		case 6: // '\006'
			return "long";

		case 7: // '\007'
			return "double";

		case 8: // '\b'
			return "float";

		case 10: // '\n'
			return "int";

		case 11: // '\013'
			return "long";

		case 14: // '\016'
			return "long";

		case 15: // '\017'
			return "long";

		case 3: // '\003'
		case 4: // '\004'
		case 9: // '\t'
		case 12: // '\f'
		case 13: // '\r'
		default:
			tiae();
			break;
		}
		return null;
	}

	public String getJavaTypeAsTypeName() {
		switch (getType()) {
		case 2003:
			return "Types.ARRAY";

		case -5:
			return "Types.BIGINT";

		case -2:
			return "Types.BINARY";

		case -7:
			return "Types.BIT";

		case 2004:
			return "Types.BLOB";

		case 16: // '\020'
			return "Types.BOOLEAN";

		case 1: // '\001'
			return "Types.CHAR";

		case 2005:
			return "Types.CLOB";

		case 70: // 'F'
			return "Types.DATALINK";

		case 91: // '['
			return "Types.DATE";

		case 3: // '\003'
			return "Types.DECIMAL";

		case 2001:
			return "Types.DISTINCT";

		case 8: // '\b'
			return "Types.DOUBLE";

		case 6: // '\006'
			return "Types.FLOAT";

		case 4: // '\004'
			return "Types.INTEGER";

		case 2000:
			return "Types.JAVA_OBJECT";

		case -4:
			return "Types.LONGVARBINARY";

		case -1:
			return "Types.LONGVARCHAR";

		case 0: // '\0'
			return "Types.NULL";

		case 2: // '\002'
			return "Types.NUMERIC";

		case 1111:
			return "Types.OTHER";

		case 7: // '\007'
			return "Types.REAL";

		case 2006:
			return "Types.REF";

		case 5: // '\005'
			return "Types.SMALLINT";

		case 2002:
			return "Types.STRUCT";

		case 92: // '\\'
			return "Types.TIME";

		case 93: // ']'
			return "Types.TIMESTAMP";

		case -6:
			return "Types.TINYINT";

		case -3:
			return "Types.VARBINARY";

		case 12: // '\f'
			return "Types.VARCHAR";
		}
		tuoe();
		return null;
	}
	public String getDatabaseType() {
		switch (getType()) {
		case 2003:
			return "ARRAY";

		case -5:
			return "BIGINT";

		case -2:
			return "BINARY";

		case -7:
			return "BIT";

		case 2004:
			return "BLOB";

		case 16: // '\020'
			return "BOOLEAN";

		case 1: // '\001'
			return "CHAR";

		case 2005:
			return "CLOB";

		case 70: // 'F'
			return "DATALINK";

		case 91: // '['
			return "DATE";

		case 3: // '\003'
			return "DECIMAL";

		case 2001:
			return "DISTINCT";

		case 8: // '\b'
			return "DOUBLE";

		case 6: // '\006'
			return "FLOAT";

		case 4: // '\004'
			return "INTEGER";

		case 2000:
			return "JAVA_OBJECT";

		case -4:
			return "LONGVARBINARY";

		case -1:
			return "LONGVARCHAR";

		case 0: // '\0'
			return "NULL";

		case 2: // '\002'
			return "NUMERIC";

		case 1111:
			return "OTHER";

		case 7: // '\007'
			return "REAL";

		case 2006:
			return "REF";

		case 5: // '\005'
			return "SMALLINT";

		case 2002:
			return "STRUCT";

		case 92: // '\\'
			return "TIME";

		case 93: // ']'
			return "TIMESTAMP";

		case -6:
			return "TINYINT";

		case -3:
			return "VARBINARY";

		case 12: // '\f'
			return "VARCHAR";
		}
		tuoe();
		return null;
	}
	public boolean isColumnNumeric() {
		switch (getMappedType()) {
		case 7: // '\007'
		case 8: // '\b'
		case 10: // '\n'
		case 11: // '\013'
			return true;

		case 9: // '\t'
		default:
			return false;
		}
	}

	public boolean hasCompareTo() throws Exception {
		switch (getMappedType()) {
		case 0: // '\0'
			return false;

		case 1: // '\001'
			return true;

		case 2: // '\002'
			return false;

		case 3: // '\003'
			return false;

		case 4: // '\004'
			return false;

		case 5: // '\005'
			return true;

		case 6: // '\006'
			return true;

		case 7: // '\007'
			return true;

		case 8: // '\b'
			return true;

		case 10: // '\n'
			return true;

		case 11: // '\013'
			return true;

		case 12: // '\f'
			return false;

		case 13: // '\r'
			return true;

		case 14: // '\016'
			return true;

		case 15: // '\017'
			return true;

		case 16: // '\020'
			return false;

		case 17: // '\021'
			return false;

		case 9: // '\t'
		default:
			return false;
		}
	}

	public boolean useEqualsInSetter() throws Exception {
		switch (getMappedType()) {
		case 2: // '\002'
			return true;
		}
		return false;
	}

	public String getResultSetMethodObject(String pos) {
		switch (getMappedType()) {
		case 0: // '\0'
			return "rs.getArray(" + pos + ")";

		case 11: // '\013'
			return "Manager.getLong(rs, " + pos + ")";

		case 3: // '\003'
			return "rs.getBytes(" + pos + ")";

		case 9: // '\t'
			return "rs.getBlob(" + pos + ")";

		case 2: // '\002'
			return "Manager.getBoolean(rs, " + pos + ")";

		case 13: // '\r'
			return "rs.getString(" + pos + ")";

		case 4: // '\004'
			return "rs.getClob(" + pos + ")";

		case 16: // '\020'
			return "rs.getURL(" + pos + ")";

		case 1: // '\001'
			return "rs.getBigDecimal(" + pos + ")";

		case 7: // '\007'
			return "Manager.getDouble(rs, " + pos + ")";

		case 8: // '\b'
			return "Manager.getFloat(rs, " + pos + ")";

		case 10: // '\n'
			return "Manager.getInteger(rs, " + pos + ")";

		case 17: // '\021'
			return "rs.getObject(" + pos + ")";

		case 12: // '\f'
			return "rs.getRef(" + pos + ")";

		case 5: // '\005'
			return "rs.getDate(" + pos + ")";

		case 14: // '\016'
			return "rs.getTime(" + pos + ")";

		case 15: // '\017'
			return "rs.getTimestamp(" + pos + ")";

		case 6: // '\006'
			switch (getType()) {
			case 92: // '\\'
				return "rs.getTime(" + pos + ")";

			case 93: // ']'
				return "rs.getTimestamp(" + pos + ")";

			case 91: // '['
				return "rs.getDate(" + pos + ")";
			}
			break;
		}
		tuoe();
		return null;
	}

	public String getPreparedStatementMethod(String var, int pos) {
		return getPreparedStatementMethod(var, Integer.toString(pos));
	}

	public String getPreparedStatementMethod(String var, String pos) {
		switch (getMappedType()) {
		case 0: // '\0'
			return "ps.setArray(" + pos + ", " + var + ");";

		case 11: // '\013'
			return "Manager.setLong(ps, " + pos + ", " + var + ");";

		case 3: // '\003'
			return "ps.setBytes(" + pos + ", " + var + ");";

		case 9: // '\t'
			return "ps.setBlob(" + pos + ", " + var + ");";

		case 2: // '\002'
			return "Manager.setBoolean(ps, " + pos + ", " + var + ");";

		case 13: // '\r'
			return "ps.setString(" + pos + ", " + var + ");";

		case 4: // '\004'
			return "ps.setClob(" + pos + ", " + var + ");";

		case 16: // '\020'
			return "ps.setURL(" + pos + ", " + var + ");";

		case 1: // '\001'
			return "ps.setBigDecimal(" + pos + ", " + var + ");";

		case 7: // '\007'
			return "Manager.setDouble(ps, " + pos + ", " + var + ");";

		case 10: // '\n'
			return "Manager.setInteger(ps, " + pos + ", " + var + ");";

		case 17: // '\021'
			return "ps.setObject(" + pos + ", " + var + ");";

		case 8: // '\b'
			return "Manager.setFloat(ps, " + pos + ", " + var + ");";

		case 5: // '\005'
			return "ps.setDate(" + pos + ", " + var + ");";

		case 14: // '\016'
			return "ps.setTime(" + pos + ", " + var + ");";

		case 15: // '\017'
			return "ps.setTimestamp(" + pos + ", " + var + ");";

		case 6: // '\006'
			switch (getType()) {
			case 93: // ']'
				return "if (" + var + " == null) ps.setNull(" + pos + ", "
						+ getJavaTypeAsTypeName() + "); else ps.setTimestamp("
						+ pos + ", new java.sql.Timestamp(" + var
						+ ".getTime()));";

			case 91: // '['
				return "if (" + var + " == null) ps.setNull(" + pos + ", "
						+ getJavaTypeAsTypeName() + "); else ps.setDate(" + pos
						+ ", new java.sql.Date(" + var + ".getTime()));";

			case 92: // '\\'
				return "if (" + var + " == null) ps.setNull(" + pos + ", "
						+ getJavaTypeAsTypeName() + "); else ps.setTime(" + pos
						+ ", new java.sql.Time(" + var + ".getTime()));";
			}
			return null;

		case 12: // '\f'
			return "ps.setRef(" + pos + ", " + var + ");";
		}
		return "ps.setObject(" + pos + ", " + var + ");";
	}

	static boolean isReserved(String s) {
		for (int i = 0; i < reserved_words.length; i++) {
			if (s.compareToIgnoreCase(reserved_words[i]) == 0) {
				return true;
			}
		}

		return false;
	}

	public static final int M_ARRAY = 0;
	public static final int M_BIGDECIMAL = 1;
	public static final int M_BOOLEAN = 2;
	public static final int M_BYTES = 3;
	public static final int M_CLOB = 4;
	public static final int M_SQLDATE = 5;
	public static final int M_UTILDATE = 6;
	public static final int M_DOUBLE = 7;
	public static final int M_FLOAT = 8;
	public static final int M_BLOB = 9;
	public static final int M_INTEGER = 10;
	public static final int M_LONG = 11;
	public static final int M_REF = 12;
	public static final int M_STRING = 13;
	public static final int M_TIME = 14;
	public static final int M_TIMESTAMP = 15;
	public static final int M_URL = 16;
	public static final int M_OBJECT = 17;
	private String catalog;
	private String schema;
	private String tableName;
	private String name;
	private String remarks;
	private String defaultValue;
	private int size;
	private int decDigits;
	private int radix;
	private int nullable;
	private int ordinal;
	private short type;
	private boolean isPrimaryKey;
	private String strCheckingType;
	private String strForeignKeyColName;
	private String strForeignKeyTabName;
	private Database db;
	private Column pointsTo;
	static String reserved_words[] = { "null", "true", "false", "abstract",
			"double", "int", "strictfp", "boolean", "else", "interface",
			"super", "break", "extends", "long", "switch", "byte", "final",
			"native", "synchronized", "case", "finally", "new", "this",
			"catch", "float", "package", "throw", "char", "for", "private",
			"throws", "class", "goto", "protected", "transient", "const", "if",
			"public", "try", "continue", "implements", "return", "void",
			"default", "import", "short", "volatile", "do", "instanceof",
			"static", "while", "assert" };

}
