// Decompiled by Jad v1.5.8e2. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://kpdus.tripod.com/jad.html
// Decompiler options: packimports(3)
// Source File Name:   UserCodeParser.java

package com.portofolio.sql2java;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Enumeration;
import java.util.Hashtable;

public class UserCodeParser {

	public UserCodeParser(String filename) throws Exception {
		LINE_SEP = System.getProperty("line.separator");
		parse(filename);
	}

	public String getFilename() {
		return filename;
	}

	public boolean isNew() {
		return isNew;
	}

	public void parse(String filename) throws Exception {
		codeHash = new Hashtable();
		boolean inBlock = false;
		String blockName = null;
		StringBuffer code = new StringBuffer();
		isNew = true;
		File file = new File(filename);
		if (file.exists()) {
			isNew = false;
			BufferedReader reader = new BufferedReader(new FileReader(file));
			for (String line = reader.readLine(); line != null; line = reader
					.readLine()) {
				if (inBlock) {
					code.append(line).append(LINE_SEP);
				}
				if (line.indexOf("// ") == -1) {
					continue;
				}
				if (inBlock) {
					if (line.equals("// " + blockName + "- ")) {
						codeHash.put(blockName, code.toString());
						inBlock = false;
					}
					continue;
				}
				blockName = parseName(line);
				if (!blockName.equals("")) {
					inBlock = true;
					code.setLength(0);
					code.append(line).append(LINE_SEP);
				}
			}

			reader.close();
		}
	}

	private String parseName(String line) {
		int startPos = line.indexOf("// ");
		if (startPos == -1) {
			return "";
		}
		startPos += "// ".length();
		if (startPos >= line.length() + 1) {
			return "";
		}
		int endPos = line.indexOf("+ ", startPos);
		if (endPos == -1) {
			return "";
		} else {
			String name = line.substring(startPos, endPos);
			return name;
		}
	}

	public boolean hasBlock(String name) {
		return codeHash.get(name) != null;
	}

	public String getBlock(String name) {
		String code = (String) codeHash.get(name);
		if (code == null) {
			code = generateNewBlock(name);
			codeHash.put(name, code);
		}
		return code;
	}

	public String[] getBlockNames() {
		String list[] = new String[codeHash.size()];
		int i = 0;
		for (Enumeration e = codeHash.keys(); e.hasMoreElements();) {
			list[i++] = (String) e.nextElement();
		}

		return list;
	}

	private String generateNewBlock(String name) {
		StringBuffer str = new StringBuffer(512);
		str.append("// ");
		str.append(name);
		str.append("+ ");
		str.append(LINE_SEP).append(LINE_SEP);
		str.append("// ");
		str.append(name);
		str.append("- ");
		return str.toString();
	}

	private static final String START = "// ";
	private static final String BLOCK_BEGIN = "+ ";
	private static final String BLOCK_END = "- ";
	private String LINE_SEP;
	private Hashtable codeHash;
	private String filename;
	private boolean isNew;
}
