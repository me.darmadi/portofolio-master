package com.portofolio.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.portofolio.EnumAlamat;
import com.portofolio.base.BaseServiceImpl;
import com.portofolio.dao.AlamatDao;
import com.portofolio.dao.ProfileDao;
import com.portofolio.dao.RuanganDao;
import com.portofolio.dto.ProfileDto;
import com.portofolio.entity.Alamat;
import com.portofolio.entity.Profile;
import com.portofolio.entity.vo.AlamatId;
import com.portofolio.exception.InfoException;
import com.portofolio.util.CommonUtil;


@Lazy
@Service
public class ProfileService extends BaseServiceImpl {

	@Lazy
	@Autowired
	private ProfileDao profileDao;

	@Lazy
	@Autowired
	private LoginUserService loginUserService;

	@Lazy
	@Autowired
	private RuanganDao ruanganDao;
	
	@Lazy
	@Autowired
	private LanguageService languageService;
	
	@Lazy
	@Autowired
	private AlamatDao alamatDao;


	@Transactional
	public Map<String, Object> update(ProfileDto profileDto,Integer version) {
		result.clear();
		Profile profile = CommonUtil.findOne(profileDao, getKdProfile());
		if (CommonUtil.isNotNullOrEmpty(profile)) {
			if (version < profile.getVersion()) {
				throw new InfoException(languageService.getTranslateByKey("global.message.transactionLocking"));
			}
			BeanUtils.copyProperties(profileDto, profile);
			profile.setVersion(version);
			profile = profileDao.save(profile);
			
			Integer kdAlamat=null;
			List<Alamat> alamatPegawai=alamatDao.findAlamatProfile(getKdProfile());
			if(CommonUtil.isNotNullOrEmpty(alamatPegawai)) {
				Alamat alamat=alamatPegawai.get(0);
				kdAlamat=alamat.getId().getKdAlamat();
			}
			
			AlamatId alamatiId = new AlamatId();
			Alamat alamat = new Alamat();
			if(CommonUtil.isNullOrEmpty(kdAlamat)) {
				alamatiId.setKdAlamat(getIdTerahir(Alamat.class.getSimpleName()));
				alamatiId.setKdProfile(getKdProfile());
				
				alamat.setStatusEnabled(true);
				alamat.setNoRec(generateUuid32());
				alamat.setVersion(1);
				alamat.setId(alamatiId);
				alamat.setIsBillingAddress(1);
				alamat.setEnumAlamat(EnumAlamat.PROFILE.getName());
				alamat.setIsPrimaryAddress(1);
				alamat.setIsShippingAddress(1);
				
			}else {
				alamatiId.setKdAlamat(kdAlamat);
				alamatiId.setKdProfile(getKdProfile());
				alamat=CommonUtil.findOne(alamatDao, alamatiId);
			}
			
			BeanUtils.copyProperties(profileDto.getAlamat(), alamat);
			alamat = alamatDao.save(alamat);
			result.put("profile", profile);
			resultUpdated();
		} else {
			resultNotFound();
		}

		return result;
	}

	
	public Map<String, Object> grid(Integer page, Integer limit, String sort, String dir, String namaLengkap,boolean statusEnabled) {
		result.clear();
		page--;
		PageRequest pageReq = PageRequest.of(page, limit, "desc".equals(sort) ? Direction.DESC : Direction.ASC, dir);
		Page<Map<String, Object>> pageRes;
		if (CommonUtil.isNullOrEmpty(namaLengkap)) {
			pageRes = profileDao.findAllList(statusEnabled, pageReq);
		} else {
			pageRes = profileDao.findAllList(namaLengkap, statusEnabled, pageReq);
		}
		result.put("profile", pageRes.getContent());
		result.put("totalPages", pageRes.getTotalPages());
		result.put("totalRow", pageRes.getTotalElements());
		resultSuccessful();

		return result;
	}

	
	public Map<String, Object> findByKdProfile() {
		result.clear();
		Map<String, Object> profile = profileDao.findByKdProfile(getKdProfile(),CONSTANT_STATUS_ENABLED_TRUE);
		result.put("profile", profile);
		result.put("alamat",null);
		List<Map<String, Object>> alamatPegawai=alamatDao.findAlamatProfile(getKdProfile(),CONSTANT_STATUS_ENABLED_TRUE);
		result.put("alamat", null);
		if(CommonUtil.isNotNullOrEmpty(alamatPegawai)) {
			Map<String, Object> alamat=alamatPegawai.get(0);
			result.put("alamat", alamat);
		}
		resultSuccessful();
		return result;
	}

	@Transactional
	public Map<String, Object> deleteByKdProfile(Integer kdProfile, Integer version) {
		result.clear();
		Profile profile = CommonUtil.findOne(profileDao, kdProfile);
		if (CommonUtil.isNotNullOrEmpty(profile)) {
			if (version < profile.getVersion()) {
				throw new InfoException(languageService.getTranslateByKey("global.message.transactionLocking"));
			}
			profile.setStatusEnabled(false);
			profile.setVersion(version);
			profile = profileDao.save(profile);
			result.put("profile", profile);
			resultSuccessful();
		} else {
			resultNotFound();
		}

		return result;
	}

	

}
