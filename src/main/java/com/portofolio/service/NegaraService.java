package com.portofolio.service;

import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.portofolio.base.BaseServiceImpl;
import com.portofolio.dao.NegaraDao;
import com.portofolio.dto.NegaraDto;
import com.portofolio.entity.Negara;
import com.portofolio.exception.InfoException;
import com.portofolio.util.CommonUtil;


@Lazy
@Service
public class NegaraService extends BaseServiceImpl {

	@Lazy
	@Autowired
	private NegaraDao negaraDao;

	@Lazy
	@Autowired
	private LoginUserService loginUserService;
	
	@Lazy
	@Autowired
	private LanguageService languageService;

	@Transactional
	public Map<String, Object> save(NegaraDto negaraDto) {
		result.clear();
		Integer kdNegara = getIdTerahirNoNegara(Negara.class, "kdNegara");
		Negara negara = CommonUtil.findOne(negaraDao, kdNegara);
		if (CommonUtil.isNullOrEmpty(negara)) {
			negara = new Negara();
			BeanUtils.copyProperties(negaraDto, negara);
			negara.setNoRec(generateUuid32());
			negara.setVersion(1);
			negara.setKdNegara(kdNegara);
			negara = negaraDao.save(negara);
			result.put("negara", negara);
			resultCreated();
		} else {
			throw new InfoException(languageService.getTranslateByKey("negara.message.dataAlreadyExist"));
		}

		return result;
	}

	@Transactional
	public Map<String, Object> update(NegaraDto negaraDto, Integer kdNegara, Integer version) {
		result.clear();
		Negara negara = CommonUtil.findOne(negaraDao, kdNegara);
		if (CommonUtil.isNotNullOrEmpty(negara)) {
			if (version < negara.getVersion()) {
				throw new InfoException(languageService.getTranslateByKey("global.message.transactionLocking"));
			}
			String noRec = negara.getNoRec();
			BeanUtils.copyProperties(negaraDto, negara);
			negara.setNoRec(noRec);
			negara.setVersion(version);
			negara = negaraDao.save(negara);
			result.put("negara", negara);
			resultUpdated();
		} else {
			resultNotFound();
		}
		return result;
	}

	
	public Map<String, Object> grid(Integer page, Integer limit, String sort, String dir, String namanegara,boolean statusEnabled) {
		result.clear();
		page--;
		PageRequest pageReq = PageRequest.of(page, limit, "desc".equals(sort) ? Direction.DESC : Direction.ASC, dir);
		Page<Map<String, Object>> pageRes;
		if (CommonUtil.isNullOrEmpty(namanegara)) {
			pageRes = negaraDao.findAllList(statusEnabled, pageReq);
		} else {
			pageRes = negaraDao.findAllList(namanegara, statusEnabled, pageReq);
		}

		result.put("negara", pageRes.getContent());
		result.put("totalPages", pageRes.getTotalPages());
		result.put("totalRow", pageRes.getTotalElements());
		resultSuccessful();

		return result;
	}

	
	public Map<String, Object> findByKdNegara(Integer kdNegara) {
		result.clear();
		Map<String, Object> negara = negaraDao.findByKdNegara(kdNegara, CONSTANT_STATUS_ENABLED_TRUE);
		result.put("negara", negara);
		resultSuccessful();

		return result;
	}

	@Transactional
	public Map<String, Object> deleteByKode(Integer kdNegara, Integer version) {
		result.clear();
		Negara negara = CommonUtil.findOne(negaraDao, kdNegara);
		if (CommonUtil.isNotNullOrEmpty(negara)) {
			if (version < negara.getVersion()) {
				throw new InfoException(languageService.getTranslateByKey("global.message.transactionLocking"));
			}
			negara.setStatusEnabled(false);
			negara.setVersion(version);
			negaraDao.save(negara);
			result.put("negara", negara);
			resultDeleted();
		} else {
			resultNotFound();
		}

		return result;
	}

	

}
