package com.portofolio.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.portofolio.base.BaseServiceImpl;
import com.portofolio.dao.MapModulAplikasiToObjekModulDao;
import com.portofolio.dto.SessionDto;
import com.portofolio.entity.ObjekModulAplikasi;
import com.portofolio.util.CommonUtil;


@Service
public class MenuDinamisService extends BaseServiceImpl   {
	
	@Autowired MapModulAplikasiToObjekModulDao mapModulAplikasiToObjekModulDao;

	private List<Map<String,Object>> selectAllObjekAplikasi(Integer kdProfile, String kdModulAplikasi, Integer kdVersion, Integer kdKelompokUser, String KdObjekModulAplikasiHead, Map<String,Object> listurl, Map<String,Object> namaObjek) {
		List<ObjekModulAplikasi> objekmodulaplikasiS;
		if (kdKelompokUser == 1) {
			objekmodulaplikasiS = (KdObjekModulAplikasiHead == null) ?
					mapModulAplikasiToObjekModulDao.findAllObjekModulAplikasiDaofindMenuAdmin(kdVersion, kdModulAplikasi):
						mapModulAplikasiToObjekModulDao.findAllObjekModulAplikasiDaofindMenuAdmin(kdVersion, kdModulAplikasi, KdObjekModulAplikasiHead);			
		} else {
			objekmodulaplikasiS = (KdObjekModulAplikasiHead == null) ?
					mapModulAplikasiToObjekModulDao.findAllObjekModulAplikasiDaofindMenu(kdProfile, kdModulAplikasi, kdVersion,  kdKelompokUser):
						mapModulAplikasiToObjekModulDao.findAllObjekModulAplikasiDaofindMenu(kdProfile, kdModulAplikasi, kdVersion, kdKelompokUser, KdObjekModulAplikasiHead);
		}
		
		List<Map<String,Object>> menuutama = CommonUtil.createList();
		Map<String,Object> menu;
		
		for (ObjekModulAplikasi objekModulAplikasi:objekmodulaplikasiS) {

			List<Map<String,Object>> items = selectAllObjekAplikasi(kdProfile, kdModulAplikasi, kdVersion, kdKelompokUser, objekModulAplikasi.getKdObjekModulAplikasi(), listurl, namaObjek);
					
			if (CommonUtil.isNullOrEmpty(items)) {
				String link = objekModulAplikasi.getAlamatURLFormObjek().replaceAll("#", "");
				String label = objekModulAplikasi.getNamaObjekModulAplikasi();
				listurl.put(link, 1);
				namaObjek.put(label, link);
				
				if (objekModulAplikasi.getIsMenu() != null && objekModulAplikasi.getIsMenu().equals(1)) {
					menu = CommonUtil
							.initMap()
							.put("label", objekModulAplikasi.getNamaObjekModulAplikasi())			
							.put("icon", "fa fa-fw fa-chevron-right")
							.put("routerLink", new String[] {link})
							.getMap();
				} else {
					continue;
				}
			} else {
				menu = CommonUtil
						.initMap()
						.put("label", objekModulAplikasi.getNamaObjekModulAplikasi())			
						.put("icon", "fa fa-fw fa-bars")
						.put("items", items)
						.getMap();
			}
			
			menuutama.add(menu);
		}
		
		return menuutama;
	}
	
	public Map<String,Object> selectAllObjekAplikasi() {
		result.clear();
		SessionDto sess = getSession();
		Map<String,Object> listUrl = CommonUtil.createMap();
		Map<String,Object> namaObjek = CommonUtil.createMap();
		listUrl.put("/", 1);
		List<Map<String,Object>> menuUtama = selectAllObjekAplikasi(sess.getKdProfile(), sess.getKdModulAplikasi(), sess.getKdVersion(), sess.getKdKelompokUser(),  null, listUrl, namaObjek);
		result.put("menuUtama", menuUtama);
		resultSuccessful();
		return result;
	}


}
