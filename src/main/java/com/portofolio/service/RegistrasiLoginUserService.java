package com.portofolio.service;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.portofolio.base.BaseServiceImpl;
import com.portofolio.dao.AlamatDao;
import com.portofolio.dao.KelompokUserDao;
import com.portofolio.dao.MapLoginUserToPegawaiDao;
import com.portofolio.dao.MapLoginUserToProfileDao;
import com.portofolio.dao.ModulAplikasiDao;
import com.portofolio.dao.PegawaiDao;
import com.portofolio.dao.RegistrasiLoginUserDao;
import com.portofolio.dao.RuanganDao;
import com.portofolio.dto.MapLoginUserToPegawaiDto;
import com.portofolio.dto.RegistrasiLoginUserDto;
import com.portofolio.dto.UidPassDto;
import com.portofolio.entity.KelompokUser;
import com.portofolio.entity.LoginUser;
import com.portofolio.entity.MapLoginUserToPegawai;
import com.portofolio.entity.MapLoginUserToProfile;
import com.portofolio.entity.ModulAplikasi;
import com.portofolio.entity.vo.KelompokUserId;
import com.portofolio.exception.InfoException;
import com.portofolio.util.CommonUtil;
import com.portofolio.util.DateUtil;
import com.portofolio.util.PasswordUtil;
import com.portofolio.util.StringUtil;


@Lazy
@Service
public class RegistrasiLoginUserService extends BaseServiceImpl {

	@Lazy
	@Autowired
	private RegistrasiLoginUserDao registrasiLoginUserDao;
	
	@Lazy
	@Autowired
	private KelompokUserDao kelompokUserDao;

	@Lazy
	@Autowired
	private MapLoginUserToPegawaiDao mapLoginUserToPegawaiDao;

	@Lazy
	@Autowired
	private PegawaiDao pegawaiDao;

	@Lazy
	@Autowired
	private AlamatDao alamatDao;

	@Lazy
	@Autowired
	private LoginUserService loginUserService;

	@Lazy
	@Autowired
	private MapLoginUserToPegawaiService mapLoginUserToPegawaiService;

	@Lazy
	@Autowired
	private MapLoginUserToProfileService mapLoginUserToProfileService;

	@Lazy
	@Autowired
	private ModulAplikasiDao modulaplikasiDao;

	@Lazy
	@Autowired
	private RuanganDao ruanganDao;

	@Lazy
	@Autowired
	private ModulAplikasiService modulAplikasiService;

	@Lazy
	@Autowired
	private MapLoginUserToProfileDao maploginusertoprofileDao;



	@Transactional
	public Map<String, Object> update(RegistrasiLoginUserDto loginuserDto) {
		result.clear();
		LoginUser loginuser = registrasiLoginUserDao.findOneByNamaUserEmail(loginuserDto.getNamaUserEmail().toLowerCase());
		if (CommonUtil.isNullOrEmpty(loginuser)) {
			loginuser=new LoginUser();
			loginuser.setVersion(1);
			loginuser.setTglDaftar(DateUtil.dateNow());
			loginuser.setNoRec(generateUuid32());
		}
			Boolean email = true;
			Boolean mobile = true;
			String passOld = loginuser.getKataSandi();
			if (CommonUtil.isNotNullOrEmpty(loginuserDto.getKataSandi()) && loginuserDto.getKataSandi() == passOld) {
				loginuserDto.setKataSandi(null);
			}

			if (CommonUtil.isNotNullOrEmpty(loginuserDto.getKataSandi())) {
				loginuserDto.setKataSandi(createPassword(loginuserDto.getKataSandi()));
			} else {
				loginuserDto.setKataSandi(passOld);
			}
			if (CommonUtil.isNotNullOrEmpty(loginuser.getNamaUserEmail())) {
				if (!loginuser.getNamaUserEmail().equalsIgnoreCase(loginuserDto.getNamaUserEmail().toLowerCase())) {
					Map<String, Object> data1 = cekEmailMobile(loginuserDto.getNamaUserEmail().toLowerCase(), null);
					email = (Boolean) data1.get("email");
					if (!email) {
						throw new InfoException("Email Tidak Tersedia");
					}
				}
			}
			if (CommonUtil.isNotNullOrEmpty(loginuser.getNamaUserMobile())) {
				if (!loginuser.getNamaUserMobile().equalsIgnoreCase(loginuserDto.getNamaUserMobile().toLowerCase())) {
					Map<String, Object> data1 = cekEmailMobile(null, loginuserDto.getNamaUserMobile().toLowerCase());
					mobile = (Boolean) data1.get("mobile");
					if (!mobile) {
						throw new InfoException("Mobile Tidak Tersedia");
					}
				}
			}

			if (email && mobile) {
				BeanUtils.copyProperties(loginuserDto, loginuser);
				loginuser = registrasiLoginUserDao.save(loginuser);
				if (CommonUtil.isNotNullOrEmpty(loginuserDto.getKdPegawai())) {
					// save to map login user to pegawai
					MapLoginUserToPegawaiDto mapLoginUserToPegawaiDto = new MapLoginUserToPegawaiDto();
					mapLoginUserToPegawaiDto.setKdUser(loginuser.getKdUser());
					mapLoginUserToPegawaiDto.setKdPegawai(loginuserDto.getKdPegawai());
					mapLoginUserToPegawaiDto.setKdKelompokUser(loginuserDto.getKdKelompokUser());
					mapLoginUserToPegawaiDto.setStatusEnabled(loginuserDto.getStatusEnabled());
					result.put("mapLoginUserToPegawai", mapLoginUserToPegawaiService.update(mapLoginUserToPegawaiDto).get("mapLoginUserToPegawai"));
				}
				if (CommonUtil.isNotNullOrEmpty(loginuserDto.getMapLoginUserToProfileDto())) {
					result.put("mapLoginUserToProfile", mapLoginUserToProfileService.update(loginuserDto.getMapLoginUserToProfileDto(),loginuser.getKdUser()).get("mapLoginUserToProfile"));
				}
			
		}
		resultUpdated();
		return result;
	}

	
	public Map<String, Object> findByUidPass(UidPassDto entity) {
		Map<String, Object> data = CommonUtil.createMap();
		Map<String, Object> loginuser = registrasiLoginUserDao.findByUserName(entity.getNamaUserMobile(),
				entity.getNamaUserEmail().toLowerCase());

		if (CommonUtil.isNotNullOrEmpty(loginuser)) {
			Boolean pass = checkPassword(entity.getKataSandi(), loginuser.get("kataSandi").toString());
			if (pass) {
				loginuser.remove("kataSandi");
				// get pegawai
				data.put("LoginUser", loginuser);
				data.put("kdPegawai", mapLoginUserToPegawaiDao
						.findOneByKdUserDao(Integer.parseInt(loginuser.get("kodeUser").toString()), getKdProfile()));
				data.put("message", "user sudah ada");
			} else {
				data.put("message", "kata sandi salah");
			}

		}
		if (CommonUtil.isNullOrEmpty(loginuser)) {
			data.put("message", "user belum ada");
		}

		return data;
	}



	
	public Map<String, Object> cekUserEmail(String email) {
		result.clear();
		LoginUser loginuserTmp = registrasiLoginUserDao.findOneByNamaUserEmail(email.toLowerCase());
		if (CommonUtil.isNotNullOrEmpty(loginuserTmp)) {
			result.put("statusEmail", false);
		} else {
			result.put("statusEmail", true);
		}
		resultSuccessful();
		return result;
	}

	
	public Map<String, Object> cekUserMobile(String mobile) {
		result.clear();
		LoginUser loginuserTmp2 = registrasiLoginUserDao.findOneByNamaUserMobile(mobile);

		if (CommonUtil.isNotNullOrEmpty(loginuserTmp2)) {
			result.put("statusUserMobile", false);
		} else {
			result.put("statusUserMobile", true);
		}
		resultSuccessful();
		return result;
	}

	public Map<String, Object> cekEmailMobile(String email, String mobile) {
		result.clear();
		UidPassDto uidPassDto = new UidPassDto();

		if (CommonUtil.isNotNullOrEmpty(email)) {
			uidPassDto.setNamaUserEmail(email);
			Map<String, Object> data1 = cekUserEmail(email);
			if ((boolean) data1.get("statusEmail")) {
				result.put("email", true);
			} else {
				result.put("email", false);
			}
		}
		if (CommonUtil.isNotNullOrEmpty(mobile)) {
			uidPassDto.setNamaUserMobile(mobile);
			Map<String, Object> data2 = cekUserMobile(mobile);
			if ((boolean) data2.get("statusUserMobile")) {
				result.put("mobile", true);
			} else {
				result.put("mobile", false);
			}
		}
		resultSuccessful();
		return result;
	}

	
	public Map<String, Object> findOneByKdPegawai(String kode) {
		result.clear();
		Map<String, Object> data = CommonUtil.createMap();
		List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
		String kdUser = mapLoginUserToPegawaiDao.findOneByKdPegawaiDao(kode, getKdProfile());
		if (CommonUtil.isNotNullOrEmpty(kdUser)) {
			data.put("kdUser", kdUser);
			Map<String, Object> loginuser = findOne((long) Integer.parseInt(kdUser));
			// loginuser.remove("kataSandi");
			data.put("dataUser", loginuser);
			MapLoginUserToPegawai mapLoginUserToPegawai = mapLoginUserToPegawaiDao.findOneByIdKdUserAndIdKdPegawaiAndIdKdProfile((long) Integer.parseInt(kdUser), kode,getKdProfile());
			if (CommonUtil.isNotNullOrEmpty(mapLoginUserToPegawai)) {
				Map<String, Object> tmpMap = CommonUtil.createMap();
				tmpMap.put("id", mapLoginUserToPegawai.getKelompokUser().getId().getKdKelompokUser());
				KelompokUserId kelompokUserId=new KelompokUserId();
				kelompokUserId.setKdProfile(getKdProfile());
				kelompokUserId.setKdKelompokUser(mapLoginUserToPegawai.getKdKelompokUser());
				KelompokUser kelompokUser=CommonUtil.findOne(kelompokUserDao, kelompokUserId);
				tmpMap.put("namaKelompokUser",kelompokUser.getNamaKelompokUser().toString());
				data.put("kelompokUser", tmpMap);
			}
			dataList.add(data);
		}
		result.put("loginUser", dataList);
		resultSuccessful();
		return result;
	}

	public String createPassword(String pass) {
		PasswordUtil passwordUtil = new PasswordUtil();
		try {
			pass = passwordUtil.encryptPassword(pass);
		} catch (NoSuchAlgorithmException e) {
			StringUtil.printStackTrace(e);
		} catch (UnsupportedEncodingException e) {
			StringUtil.printStackTrace(e);
		}
		return pass;
	}

	private boolean checkPassword(String password1, String password2) {
		PasswordUtil passwordUtil = new PasswordUtil();
		Boolean isValidPassword = false;

		try {
			isValidPassword = passwordUtil.isPasswordEqual(password1, password2);
		} catch (Exception ex) {

		}

		return isValidPassword;
	}

	
	public Map<String, Object> findOne(Long kdUser) {
		Map<String, Object> data = registrasiLoginUserDao.findOneByKodeDao(kdUser);
		return data;
	}

	
	public Map<String, Object> findAll(Integer page, Integer rows, String sort, String dir, String namaLengkapOrNik) {
		result.clear();
		Integer kdStatusPegawaiAktif = Integer.parseInt(getSettingDataFixed("kdStatusPegawaiAktif").getNilaiField());

		Integer kdProfile = getKdProfile();
		page--;
		PageRequest pageReq = PageRequest.of(page, rows, "desc".equals(sort) ? Direction.DESC : Direction.ASC, dir);
		Page<Map<String, Object>> pageRes;
		if (CommonUtil.isNullOrEmpty(namaLengkapOrNik)) {
			pageRes = pegawaiDao.findAllListPegawaiUser(kdProfile, kdStatusPegawaiAktif,CONSTANT_STATUS_ENABLED_TRUE, pageReq);
		} else {
			pageRes = pegawaiDao.findAllListPegawaiUser(kdProfile, kdStatusPegawaiAktif,
					"%" + namaLengkapOrNik.toLowerCase() + "%",CONSTANT_STATUS_ENABLED_TRUE, pageReq);
		}
		resultSuccessful();
		result.put("dataPegawai", pageRes.getContent());
		result.put("totalPages", pageRes.getTotalPages());
		result.put("totalRow", pageRes.getTotalElements());

		return result;
	}

	
	public Map<String, Object> findAllAksesByLoginUser(Long kode) {
		result.clear();
		
		List<MapLoginUserToProfile> modul = CommonUtil.createList();

		if (CommonUtil.isNotNullOrEmpty(kode)) {
			modul = maploginusertoprofileDao.findAllByIdKdProfileAndIdKdUser(getKdProfile(), kode);
		}

		////////
		List<ModulAplikasi> modulaplikasi = modulaplikasiDao.findAll();
		List<Map<String, Object>> modulUser = CommonUtil.createList();
		///////
		for (ModulAplikasi map : modulaplikasi) {
			Map<String, Object> modulUserMap = CommonUtil.createMap();
			modulUserMap.put("kode", map.getKdModulAplikasi());
			modulUserMap.put("namaModulAplikasi", map.getNamaModulAplikasi());
			modulUserMap.put("statusEnabled", false);
			for (MapLoginUserToProfile map2 : modul) {
				if (map2.getId().getKdModulAplikasi().equals(map.getKdModulAplikasi())) {
					if(map2.getStatusEnabled()) {
						modulUserMap.replace("statusEnabled", true);
					}
					
				}
			}
			modulUser.add(modulUserMap);
		}
		result.put("dataModulAplikasi", modulUser);

		List<Map<String, Object>> ruangans = ruanganDao.getHead(getKdProfile(),CONSTANT_STATUS_ENABLED_TRUE);

		for (Map<String, Object> map : ruangans) {
			map.put("statusEnabled", false);
			// ceks status data akses user
			for (MapLoginUserToProfile map2 : modul) {
				if (map2.getId().getKdRuangan().equals(map.get("kdRuangan").toString())) {
					if(map2.getStatusEnabled()) {
						map.replace("statusEnabled", map2.getStatusEnabled().booleanValue());
					}
					
				}
			}
			// ceks status data akses user detail
			List<Map<String, Object>> childRuangans = ruanganDao.findAllMAListWithHead(getKdProfile(),map.get("kdRuangan").toString(),CONSTANT_STATUS_ENABLED_TRUE);
			for (Map<String, Object> childMap : childRuangans) {
				childMap.put("statusEnabled", false);
				for (MapLoginUserToProfile map2 : modul) {
					if (map2.getId().getKdRuangan().equals(childMap.get("kdRuangan").toString())) {
						if(map2.getStatusEnabled()) {
							childMap.replace("statusEnabled", map2.getStatusEnabled().booleanValue());
						}
						
					}
				}
			}
			map.put("child", childRuangans);
		}

		result.put("dataRuangan", ruangans);
		resultSuccessful();
		return result;
	}
}
