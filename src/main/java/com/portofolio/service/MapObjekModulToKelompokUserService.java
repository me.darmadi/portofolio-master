
package com.portofolio.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.portofolio.base.BaseServiceImpl;
import com.portofolio.dao.MapModulAplikasiToObjekModulDao;
import com.portofolio.dao.MapObjekModulToKelompokUserDao;
import com.portofolio.dao.ObjekModulAplikasiDao;
import com.portofolio.dto.DataMapObjekModulToKelompokUserDto;
import com.portofolio.dto.MapObjekModulToKelompokUserDto;
import com.portofolio.dto.SessionDto;
import com.portofolio.entity.MapObjekModulToKelompokUser;
import com.portofolio.entity.ObjekModulAplikasi;
import com.portofolio.entity.vo.MapObjekModulToKelompokUserId;
import com.portofolio.util.CommonUtil;


@Lazy
@Service
public class MapObjekModulToKelompokUserService extends BaseServiceImpl {

	@Lazy
	@Autowired
	private MapObjekModulToKelompokUserDao mapObjekModulToKelompokUserDao;

	@Lazy
	@Autowired
	private MapModulAplikasiToObjekModulDao mapModulAplikasiToObjekModulDao;

	@Lazy
	@Autowired
	private ObjekModulAplikasiDao objekModulAplikasiDao;

	@Lazy
	@Autowired
	private LoginUserService loginUserService;

	
	@Transactional
	public Map<String, Object> saveOrUpdate(DataMapObjekModulToKelompokUserDto dto) {
		result.clear();
		try {
			List<MapObjekModulToKelompokUserDto> dtoMapObjek = dto.getMapObjekModulToKelompokUser();
			List<MapObjekModulToKelompokUser> results=new ArrayList<MapObjekModulToKelompokUser>();
			for (MapObjekModulToKelompokUserDto mapObjek: dtoMapObjek) {
				MapObjekModulToKelompokUserId id = new MapObjekModulToKelompokUserId();
				id.setKdProfile(getKdProfile());
				id.setKdObjekModulAplikasi(mapObjek.getKdObjekModulAplikasi());
				id.setKdKelompokUser(mapObjek.getKdKelompokUser());
				id.setKdModulAplikasi(mapObjek.getKdModulAplikasi());
				id.setKdVersion(mapObjek.getKdVersion());
				MapObjekModulToKelompokUser entity = CommonUtil.findOne(mapObjekModulToKelompokUserDao, id);
				if (CommonUtil.isNullOrEmpty(entity)) {
					entity = new MapObjekModulToKelompokUser();
					entity.setId(id);
					entity.setNoRec(generateUuid32());
					entity.setVersion(1);
				}
				BeanUtils.copyProperties(mapObjek, entity);		
				entity=mapObjekModulToKelompokUserDao.save(entity);
				results.add(entity);
			}
			result.put("mapObjekModulToKelompokUser",results);
			resultUpdated();
		} catch (Exception e) {
			resultError(e);
		}
		
		return result;
	}
	
	
	public Map<String, Object> grid(Integer page, Integer limit, String sort, String dir, Integer kdKelompokUser) {
		result.clear();
		page--;
		PageRequest pageReq = PageRequest.of(page, limit, "desc".equals(sort) ? Direction.DESC : Direction.ASC, dir);
		Page<Map<String, Object>> pageRes;
		if (CommonUtil.isNullOrEmpty(kdKelompokUser)) {
			pageRes = mapObjekModulToKelompokUserDao.findAllList(getKdProfile(), pageReq);
		} else {
			pageRes = mapObjekModulToKelompokUserDao.findAllList(getKdProfile(), kdKelompokUser, pageReq);
		}
		result.put("mapObjekModulToKelompokUser", pageRes.getContent());
		result.put("totalPages", pageRes.getTotalPages());
		result.put("totalRow", pageRes.getTotalElements());
		resultSuccessful();
		return result;
	}

	
	public Map<String, Object> findByKdKelompokUser(Integer kode) {
		result.clear();
		List<Map<String, Object>> mapobjekmodultokelompokuser = mapObjekModulToKelompokUserDao
				.findByKode(getKdProfile(), kode);
		result.put("mapObjekModulToKelompokUser", mapobjekmodultokelompokuser);
		resultSuccessful();
		return result;

	}

	@Transactional
	public Map<String, Object> deleteByKode(Integer kdKelompokUser, String kdObjekModulAplikasi) {
		result.clear();
		MapObjekModulToKelompokUser mapobjekmodultokelompokuser = mapObjekModulToKelompokUserDao
				.findOneByIdKdKelompokUserAndIdKdObjekModulAplikasiAndIdKdProfile(kdKelompokUser, kdObjekModulAplikasi,
						getKdProfile());

		if (CommonUtil.isNotNullOrEmpty(mapobjekmodultokelompokuser)) {
			mapobjekmodultokelompokuser.setStatusEnabled(false);
			mapObjekModulToKelompokUserDao.save(mapobjekmodultokelompokuser);
			result.put("mapObjekModulToKelompokUser", mapobjekmodultokelompokuser);
		}

		return result;
	}
	
	
	public Map<String, Object> findModulAndObjekModulAplikasi(Integer kdKelompokUser) {
		result.clear();
		
		List<Map<String,Object>> objekmodulaplikasiM = objekModulAplikasiDao.findAllObjekModulAplikasiDaofindMenuMapObjek();
		List<Map<String,Object>> objekmodulaplikasiK = objekModulAplikasiDao.findAllObjekModulAplikasiDaofindMenuKelompoUser(getKdProfile(), kdKelompokUser);

		
		List<Map<String,Object>> oo = objekmodulaplikasiM
				.stream()
				.map(x -> {
							Map<String,Object> baru = x;
							
							Map<String,Object> opt = CommonUtil.createMap();
							
							Integer kdKel = kdKelompokUser;
							
							opt.put("kdKelompokUser", kdKel);
							opt.put("tampil", 0);
							opt.put("simpan", 0);
							opt.put("ubah", 0);
							opt.put("hapus", 0);
							opt.put("cetak", 0);
							opt.put("cek", false);
							opt.put("statusEnabled", false);
							
							Map<String,Object> ubah = objekmodulaplikasiK
									.stream()
									.filter(y -> y.get("kdObjekModulAplikasi").equals(baru.get("kdObjekModulAplikasi")))
									.findAny()
									.orElse(opt);
							
							
							
							baru.put("kdKelompokUser", kdKel);
							
							baru.put("tampil", ubah.get("tampil"));
							baru.put("simpan", ubah.get("simpan"));
							baru.put("ubah", ubah.get("ubah"));
							baru.put("hapus", ubah.get("hapus"));
							baru.put("cetak", ubah.get("cetak"));

							baru.put("btampil", ubah.get("tampil").equals(1));
							baru.put("bsimpan", ubah.get("simpan").equals(1));
							baru.put("bubah", ubah.get("ubah").equals(1));
							baru.put("bhapus", ubah.get("hapus").equals(1));
							baru.put("bcetak", ubah.get("cetak").equals(1));
							
							baru.put("cek", ubah.get("tampil").equals(1) &&
									ubah.get("simpan").equals(1) &&
									ubah.get("ubah").equals(1) &&
									ubah.get("ubah").equals(1) &&
									ubah.get("cetak").equals(1) );		
							
							baru.put("statusEnabled", ubah.get("statusEnabled"));
							
							return baru;
		}).collect(Collectors.toList());
		result.put("data", oo);
		resultSuccessful();			
		return result;

	}
	
	
	public Map<String, Object> findModulAndObjekModulAplikasiTree(Integer kdKelompokUser, String kdModulAplikasi, Integer kdVersion) {
		result.clear();
		List<Map<String,Object>> objekmodulaplikasiM = objekModulAplikasiDao.findAllObjekModulAplikasiDaofindMenuMapObjekHead(kdVersion,kdModulAplikasi);
		List<Map<String,Object>> objekmodulaplikasiK = objekModulAplikasiDao.findAllObjekModulAplikasiDaofindMenuKelompoUserHead(getKdProfile(), kdKelompokUser,kdModulAplikasi,kdVersion);

		
		List<Map<String,Object>> oo = objekmodulaplikasiM
				.stream()
				.map(x -> {
							Map<String,Object> baru = x;
							
							Map<String,Object> opt = CommonUtil.createMap();
							
							Integer kdKel = kdKelompokUser;
							
							opt.put("kdKelompokUser", kdKel);
							opt.put("tampil", 0);
							opt.put("simpan", 0);
							opt.put("ubah", 0);
							opt.put("hapus", 0);
							opt.put("cetak", 0);
							opt.put("cek", false);
							opt.put("statusEnabled", false);
							
							Map<String,Object> ubah = objekmodulaplikasiK
									.stream()
									.filter(y -> y.get("kdObjekModulAplikasi").equals(baru.get("kdObjekModulAplikasi")))
									.findAny()
									.orElse(opt);
							
							
							
							baru.put("kdKelompokUser", kdKel);
							
							baru.put("tampil", ubah.get("tampil"));
							baru.put("simpan", ubah.get("simpan"));
							baru.put("ubah", ubah.get("ubah"));
							baru.put("hapus", ubah.get("hapus"));
							baru.put("cetak", ubah.get("cetak"));

							baru.put("btampil", ubah.get("tampil").equals(1));
							baru.put("bsimpan", ubah.get("simpan").equals(1));
							baru.put("bubah", ubah.get("ubah").equals(1));
							baru.put("bhapus", ubah.get("hapus").equals(1));
							baru.put("bcetak", ubah.get("cetak").equals(1));
							
							baru.put("cek", ubah.get("tampil").equals(1) &&
									ubah.get("simpan").equals(1) &&
									ubah.get("ubah").equals(1) &&
									ubah.get("ubah").equals(1) &&
									ubah.get("cetak").equals(1) );		
							
							baru.put("statusEnabled", ubah.get("statusEnabled"));
							baru.put("children", findModulAndObjekModulAplikasiChildren(kdKelompokUser, (String) baru.get("kdObjekModulAplikasi"),(String) baru.get("kdModulAplikasi"),kdVersion));
							return baru;
		}).collect(Collectors.toList());
					
		result.put("data", oo);
		resultSuccessful();			
		return result;

	}
	
	public List<Map<String, Object>> findModulAndObjekModulAplikasiChildren(Integer kdKelompokUser, String kdObjekModulAplikasiHead, String kdModulAplikasi,Integer kdVersion) {
	
		
		List<Map<String,Object>> objekmodulaplikasiM = objekModulAplikasiDao.findAllObjekModulAplikasiDaofindMenuMapObjekByHead(kdObjekModulAplikasiHead,kdVersion,kdModulAplikasi);
		List<Map<String,Object>> objekmodulaplikasiK = objekModulAplikasiDao.findAllObjekModulAplikasiDaofindMenuKelompoUserByHead(getKdProfile(),kdObjekModulAplikasiHead , kdKelompokUser,kdVersion,kdModulAplikasi);
		
		
		List<Map<String,Object>> oo = objekmodulaplikasiM
				.stream()
				.map(x -> {
							Map<String,Object> baru = x;
							
							Map<String,Object> opt = CommonUtil.createMap();
							
							Integer kdKel = kdKelompokUser;
							
							opt.put("kdKelompokUser", kdKel);
							opt.put("tampil", 0);
							opt.put("simpan", 0);
							opt.put("ubah", 0);
							opt.put("hapus", 0);
							opt.put("cetak", 0);
							opt.put("cek", false);
							opt.put("statusEnabled", false);
							
							Map<String,Object> ubah = objekmodulaplikasiK
									.stream()
									.filter(y -> y.get("kdObjekModulAplikasi").equals(baru.get("kdObjekModulAplikasi")))
									.findAny()
									.orElse(opt);
							
							
							
							baru.put("kdKelompokUser", kdKel);
							
							baru.put("tampil", ubah.get("tampil"));
							baru.put("simpan", ubah.get("simpan"));
							baru.put("ubah", ubah.get("ubah"));
							baru.put("hapus", ubah.get("hapus"));
							baru.put("cetak", ubah.get("cetak"));

							baru.put("btampil", ubah.get("tampil").equals(1));
							baru.put("bsimpan", ubah.get("simpan").equals(1));
							baru.put("bubah", ubah.get("ubah").equals(1));
							baru.put("bhapus", ubah.get("hapus").equals(1));
							baru.put("bcetak", ubah.get("cetak").equals(1));
							
							baru.put("cek", ubah.get("tampil").equals(1) &&
									ubah.get("simpan").equals(1) &&
									ubah.get("ubah").equals(1) &&
									ubah.get("ubah").equals(1) &&
									ubah.get("cetak").equals(1) );		
							
							baru.put("statusEnabled", ubah.get("statusEnabled"));
							baru.put("children", findModulAndObjekModulAplikasiChildren(kdKelompokUser, (String) baru.get("kdObjekModulAplikasi"),(String) baru.get("kdModulAplikasi"),kdVersion));
							return baru;
		}).collect(Collectors.toList());
					
		if(CommonUtil.isNotNullOrEmpty(oo)) {
			System.out.println();
		}

		return oo;

	}
	

	
	public List<Map<String, Object>> findObjByHead(String kdHead, Integer kdKelompokUser) {
		List<Map<String, Object>> data = CommonUtil.createList();
		List<ObjekModulAplikasi> obj = objekModulAplikasiDao.findBykdObjekModulAplikasiHeadAndStatusEnabled(kdHead,
				true);
		for (ObjekModulAplikasi objekModulAplikasi : obj) {
			Map<String, Object> datamap = CommonUtil.createMap();
			Map<String, Object> datatmp = CommonUtil.createMap();
			datatmp.put("kdObjekModulAplikasi", objekModulAplikasi.getKdObjekModulAplikasi());
			datatmp.put("namaObjekModulAplikasi", objekModulAplikasi.getNamaObjekModulAplikasi());
			Map<String, Object> datas = mapObjekModulToKelompokUserDao.findByProfileKdObj(getKdProfile(),
					objekModulAplikasi.getKdObjekModulAplikasi(), kdKelompokUser);
			if (CommonUtil.isNotNullOrEmpty(datas)) {
				datatmp.put("simpan", datas.get("simpan"));
				datatmp.put("hapus", datas.get("hapus"));
				datatmp.put("ubah", datas.get("ubah"));
				datatmp.put("cetak", datas.get("cetak"));
				datatmp.put("tampil", datas.get("tampil"));
			} else {
				datatmp.put("simpan", 0);
				datatmp.put("hapus", 0);
				datatmp.put("ubah", 0);
				datatmp.put("cetak", 0);
				datatmp.put("tampil", 0);
			}
			datamap.put("data", datatmp); 
			datamap.put("children", findObjByHead(objekModulAplikasi.getKdObjekModulAplikasi(), kdKelompokUser));
			data.add(datamap);
		}
		return data;
	}
	
}


