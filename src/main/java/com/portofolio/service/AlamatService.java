package com.portofolio.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.portofolio.EnumAlamat;
import com.portofolio.base.BaseServiceImpl;
import com.portofolio.dao.AlamatDao;
import com.portofolio.dto.AlamatDto;
import com.portofolio.entity.Alamat;
import com.portofolio.entity.vo.AlamatId;
import com.portofolio.exception.InfoException;
import com.portofolio.util.CommonUtil;


@Lazy
@Service
public class AlamatService extends BaseServiceImpl {

	@Lazy
	@Autowired
	private AlamatDao alamatDao;

	@Lazy
	@Autowired
	private LoginUserService loginUserService;
	
	@Lazy
	@Autowired
	private LanguageService languageService;


	@Transactional
	public Map<String, Object> save(AlamatDto alamatDto) {
		result.clear();
		
		AlamatId id = new AlamatId();
		id.setKdAlamat(getIdTerahir(Alamat.class.getSimpleName()));
		id.setKdProfile(getKdProfile());
		Alamat alamat = CommonUtil.findOne(alamatDao, id);
		
		if(CommonUtil.isNullOrEmpty(alamat)) {
			alamat=new Alamat();
			BeanUtils.copyProperties(alamatDto, alamat);
			alamat.setStatusEnabled(true);
			alamat.setNoRec(generateUuid32());
			alamat.setVersion(1);
			alamat.setId(id);
			alamat.setKdNegara(getKdNegara());
			alamat.setIsBillingAddress(1);
			alamat.setIsPrimaryAddress(1);
			alamat.setIsShippingAddress(1);
			alamat = alamatDao.save(alamat);
			result.put("alamat",alamat);
			resultCreated();
		}else {
			throw new InfoException(languageService.getTranslateByKey("alamat.message.dataAlreadyExist"));
		}
		
		return result;
	}

	@Transactional
	public Map<String, Object> update(AlamatDto alamatDto, Integer kdAlamat,Integer version) {
		result.clear();
		
		AlamatId id = new AlamatId();
		id.setKdAlamat(kdAlamat);
		id.setKdProfile(getKdProfile());
		Alamat alamat = CommonUtil.findOne(alamatDao, id);
		
		if(CommonUtil.isNotNullOrEmpty(alamat)) {
			BeanUtils.copyProperties(alamatDto, alamat);
			alamat.setVersion(version);
			alamat = alamatDao.save(alamat);
			result.put("alamat",alamat);
			resultUpdated();
		} else {
			resultNotFound();
		}

		return result;
	}

	
	public Map<String, Object> grid(Integer page, Integer limit, String sort, String dir, String alamatLengkap,String enumAlamat) {
		result.clear();
		Integer kdProfile = getKdProfile();
		page--;
		PageRequest pageReq = PageRequest.of(page, limit, "desc".equals(sort) ? Direction.DESC : Direction.ASC, dir);
		Page<Map<String, Object>> pageRes;
		if (CommonUtil.isNullOrEmpty(alamatLengkap)) {
			pageRes = alamatDao.findAllList(kdProfile, CONSTANT_STATUS_ENABLED_TRUE,enumAlamat,pageReq);
		} else {
			pageRes = alamatDao.findAllList(kdProfile, alamatLengkap, CONSTANT_STATUS_ENABLED_TRUE,enumAlamat,pageReq);
		}
		result.put("alamat", pageRes.getContent());
		result.put("totalPages", pageRes.getTotalPages());
		result.put("totalRow", pageRes.getTotalElements());
		resultSuccessful();

		return result;
	}

	
	public Map<String, Object> getEnumAlamat() {
		result.clear();
		List<Map<String,Object>> dataEnumAlamat=new ArrayList<>();
		
		Map<String,Object> data1=new HashMap<String, Object>();
		data1.put("name", EnumAlamat.PEGAWAI.getName());
		
		Map<String,Object> data2=new HashMap<String, Object>();
		data2.put("name", EnumAlamat.PROFILE.getName());
		
		Map<String,Object> data3=new HashMap<String, Object>();
		data3.put("name", EnumAlamat.REKANAN.getName());
		
		dataEnumAlamat.add(data1);
		dataEnumAlamat.add(data2);
		dataEnumAlamat.add(data3);
		
		result.put("enumAlamats", dataEnumAlamat);
		resultSuccessful();

		return result;
	}

	
	public Map<String, Object> findByKdAlamat(Integer kdAlamat) {
		result.clear();
		Map<String, Object> alamat = alamatDao.findByKdAlamat(getKdProfile(), kdAlamat,CONSTANT_STATUS_ENABLED_TRUE);
		result.put("alamat", alamat);
		resultSuccessful();
		return result;
	}



	@Transactional
	public Map<String, Object> deleteByKdAlamat(Integer kdAlamat,Integer version) {
		result.clear();
		
		AlamatId id = new AlamatId();
		id.setKdAlamat(kdAlamat);
		id.setKdProfile(getKdProfile());
		Alamat alamat = CommonUtil.findOne(alamatDao, id);
		
		if(CommonUtil.isNotNullOrEmpty(alamat)) {
			alamat.setVersion(version);
			alamat.setStatusEnabled(true);
			alamat = alamatDao.save(alamat);
			result.put("alamat",alamat);
			resultDeleted();
		} else {
			resultNotFound();
		}

		return result;
	}

	public Map<String, Object> gridAlamatProfile(Integer page, Integer limit, String sort, String dir,
			String alamatLengkap, boolean statusEnabled) {
		result.clear();
		page--;
		PageRequest pageReq = PageRequest.of(page, limit, "desc".equals(sort) ? Direction.DESC : Direction.ASC, dir);
		Page<Map<String, Object>> pageRes;
		String enumAlamatProfile=EnumAlamat.PROFILE.getName();
		if (CommonUtil.isNullOrEmpty(alamatLengkap)) {
			pageRes = alamatDao.gridAlamatProfile(enumAlamatProfile,statusEnabled,pageReq);
		} else {
			pageRes = alamatDao.gridAlamatProfile(enumAlamatProfile,alamatLengkap, statusEnabled,pageReq);
		}
		result.put("alamat", pageRes.getContent());
		result.put("totalPages", pageRes.getTotalPages());
		result.put("totalRow", pageRes.getTotalElements());
		resultSuccessful();

		return result;
	}


}
