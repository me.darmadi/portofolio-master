package com.portofolio.service;

import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
/*import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;*/
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.portofolio.base.BaseServiceImpl;
import com.portofolio.dao.MapLoginUserToPegawaiDao;
import com.portofolio.dto.MapLoginUserToPegawaiDto;
import com.portofolio.entity.MapLoginUserToPegawai;
import com.portofolio.entity.vo.MapLoginUserToPegawaiId;
import com.portofolio.exception.InfoException;
import com.portofolio.util.CommonUtil;


@Lazy
@Service
public class MapLoginUserToPegawaiService extends BaseServiceImpl {

	@Lazy
	@Autowired
	private MapLoginUserToPegawaiDao mapLoginUserToPegawaiDao;

	@Lazy
	@Autowired
	private LoginUserService loginUserService;


	@Transactional
	public Map<String, Object> update(MapLoginUserToPegawaiDto mapLoginUserToPegawaiDto) {
		result.clear();
		if(CommonUtil.isNotNullOrEmpty(mapLoginUserToPegawaiDto.getKdUser())) {
			MapLoginUserToPegawai mlutp=mapLoginUserToPegawaiDao.findOneByIdKdUserAndIdKdProfile(mapLoginUserToPegawaiDto.getKdUser(), getKdProfile());
			if(CommonUtil.isNotNullOrEmpty(mlutp)) {
				//throw new InfoException("email ready use");
			}
		}
		MapLoginUserToPegawaiId idP = new MapLoginUserToPegawaiId();
		idP.setKdPegawai(mapLoginUserToPegawaiDto.getKdPegawai());
		idP.setKdProfile(getKdProfile());
		idP.setKdUser(mapLoginUserToPegawaiDto.getKdUser());
		MapLoginUserToPegawai mapLoginUserToPegawai=CommonUtil.findOne(mapLoginUserToPegawaiDao, idP);
		if (CommonUtil.isNotNullOrEmpty(mapLoginUserToPegawai)) { 
			BeanUtils.copyProperties(mapLoginUserToPegawaiDto, mapLoginUserToPegawai); 
			mapLoginUserToPegawai = mapLoginUserToPegawaiDao.save(mapLoginUserToPegawai);
			
		} else {
			mapLoginUserToPegawai = new MapLoginUserToPegawai();
			BeanUtils.copyProperties(mapLoginUserToPegawaiDto, mapLoginUserToPegawai);
			mapLoginUserToPegawai.setStatusEnabled(true);
			mapLoginUserToPegawai.setNoRec(generateUuid32());
			mapLoginUserToPegawai.setVersion(1);
			mapLoginUserToPegawai.setId(idP);
			mapLoginUserToPegawai = mapLoginUserToPegawaiDao.save(mapLoginUserToPegawai);
			
		
		}
		result.put("mapLoginUserToPegawai", mapLoginUserToPegawai);
		resultUpdated();
		return result;
	}

	
	public Map<String, Object> grid(Integer page, Integer limit, String sort, String dir, Long kdUser) {
		result.clear();
		Integer kdProfile = getKdProfile();
		page--;
		PageRequest pageReq = PageRequest.of(page, limit, "desc".equals(sort) ? Direction.DESC : Direction.ASC, dir);
		Page<Map<String, Object>> pageRes;
		if (CommonUtil.isNullOrEmpty(kdUser)) {
			pageRes = mapLoginUserToPegawaiDao.findAllList(kdProfile, pageReq);
		} else {
			pageRes = mapLoginUserToPegawaiDao.findAllList(kdProfile, kdUser, pageReq);
		}
		result.put("mapLoginUserToPegawai", pageRes.getContent());
		result.put("totalPages", pageRes.getTotalPages());
		result.put("totalRow", pageRes.getTotalElements());
		resultSuccessful();
		return result;
	}

	
	public Map<String, Object> findByKode(Long kode) {
		result.clear();
		Map<String, Object> mapLoginUserToPegawai = mapLoginUserToPegawaiDao.findByKode(getKdProfile(), kode);
		result.put("mapLoginUserToPegawai", mapLoginUserToPegawai);
		resultSuccessful();
		return result;
	}



	@Transactional
	public MapLoginUserToPegawai findByKdPegawai(String kode) {
		result.clear();
		MapLoginUserToPegawai mapLoginUserToPegawai = mapLoginUserToPegawaiDao.findOneByIdKdPegawaiAndIdKdProfile(kode,getKdProfile());
		return mapLoginUserToPegawai;
	}

	@Transactional
	public Map<String, Object> findOneByKdPegawai(String kode) {
		result.clear();
		String kdUser = mapLoginUserToPegawaiDao.findOneByKdPegawaiDao(kode,
				getKdProfile());
		result.put("kdUser", kdUser);
		resultSuccessful();
		return result;
	}


}
