/* --------------------------------------------------------
 Generated by Lukman Hakim using sql2java (http://sql2java.sourceforge.net/ )
 jdbc driver used at code generation time: net.sourceforge.jtds.jdbc.Driver
 
 Author : Lukman Hakim (lukman.uki@gmail.com) 
 Date Created : 26/10/2017
 --------------------------------------------------------
*/
package com.portofolio.service;

import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.portofolio.base.BaseServiceImpl;
import com.portofolio.dao.KotaKabupatenDao;
import com.portofolio.dto.KotaKabupatenDto;
import com.portofolio.entity.KotaKabupaten;
import com.portofolio.entity.vo.KotaKabupatenId;
import com.portofolio.util.CommonUtil;


@Lazy
@Service
public class KotaKabupatenService extends BaseServiceImpl {

	@Lazy
	@Autowired
	private KotaKabupatenDao kotakabupatenDao;

	@Lazy
	@Autowired
	private LoginUserService loginUserService;

	@Transactional
	public Map<String, Object> save(KotaKabupatenDto kotakabupatenDto) {
		result.clear();
		KotaKabupatenId id = new KotaKabupatenId();
		id.setKdKotaKabupaten(getIdTerahirByNegara(KotaKabupaten.class, "kdKotaKabupaten"));
		id.setKdNegara(getKdNegara());
		KotaKabupaten kotaKabupaten = CommonUtil.findOne(kotakabupatenDao, id);
		if (CommonUtil.isNullOrEmpty(kotaKabupaten)) {
			kotaKabupaten = new KotaKabupaten();
			BeanUtils.copyProperties(kotakabupatenDto, kotaKabupaten);
			kotaKabupaten.setNoRec(generateUuid32());
			kotaKabupaten.setVersion(1);

			kotaKabupaten.setId(id);
			kotaKabupaten = kotakabupatenDao.save(kotaKabupaten);
			result.put("kotaKabupaten", kotaKabupaten);
			resultCreated();
		}

		return result;
	}

	@Transactional
	public Map<String, Object> update(KotaKabupatenDto kotakabupatenDto, Integer kdKotaKabupaten, Integer version) {
		result.clear();
		KotaKabupatenId id = new KotaKabupatenId();
		id.setKdKotaKabupaten(kdKotaKabupaten);
		id.setKdNegara(getKdNegara());
		KotaKabupaten kotaKabupaten = CommonUtil.findOne(kotakabupatenDao, id);
		if (CommonUtil.isNotNullOrEmpty(kotaKabupaten)) {
			BeanUtils.copyProperties(kotakabupatenDto, kotaKabupaten);
			kotaKabupaten.setVersion(version);
			kotaKabupaten = kotakabupatenDao.save(kotaKabupaten);
			result.put("kotaKabupaten", kotaKabupaten);
			resultUpdated();
		} else {
			resultNotFound();
		}

		return result;
	}

	
	public Map<String, Object> grid(Integer page, Integer limit, String sort, String dir, String namakotakabupaten,boolean statusEnabled) {
		result.clear();
		page--;
		PageRequest pageReq = PageRequest.of(page, limit, "desc".equals(sort) ? Direction.DESC : Direction.ASC, dir);
		Page<Map<String, Object>> pageRes;
		if (CommonUtil.isNullOrEmpty(namakotakabupaten)) {
			pageRes = kotakabupatenDao.findAllList(getKdNegara(), statusEnabled, pageReq);
		} else {
			pageRes = kotakabupatenDao.findAllList(getKdNegara(), namakotakabupaten, statusEnabled, pageReq);
		}
		result.put("kotaKabupaten", pageRes.getContent());
		result.put("totalPages", pageRes.getTotalPages());
		result.put("totalRow", pageRes.getTotalElements());
		resultSuccessful();

		return result;
	}

	
	public Map<String, Object> findByKdKotaKabupaten(Integer kdKotaKabupaten) {
		result.clear();
		result.put("kotakabupaten",kotakabupatenDao.findByKdKotaKabupaten(getKdNegara(), kdKotaKabupaten, CONSTANT_STATUS_ENABLED_TRUE));
		resultSuccessful();
		return result;
	}

	@Transactional
	public Map<String, Object> deleteByKode(Integer kdKotaKabupaten, Integer version) {
		result.clear();
		KotaKabupatenId id = new KotaKabupatenId();
		id.setKdKotaKabupaten(kdKotaKabupaten);
		id.setKdNegara(getKdNegara());
		KotaKabupaten kotaKabupaten = CommonUtil.findOne(kotakabupatenDao, id);
		if (CommonUtil.isNotNullOrEmpty(kotaKabupaten)) {
			kotaKabupaten.setVersion(version);
			kotaKabupaten.setStatusEnabled(false);
			kotaKabupaten = kotakabupatenDao.save(kotaKabupaten);
			result.put("kotaKabupaten", kotaKabupaten);
			resultDeleted();
		} else {
			resultNotFound();
		}

		return result;
	}

	
	public Map<String, Object> findByKdPropinsi(Integer kdPropinsi) {
		result.clear();
		result.put("kotaKabupaten",kotakabupatenDao.findByKdPropinsi(kdPropinsi, getKdNegara(), CONSTANT_STATUS_ENABLED_TRUE));
		resultSuccessful();
		return result;

	}

}
