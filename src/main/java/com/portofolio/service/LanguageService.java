package com.portofolio.service;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;
import java.util.Vector;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.annotation.JsonValue;
import com.portofolio.base.BaseServiceImpl;
import com.portofolio.dao.LanguageSettingDao;
import com.portofolio.dto.LanguageDetailDto;
import com.portofolio.dto.SessionDto;
import com.portofolio.entity.LanguageSetting;
import com.portofolio.entity.vo.LanguageSettingId;
import com.portofolio.util.CommonUtil;
import static java.util.stream.Collectors.toMap;


@Lazy
@Service
public class LanguageService extends BaseServiceImpl {


	@Autowired
	private LanguageSettingDao languageSettingDao;
	
	@Autowired
	private LoginUserService loginUserService;
	
	@Value("${file.translate}")
	private String fileTranslate;
	
	@Value("${spring.application.name}")
	private String applicationName;
	
	
	

	@Transactional(readOnly=false)
	public Map<String, Object> save(LanguageDetailDto dto) throws IOException {
		SessionDto sessionDto=getSession();
		result.clear();
		LanguageSettingId languageSettingId=new LanguageSettingId();
		BeanUtils.copyProperties(dto, languageSettingId); 
		languageSettingId.setKdProfile(sessionDto.getKdProfile());
		LanguageSetting languageSetting=CommonUtil.findOne(languageSettingDao, languageSettingId);
		
		if(CommonUtil.isNullOrEmpty(languageSetting)) {
			languageSetting=new LanguageSetting();
			languageSetting.setId(languageSettingId);
			languageSetting.setStatusEnabled(true);
			languageSetting.setNoRec(generateUuid32());
			languageSettingDao.save(languageSetting);
		}	
		
		

		try {
			OutputStream output = new FileOutputStream(
					fileTranslate + sessionDto.getKdProfile() + "-" + dto.getKdModulAplikasi() + "-"
							+ dto.getKdBahasa() + "-" + dto.getKdVersion() + ".properties");

			SortedProperties prop = new SortedProperties();

			for (Map<String, String> data : dto.getData()) {
				for (Map.Entry<String, String> entry : data.entrySet()) {
					prop.put(entry.getKey(), entry.getValue());
				}
			}
			// save properties to project root folder
			prop.store(output, null);

		}catch (Exception e) {
			e.printStackTrace();
		}
		result.put("languageSettingId", languageSettingId);
		resultSuccessful();
		
		
		return result;
	}

	
	class SortedProperties extends Properties {
		  public Enumeration keys() {
		     Enumeration keysEnum = super.keys();
		     Vector<String> keyList = new Vector<String>();
		     while(keysEnum.hasMoreElements()){
		       keyList.add((String)keysEnum.nextElement());
		     }
		     Collections.sort(keyList);
		     return keyList.elements();
		  }

		}

	public Map<String, Object> findByKdModulAplikasiAndKdVersionAndKdBahasaTree(String kdModulAplikasi, Integer kdVersion,Integer kdBahasa) throws IOException {
		SessionDto sessionDto=getSession();
		result.clear();
		String filename=sessionDto.getKdProfile() + "-" + kdModulAplikasi+ "-" + kdBahasa.toString() + "-" + kdVersion.toString() + ".properties";
		String filepath = fileTranslate+filename;
		
		Properties properties = new Properties();
        InputStream in = new FileInputStream(filepath);
        properties.load(in);
        in.close();
        
        Map<String, Object> map = new TreeMap<>();

        for (Object key : properties.keySet()) {
            List<String> keyList = Arrays.asList(((String) key).split("\\."));
            Map<String, Object> valueMap = createTree(keyList, map);
            String value = properties.getProperty((String) key);
            valueMap.put(keyList.get(keyList.size() - 1), value);
           
        }
        
        result.put("translation", map);
        
		resultSuccessful();
		return result;
	}
	
	public Map<String, Object> findByKdModulAplikasiAndKdVersionAndKdBahasaFlat(String kdModulAplikasi, Integer kdVersion,Integer kdBahasa) throws IOException {
		result.clear();
		SessionDto sessionDto=getSession();
		String filename=sessionDto.getKdProfile() + "-" + kdModulAplikasi+ "-" + kdBahasa.toString() + "-" + kdVersion.toString() + ".properties";
		String filepath = fileTranslate+filename;
		
		SortedProperties properties = new SortedProperties();
        InputStream in = new FileInputStream(filepath);
        properties.load(in);
        in.close();
        HashMap<String, String> map=new HashMap<String, String>();
        for(Object k:properties.keySet()){
            String key=(String) k;
            String value=properties.getProperty(key);
            map.put(key, value);
        }
        
        Map<String, Object> mapSorted = new LinkedHashMap<>();
        map.entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .forEachOrdered(x -> mapSorted.put(x.getKey(), x.getValue()));
        result.put("translation", mapSorted);
        
		resultSuccessful();
		return result;
	}
	
	
	public Map<String, Object> getLang() throws IOException {
		result.clear();
		SessionDto sessionDto=getSession();
		String filename=sessionDto.getKdProfile() + "-" + sessionDto.getKdModulAplikasi()+ "-" + sessionDto.getKdBahasa().toString() + "-" + sessionDto.getKdVersion().toString() + ".properties";
		String filepath = fileTranslate+filename;
		
		SortedProperties properties = new SortedProperties();
        InputStream in = new FileInputStream(filepath);
        properties.load(in);
        in.close();
        HashMap<String, String> map=new HashMap<String, String>();
        for(Object k:properties.keySet()){
            String key=(String) k;
            String value=properties.getProperty(key);
            map.put(key, value);
        }
        
        Map<String, Object> mapSorted = new LinkedHashMap<>();
        map.entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .forEachOrdered(x -> mapSorted.put(x.getKey(), x.getValue()));
        result.put("translation", mapSorted);
        
		resultSuccessful();
		return result;
	}
	
	
	public String getTranslateByKey(String keyword) {
		String filepath = fileTranslate+applicationName;
		Properties properties =null;
		try {
			properties= new Properties();
	        InputStream in;
			in = new FileInputStream(filepath);
			 properties.load(in);
		        in.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}
       
        String getTransalateByKey=(String) properties.get(keyword); 
        if(CommonUtil.isNullOrEmpty(getTransalateByKey)) {
        	getTransalateByKey=keyword;
        }
        
        return getTransalateByKey;
	}
	
	@SuppressWarnings("unchecked")
	private Map<String, Object> createTree(List<String> keys, Map<String, Object> map) {
	    Map<String, Object> valueMap = (Map<String, Object>) map.get(keys.get(0));
	    if (valueMap == null) {
	        valueMap = new HashMap<String, Object>();
	    }
	    map.put(keys.get(0), valueMap);
	    Map<String, Object> out = valueMap;
	    if (keys.size() > 2) {
	        out = createTree(keys.subList(1, keys.size()), valueMap);
	    }
	    return out;
	}
	

	
}
