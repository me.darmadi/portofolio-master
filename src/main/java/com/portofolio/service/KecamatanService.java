package com.portofolio.service;

import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.portofolio.base.BaseServiceImpl;
import com.portofolio.dao.KecamatanDao;
import com.portofolio.dto.KecamatanDto;
import com.portofolio.entity.Kecamatan;
import com.portofolio.entity.vo.KecamatanId;
import com.portofolio.exception.InfoException;
import com.portofolio.util.CommonUtil;

@Lazy
@Service
public class KecamatanService extends BaseServiceImpl {

	@Lazy
	@Autowired
	private KecamatanDao kecamatanDao;

	@Lazy
	@Autowired
	private LoginUserService loginUserService;
	
	@Lazy
	@Autowired
	private LanguageService languageService;

	@Transactional
	public Map<String, Object> save(KecamatanDto kecamatanDto) {
		result.clear();
		KecamatanId id = new KecamatanId();
		id.setKdKecamatan(getIdTerahirByNegara(Kecamatan.class, "kdKecamatan"));
		id.setKdNegara(getKdNegara());
		Kecamatan kecamatan = CommonUtil.findOne(kecamatanDao, id);
		if (CommonUtil.isNullOrEmpty(kecamatan)) {
			kecamatan = new Kecamatan();
			BeanUtils.copyProperties(kecamatanDto, kecamatan);
			kecamatan.setStatusEnabled(true);
			kecamatan.setNoRec(generateUuid32());
			kecamatan.setVersion(1);
			kecamatan.setId(id);
			kecamatan = kecamatanDao.save(kecamatan);
			result.put("kecamatan", kecamatan);
			resultCreated();
		} else {
			throw new InfoException(languageService.getTranslateByKey("desaKelurahan.message.dataAlreadyExist"));
		}

		return result;

	}

	@Transactional
	public Map<String, Object> update(KecamatanDto kecamatanDto, Integer kdKecamatan, Integer version) {
		result.clear();
		KecamatanId id = new KecamatanId();
		id.setKdKecamatan(kdKecamatan);
		id.setKdNegara(getKdNegara());
		Kecamatan kecamatan = CommonUtil.findOne(kecamatanDao, id);
		if (CommonUtil.isNotNullOrEmpty(kecamatan)) {
			if (version < kecamatan.getVersion()) {
				throw new InfoException(languageService.getTranslateByKey("global.message.transactionLocking"));
			}
			BeanUtils.copyProperties(kecamatanDto, kecamatan);
			kecamatan.setVersion(version);
			kecamatan = kecamatanDao.save(kecamatan);
			result.put("kecamatan", kecamatan);
			resultUpdated();
		} else {
			resultNotFound();
		}

		return result;
	}

	
	public Map<String, Object> grid(Integer page, Integer limit, String sort, String dir, String namakecamatan,boolean statusEnabled) {
		result.clear();
		page--;
		PageRequest pageReq = PageRequest.of(page, limit, "desc".equals(sort) ? Direction.DESC : Direction.ASC, dir);
		Page<Map<String, Object>> pageRes;
		if (CommonUtil.isNullOrEmpty(namakecamatan)) {
			pageRes = kecamatanDao.findAllList(getKdNegara(), statusEnabled, pageReq);
		} else {
			pageRes = kecamatanDao.findAllList(getKdNegara(), namakecamatan, statusEnabled, pageReq);
		}
		result.put("Kecamatan", pageRes.getContent());
		result.put("totalPages", pageRes.getTotalPages());
		result.put("totalRow", pageRes.getTotalElements());
		resultSuccessful();
		return result;
	}

	
	public Map<String, Object> findByKdKecamatan(Integer kdKecamatan) {
		result.clear();
		result.put("kecamatan",kecamatanDao.findByKdKecamatan(getKdNegara(), kdKecamatan, CONSTANT_STATUS_ENABLED_TRUE));
		resultSuccessful();
		return result;
	}

	@Transactional
	public Map<String, Object> deleteByKode(Integer kdKecamatan, Integer version) {
		result.clear();
		KecamatanId id = new KecamatanId();
		id.setKdKecamatan(kdKecamatan);
		id.setKdNegara(getKdNegara());
		Kecamatan kecamatan = CommonUtil.findOne(kecamatanDao, id);
		if (CommonUtil.isNotNullOrEmpty(kecamatan)) {
			if (version < kecamatan.getVersion()) {
				throw new InfoException(languageService.getTranslateByKey("global.message.transactionLocking"));
			}
			kecamatan.setStatusEnabled(false);
			kecamatan.setVersion(version);
			kecamatan = kecamatanDao.save(kecamatan);
			result.put("kecamatan", kecamatan);
			resultDeleted();
		} else {
			resultNotFound();
		}

		return result;
	}

	
	public Map<String, Object> findByKdKotaKabupaten(Integer kdKotaKabupaten) {
		result.clear();
		result.put("kecamatan",kecamatanDao.findByKdKotaKabupaten(getKdNegara(), kdKotaKabupaten, CONSTANT_STATUS_ENABLED_TRUE));
		resultSuccessful();
		return result;
	}

}
