package com.portofolio.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.portofolio.base.BaseServiceImpl;
import com.portofolio.dao.SettingDataFixedMasterDao;
import com.portofolio.dao.custom.SettingDataFixedDaoCustom;
import com.portofolio.dto.SettingDataFixedDto;
import com.portofolio.entity.Agama;
import com.portofolio.entity.SettingDataFixed;
import com.portofolio.entity.vo.SettingDataFixedId;
import com.portofolio.exception.InfoException;
import com.portofolio.util.CommonUtil;


@Lazy
@Service
public class SettingDataFixedMasterService extends BaseServiceImpl {

	@Lazy
	@Autowired
	private SettingDataFixedMasterDao settingDataFixedMasterDao;

	@Lazy
	@Autowired
	private SettingDataFixedDaoCustom settingDataFixedDaoCustom;

	@Lazy
	@Autowired
	private LoginUserService loginUserService;
	
	@Lazy
	@Autowired
	private LanguageService languageService;

	@Transactional
	public Map<String, Object> save(SettingDataFixedDto settingdatafixedDto) {
		result.clear();
		settingdatafixedDto.setNamaField(settingdatafixedDto.getNamaField().trim());
		SettingDataFixedId settingDataFixedId=new SettingDataFixedId();
		settingDataFixedId.setKdProfile(getKdProfile());
		settingDataFixedId.setNamaField(settingdatafixedDto.getNamaField().trim());
		SettingDataFixed settingdatafixed = CommonUtil.findOne(settingDataFixedMasterDao, settingDataFixedId);
		if (CommonUtil.isNullOrEmpty(settingdatafixed)) {
			settingdatafixed = new SettingDataFixed();
			BeanUtils.copyProperties(settingdatafixedDto, settingdatafixed);
			settingdatafixed.setNoRec(generateUuid32());
			settingdatafixed.setVersion(1);
			SettingDataFixedId id = new SettingDataFixedId();
			id.setNamaField(settingdatafixedDto.getNamaField());
			id.setKdProfile(getKdProfile());
			settingdatafixed.setId(id);
			settingdatafixed = settingDataFixedMasterDao.save(settingdatafixed);
			result.put("settingdatafixed", settingdatafixed);
			resultCreated();
		} else {
			throw new InfoException(languageService.getTranslateByKey("settingdatafixed.message.dataAlreadyExist"));
		}

		return result;
	}

	@Transactional
	public Map<String, Object> update(SettingDataFixedDto settingdatafixedDto, Integer version) {
		result.clear();
		settingdatafixedDto.setNamaField(settingdatafixedDto.getNamaField().trim());
		SettingDataFixedId settingDataFixedId=new SettingDataFixedId();
		settingDataFixedId.setKdProfile(getKdProfile());
		settingDataFixedId.setNamaField(settingdatafixedDto.getNamaField().trim());
		SettingDataFixed settingdatafixed = CommonUtil.findOne(settingDataFixedMasterDao, settingDataFixedId);
		if (CommonUtil.isNotNullOrEmpty(settingdatafixed)) {
			if (version < settingdatafixed.getVersion()) {
				throw new InfoException(languageService.getTranslateByKey("global.message.transactionLocking"));
			}
			BeanUtils.copyProperties(settingdatafixedDto, settingdatafixed);
			settingdatafixed.setVersion(version);
			settingdatafixed = settingDataFixedMasterDao.save(settingdatafixed);
			result.put("namaField", settingdatafixed);
			resultUpdated();
		} else {
			resultNotFound();
		}

		return result;
	}

	@Transactional(readOnly = false)
	public Map<String, Object> deleteById(String namaField, Integer version) {
		result.clear();
		SettingDataFixedId settingDataFixedId=new SettingDataFixedId();
		settingDataFixedId.setKdProfile(getKdProfile());
		settingDataFixedId.setNamaField(namaField.trim());
		SettingDataFixed settingdatafixed = CommonUtil.findOne(settingDataFixedMasterDao, settingDataFixedId);
		if (CommonUtil.isNotNullOrEmpty(settingdatafixed)) {
			if (version < settingdatafixed.getVersion()) {
				throw new InfoException(languageService.getTranslateByKey("global.message.transactionLocking"));
			}
			settingdatafixed.setStatusEnabled(false);
			settingdatafixed.setVersion(version);
			settingDataFixedMasterDao.save(settingdatafixed);
			result.put("settingdatafixed", settingdatafixed);
			resultDeleted();
		} else {
			resultNotFound();
		}

		return result;
	}


	
	public Map<String, Object> grid(Integer page, Integer limit, String sort, String dir, String namaField, boolean statusEnabled) {
		result.clear();
		Integer kdProfile = getKdProfile();
		page--;
		PageRequest pageReq = PageRequest.of(page, limit, "desc".equals(sort) ? Direction.DESC : Direction.ASC, dir);
		Page<Map<String, Object>> pageRes;
		if (CommonUtil.isNullOrEmpty(namaField)) {
			pageRes = settingDataFixedMasterDao.findAllList(kdProfile,statusEnabled, pageReq);
		} else {
			pageRes = settingDataFixedMasterDao.findAllList(kdProfile, namaField,statusEnabled, pageReq);
		}
		result.put("settingDataFixed", pageRes.getContent());
		result.put("totalPages", pageRes.getTotalPages());
		result.put("totalRow", pageRes.getTotalElements());
		resultSuccessful();
		return result;
	}

	
	public Map<String, Object> findByNamaField(String namaField) {
		result.clear();
		Map<String, Object> settingdatafixed = settingDataFixedMasterDao.findByNamaField(getKdProfile(), namaField,CONSTANT_STATUS_ENABLED_TRUE);
		result.put("settingDataFixed", settingdatafixed);
		resultSuccessful();
		return result;
	}

	
	public Map<String, Object> getListTable() {
		result.clear();
		List<Map<String, Object>> resultList = CommonUtil.createList();
		List<String> data = settingDataFixedDaoCustom.getTabelName();
		for (String nama : data) {
			if (nama.contains("_M")) {
				Map<String, Object> tmp = CommonUtil.createMap();
				tmp.put("namatabel", nama);
				resultList.add(tmp);
			}
		}
		result.put("tabel", resultList);
		resultSuccessful();
		return result;
	}

	
	public Map<String, Object> findFieldFromSelectedTabel(String tabelName) {
		result.clear();
		List<Map<String, Object>> resultList = CommonUtil.createList();
		List<String> data = settingDataFixedDaoCustom.getFieldTabel(tabelName);
		for (String nama : data) {
			Map<String, Object> tmp = CommonUtil.createMap();
			if (!nama.equalsIgnoreCase("KdProfile")) {
				tmp.put("namaField", nama);
				resultList.add(tmp);
			}
		}
		result.put("index", "ReportDisplay");
		result.put("field", resultList);
		resultSuccessful();
		return result;
	}

	
	public Map<String, Object> findPrimaryKeyFieldFromSelectedTable(String tabelName) {
		result.clear();
		List<Map<String, Object>> resultList = CommonUtil.createList();
		List<String> data = settingDataFixedDaoCustom.getPK(tabelName);
		for (String nama : data) {
			Map<String, Object> tmp = CommonUtil.createMap();
			if (!nama.equalsIgnoreCase("KdProfile")) {
				tmp.put("namaField", nama);
				resultList.add(tmp);
			}

		}
		result.put("field", resultList);
		resultSuccessful();
		return result;
	}

	
	public Map<String, Object> findDataFromSelectedTableAndField(String tabelName, String field1, String field2) {
		result.clear();
		List<String> data = CommonUtil.createList();
		List<Map<String, Object>> resultList = CommonUtil.createList();
		if (CommonUtil.isNotNullOrEmpty(tabelName)) {
			if (CommonUtil.isNotNullOrEmpty(field1) && CommonUtil.isNotNullOrEmpty(field2)) {
				data = settingDataFixedDaoCustom.getData(tabelName, field1, field2, getKdProfile(), getKdNegara());
				for (String values : data) {
					Map<String, Object> tmp = CommonUtil.createMap();
					String[] valuesArray = values.split(",");
					tmp.put("id", valuesArray[0]);
					tmp.put("display", valuesArray[1]);
					resultList.add(tmp);
				}

			}

		}

		
		result.put("data", resultList);
		resultSuccessful();
		return result;
	}

	
	public Map<String, Object> getDataOne(String tabelName, String field1) {
		Map<String, Object> result = CommonUtil.createMap();
		List<String> data = CommonUtil.createList();
		List<Map<String, Object>> resultList = CommonUtil.createList(); 
		if (CommonUtil.isNotNullOrEmpty(tabelName)) {
			if (CommonUtil.isNotNullOrEmpty(field1)) {
				data = settingDataFixedDaoCustom.getDataOneField(tabelName, field1, getKdProfile(),
						getKdProfile());
			}
		}

		for (String nama : data) {
			Map<String, Object> tmp = CommonUtil.createMap();
			tmp.put("data", nama);
			resultList.add(tmp);
		}
		result.put("data", resultList);
		return result;
	}

}
