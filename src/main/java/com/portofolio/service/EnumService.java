package com.portofolio.service;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.portofolio.EnumAlamat;
import com.portofolio.EnumJenisMaping;
import com.portofolio.EnumKelompokTransaksi;
import com.portofolio.base.BaseServiceImpl;
import com.portofolio.dao.AgamaDao;
import com.portofolio.dto.AgamaDto;
import com.portofolio.dto.SessionDto;
import com.portofolio.entity.Agama;
import com.portofolio.entity.vo.AgamaId;
import com.portofolio.exception.InfoException;
import com.portofolio.util.CommonUtil;

@Lazy
@Service
public class EnumService extends BaseServiceImpl {

	@Lazy
	@Autowired
	private AgamaDao agamaDao;
	
	@Lazy
	@Autowired
	private LoginUserService loginUserService;
	
	@Lazy
	@Autowired
	private LanguageService languageService;




	@Transactional(readOnly = true)
	public Map<String, Object> findByKdAgama(Integer kdAgama) {
		result.clear();
		Map<String, Object> agama = agamaDao.findByKdAgama(getKdNegara(), kdAgama, CONSTANT_STATUS_ENABLED_TRUE);
		result.put("agama", agama);
		resultSuccessful();
		return result;
	}




	public Map<String, Object> findAllEnumKelompokTransaksi() {
		result.clear();
		List<Map<String,Object>> tmp=new ArrayList<Map<String,Object>>();
		
		for(EnumKelompokTransaksi a:EnumSet.allOf(EnumKelompokTransaksi.class)) {
			Map<String,Object> dataEnumAlamat=new HashMap<String, Object>();
			dataEnumAlamat.put("id", a.getId());
			dataEnumAlamat.put("name", a.getName());
			tmp.add(dataEnumAlamat);
		}
		result.put("enumKelompokTransaksi", tmp);
		resultSuccessful();
		return result;
	}




	public Map<String, Object> findAllEnumJenisMaping() {

		result.clear();
		List<Map<String,Object>> tmp=new ArrayList<Map<String,Object>>();
		
		for(EnumJenisMaping a:EnumSet.allOf(EnumJenisMaping.class)) {
			Map<String,Object> dataEnumAlamat=new HashMap<String, Object>();
			dataEnumAlamat.put("id", a.getId());
			dataEnumAlamat.put("name", a.getName());
			tmp.add(dataEnumAlamat);
		}
		result.put("enumJenisMaping", tmp);
		resultSuccessful();
		return result;
	
	}



}
