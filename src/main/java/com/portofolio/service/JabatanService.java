/* --------------------------------------------------------
 Generated by Lukman Hakim using sql2java (http://sql2java.sourceforge.net/ )
 jdbc driver used at code generation time: net.sourceforge.jtds.jdbc.Driver
 
 Author : Lukman Hakim (lukman.uki@gmail.com) 
 Date Created : 26/10/2017
 --------------------------------------------------------
*/
package com.portofolio.service;

import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.portofolio.base.BaseServiceImpl;
import com.portofolio.dao.JabatanDao;
import com.portofolio.dto.JabatanDto;
import com.portofolio.entity.Jabatan;
import com.portofolio.entity.vo.JabatanId;
import com.portofolio.exception.InfoException;
import com.portofolio.util.CommonUtil;


@Lazy
@Service
public class JabatanService extends BaseServiceImpl {

	@Lazy
	@Autowired
	private JabatanDao jabatanDao;

	@Lazy
	@Autowired
	private LoginUserService loginUserService;
	
	@Lazy
	@Autowired
	private LanguageService languageService;

	@Transactional
	public Map<String, Object> save(JabatanDto jabatanDto) {
		result.clear();
		JabatanId id = new JabatanId();
		String kdJabatan =getIdTerahir(Jabatan.class.getSimpleName(), 2);
		id.setKdJabatan(kdJabatan);
		id.setKdProfile(getKdProfile());
		Jabatan jabatan = CommonUtil.findOne(jabatanDao, id);
		if (CommonUtil.isNullOrEmpty(jabatan)) {
			jabatan = new Jabatan();
			BeanUtils.copyProperties(jabatanDto, jabatan);
			jabatan.setNoRec(generateUuid32());
			jabatan.setVersion(1);
			jabatan.setId(id);
			jabatan = jabatanDao.save(jabatan);
			result.put("jabatan", jabatan);
			resultCreated();
		} else {
			throw new InfoException(languageService.getTranslateByKey("jabatan.message.dataAlreadyExist"));
		}

		return result;
	}

	@Transactional
	public Map<String, Object> update(JabatanDto jabatanDto, String kdJabatan, Integer version) {
		result.clear();
		JabatanId id = new JabatanId();
		id.setKdJabatan(kdJabatan);
		id.setKdProfile(getKdProfile());
		Jabatan jabatan = CommonUtil.findOne(jabatanDao, id);
		if (CommonUtil.isNotNullOrEmpty(jabatan)) {
			if (version < jabatan.getVersion()) {
				throw new InfoException(languageService.getTranslateByKey("global.message.transactionLocking"));
			}
			BeanUtils.copyProperties(jabatanDto, jabatan);
			jabatan.setVersion(version);
			jabatan = jabatanDao.save(jabatan);
			result.put("jabatan", jabatan);
			resultUpdated();
		} else {
			resultNotFound();
		}

		return result;
	}

	
	public Map<String, Object> grid(Integer page, Integer limit, String sort, String dir, String namaJabatan,boolean statusEnabled) {
		result.clear();
		page--;
		PageRequest pageReq = PageRequest.of(page, limit, "desc".equals(sort) ? Direction.DESC : Direction.ASC, dir);
		Page<Map<String, Object>> pageRes;
		if (CommonUtil.isNullOrEmpty(namaJabatan)) {
			pageRes = jabatanDao.findAllList(getKdProfile(), statusEnabled, pageReq);
		} else {
			pageRes = jabatanDao.findAllList(getKdProfile(), namaJabatan, statusEnabled, pageReq);
		}
		result.put("jabatan", pageRes.getContent());
		result.put("totalPages", pageRes.getTotalPages());
		result.put("totalRow", pageRes.getTotalElements());
		resultSuccessful();

		return result;
	}

	
	public Map<String, Object> findByKdJabatan(String kdJabatan) {
		result.clear();
		result.put("jabatan", jabatanDao.findByKdJabatan(getKdProfile(), kdJabatan, CONSTANT_STATUS_ENABLED_TRUE));
		resultSuccessful();
		return result;
	}

	@Transactional
	public Map<String, Object> deleteById(String kdJabatan, Integer version) {
		result.clear();
		JabatanId id = new JabatanId();
		id.setKdJabatan(kdJabatan);
		id.setKdProfile(getKdProfile());
		Jabatan jabatan = CommonUtil.findOne(jabatanDao, id);
		if (CommonUtil.isNotNullOrEmpty(jabatan)) {
			if (version < jabatan.getVersion()) {
				throw new InfoException(languageService.getTranslateByKey("global.message.transactionLocking"));
			}
			jabatan.setStatusEnabled(false);
			jabatan.setVersion(version);
			jabatan = jabatanDao.save(jabatan);
			result.put("jabatan", jabatan);
			resultDeleted();
		} else {
			resultNotFound();
		}

		return result;
	}

	


}
