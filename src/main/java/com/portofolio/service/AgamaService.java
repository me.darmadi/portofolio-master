package com.portofolio.service;

import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.portofolio.base.BaseServiceImpl;
import com.portofolio.dao.AgamaDao;
import com.portofolio.dto.AgamaDto;
import com.portofolio.dto.SessionDto;
import com.portofolio.entity.Agama;
import com.portofolio.entity.vo.AgamaId;
import com.portofolio.exception.InfoException;
import com.portofolio.util.CommonUtil;

@Lazy
@Service
public class AgamaService extends BaseServiceImpl {

	@Lazy
	@Autowired
	private AgamaDao agamaDao;
	
	@Lazy
	@Autowired
	private LoginUserService loginUserService;
	
	@Lazy
	@Autowired
	private LanguageService languageService;

	@Transactional(readOnly = false)
	public Map<String, Object> save(AgamaDto agamaDto) {
		result.clear();
		AgamaId id = new AgamaId();
		id.setKdAgama(getIdTerahirByNegara(Agama.class, "kdAgama"));
		id.setKdNegara(getKdNegara());
		Agama agama = CommonUtil.findOne(agamaDao, id);
		if (CommonUtil.isNullOrEmpty(agama)) {
			agama = new Agama();
			BeanUtils.copyProperties(agamaDto, agama);
			agama.setNoRec(generateUuid32());
			agama.setVersion(1);
			agama.setId(id);
			agama = agamaDao.save(agama);
			result.put("agama", agama);
			resultCreated();
		} else {
			throw new InfoException(languageService.getTranslateByKey("agama.message.dataAlreadyExist"));
		}

		return result;
	}

	@Transactional(readOnly = false)
	public Map<String, Object> update(Integer kdAgama, Integer version, AgamaDto agamaDto) {
		result.clear();
		Agama agama = agamaDao.findOneByIdKdAgamaAndIdKdNegara(kdAgama, getKdNegara());
		if (CommonUtil.isNotNullOrEmpty(agama)) {
			if (version < agama.getVersion()) {
				throw new InfoException(languageService.getTranslateByKey("global.message.transactionLocking"));
			}
			String noRec = agama.getNoRec();
			BeanUtils.copyProperties(agamaDto, agama);
			agama.setNoRec(noRec);
			agama.setVersion(version);
			agama = agamaDao.save(agama);
			result.put("agama", agama);
			resultUpdated();
		} else {
			resultNotFound();
		}

		return result;
	}

	@Transactional(readOnly = false)
	public Map<String, Object> deleteById(Integer kdAgama, Integer version) {
		result.clear();
		Agama agama = agamaDao.findOneByIdKdAgamaAndIdKdNegara(kdAgama, getKdNegara());
		if (CommonUtil.isNotNullOrEmpty(agama)) {
			if (version < agama.getVersion()) {
				throw new InfoException(languageService.getTranslateByKey("global.message.transactionLocking"));
			}
			agama.setStatusEnabled(false);
			agama.setVersion(version);
			agamaDao.save(agama);
			result.put("agama", agama);
			resultDeleted();
		} else {
			resultNotFound();
		}

		return result;
	}

	@Transactional(readOnly = true)
	public Map<String, Object> grid(Integer page, Integer limit, String sort, String dir, String namaAgama, boolean statusEnabled) {
		result.clear();
		SessionDto ses = loginUserService.getSession();
		page--;
		PageRequest pageReq = new PageRequest(page, limit, "desc".equals(sort) ? Direction.DESC : Direction.ASC, dir);
		Page<Map<String, Object>> pageRes;
		if (CommonUtil.isNullOrEmpty(namaAgama)) {
			pageRes = agamaDao.findAllList(ses.getKdNegaraProfile(), statusEnabled, pageReq);
		} else {
			pageRes = agamaDao.findAllList(ses.getKdNegaraProfile(), namaAgama, statusEnabled, pageReq);
		}
		result.put("agama", pageRes.getContent());
		result.put("totalPages", pageRes.getTotalPages());
		result.put("totalRow", pageRes.getTotalElements());
		resultSuccessful();

		return result;
	}


	@Transactional(readOnly = true)
	public Map<String, Object> findByKdAgama(Integer kdAgama) {
		result.clear();
		Map<String, Object> agama = agamaDao.findByKdAgama(getKdNegara(), kdAgama, CONSTANT_STATUS_ENABLED_TRUE);
		result.put("agama", agama);
		resultSuccessful();
		return result;
	}



}
