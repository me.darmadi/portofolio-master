package com.portofolio.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.portofolio.base.BaseServiceImpl;
import com.portofolio.dao.ModulAplikasiDao;
import com.portofolio.dao.RuanganDao;
import com.portofolio.dto.ModulAplikasiDto;
import com.portofolio.entity.ModulAplikasi;
import com.portofolio.entity.ObjekModulAplikasi;
import com.portofolio.exception.InfoException;
import com.portofolio.util.CommonUtil;


@Lazy
@Service
public class ModulAplikasiService extends BaseServiceImpl {

	@Lazy
	@Autowired
	private ModulAplikasiDao modulaplikasiDao;

	@Lazy
	@Autowired
	private LoginUserService loginUserService;

	@Lazy
	@Autowired
	private RuanganDao ruanganDao;
	
	@Lazy
	@Autowired
	private LanguageService languageService;

	@Transactional
	public Map<String, Object> save(ModulAplikasiDto modulaplikasiDto) {
		result.clear();
		String kdModulAplikasi = String.format("%06d", getIdTerahirNoNegara(ModulAplikasi.class, "kdModulAplikasi"));
		ModulAplikasi modulaplikasi = CommonUtil.findOne(modulaplikasiDao, kdModulAplikasi);
		if (CommonUtil.isNullOrEmpty(modulaplikasi)) {
			modulaplikasi = new ModulAplikasi();
			BeanUtils.copyProperties(modulaplikasiDto, modulaplikasi);
			modulaplikasi.setKdModulAplikasi(kdModulAplikasi);
			modulaplikasi.setNoRec(generateUuid32());
			modulaplikasi.setVersion(1);
			modulaplikasi = modulaplikasiDao.save(modulaplikasi);
			result.put("modulAplikasi", modulaplikasi);
			resultCreated();
		} else {
			throw new InfoException(languageService.getTranslateByKey("modulAplikasi.message.dataAlreadyExist"));
		}

		return result;
	}

	@Transactional
	public Map<String, Object> update(ModulAplikasiDto modulaplikasiDto, String kdModulAplikasi, Integer version) {
		result.clear();
		ModulAplikasi modulaplikasi = CommonUtil.findOne(modulaplikasiDao, kdModulAplikasi);
		if (CommonUtil.isNotNullOrEmpty(modulaplikasi)) {
			if (version < modulaplikasi.getVersion()) {
				throw new InfoException(languageService.getTranslateByKey("global.message.transactionLocking"));
			}
			BeanUtils.copyProperties(modulaplikasiDto, modulaplikasi);
			modulaplikasi.setVersion(version);
			modulaplikasi = modulaplikasiDao.save(modulaplikasi);
			result.put("modulAplikasi", modulaplikasi);
			resultUpdated();
		} else {
			resultNotFound();
		}

		return result;
	}

	
	public Map<String, Object> grid(Integer page, Integer limit, String sort, String dir, String namamodulaplikasi,boolean statusEnabled) {
		result.clear();
		page--;
		PageRequest pageReq = PageRequest.of(page, limit, "desc".equals(sort) ? Direction.DESC : Direction.ASC, dir);
		Page<Map<String, Object>> pageRes;
		if (CommonUtil.isNullOrEmpty(namamodulaplikasi)) {
			pageRes = modulaplikasiDao.findAllList(statusEnabled, pageReq);
		} else {
			pageRes = modulaplikasiDao.findAllList(namamodulaplikasi, statusEnabled, pageReq);
		}
		result.put("modulAplikasi", pageRes.getContent());
		result.put("totalPages", pageRes.getTotalPages());
		result.put("totalRow", pageRes.getTotalElements());
		resultSuccessful();

		return result;
	}

	
	public Map<String, Object> findByKdModulAplikasi(String kdModulAplikasi) {
		result.clear();
		Map<String, Object> modulaplikasi = modulaplikasiDao.findByKdModulAplikasi(kdModulAplikasi,CONSTANT_STATUS_ENABLED_TRUE);
		result.put("modulAplikasi", modulaplikasi);
		resultSuccessful();
		return result;
	}

	@Transactional
	public Map<String, Object> deleteByKdModulAplikasi(String kdModulAplikasi, Integer version) {
		result.clear();
		ModulAplikasi modulaplikasi = CommonUtil.findOne(modulaplikasiDao, kdModulAplikasi);
		if (CommonUtil.isNotNullOrEmpty(modulaplikasi)) {
			if (version < modulaplikasi.getVersion()) {
				throw new InfoException(languageService.getTranslateByKey("global.message.transactionLocking"));
			}
			modulaplikasi.setStatusEnabled(false);
			modulaplikasi.setVersion(version);
			modulaplikasi = modulaplikasiDao.save(modulaplikasi);
			result.put("modulAplikasi", modulaplikasi);
			resultSuccessful();
		} else {
			resultNotFound();
		}

		return result;
	}

	

}
