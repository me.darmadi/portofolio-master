package com.portofolio.service;

import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.portofolio.base.BaseServiceImpl;
import com.portofolio.dao.BahasaDao;
import com.portofolio.dto.BahasaDto;
import com.portofolio.entity.Bahasa;
import com.portofolio.entity.vo.BahasaId;
import com.portofolio.exception.InfoException;
import com.portofolio.util.CommonUtil;


@Lazy
@Service
public class BahasaService extends BaseServiceImpl {

	@Lazy
	@Autowired
	private BahasaDao bahasaDao;

	@Lazy
	@Autowired
	private LoginUserService loginUserService;
	
	@Lazy
	@Autowired
	private LanguageService languageService;

	@Transactional
	public Map<String, Object> save(BahasaDto dto) {
		result.clear();
		BahasaId id = new BahasaId();
		id.setKdBahasa(getIdTerahirByNegara(Bahasa.class, "kdBahasa"));
		id.setKdNegara(getKdNegara());
		Bahasa bahasa = CommonUtil.findOne(bahasaDao, id);
		if (CommonUtil.isNullOrEmpty(bahasa)) {
			bahasa = new Bahasa();
			BeanUtils.copyProperties(dto, bahasa);
			bahasa.setId(id);
			bahasa.setNoRec(generateUuid32());
			bahasa.setVersion(1);
			bahasa = bahasaDao.save(bahasa);
			result.put("bahasa", bahasa);
			resultCreated();
		} else {
			throw new InfoException(languageService.getTranslateByKey("bahasa.message.dataAlreadyExist"));
		}

		return result;
	}

	

	@Transactional
	public Map<String, Object> update(BahasaDto dto, Integer kdBahasa, Integer version) {
		result.clear();

		BahasaId id = new BahasaId();
		id.setKdBahasa(kdBahasa);
		id.setKdNegara(getKdNegara());
		Bahasa bahasa = CommonUtil.findOne(bahasaDao, id);
		if (CommonUtil.isNotNullOrEmpty(bahasa)) {
			if (version < bahasa.getVersion()) {
				throw new InfoException(languageService.getTranslateByKey("global.message.transactionLocking"));
			}
			BeanUtils.copyProperties(dto, bahasa);
			bahasa.setVersion(version);
			bahasa = bahasaDao.save(bahasa);
			result.put("bahasa", bahasa);
			resultUpdated();
		} else {
			resultNotFound();
		}

		return result;

	}

	
	public Map<String, Object> findAll(Integer page, Integer limit, String sort, String dir, String namabahasa,boolean statusEnabled) {
		result.clear();
		page--;
		PageRequest pageReq = PageRequest.of(page, limit, "desc".equals(sort) ? Direction.DESC : Direction.ASC, dir);
		Page<Map<String, Object>> pageRes;
		if (CommonUtil.isNullOrEmpty(namabahasa)) {
			pageRes = bahasaDao.findAllList(statusEnabled, pageReq);
		} else {
			pageRes = bahasaDao.findAllList(namabahasa, statusEnabled, pageReq);
		}
		result.put("bahasa", pageRes.getContent());
		result.put("totalPages", pageRes.getTotalPages());
		result.put("totalRow", pageRes.getTotalElements());
		resultSuccessful();

		return result;
	}

	
	public Map<String, Object> findById(Integer kdBahasa) {
		result.clear();
		Map<String, Object> bahasa = bahasaDao.findById(kdBahasa, getKdNegara(), CONSTANT_STATUS_ENABLED_TRUE);
		result.put("bahasa", bahasa);
		resultSuccessful();
		return result;
	}

	@Transactional
	public Map<String, Object> deleteById(Integer kdBahasa, Integer version) {
		result.clear();
		BahasaId id = new BahasaId();
		id.setKdBahasa(kdBahasa);
		id.setKdNegara(getKdNegara());
		Bahasa bahasa = CommonUtil.findOne(bahasaDao, id);
		if (CommonUtil.isNotNullOrEmpty(bahasa)) {
			if (version < bahasa.getVersion()) {
				throw new InfoException(languageService.getTranslateByKey("global.message.transactionLocking"));
			}
			bahasa.setStatusEnabled(false);
			bahasa.setVersion(version);
			bahasa = bahasaDao.save(bahasa);
			result.put("bahasa", bahasa);
			resultDeleted();
		} else {
			resultNotFound();
		}

		return result;
	}

}
