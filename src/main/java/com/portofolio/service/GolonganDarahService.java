package com.portofolio.service;

import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.portofolio.base.BaseServiceImpl;
import com.portofolio.dao.GolonganDarahDao;
import com.portofolio.dto.GolonganDarahDto;
import com.portofolio.entity.GolonganDarah;
import com.portofolio.entity.vo.GolonganDarahId;
import com.portofolio.exception.InfoException;
import com.portofolio.util.CommonUtil;


@Lazy
@Service
public class GolonganDarahService extends BaseServiceImpl {

	@Lazy
	@Autowired
	private GolonganDarahDao golonganDarahDao;

	@Lazy
	@Autowired
	private LoginUserService loginUserService;
	
	@Lazy
	@Autowired
	private LanguageService languageService;

	@Transactional
	public Map<String, Object> save(GolonganDarahDto golonganDarahDto) {
		result.clear();
		GolonganDarahId id = new GolonganDarahId();
		id.setKdGolonganDarah(getIdTerahirByNegara(GolonganDarah.class, "kdGolonganDarah"));
		id.setKdNegara(getKdNegara());
		GolonganDarah golonganDarah = CommonUtil.findOne(golonganDarahDao, id);
		if(CommonUtil.isNullOrEmpty(golonganDarah)) {
			golonganDarah=new GolonganDarah();	
			BeanUtils.copyProperties(golonganDarahDto, golonganDarah);
			golonganDarah.setStatusEnabled(true);
			golonganDarah.setNoRec(generateUuid32());
			golonganDarah.setVersion(1);
			golonganDarah.setId(id);
			golonganDarah = golonganDarahDao.save(golonganDarah);
			result.put("golonganDarah", golonganDarah);
			resultCreated();
		}else {
			throw new InfoException(languageService.getTranslateByKey("golonganDarah.message.dataAlreadyExist"));
		}
		
		return result;
	}

	@Transactional
	public Map<String, Object> update(GolonganDarahDto golonganDarahDto,Integer kdGolonganDarah, Integer version) {
		result.clear();
		GolonganDarahId id = new GolonganDarahId();
		id.setKdGolonganDarah(kdGolonganDarah);
		id.setKdNegara(getKdNegara());
		GolonganDarah golonganDarah = CommonUtil.findOne(golonganDarahDao, id);
		if (CommonUtil.isNotNullOrEmpty(golonganDarah)) {
			if (version < golonganDarah.getVersion()) {
				throw new InfoException(languageService.getTranslateByKey("global.message.transactionLocking"));
			}
			BeanUtils.copyProperties(golonganDarahDto, golonganDarah);
			golonganDarah.setVersion(version);
			golonganDarah = golonganDarahDao.save(golonganDarah);
			result.put("golonganDarah", golonganDarah);
			resultUpdated();
		} else {
			resultNotFound();
		}

		return result;
	}

	
	public Map<String, Object> grid(Integer page, Integer limit, String sort, String dir, String namagolonganDarah,boolean statusEnabled) {
		result.clear();
		page--;
		PageRequest pageReq = PageRequest.of(page, limit, "desc".equals(sort) ? Direction.DESC : Direction.ASC, dir);
		Page<Map<String, Object>> pageRes;
		if (CommonUtil.isNullOrEmpty(namagolonganDarah)) {
			pageRes = golonganDarahDao.findAllList(getKdNegara(),statusEnabled,pageReq);
		} else {
			pageRes = golonganDarahDao.findAllList(getKdNegara(), namagolonganDarah,statusEnabled, pageReq);
		}
		result.put("golonganDarah", pageRes.getContent());
		result.put("totalPages", pageRes.getTotalPages());
		result.put("totalRow", pageRes.getTotalElements());
		resultSuccessful();
		return result;
	}

	
	public Map<String, Object> findByKdGolonganDarah(Integer kdGolonganDarah) {
		result.clear();
		Map<String, Object> agama = golonganDarahDao.findByKdGolonganDarah(getKdNegara(), kdGolonganDarah, CONSTANT_STATUS_ENABLED_TRUE);
		result.put("golonganDarah", agama);
		resultSuccessful();
		return result;
	}

	@Transactional
	public Map<String, Object> deleteById(Integer kdGolonganDarah, Integer version) {
		result.clear();
		GolonganDarahId id = new GolonganDarahId();
		id.setKdGolonganDarah(kdGolonganDarah);
		id.setKdNegara(getKdNegara());
		GolonganDarah golonganDarah = CommonUtil.findOne(golonganDarahDao, id);
		if (CommonUtil.isNotNullOrEmpty(golonganDarah)) {
			if (version < golonganDarah.getVersion()) {
				throw new InfoException(languageService.getTranslateByKey("global.message.transactionLocking"));
			}
			golonganDarah.setStatusEnabled(false);
			golonganDarah = golonganDarahDao.save(golonganDarah);
			result.put("golonganDarah", golonganDarah);
			resultDeleted();
		} else {
			resultNotFound();
		}

		return result;
	}


}
