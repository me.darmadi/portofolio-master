package com.portofolio.service;

import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.portofolio.base.BaseServiceImpl;
import com.portofolio.dao.SukuDao;
import com.portofolio.dto.SukuDto;
import com.portofolio.entity.Suku;
import com.portofolio.entity.vo.SukuId;
import com.portofolio.exception.InfoException;
import com.portofolio.util.CommonUtil;


@Lazy
@Service
public class SukuService extends BaseServiceImpl {

	@Lazy
	@Autowired
	private SukuDao sukuDao;

	@Lazy
	@Autowired
	private LoginUserService loginUserService;
	
	@Lazy
	@Autowired
	private LanguageService languageService;

	@Transactional
	public Map<String, Object> save(SukuDto sukuDto) {
		result.clear();
		SukuId id = new SukuId();
		id.setKdSuku(getIdTerahirByNegara(Suku.class, "kdSuku"));
		id.setKdNegara(getKdNegara());
		Suku suku = CommonUtil.findOne(sukuDao, id);
		if (CommonUtil.isNullOrEmpty(suku)) {
			suku = new Suku();
			BeanUtils.copyProperties(sukuDto, suku);
			suku.setNoRec(generateUuid32());
			suku.setVersion(1);
			suku.setId(id);
			suku = sukuDao.save(suku);
			result.put("suku", suku);
			resultCreated();
		} else {
			throw new InfoException(languageService.getTranslateByKey("suku.message.dataAlreadyExist"));
		}

		return result;
	}

	@Transactional
	public Map<String, Object> update(SukuDto sukuDto, Integer kdSuku, Integer version) {
		result.clear();
		SukuId id = new SukuId();
		id.setKdSuku(kdSuku);
		id.setKdNegara(getKdNegara());
		Suku suku = CommonUtil.findOne(sukuDao, id);
		if (CommonUtil.isNotNullOrEmpty(suku)) {
			if (version < suku.getVersion()) {
				throw new InfoException(languageService.getTranslateByKey("global.message.transactionLocking"));
			}
			BeanUtils.copyProperties(sukuDto, suku);
			suku.setVersion(version);
			suku = sukuDao.save(suku);
			result.put("suku", suku);
			resultUpdated();
		} else {
			resultNotFound();
		}

		return result;
	}

	
	public Map<String, Object> grid(Integer page, Integer limit, String sort, String dir, String namasuku,boolean statusEnabled) {
		result.clear();
		page--;
		PageRequest pageReq = PageRequest.of(page, limit, "desc".equals(sort) ? Direction.DESC : Direction.ASC, dir);
		Page<Map<String, Object>> pageRes;
		if (CommonUtil.isNullOrEmpty(namasuku)) {
			pageRes = sukuDao.findAllList(getKdNegara(), statusEnabled, pageReq);
		} else {
			pageRes = sukuDao.findAllList(getKdNegara(), namasuku, statusEnabled, pageReq);
		}
		result.put("suku", pageRes.getContent());
		result.put("totalPages", pageRes.getTotalPages());
		result.put("totalRow", pageRes.getTotalElements());
		resultSuccessful();

		return result;

	}

	
	public Map<String, Object> findByKdSuku(Integer kdSuku) {
		result.clear();
		Map<String, Object> suku = sukuDao.findByKdSuku(getKdNegara(), kdSuku, CONSTANT_STATUS_ENABLED_TRUE);
		result.put("suku", suku);
		resultSuccessful();

		return result;
	}

	@Transactional
	public Map<String, Object> deleteByKdSuku(Integer kdSuku, Integer version) {
		result.clear();
		SukuId id = new SukuId();
		id.setKdSuku(kdSuku);
		id.setKdNegara(getKdNegara());
		Suku suku = CommonUtil.findOne(sukuDao, id);
		if (CommonUtil.isNotNullOrEmpty(suku)) {
			if (version < suku.getVersion()) {
				throw new InfoException(languageService.getTranslateByKey("global.message.transactionLocking"));
			}
			suku.setVersion(version);
			suku.setStatusEnabled(false);
			suku = sukuDao.save(suku);
			result.put("suku", suku);
			resultDeleted();
		} else {
			resultNotFound();
		}

		return result;

	}



}
