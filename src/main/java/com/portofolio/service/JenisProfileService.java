/* --------------------------------------------------------
 Generated by Lukman Hakim using sql2java (http://sql2java.sourceforge.net/ )
 jdbc driver used at code generation time: net.sourceforge.jtds.jdbc.Driver
 
 Author : Lukman Hakim (lukman.uki@gmail.com) 
 Date Created : 26/10/2017
 --------------------------------------------------------
*/
package com.portofolio.service;

import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.portofolio.base.BaseServiceImpl;
import com.portofolio.dao.JenisProfileDao;
import com.portofolio.dto.JenisProfileDto;
import com.portofolio.entity.JenisProfile;
import com.portofolio.exception.InfoException;
import com.portofolio.util.CommonUtil;


@Lazy
@Service
public class JenisProfileService extends BaseServiceImpl {

	@Lazy
	@Autowired
	private JenisProfileDao jenisProfileDao;

	@Lazy
	@Autowired
	private LoginUserService loginUserService;
	
	@Lazy
	@Autowired
	private LanguageService languageService;

	@Transactional
	public Map<String, Object> save(JenisProfileDto jenisProfileDto) {
		result.clear();
		Integer kdJenisProfile = getIdTerahirNoNegara(JenisProfile.class, "kdJenisProfile");
		JenisProfile jenisProfile = CommonUtil.findOne(jenisProfileDao, kdJenisProfile);
		if (CommonUtil.isNullOrEmpty(jenisProfile)) {
			jenisProfile = new JenisProfile();
			BeanUtils.copyProperties(jenisProfileDto, jenisProfile);
			jenisProfile.setStatusEnabled(true);
			jenisProfile.setNoRec(generateUuid32());
			jenisProfile.setVersion(1);
			jenisProfile = jenisProfileDao.save(jenisProfile);
			result.put("jenisProfile", jenisProfile);
			resultCreated();
		} else {
			throw new InfoException(languageService.getTranslateByKey("jenisProfile.message.dataAlreadyExist"));
		}

		return result;
	}

	@Transactional
	public Map<String, Object> update(JenisProfileDto jenisProfileDto, Integer kdJenisProfile, Integer version) {
		result.clear();
		JenisProfile jenisProfile = CommonUtil.findOne(jenisProfileDao, kdJenisProfile);
		if (CommonUtil.isNotNullOrEmpty(jenisProfile)) {
			if (version < jenisProfile.getVersion()) {
				throw new InfoException(languageService.getTranslateByKey("global.message.transactionLocking"));
			}
			BeanUtils.copyProperties(jenisProfileDto, jenisProfile);
			jenisProfile.setVersion(version);
			jenisProfile = jenisProfileDao.save(jenisProfile);
			result.put("jenisProfile", jenisProfile);
			resultUpdated();
		} else {
			resultNotFound();
		}

		return result;
	}

	
	public Map<String, Object> grid(Integer page, Integer limit, String sort, String dir, String namajenisProfile,boolean statusEnabled) {
		result.clear();
		page--;
		PageRequest pageReq = PageRequest.of(page, limit, "desc".equals(sort) ? Direction.DESC : Direction.ASC, dir);
		Page<Map<String, Object>> pageRes;
		if (CommonUtil.isNullOrEmpty(namajenisProfile)) {
			pageRes = jenisProfileDao.findAllList(statusEnabled, pageReq);
		} else {
			pageRes = jenisProfileDao.findAllList(namajenisProfile, statusEnabled, pageReq);
		}
		result.put("jenisProfile", pageRes.getContent());
		result.put("totalPages", pageRes.getTotalPages());
		result.put("totalRow", pageRes.getTotalElements());
		resultSuccessful();

		return result;
	}

	
	public Map<String, Object> findByKdJenisProfile(Integer kdJenisProfile) {
		result.clear();
		Map<String, Object> jenisProfile = jenisProfileDao.findByKdJenisProfile(kdJenisProfile,CONSTANT_STATUS_ENABLED_TRUE);
		result.put("jenisProfile", jenisProfile);
		resultSuccessful();
		return result;
	}

	@Transactional
	public Map<String, Object> deleteByKdJenisProfile(Integer kdJenisProfile, Integer version) {
		result.clear();
		JenisProfile jenisProfile = CommonUtil.findOne(jenisProfileDao, kdJenisProfile);
		if (CommonUtil.isNotNullOrEmpty(jenisProfile)) {
			if (version < jenisProfile.getVersion()) {
				throw new InfoException(languageService.getTranslateByKey("global.message.transactionLocking"));
			}
			jenisProfile.setVersion(version);
			jenisProfile.setStatusEnabled(false);
			jenisProfile = jenisProfileDao.save(jenisProfile);
			result.put("jenisProfile", jenisProfile);
			resultDeleted();
		} else {
			resultNotFound();
		}

		return result;

	}

}
