package com.portofolio.service;

import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.portofolio.base.BaseServiceImpl;
import com.portofolio.dao.JenisKelaminDao;
import com.portofolio.dto.JenisKelaminDto;
import com.portofolio.entity.JenisKelamin;
import com.portofolio.entity.vo.JenisKelaminId;
import com.portofolio.exception.InfoException;
import com.portofolio.util.CommonUtil;


@Lazy
@Service
public class JenisKelaminService extends BaseServiceImpl {

	@Lazy
	@Autowired
	private JenisKelaminDao jeniskelaminDao;

	@Lazy
	@Autowired
	private LoginUserService loginUserService;
	
	@Lazy
	@Autowired
	private LanguageService languageService;

	@Transactional
	public Map<String, Object> save(JenisKelaminDto jeniskelaminDto) {
		result.clear();
		JenisKelaminId id = new JenisKelaminId();
		id.setKdJenisKelamin(getIdTerahirByNegara(JenisKelamin.class, "kdJenisKelamin"));
		id.setKdNegara(getKdNegara());
		JenisKelamin jenisKelamin = CommonUtil.findOne(jeniskelaminDao, id);
		if (CommonUtil.isNullOrEmpty(jenisKelamin)) {
			jenisKelamin = new JenisKelamin();
			BeanUtils.copyProperties(jeniskelaminDto, jenisKelamin);
			jenisKelamin.setNoRec(generateUuid32());
			jenisKelamin.setVersion(1);

			jenisKelamin.setId(id);
			jenisKelamin = jeniskelaminDao.save(jenisKelamin);
			result.put("jenisKelamin", jenisKelamin);
			resultCreated();
		} else {
			throw new InfoException(languageService.getTranslateByKey("jenisKelamin.message.dataAlreadyExist"));
		}

		return result;
	}

	@Transactional
	public Map<String, Object> update(JenisKelaminDto jeniskelaminDto, Integer kdJenisKelamin, Integer version) {
		result.clear();
		JenisKelaminId id = new JenisKelaminId();
		id.setKdJenisKelamin(kdJenisKelamin);
		id.setKdNegara(getKdNegara());
		JenisKelamin jenisKelamin = CommonUtil.findOne(jeniskelaminDao, id);
		if (CommonUtil.isNotNullOrEmpty(jenisKelamin)) {
			if (version < jenisKelamin.getVersion()) {
				throw new InfoException(languageService.getTranslateByKey("global.message.transactionLocking"));
			}
			BeanUtils.copyProperties(jeniskelaminDto, jenisKelamin);
			jenisKelamin.setVersion(version);
			jenisKelamin = jeniskelaminDao.save(jenisKelamin);
			result.put("jenisKelamin", jenisKelamin);
			resultUpdated();
		} else {
			resultNotFound();
		}

		return result;
	}

	
	public Map<String, Object> grid(Integer page, Integer limit, String sort, String dir, String namajeniskelamin,boolean statusEnabled) {
		result.clear();
		page--;
		PageRequest pageReq = PageRequest.of(page, limit, "desc".equals(sort) ? Direction.DESC : Direction.ASC, dir);
		Page<Map<String, Object>> pageRes;
		if (CommonUtil.isNullOrEmpty(namajeniskelamin)) {
			pageRes = jeniskelaminDao.findAllList(getKdNegara(), statusEnabled, pageReq);
		} else {
			pageRes = jeniskelaminDao.findAllList(getKdNegara(), namajeniskelamin, statusEnabled, pageReq);
		}
		result.put("jenisKelamin", pageRes.getContent());
		result.put("totalPages", pageRes.getTotalPages());
		result.put("totalRow", pageRes.getTotalElements());
		resultSuccessful();

		return result;
	}

	
	public Map<String, Object> findByKdJenisKelamin(Integer kdJenisKelamin) {
		result.clear();
		Map<String, Object> jeniskelamin = jeniskelaminDao.findByKdJenisKelamin(getKdNegara(), kdJenisKelamin,CONSTANT_STATUS_ENABLED_TRUE);
		result.put("jenisKelamin", jeniskelamin);
		resultSuccessful();

		return result;
	}

	@Transactional
	public Map<String, Object> deleteByKode(Integer kdJenisKelamin, Integer version) {
		result.clear();
		JenisKelaminId id = new JenisKelaminId();
		id.setKdJenisKelamin(kdJenisKelamin);
		id.setKdNegara(getKdNegara());
		JenisKelamin jenisKelamin = CommonUtil.findOne(jeniskelaminDao, id);
		if (CommonUtil.isNotNullOrEmpty(jenisKelamin)) {
			if (version < jenisKelamin.getVersion()) {
				throw new InfoException(languageService.getTranslateByKey("global.message.transactionLocking"));
			}
			jenisKelamin.setVersion(version);
			jenisKelamin.setStatusEnabled(false);
			jenisKelamin = jeniskelaminDao.save(jenisKelamin);
			result.put("jenisKelamin", jenisKelamin);
			resultDeleted();
		} else {
			resultNotFound();
		}
		return result;
	}


}
