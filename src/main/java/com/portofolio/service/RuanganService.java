package com.portofolio.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.portofolio.base.BaseServiceImpl;
import com.portofolio.dao.RuanganDao;
import com.portofolio.dto.RuanganDto;
import com.portofolio.dtosql.RuanganDtoSql;
import com.portofolio.entity.Ruangan;
import com.portofolio.entity.vo.RuanganId;
import com.portofolio.exception.InfoException;
import com.portofolio.util.CommonUtil;



@Lazy
@Service
public class RuanganService extends BaseServiceImpl {
	
	@Lazy
	@Autowired
	private RuanganDao ruanganDao;

	@Lazy
	@Autowired
	private LoginUserService loginUserService;
	
	@Lazy
	@Autowired
	private LanguageService languageService;
	
	
	@Transactional
	public Map<String, Object> save(RuanganDto ruanganDto) {
		result.clear();
		RuanganId id = new RuanganId();
		id.setKdRuangan(getIdTerahir(Ruangan.class.getSimpleName(), 3));
		id.setKdProfile(getKdProfile());

		Ruangan ruangan = CommonUtil.findOne(ruanganDao, id);
		if (CommonUtil.isNullOrEmpty(ruangan)) {
			ruangan = new Ruangan();
			ruangan.setId(id);
			BeanUtils.copyProperties(ruanganDto, ruangan);
			ruangan.setNoRec(generateUuid32());
			ruangan.setVersion(1);
			ruangan = ruanganDao.save(ruangan);
		} else {
			throw new InfoException(languageService.getTranslateByKey("ruangan.message.dataAlreadyExist"));
		}

		result.put("ruangan", ruangan);
		resultSuccessful();

		return result;
	}
	
	@Transactional
	public Map<String, Object> update(RuanganDto ruanganDto, String kdRuangan, Integer version) {
		result.clear();
		RuanganId id = new RuanganId();
		id.setKdRuangan(kdRuangan);
		id.setKdProfile(getKdProfile());
		Ruangan ruangan = CommonUtil.findOne(ruanganDao, id);
		if (CommonUtil.isNotNullOrEmpty(ruangan)) {
			if (version < ruangan.getVersion()) {
				throw new InfoException(languageService.getTranslateByKey("global.message.transactionLocking"));
			}
			ruangan.setId(id);
			BeanUtils.copyProperties(ruanganDto, ruangan);
			ruangan.setStatusEnabled(true);
			ruangan.setNoRec(generateUuid32());
			ruangan.setVersion(version);
			ruangan = ruanganDao.save(ruangan);
			result.put("ruangan", ruangan);
			resultUpdated();
		} else {
			resultNotFound();
		}

		return result;
	}
	
	@Transactional
	public Map<String, Object> deleteByKdRuangan(String kdRuangan, Integer version) {
		result.clear();
		RuanganId id = new RuanganId();
		id.setKdRuangan(kdRuangan);
		id.setKdProfile(getKdProfile());
		Ruangan ruangan = CommonUtil.findOne(ruanganDao, id);
		if (CommonUtil.isNotNullOrEmpty(ruangan)) {
			if (version < ruangan.getVersion()) {
				throw new InfoException(languageService.getTranslateByKey("global.message.transactionLocking"));
			}
			ruangan.setStatusEnabled(false);
			ruangan.setVersion(version);
			ruangan = ruanganDao.save(ruangan);
			result.put("ruangan", ruangan);
			resultDeleted();
		} else {
			resultNotFound();
		}

		return result;
	}
	
	public Map<String, Object> grid(String sort, String dir, String namaRuangan, boolean statusEnabed) {
		result.clear();
		PageRequest pageReq = new PageRequest(0, Integer.MAX_VALUE,"desc".equals(sort) ? Direction.DESC : Direction.ASC, dir);
		List<RuanganDtoSql> list = new ArrayList<>();
		if (CommonUtil.isNullOrEmpty(namaRuangan)) {
			list = ruanganDao.findAllList(getKdProfile(), statusEnabed, pageReq);
			result = createTree(list);
		} else {
			list = ruanganDao.findAllList(getKdProfile(), namaRuangan, statusEnabed, pageReq);
			result = createTree(list);
		}
		resultSuccessful();

		return result;
	}
	
	
	private Map<String, Object> createTree(List<RuanganDtoSql> nodes) {

		Map<String, RuanganDtoSql> mapTmp = new HashMap<>();

		// Save all nodes to a map
		for (RuanganDtoSql current : nodes) {
			mapTmp.put(current.getKdRuangan(), current);
		}

		// loop and assign parent/child relationships
		for (RuanganDtoSql current : nodes) {
			String parentId = current.getKdRuanganHead();

			if (parentId != null) {
				RuanganDtoSql parent = mapTmp.get(parentId);
				if (parent != null) {
					// current.setRuanganHead(parent);
					parent.addChild(current);
					mapTmp.put(parentId, parent);
					mapTmp.put(current.getKdRuangan(), current);
				}
			}
		}

		List<RuanganDtoSql> ruanganDtoSqls = new ArrayList<>();
		for (RuanganDtoSql d : mapTmp.values()) {
			if (CommonUtil.isNullOrEmpty(d.getKdRuanganHead()) || CommonUtil.isNotNullOrEmpty(d.getChildren())) {
				ruanganDtoSqls.add(d);
			}

		}
		Map<String, Object> da = new HashMap<>();
		da.put("ruangans", ruanganDtoSqls);
		return da;
	}
	
	public Map<String, Object> findByKdRuangan(String kdRuangan, boolean isHierarki) {
		result.clear();
		List<RuanganDtoSql> list = new ArrayList<>();
		if (isHierarki) {
			list = ruanganDao.findByKdRuanganWithIsHierarkiTrue(getKdProfile(), kdRuangan,CONSTANT_STATUS_ENABLED_TRUE);
			result = createTree(list);
		} else {
			list = ruanganDao.findByKdRuanganWithIsHierarkiFalse(getKdProfile(), kdRuangan,CONSTANT_STATUS_ENABLED_TRUE);
			result.put("ruangans", list);
		}
		resultSuccessful();
		return result;

	}
	
	
}

