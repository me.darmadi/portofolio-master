package com.portofolio;
public enum EnumAlamat {

    PROFILE(1, "Profile"),
    REKANAN(2, "Rekanan"),
    PEGAWAI(3, "Pegawai"),
	PERSON(4, "Person");

    private java.lang.String name;

    private java.lang.Integer id;

    EnumAlamat(Integer id, java.lang.String name) {
        this.name = name;
        this.id = id;
    }

    public java.lang.String getName() {
        return name;
    }

    public java.lang.Integer getId() {
        return id;
    }
}