package com.portofolio;
public enum EnumJenisMaping {

    READ(1, "Read"),
    WRITE(2, "Write");

    private java.lang.String name;

    private java.lang.Integer id;

    EnumJenisMaping(Integer id, java.lang.String name) {
        this.name = name;
        this.id = id;
    }

    public java.lang.String getName() {
        return name;
    }

    public java.lang.Integer getId() {
        return id;
    }
}