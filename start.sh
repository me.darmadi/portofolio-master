#/bin/sh

FILE=main.properties
if [ ! -f $FILE ]
then

cat > $FILE <<EOF
spring.cloud.config.name=klinik1-java
spring.cloud.config.uri=http://127.0.0.1:8888
EOF

fi

kill $(lsof -t -i :9999)
sleep 2

java -XX:+UseG1GC -Djava.awt.headless=true \
-Dspring.config.location=main.properties \
-jar target/klinik1-java-0.0.1.jar >./output.log 2>&1 &

#java -Dcom.sun.management.jmxremote \
#-Dcom.sun.management.jmxremote.local.only=false \
#-Dcom.sun.management.jmxremote.ssl=false \
#-Dcom.sun.management.jmxremote.authenticate=false \
#-Dcom.sun.management.jmxremote.port=1102 \
#-Dcom.sun.management.jmxremote.rmi.port=1102 \
#-Djava.rmi.server.hostname=0.0.0.0 \
#-XX:+UseG1GC -Djava.awt.headless=true \
#-jar target/klinik1-java-0.0.1.jar >./output.log 2>&1 &